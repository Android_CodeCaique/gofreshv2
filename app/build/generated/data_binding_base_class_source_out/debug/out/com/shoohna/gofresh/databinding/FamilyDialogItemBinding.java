// Generated by data binding compiler. Do not edit!
package com.shoohna.gofresh.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.shoohna.gofresh.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FamilyDialogItemBinding extends ViewDataBinding {
  @NonNull
  public final TextView addFamilyTxtViewId;

  @NonNull
  public final ImageView imageView33;

  @NonNull
  public final MaterialSpinner spinner;

  @NonNull
  public final MaterialSpinner spinner2;

  protected FamilyDialogItemBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView addFamilyTxtViewId, ImageView imageView33, MaterialSpinner spinner,
      MaterialSpinner spinner2) {
    super(_bindingComponent, _root, _localFieldCount);
    this.addFamilyTxtViewId = addFamilyTxtViewId;
    this.imageView33 = imageView33;
    this.spinner = spinner;
    this.spinner2 = spinner2;
  }

  @NonNull
  public static FamilyDialogItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.family_dialog_item, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FamilyDialogItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FamilyDialogItemBinding>inflateInternal(inflater, R.layout.family_dialog_item, root, attachToRoot, component);
  }

  @NonNull
  public static FamilyDialogItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.family_dialog_item, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FamilyDialogItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FamilyDialogItemBinding>inflateInternal(inflater, R.layout.family_dialog_item, null, false, component);
  }

  public static FamilyDialogItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FamilyDialogItemBinding bind(@NonNull View view, @Nullable Object component) {
    return (FamilyDialogItemBinding)bind(component, view, R.layout.family_dialog_item);
  }
}
