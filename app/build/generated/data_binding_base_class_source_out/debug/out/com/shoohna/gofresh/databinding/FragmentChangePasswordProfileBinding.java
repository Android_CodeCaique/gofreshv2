// Generated by data binding compiler. Do not edit!
package com.shoohna.gofresh.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.textfield.TextInputLayout;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.ui.home.ui.changePasswordProfile.ChangePasswordViewModel;
import com.tuyenmonkey.mkloader.MKLoader;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentChangePasswordProfileBinding extends ViewDataBinding {
  @NonNull
  public final Button button2;

  @NonNull
  public final CardView cardView6;

  @NonNull
  public final TextInputLayout confirmNewPasswordLayout;

  @NonNull
  public final ImageView imageView32;

  @NonNull
  public final MKLoader mkLoaderId;

  @NonNull
  public final TextInputLayout newPasswordLayout;

  @NonNull
  public final TextInputLayout oldPasswordLayout;

  @NonNull
  public final TextView textView4;

  @Bindable
  protected ChangePasswordViewModel mVM;

  protected FragmentChangePasswordProfileBinding(Object _bindingComponent, View _root,
      int _localFieldCount, Button button2, CardView cardView6,
      TextInputLayout confirmNewPasswordLayout, ImageView imageView32, MKLoader mkLoaderId,
      TextInputLayout newPasswordLayout, TextInputLayout oldPasswordLayout, TextView textView4) {
    super(_bindingComponent, _root, _localFieldCount);
    this.button2 = button2;
    this.cardView6 = cardView6;
    this.confirmNewPasswordLayout = confirmNewPasswordLayout;
    this.imageView32 = imageView32;
    this.mkLoaderId = mkLoaderId;
    this.newPasswordLayout = newPasswordLayout;
    this.oldPasswordLayout = oldPasswordLayout;
    this.textView4 = textView4;
  }

  public abstract void setVM(@Nullable ChangePasswordViewModel VM);

  @Nullable
  public ChangePasswordViewModel getVM() {
    return mVM;
  }

  @NonNull
  public static FragmentChangePasswordProfileBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_change_password_profile, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentChangePasswordProfileBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentChangePasswordProfileBinding>inflateInternal(inflater, R.layout.fragment_change_password_profile, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentChangePasswordProfileBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_change_password_profile, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentChangePasswordProfileBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentChangePasswordProfileBinding>inflateInternal(inflater, R.layout.fragment_change_password_profile, null, false, component);
  }

  public static FragmentChangePasswordProfileBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentChangePasswordProfileBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (FragmentChangePasswordProfileBinding)bind(component, view, R.layout.fragment_change_password_profile);
  }
}
