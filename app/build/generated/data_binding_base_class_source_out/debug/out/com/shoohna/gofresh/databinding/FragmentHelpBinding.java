// Generated by data binding compiler. Do not edit!
package com.shoohna.gofresh.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.shoohna.gofresh.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentHelpBinding extends ViewDataBinding {
  @NonNull
  public final ImageView back;

  @NonNull
  public final ImageView imageView12;

  @NonNull
  public final TextView textView35;

  protected FragmentHelpBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView back, ImageView imageView12, TextView textView35) {
    super(_bindingComponent, _root, _localFieldCount);
    this.back = back;
    this.imageView12 = imageView12;
    this.textView35 = textView35;
  }

  @NonNull
  public static FragmentHelpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_help, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentHelpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentHelpBinding>inflateInternal(inflater, R.layout.fragment_help, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentHelpBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_help, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentHelpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentHelpBinding>inflateInternal(inflater, R.layout.fragment_help, null, false, component);
  }

  public static FragmentHelpBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentHelpBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentHelpBinding)bind(component, view, R.layout.fragment_help);
  }
}
