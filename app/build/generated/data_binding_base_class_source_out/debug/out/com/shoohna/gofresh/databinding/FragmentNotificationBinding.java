// Generated by data binding compiler. Do not edit!
package com.shoohna.gofresh.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.ui.home.ui.notification.NotificationViewModel;
import com.tuyenmonkey.mkloader.MKLoader;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentNotificationBinding extends ViewDataBinding {
  @NonNull
  public final ImageView back;

  @NonNull
  public final MKLoader mkLoaderId;

  @NonNull
  public final RecyclerView notificationRecyclerViewId;

  @NonNull
  public final TextView textView23;

  @Bindable
  protected NotificationViewModel mVM;

  protected FragmentNotificationBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView back, MKLoader mkLoaderId, RecyclerView notificationRecyclerViewId,
      TextView textView23) {
    super(_bindingComponent, _root, _localFieldCount);
    this.back = back;
    this.mkLoaderId = mkLoaderId;
    this.notificationRecyclerViewId = notificationRecyclerViewId;
    this.textView23 = textView23;
  }

  public abstract void setVM(@Nullable NotificationViewModel VM);

  @Nullable
  public NotificationViewModel getVM() {
    return mVM;
  }

  @NonNull
  public static FragmentNotificationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_notification, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentNotificationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentNotificationBinding>inflateInternal(inflater, R.layout.fragment_notification, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentNotificationBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_notification, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentNotificationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentNotificationBinding>inflateInternal(inflater, R.layout.fragment_notification, null, false, component);
  }

  public static FragmentNotificationBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentNotificationBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentNotificationBinding)bind(component, view, R.layout.fragment_notification);
  }
}
