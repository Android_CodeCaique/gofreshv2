// Generated by data binding compiler. Do not edit!
package com.shoohna.gofresh.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.shoohna.gofresh.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentRegisterIntroBinding extends ViewDataBinding {
  @NonNull
  public final Button btnContiune;

  @NonNull
  public final ConstraintLayout constraintLayout10;

  @NonNull
  public final TextView textView39;

  @NonNull
  public final TextView textView41;

  protected FragmentRegisterIntroBinding(Object _bindingComponent, View _root, int _localFieldCount,
      Button btnContiune, ConstraintLayout constraintLayout10, TextView textView39,
      TextView textView41) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnContiune = btnContiune;
    this.constraintLayout10 = constraintLayout10;
    this.textView39 = textView39;
    this.textView41 = textView41;
  }

  @NonNull
  public static FragmentRegisterIntroBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_register_intro, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentRegisterIntroBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentRegisterIntroBinding>inflateInternal(inflater, R.layout.fragment_register_intro, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentRegisterIntroBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_register_intro, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentRegisterIntroBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentRegisterIntroBinding>inflateInternal(inflater, R.layout.fragment_register_intro, null, false, component);
  }

  public static FragmentRegisterIntroBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentRegisterIntroBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentRegisterIntroBinding)bind(component, view, R.layout.fragment_register_intro);
  }
}
