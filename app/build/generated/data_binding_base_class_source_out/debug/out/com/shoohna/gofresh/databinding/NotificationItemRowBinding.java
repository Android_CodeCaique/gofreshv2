// Generated by data binding compiler. Do not edit!
package com.shoohna.gofresh.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.pojo.responses.NotificationData;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class NotificationItemRowBinding extends ViewDataBinding {
  @NonNull
  public final CardView imageView8;

  @NonNull
  public final TextView textView21;

  @NonNull
  public final TextView textView22;

  @Bindable
  protected NotificationData mModel;

  protected NotificationItemRowBinding(Object _bindingComponent, View _root, int _localFieldCount,
      CardView imageView8, TextView textView21, TextView textView22) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imageView8 = imageView8;
    this.textView21 = textView21;
    this.textView22 = textView22;
  }

  public abstract void setModel(@Nullable NotificationData Model);

  @Nullable
  public NotificationData getModel() {
    return mModel;
  }

  @NonNull
  public static NotificationItemRowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.notification_item_row, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static NotificationItemRowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<NotificationItemRowBinding>inflateInternal(inflater, R.layout.notification_item_row, root, attachToRoot, component);
  }

  @NonNull
  public static NotificationItemRowBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.notification_item_row, null, false, component)
   */
  @NonNull
  @Deprecated
  public static NotificationItemRowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<NotificationItemRowBinding>inflateInternal(inflater, R.layout.notification_item_row, null, false, component);
  }

  public static NotificationItemRowBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static NotificationItemRowBinding bind(@NonNull View view, @Nullable Object component) {
    return (NotificationItemRowBinding)bind(component, view, R.layout.notification_item_row);
  }
}
