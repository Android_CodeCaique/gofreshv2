// Generated by data binding compiler. Do not edit!
package com.shoohna.gofresh.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.makeramen.roundedimageview.RoundedImageView;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.pojo.model.ProductListModel;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ProductListRowBinding extends ViewDataBinding {
  @NonNull
  public final CardView cardView2;

  @NonNull
  public final TextView categorieNameId;

  @NonNull
  public final TextView discountId;

  @NonNull
  public final ConstraintLayout discountLayoutShowId;

  @NonNull
  public final TextView distanceId;

  @NonNull
  public final ConstraintLayout mainConstraintLayoutId;

  @NonNull
  public final TextView priceId;

  @NonNull
  public final RoundedImageView productImgId;

  @Bindable
  protected ProductListModel mModel;

  protected ProductListRowBinding(Object _bindingComponent, View _root, int _localFieldCount,
      CardView cardView2, TextView categorieNameId, TextView discountId,
      ConstraintLayout discountLayoutShowId, TextView distanceId,
      ConstraintLayout mainConstraintLayoutId, TextView priceId, RoundedImageView productImgId) {
    super(_bindingComponent, _root, _localFieldCount);
    this.cardView2 = cardView2;
    this.categorieNameId = categorieNameId;
    this.discountId = discountId;
    this.discountLayoutShowId = discountLayoutShowId;
    this.distanceId = distanceId;
    this.mainConstraintLayoutId = mainConstraintLayoutId;
    this.priceId = priceId;
    this.productImgId = productImgId;
  }

  public abstract void setModel(@Nullable ProductListModel Model);

  @Nullable
  public ProductListModel getModel() {
    return mModel;
  }

  @NonNull
  public static ProductListRowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.product_list_row, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ProductListRowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ProductListRowBinding>inflateInternal(inflater, R.layout.product_list_row, root, attachToRoot, component);
  }

  @NonNull
  public static ProductListRowBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.product_list_row, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ProductListRowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ProductListRowBinding>inflateInternal(inflater, R.layout.product_list_row, null, false, component);
  }

  public static ProductListRowBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ProductListRowBinding bind(@NonNull View view, @Nullable Object component) {
    return (ProductListRowBinding)bind(component, view, R.layout.product_list_row);
  }
}
