package com.shoohna.gofresh;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.shoohna.gofresh.databinding.AboutUsItemRowBindingImpl;
import com.shoohna.gofresh.databinding.ActivityMainBindingImpl;
import com.shoohna.gofresh.databinding.ActivityWelcomeBindingImpl;
import com.shoohna.gofresh.databinding.ApplicationChatItemBindingImpl;
import com.shoohna.gofresh.databinding.BestSalesListRowBindingImpl;
import com.shoohna.gofresh.databinding.BottomSheetLayoutBindingImpl;
import com.shoohna.gofresh.databinding.CartItemRowBindingImpl;
import com.shoohna.gofresh.databinding.CheckoutMycartItemRowBindingImpl;
import com.shoohna.gofresh.databinding.CreditCardViewBindingImpl;
import com.shoohna.gofresh.databinding.FamilyDialogItemBindingImpl;
import com.shoohna.gofresh.databinding.FavoriteViewRowBindingImpl;
import com.shoohna.gofresh.databinding.FilterItemRowBindingImpl;
import com.shoohna.gofresh.databinding.FragmentAboutUsBindingImpl;
import com.shoohna.gofresh.databinding.FragmentAddCreditCardBindingImpl;
import com.shoohna.gofresh.databinding.FragmentAddFamilyRegisterBindingImpl;
import com.shoohna.gofresh.databinding.FragmentAddNewAddressBindingImpl;
import com.shoohna.gofresh.databinding.FragmentCartBindingImpl;
import com.shoohna.gofresh.databinding.FragmentChangePasswordProfileBindingImpl;
import com.shoohna.gofresh.databinding.FragmentChatBindingImpl;
import com.shoohna.gofresh.databinding.FragmentCheckoutProcessBindingImpl;
import com.shoohna.gofresh.databinding.FragmentContactUsBindingImpl;
import com.shoohna.gofresh.databinding.FragmentFamilyDialogBindingImpl;
import com.shoohna.gofresh.databinding.FragmentFavoriteBindingImpl;
import com.shoohna.gofresh.databinding.FragmentForgetPasswordBindingImpl;
import com.shoohna.gofresh.databinding.FragmentHelpBindingImpl;
import com.shoohna.gofresh.databinding.FragmentHomeBindingImpl;
import com.shoohna.gofresh.databinding.FragmentLoginBindingImpl;
import com.shoohna.gofresh.databinding.FragmentMoreBindingImpl;
import com.shoohna.gofresh.databinding.FragmentMoreSettingBindingImpl;
import com.shoohna.gofresh.databinding.FragmentMyAddressBindingImpl;
import com.shoohna.gofresh.databinding.FragmentNotificationBindingImpl;
import com.shoohna.gofresh.databinding.FragmentOrderDataBindingImpl;
import com.shoohna.gofresh.databinding.FragmentOrderStatusBindingImpl;
import com.shoohna.gofresh.databinding.FragmentPaymentWayBindingImpl;
import com.shoohna.gofresh.databinding.FragmentPersonalInformationBindingImpl;
import com.shoohna.gofresh.databinding.FragmentPreAuthorizationPaymentBindingImpl;
import com.shoohna.gofresh.databinding.FragmentProductBindingImpl;
import com.shoohna.gofresh.databinding.FragmentProductDetailsBindingImpl;
import com.shoohna.gofresh.databinding.FragmentProfileBindingImpl;
import com.shoohna.gofresh.databinding.FragmentRecietBindingImpl;
import com.shoohna.gofresh.databinding.FragmentRegisterBindingImpl;
import com.shoohna.gofresh.databinding.FragmentRegisterIntroBindingImpl;
import com.shoohna.gofresh.databinding.FragmentResetOasswordBindingImpl;
import com.shoohna.gofresh.databinding.FragmentShippingBindingImpl;
import com.shoohna.gofresh.databinding.FragmentSplashBindingImpl;
import com.shoohna.gofresh.databinding.FragmentVerifyCodeBindingImpl;
import com.shoohna.gofresh.databinding.FragmentWalletBindingImpl;
import com.shoohna.gofresh.databinding.FragmentWelcomeBindingImpl;
import com.shoohna.gofresh.databinding.HelpItemRowBindingImpl;
import com.shoohna.gofresh.databinding.HighRatedListRowBindingImpl;
import com.shoohna.gofresh.databinding.ItemCategoryBindingImpl;
import com.shoohna.gofresh.databinding.ItemProductCategoryBindingImpl;
import com.shoohna.gofresh.databinding.LearnBindingImpl;
import com.shoohna.gofresh.databinding.MyAddressItemRowBindingImpl;
import com.shoohna.gofresh.databinding.MyChatItemBindingImpl;
import com.shoohna.gofresh.databinding.MyOrderProductItemBindingImpl;
import com.shoohna.gofresh.databinding.NotificationItemRowBindingImpl;
import com.shoohna.gofresh.databinding.OffersListRowBindingImpl;
import com.shoohna.gofresh.databinding.OrderDataRowBindingImpl;
import com.shoohna.gofresh.databinding.OrderStatusRowBindingImpl;
import com.shoohna.gofresh.databinding.ProductBestSaleRowBindingImpl;
import com.shoohna.gofresh.databinding.ProductDetailsColorItemRowBindingImpl;
import com.shoohna.gofresh.databinding.ProductDetailsRowBindingImpl;
import com.shoohna.gofresh.databinding.ProductDetailsSheetBindingImpl;
import com.shoohna.gofresh.databinding.ProductListRowBindingImpl;
import com.shoohna.gofresh.databinding.RecietItemRowBindingImpl;
import com.shoohna.gofresh.databinding.ShippingInformationItemRowBindingImpl;
import com.shoohna.gofresh.databinding.SizeItemRowBindingImpl;
import com.shoohna.gofresh.databinding.WalletAnalysisRowBindingImpl;
import com.shoohna.gofresh.databinding.WalletWithdrawItemBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ABOUTUSITEMROW = 1;

  private static final int LAYOUT_ACTIVITYMAIN = 2;

  private static final int LAYOUT_ACTIVITYWELCOME = 3;

  private static final int LAYOUT_APPLICATIONCHATITEM = 4;

  private static final int LAYOUT_BESTSALESLISTROW = 5;

  private static final int LAYOUT_BOTTOMSHEETLAYOUT = 6;

  private static final int LAYOUT_CARTITEMROW = 7;

  private static final int LAYOUT_CHECKOUTMYCARTITEMROW = 8;

  private static final int LAYOUT_CREDITCARDVIEW = 9;

  private static final int LAYOUT_FAMILYDIALOGITEM = 10;

  private static final int LAYOUT_FAVORITEVIEWROW = 11;

  private static final int LAYOUT_FILTERITEMROW = 12;

  private static final int LAYOUT_FRAGMENTABOUTUS = 13;

  private static final int LAYOUT_FRAGMENTADDCREDITCARD = 14;

  private static final int LAYOUT_FRAGMENTADDFAMILYREGISTER = 15;

  private static final int LAYOUT_FRAGMENTADDNEWADDRESS = 16;

  private static final int LAYOUT_FRAGMENTCART = 17;

  private static final int LAYOUT_FRAGMENTCHANGEPASSWORDPROFILE = 18;

  private static final int LAYOUT_FRAGMENTCHAT = 19;

  private static final int LAYOUT_FRAGMENTCHECKOUTPROCESS = 20;

  private static final int LAYOUT_FRAGMENTCONTACTUS = 21;

  private static final int LAYOUT_FRAGMENTFAMILYDIALOG = 22;

  private static final int LAYOUT_FRAGMENTFAVORITE = 23;

  private static final int LAYOUT_FRAGMENTFORGETPASSWORD = 24;

  private static final int LAYOUT_FRAGMENTHELP = 25;

  private static final int LAYOUT_FRAGMENTHOME = 26;

  private static final int LAYOUT_FRAGMENTLOGIN = 27;

  private static final int LAYOUT_FRAGMENTMORE = 28;

  private static final int LAYOUT_FRAGMENTMORESETTING = 29;

  private static final int LAYOUT_FRAGMENTMYADDRESS = 30;

  private static final int LAYOUT_FRAGMENTNOTIFICATION = 31;

  private static final int LAYOUT_FRAGMENTORDERDATA = 32;

  private static final int LAYOUT_FRAGMENTORDERSTATUS = 33;

  private static final int LAYOUT_FRAGMENTPAYMENTWAY = 34;

  private static final int LAYOUT_FRAGMENTPERSONALINFORMATION = 35;

  private static final int LAYOUT_FRAGMENTPREAUTHORIZATIONPAYMENT = 36;

  private static final int LAYOUT_FRAGMENTPRODUCT = 37;

  private static final int LAYOUT_FRAGMENTPRODUCTDETAILS = 38;

  private static final int LAYOUT_FRAGMENTPROFILE = 39;

  private static final int LAYOUT_FRAGMENTRECIET = 40;

  private static final int LAYOUT_FRAGMENTREGISTER = 41;

  private static final int LAYOUT_FRAGMENTREGISTERINTRO = 42;

  private static final int LAYOUT_FRAGMENTRESETOASSWORD = 43;

  private static final int LAYOUT_FRAGMENTSHIPPING = 44;

  private static final int LAYOUT_FRAGMENTSPLASH = 45;

  private static final int LAYOUT_FRAGMENTVERIFYCODE = 46;

  private static final int LAYOUT_FRAGMENTWALLET = 47;

  private static final int LAYOUT_FRAGMENTWELCOME = 48;

  private static final int LAYOUT_HELPITEMROW = 49;

  private static final int LAYOUT_HIGHRATEDLISTROW = 50;

  private static final int LAYOUT_ITEMCATEGORY = 51;

  private static final int LAYOUT_ITEMPRODUCTCATEGORY = 52;

  private static final int LAYOUT_LEARN = 53;

  private static final int LAYOUT_MYADDRESSITEMROW = 54;

  private static final int LAYOUT_MYCHATITEM = 55;

  private static final int LAYOUT_MYORDERPRODUCTITEM = 56;

  private static final int LAYOUT_NOTIFICATIONITEMROW = 57;

  private static final int LAYOUT_OFFERSLISTROW = 58;

  private static final int LAYOUT_ORDERDATAROW = 59;

  private static final int LAYOUT_ORDERSTATUSROW = 60;

  private static final int LAYOUT_PRODUCTBESTSALEROW = 61;

  private static final int LAYOUT_PRODUCTDETAILSCOLORITEMROW = 62;

  private static final int LAYOUT_PRODUCTDETAILSROW = 63;

  private static final int LAYOUT_PRODUCTDETAILSSHEET = 64;

  private static final int LAYOUT_PRODUCTLISTROW = 65;

  private static final int LAYOUT_RECIETITEMROW = 66;

  private static final int LAYOUT_SHIPPINGINFORMATIONITEMROW = 67;

  private static final int LAYOUT_SIZEITEMROW = 68;

  private static final int LAYOUT_WALLETANALYSISROW = 69;

  private static final int LAYOUT_WALLETWITHDRAWITEM = 70;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(70);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.about_us_item_row, LAYOUT_ABOUTUSITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.activity_welcome, LAYOUT_ACTIVITYWELCOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.application_chat_item, LAYOUT_APPLICATIONCHATITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.best_sales_list_row, LAYOUT_BESTSALESLISTROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.bottom_sheet_layout, LAYOUT_BOTTOMSHEETLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.cart_item_row, LAYOUT_CARTITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.checkout_mycart_item_row, LAYOUT_CHECKOUTMYCARTITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.credit_card_view, LAYOUT_CREDITCARDVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.family_dialog_item, LAYOUT_FAMILYDIALOGITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.favorite_view_row, LAYOUT_FAVORITEVIEWROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.filter_item_row, LAYOUT_FILTERITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_about_us, LAYOUT_FRAGMENTABOUTUS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_add_credit_card, LAYOUT_FRAGMENTADDCREDITCARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_add_family_register, LAYOUT_FRAGMENTADDFAMILYREGISTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_add_new_address, LAYOUT_FRAGMENTADDNEWADDRESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_cart, LAYOUT_FRAGMENTCART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_change_password_profile, LAYOUT_FRAGMENTCHANGEPASSWORDPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_chat, LAYOUT_FRAGMENTCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_checkout_process, LAYOUT_FRAGMENTCHECKOUTPROCESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_contact_us, LAYOUT_FRAGMENTCONTACTUS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_family_dialog, LAYOUT_FRAGMENTFAMILYDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_favorite, LAYOUT_FRAGMENTFAVORITE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_forget_password, LAYOUT_FRAGMENTFORGETPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_help, LAYOUT_FRAGMENTHELP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_login, LAYOUT_FRAGMENTLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_more, LAYOUT_FRAGMENTMORE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_more_setting, LAYOUT_FRAGMENTMORESETTING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_my_address, LAYOUT_FRAGMENTMYADDRESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_notification, LAYOUT_FRAGMENTNOTIFICATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_order_data, LAYOUT_FRAGMENTORDERDATA);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_order_status, LAYOUT_FRAGMENTORDERSTATUS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_payment_way, LAYOUT_FRAGMENTPAYMENTWAY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_personal_information, LAYOUT_FRAGMENTPERSONALINFORMATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_pre_authorization_payment, LAYOUT_FRAGMENTPREAUTHORIZATIONPAYMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_product, LAYOUT_FRAGMENTPRODUCT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_product_details, LAYOUT_FRAGMENTPRODUCTDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_profile, LAYOUT_FRAGMENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_reciet, LAYOUT_FRAGMENTRECIET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_register, LAYOUT_FRAGMENTREGISTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_register_intro, LAYOUT_FRAGMENTREGISTERINTRO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_reset_oassword, LAYOUT_FRAGMENTRESETOASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_shipping, LAYOUT_FRAGMENTSHIPPING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_splash, LAYOUT_FRAGMENTSPLASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_verify_code, LAYOUT_FRAGMENTVERIFYCODE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_wallet, LAYOUT_FRAGMENTWALLET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.fragment_welcome, LAYOUT_FRAGMENTWELCOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.help_item_row, LAYOUT_HELPITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.high_rated_list_row, LAYOUT_HIGHRATEDLISTROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.item_category, LAYOUT_ITEMCATEGORY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.item_product_category, LAYOUT_ITEMPRODUCTCATEGORY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.learn, LAYOUT_LEARN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.my_address_item_row, LAYOUT_MYADDRESSITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.my_chat_item, LAYOUT_MYCHATITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.my_order_product_item, LAYOUT_MYORDERPRODUCTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.notification_item_row, LAYOUT_NOTIFICATIONITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.offers_list_row, LAYOUT_OFFERSLISTROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.order_data_row, LAYOUT_ORDERDATAROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.order_status_row, LAYOUT_ORDERSTATUSROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.product_best_sale_row, LAYOUT_PRODUCTBESTSALEROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.product_details_color_item_row, LAYOUT_PRODUCTDETAILSCOLORITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.product_details_row, LAYOUT_PRODUCTDETAILSROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.product_details_sheet, LAYOUT_PRODUCTDETAILSSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.product_list_row, LAYOUT_PRODUCTLISTROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.reciet_item_row, LAYOUT_RECIETITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.shipping_information_item_row, LAYOUT_SHIPPINGINFORMATIONITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.size_item_row, LAYOUT_SIZEITEMROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.wallet_analysis_row, LAYOUT_WALLETANALYSISROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.shoohna.gofresh.R.layout.wallet_withdraw_item, LAYOUT_WALLETWITHDRAWITEM);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ABOUTUSITEMROW: {
        if ("layout/about_us_item_row_0".equals(tag)) {
          return new AboutUsItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for about_us_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYMAIN: {
        if ("layout/activity_main_0".equals(tag)) {
          return new ActivityMainBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYWELCOME: {
        if ("layout/activity_welcome_0".equals(tag)) {
          return new ActivityWelcomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_welcome is invalid. Received: " + tag);
      }
      case  LAYOUT_APPLICATIONCHATITEM: {
        if ("layout/application_chat_item_0".equals(tag)) {
          return new ApplicationChatItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for application_chat_item is invalid. Received: " + tag);
      }
      case  LAYOUT_BESTSALESLISTROW: {
        if ("layout/best_sales_list_row_0".equals(tag)) {
          return new BestSalesListRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for best_sales_list_row is invalid. Received: " + tag);
      }
      case  LAYOUT_BOTTOMSHEETLAYOUT: {
        if ("layout/bottom_sheet_layout_0".equals(tag)) {
          return new BottomSheetLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for bottom_sheet_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_CARTITEMROW: {
        if ("layout/cart_item_row_0".equals(tag)) {
          return new CartItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for cart_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_CHECKOUTMYCARTITEMROW: {
        if ("layout/checkout_mycart_item_row_0".equals(tag)) {
          return new CheckoutMycartItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for checkout_mycart_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_CREDITCARDVIEW: {
        if ("layout/credit_card_view_0".equals(tag)) {
          return new CreditCardViewBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for credit_card_view is invalid. Received: " + tag);
      }
      case  LAYOUT_FAMILYDIALOGITEM: {
        if ("layout/family_dialog_item_0".equals(tag)) {
          return new FamilyDialogItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for family_dialog_item is invalid. Received: " + tag);
      }
      case  LAYOUT_FAVORITEVIEWROW: {
        if ("layout/favorite_view_row_0".equals(tag)) {
          return new FavoriteViewRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for favorite_view_row is invalid. Received: " + tag);
      }
      case  LAYOUT_FILTERITEMROW: {
        if ("layout/filter_item_row_0".equals(tag)) {
          return new FilterItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for filter_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTABOUTUS: {
        if ("layout/fragment_about_us_0".equals(tag)) {
          return new FragmentAboutUsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_about_us is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDCREDITCARD: {
        if ("layout/fragment_add_credit_card_0".equals(tag)) {
          return new FragmentAddCreditCardBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_credit_card is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDFAMILYREGISTER: {
        if ("layout/fragment_add_family_register_0".equals(tag)) {
          return new FragmentAddFamilyRegisterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_family_register is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDNEWADDRESS: {
        if ("layout/fragment_add_new_address_0".equals(tag)) {
          return new FragmentAddNewAddressBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_new_address is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCART: {
        if ("layout/fragment_cart_0".equals(tag)) {
          return new FragmentCartBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_cart is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHANGEPASSWORDPROFILE: {
        if ("layout/fragment_change_password_profile_0".equals(tag)) {
          return new FragmentChangePasswordProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_change_password_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHAT: {
        if ("layout/fragment_chat_0".equals(tag)) {
          return new FragmentChatBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_chat is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHECKOUTPROCESS: {
        if ("layout/fragment_checkout_process_0".equals(tag)) {
          return new FragmentCheckoutProcessBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_checkout_process is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCONTACTUS: {
        if ("layout/fragment_contact_us_0".equals(tag)) {
          return new FragmentContactUsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_contact_us is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFAMILYDIALOG: {
        if ("layout/fragment_family_dialog_0".equals(tag)) {
          return new FragmentFamilyDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_family_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFAVORITE: {
        if ("layout/fragment_favorite_0".equals(tag)) {
          return new FragmentFavoriteBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_favorite is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGETPASSWORD: {
        if ("layout/fragment_forget_password_0".equals(tag)) {
          return new FragmentForgetPasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forget_password is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTHELP: {
        if ("layout/fragment_help_0".equals(tag)) {
          return new FragmentHelpBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_help is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTHOME: {
        if ("layout/fragment_home_0".equals(tag)) {
          return new FragmentHomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTLOGIN: {
        if ("layout/fragment_login_0".equals(tag)) {
          return new FragmentLoginBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_login is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMORE: {
        if ("layout/fragment_more_0".equals(tag)) {
          return new FragmentMoreBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_more is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMORESETTING: {
        if ("layout/fragment_more_setting_0".equals(tag)) {
          return new FragmentMoreSettingBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_more_setting is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMYADDRESS: {
        if ("layout/fragment_my_address_0".equals(tag)) {
          return new FragmentMyAddressBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_my_address is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTNOTIFICATION: {
        if ("layout/fragment_notification_0".equals(tag)) {
          return new FragmentNotificationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_notification is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTORDERDATA: {
        if ("layout/fragment_order_data_0".equals(tag)) {
          return new FragmentOrderDataBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_order_data is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTORDERSTATUS: {
        if ("layout/fragment_order_status_0".equals(tag)) {
          return new FragmentOrderStatusBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_order_status is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPAYMENTWAY: {
        if ("layout/fragment_payment_way_0".equals(tag)) {
          return new FragmentPaymentWayBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_payment_way is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPERSONALINFORMATION: {
        if ("layout/fragment_personal_information_0".equals(tag)) {
          return new FragmentPersonalInformationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_personal_information is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPREAUTHORIZATIONPAYMENT: {
        if ("layout/fragment_pre_authorization_payment_0".equals(tag)) {
          return new FragmentPreAuthorizationPaymentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_pre_authorization_payment is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRODUCT: {
        if ("layout/fragment_product_0".equals(tag)) {
          return new FragmentProductBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_product is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRODUCTDETAILS: {
        if ("layout/fragment_product_details_0".equals(tag)) {
          return new FragmentProductDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_product_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROFILE: {
        if ("layout/fragment_profile_0".equals(tag)) {
          return new FragmentProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTRECIET: {
        if ("layout/fragment_reciet_0".equals(tag)) {
          return new FragmentRecietBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_reciet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREGISTER: {
        if ("layout/fragment_register_0".equals(tag)) {
          return new FragmentRegisterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_register is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREGISTERINTRO: {
        if ("layout/fragment_register_intro_0".equals(tag)) {
          return new FragmentRegisterIntroBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_register_intro is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTRESETOASSWORD: {
        if ("layout/fragment_reset_oassword_0".equals(tag)) {
          return new FragmentResetOasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_reset_oassword is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSHIPPING: {
        if ("layout/fragment_shipping_0".equals(tag)) {
          return new FragmentShippingBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_shipping is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSPLASH: {
        if ("layout/fragment_splash_0".equals(tag)) {
          return new FragmentSplashBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_splash is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTVERIFYCODE: {
        if ("layout/fragment_verify_code_0".equals(tag)) {
          return new FragmentVerifyCodeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_verify_code is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWALLET: {
        if ("layout/fragment_wallet_0".equals(tag)) {
          return new FragmentWalletBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_wallet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWELCOME: {
        if ("layout/fragment_welcome_0".equals(tag)) {
          return new FragmentWelcomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_welcome is invalid. Received: " + tag);
      }
      case  LAYOUT_HELPITEMROW: {
        if ("layout/help_item_row_0".equals(tag)) {
          return new HelpItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for help_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_HIGHRATEDLISTROW: {
        if ("layout/high_rated_list_row_0".equals(tag)) {
          return new HighRatedListRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for high_rated_list_row is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ITEMCATEGORY: {
        if ("layout/item_category_0".equals(tag)) {
          return new ItemCategoryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_category is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMPRODUCTCATEGORY: {
        if ("layout/item_product_category_0".equals(tag)) {
          return new ItemProductCategoryBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_product_category is invalid. Received: " + tag);
      }
      case  LAYOUT_LEARN: {
        if ("layout/learn_0".equals(tag)) {
          return new LearnBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for learn is invalid. Received: " + tag);
      }
      case  LAYOUT_MYADDRESSITEMROW: {
        if ("layout/my_address_item_row_0".equals(tag)) {
          return new MyAddressItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for my_address_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_MYCHATITEM: {
        if ("layout/my_chat_item_0".equals(tag)) {
          return new MyChatItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for my_chat_item is invalid. Received: " + tag);
      }
      case  LAYOUT_MYORDERPRODUCTITEM: {
        if ("layout/my_order_product_item_0".equals(tag)) {
          return new MyOrderProductItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for my_order_product_item is invalid. Received: " + tag);
      }
      case  LAYOUT_NOTIFICATIONITEMROW: {
        if ("layout/notification_item_row_0".equals(tag)) {
          return new NotificationItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for notification_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_OFFERSLISTROW: {
        if ("layout/offers_list_row_0".equals(tag)) {
          return new OffersListRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for offers_list_row is invalid. Received: " + tag);
      }
      case  LAYOUT_ORDERDATAROW: {
        if ("layout/order_data_row_0".equals(tag)) {
          return new OrderDataRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for order_data_row is invalid. Received: " + tag);
      }
      case  LAYOUT_ORDERSTATUSROW: {
        if ("layout/order_status_row_0".equals(tag)) {
          return new OrderStatusRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for order_status_row is invalid. Received: " + tag);
      }
      case  LAYOUT_PRODUCTBESTSALEROW: {
        if ("layout/product_best_sale_row_0".equals(tag)) {
          return new ProductBestSaleRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for product_best_sale_row is invalid. Received: " + tag);
      }
      case  LAYOUT_PRODUCTDETAILSCOLORITEMROW: {
        if ("layout/product_details_color_item_row_0".equals(tag)) {
          return new ProductDetailsColorItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for product_details_color_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_PRODUCTDETAILSROW: {
        if ("layout/product_details_row_0".equals(tag)) {
          return new ProductDetailsRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for product_details_row is invalid. Received: " + tag);
      }
      case  LAYOUT_PRODUCTDETAILSSHEET: {
        if ("layout/product_details_sheet_0".equals(tag)) {
          return new ProductDetailsSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for product_details_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_PRODUCTLISTROW: {
        if ("layout/product_list_row_0".equals(tag)) {
          return new ProductListRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for product_list_row is invalid. Received: " + tag);
      }
      case  LAYOUT_RECIETITEMROW: {
        if ("layout/reciet_item_row_0".equals(tag)) {
          return new RecietItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reciet_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_SHIPPINGINFORMATIONITEMROW: {
        if ("layout/shipping_information_item_row_0".equals(tag)) {
          return new ShippingInformationItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for shipping_information_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_SIZEITEMROW: {
        if ("layout/size_item_row_0".equals(tag)) {
          return new SizeItemRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for size_item_row is invalid. Received: " + tag);
      }
      case  LAYOUT_WALLETANALYSISROW: {
        if ("layout/wallet_analysis_row_0".equals(tag)) {
          return new WalletAnalysisRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for wallet_analysis_row is invalid. Received: " + tag);
      }
      case  LAYOUT_WALLETWITHDRAWITEM: {
        if ("layout/wallet_withdraw_item_0".equals(tag)) {
          return new WalletWithdrawItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for wallet_withdraw_item is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(7);

    static {
      sKeys.put(1, "FilterModel");
      sKeys.put(2, "Model");
      sKeys.put(3, "ModelRoom");
      sKeys.put(4, "VM");
      sKeys.put(0, "_all");
      sKeys.put(5, "mContext");
      sKeys.put(6, "viewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(70);

    static {
      sKeys.put("layout/about_us_item_row_0", com.shoohna.gofresh.R.layout.about_us_item_row);
      sKeys.put("layout/activity_main_0", com.shoohna.gofresh.R.layout.activity_main);
      sKeys.put("layout/activity_welcome_0", com.shoohna.gofresh.R.layout.activity_welcome);
      sKeys.put("layout/application_chat_item_0", com.shoohna.gofresh.R.layout.application_chat_item);
      sKeys.put("layout/best_sales_list_row_0", com.shoohna.gofresh.R.layout.best_sales_list_row);
      sKeys.put("layout/bottom_sheet_layout_0", com.shoohna.gofresh.R.layout.bottom_sheet_layout);
      sKeys.put("layout/cart_item_row_0", com.shoohna.gofresh.R.layout.cart_item_row);
      sKeys.put("layout/checkout_mycart_item_row_0", com.shoohna.gofresh.R.layout.checkout_mycart_item_row);
      sKeys.put("layout/credit_card_view_0", com.shoohna.gofresh.R.layout.credit_card_view);
      sKeys.put("layout/family_dialog_item_0", com.shoohna.gofresh.R.layout.family_dialog_item);
      sKeys.put("layout/favorite_view_row_0", com.shoohna.gofresh.R.layout.favorite_view_row);
      sKeys.put("layout/filter_item_row_0", com.shoohna.gofresh.R.layout.filter_item_row);
      sKeys.put("layout/fragment_about_us_0", com.shoohna.gofresh.R.layout.fragment_about_us);
      sKeys.put("layout/fragment_add_credit_card_0", com.shoohna.gofresh.R.layout.fragment_add_credit_card);
      sKeys.put("layout/fragment_add_family_register_0", com.shoohna.gofresh.R.layout.fragment_add_family_register);
      sKeys.put("layout/fragment_add_new_address_0", com.shoohna.gofresh.R.layout.fragment_add_new_address);
      sKeys.put("layout/fragment_cart_0", com.shoohna.gofresh.R.layout.fragment_cart);
      sKeys.put("layout/fragment_change_password_profile_0", com.shoohna.gofresh.R.layout.fragment_change_password_profile);
      sKeys.put("layout/fragment_chat_0", com.shoohna.gofresh.R.layout.fragment_chat);
      sKeys.put("layout/fragment_checkout_process_0", com.shoohna.gofresh.R.layout.fragment_checkout_process);
      sKeys.put("layout/fragment_contact_us_0", com.shoohna.gofresh.R.layout.fragment_contact_us);
      sKeys.put("layout/fragment_family_dialog_0", com.shoohna.gofresh.R.layout.fragment_family_dialog);
      sKeys.put("layout/fragment_favorite_0", com.shoohna.gofresh.R.layout.fragment_favorite);
      sKeys.put("layout/fragment_forget_password_0", com.shoohna.gofresh.R.layout.fragment_forget_password);
      sKeys.put("layout/fragment_help_0", com.shoohna.gofresh.R.layout.fragment_help);
      sKeys.put("layout/fragment_home_0", com.shoohna.gofresh.R.layout.fragment_home);
      sKeys.put("layout/fragment_login_0", com.shoohna.gofresh.R.layout.fragment_login);
      sKeys.put("layout/fragment_more_0", com.shoohna.gofresh.R.layout.fragment_more);
      sKeys.put("layout/fragment_more_setting_0", com.shoohna.gofresh.R.layout.fragment_more_setting);
      sKeys.put("layout/fragment_my_address_0", com.shoohna.gofresh.R.layout.fragment_my_address);
      sKeys.put("layout/fragment_notification_0", com.shoohna.gofresh.R.layout.fragment_notification);
      sKeys.put("layout/fragment_order_data_0", com.shoohna.gofresh.R.layout.fragment_order_data);
      sKeys.put("layout/fragment_order_status_0", com.shoohna.gofresh.R.layout.fragment_order_status);
      sKeys.put("layout/fragment_payment_way_0", com.shoohna.gofresh.R.layout.fragment_payment_way);
      sKeys.put("layout/fragment_personal_information_0", com.shoohna.gofresh.R.layout.fragment_personal_information);
      sKeys.put("layout/fragment_pre_authorization_payment_0", com.shoohna.gofresh.R.layout.fragment_pre_authorization_payment);
      sKeys.put("layout/fragment_product_0", com.shoohna.gofresh.R.layout.fragment_product);
      sKeys.put("layout/fragment_product_details_0", com.shoohna.gofresh.R.layout.fragment_product_details);
      sKeys.put("layout/fragment_profile_0", com.shoohna.gofresh.R.layout.fragment_profile);
      sKeys.put("layout/fragment_reciet_0", com.shoohna.gofresh.R.layout.fragment_reciet);
      sKeys.put("layout/fragment_register_0", com.shoohna.gofresh.R.layout.fragment_register);
      sKeys.put("layout/fragment_register_intro_0", com.shoohna.gofresh.R.layout.fragment_register_intro);
      sKeys.put("layout/fragment_reset_oassword_0", com.shoohna.gofresh.R.layout.fragment_reset_oassword);
      sKeys.put("layout/fragment_shipping_0", com.shoohna.gofresh.R.layout.fragment_shipping);
      sKeys.put("layout/fragment_splash_0", com.shoohna.gofresh.R.layout.fragment_splash);
      sKeys.put("layout/fragment_verify_code_0", com.shoohna.gofresh.R.layout.fragment_verify_code);
      sKeys.put("layout/fragment_wallet_0", com.shoohna.gofresh.R.layout.fragment_wallet);
      sKeys.put("layout/fragment_welcome_0", com.shoohna.gofresh.R.layout.fragment_welcome);
      sKeys.put("layout/help_item_row_0", com.shoohna.gofresh.R.layout.help_item_row);
      sKeys.put("layout/high_rated_list_row_0", com.shoohna.gofresh.R.layout.high_rated_list_row);
      sKeys.put("layout/item_category_0", com.shoohna.gofresh.R.layout.item_category);
      sKeys.put("layout/item_product_category_0", com.shoohna.gofresh.R.layout.item_product_category);
      sKeys.put("layout/learn_0", com.shoohna.gofresh.R.layout.learn);
      sKeys.put("layout/my_address_item_row_0", com.shoohna.gofresh.R.layout.my_address_item_row);
      sKeys.put("layout/my_chat_item_0", com.shoohna.gofresh.R.layout.my_chat_item);
      sKeys.put("layout/my_order_product_item_0", com.shoohna.gofresh.R.layout.my_order_product_item);
      sKeys.put("layout/notification_item_row_0", com.shoohna.gofresh.R.layout.notification_item_row);
      sKeys.put("layout/offers_list_row_0", com.shoohna.gofresh.R.layout.offers_list_row);
      sKeys.put("layout/order_data_row_0", com.shoohna.gofresh.R.layout.order_data_row);
      sKeys.put("layout/order_status_row_0", com.shoohna.gofresh.R.layout.order_status_row);
      sKeys.put("layout/product_best_sale_row_0", com.shoohna.gofresh.R.layout.product_best_sale_row);
      sKeys.put("layout/product_details_color_item_row_0", com.shoohna.gofresh.R.layout.product_details_color_item_row);
      sKeys.put("layout/product_details_row_0", com.shoohna.gofresh.R.layout.product_details_row);
      sKeys.put("layout/product_details_sheet_0", com.shoohna.gofresh.R.layout.product_details_sheet);
      sKeys.put("layout/product_list_row_0", com.shoohna.gofresh.R.layout.product_list_row);
      sKeys.put("layout/reciet_item_row_0", com.shoohna.gofresh.R.layout.reciet_item_row);
      sKeys.put("layout/shipping_information_item_row_0", com.shoohna.gofresh.R.layout.shipping_information_item_row);
      sKeys.put("layout/size_item_row_0", com.shoohna.gofresh.R.layout.size_item_row);
      sKeys.put("layout/wallet_analysis_row_0", com.shoohna.gofresh.R.layout.wallet_analysis_row);
      sKeys.put("layout/wallet_withdraw_item_0", com.shoohna.gofresh.R.layout.wallet_withdraw_item);
    }
  }
}
