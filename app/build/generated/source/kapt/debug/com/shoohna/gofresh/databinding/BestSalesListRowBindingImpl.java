package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class BestSalesListRowBindingImpl extends BestSalesListRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.cardView2, 5);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public BestSalesListRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private BestSalesListRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[5]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[4]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.TextView) bindings[3]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            );
        this.categorieNameId.setTag(null);
        this.distanceId.setTag(null);
        this.mainConstraintLayoutId.setTag(null);
        this.priceId.setTag(null);
        this.productImgId.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.responses.BestSeller) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.responses.BestSeller Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String modelName = null;
        com.shoohna.gofresh.pojo.responses.BestSeller model = mModel;
        java.lang.String modelDistance = null;
        boolean modelNameEmpty = false;
        java.lang.String modelImage = null;
        java.lang.String modelPriceProduct = null;
        java.lang.String modelPriceProductEmptyPriceIdAndroidStringNoPricePriceIdAndroidStringPriceModelPriceProduct = null;
        boolean modelPriceProductEmpty = false;
        java.lang.String priceIdAndroidStringPriceModelPriceProduct = null;
        java.lang.String modelDistanceEmptyDistanceIdAndroidStringNoDataDistanceIdAndroidStringCalorieModelDistance = null;
        java.lang.String distanceIdAndroidStringCalorieModelDistance = null;
        java.lang.String modelNameEmptyCategorieNameIdAndroidStringNoTitleModelName = null;
        boolean modelDistanceEmpty = false;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.name
                    modelName = model.getName();
                    // read Model.distance
                    modelDistance = model.getDistance();
                    // read Model.image
                    modelImage = model.getImage();
                    // read Model.price_product
                    modelPriceProduct = model.getPrice_product();
                }


                if (modelName != null) {
                    // read Model.name.empty
                    modelNameEmpty = modelName.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelNameEmpty) {
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x40L;
                }
            }
                if (modelDistance != null) {
                    // read Model.distance.empty
                    modelDistanceEmpty = modelDistance.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelDistanceEmpty) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
                if (modelPriceProduct != null) {
                    // read Model.price_product.empty
                    modelPriceProductEmpty = modelPriceProduct.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelPriceProductEmpty) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x4L) != 0) {

                // read (@android:string/price) + (Model.price_product)
                priceIdAndroidStringPriceModelPriceProduct = (priceId.getResources().getString(R.string.price)) + (modelPriceProduct);
        }
        if ((dirtyFlags & 0x10L) != 0) {

                // read (@android:string/calorie) + (Model.distance)
                distanceIdAndroidStringCalorieModelDistance = (distanceId.getResources().getString(R.string.calorie)) + (modelDistance);
        }
        if ((dirtyFlags & 0x3L) != 0) {

                // read Model.name.empty ? @android:string/noTitle : Model.name
                modelNameEmptyCategorieNameIdAndroidStringNoTitleModelName = ((modelNameEmpty) ? (categorieNameId.getResources().getString(R.string.noTitle)) : (modelName));
        }

        if ((dirtyFlags & 0x3L) != 0) {

                // read Model.price_product.empty ? @android:string/noPrice : (@android:string/price) + (Model.price_product)
                modelPriceProductEmptyPriceIdAndroidStringNoPricePriceIdAndroidStringPriceModelPriceProduct = ((modelPriceProductEmpty) ? (priceId.getResources().getString(R.string.noPrice)) : (priceIdAndroidStringPriceModelPriceProduct));
                // read Model.distance.empty ? @android:string/noData : (@android:string/calorie) + (Model.distance)
                modelDistanceEmptyDistanceIdAndroidStringNoDataDistanceIdAndroidStringCalorieModelDistance = ((modelDistanceEmpty) ? (distanceId.getResources().getString(R.string.noData)) : (distanceIdAndroidStringCalorieModelDistance));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.categorieNameId, modelNameEmptyCategorieNameIdAndroidStringNoTitleModelName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.distanceId, modelDistanceEmptyDistanceIdAndroidStringNoDataDistanceIdAndroidStringCalorieModelDistance);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.priceId, modelPriceProductEmptyPriceIdAndroidStringNoPricePriceIdAndroidStringPriceModelPriceProduct);
            com.shoohna.gofresh.util.BindingAdapters.loadImage(this.productImgId, modelImage);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.price_product.empty ? @android:string/noPrice : (@android:string/price) + (Model.price_product)
        flag 3 (0x4L): Model.price_product.empty ? @android:string/noPrice : (@android:string/price) + (Model.price_product)
        flag 4 (0x5L): Model.distance.empty ? @android:string/noData : (@android:string/calorie) + (Model.distance)
        flag 5 (0x6L): Model.distance.empty ? @android:string/noData : (@android:string/calorie) + (Model.distance)
        flag 6 (0x7L): Model.name.empty ? @android:string/noTitle : Model.name
        flag 7 (0x8L): Model.name.empty ? @android:string/noTitle : Model.name
    flag mapping end*/
    //end
}