package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class BottomSheetLayoutBindingImpl extends BottomSheetLayoutBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView14, 15);
        sViewsWithIds.put(R.id.view, 16);
        sViewsWithIds.put(R.id.textView15, 17);
        sViewsWithIds.put(R.id.recyclerView, 18);
        sViewsWithIds.put(R.id.textView16, 19);
        sViewsWithIds.put(R.id.textView17, 20);
        sViewsWithIds.put(R.id.textView18, 21);
        sViewsWithIds.put(R.id.rangeSeekBar, 22);
        sViewsWithIds.put(R.id.leftRangeTxtViewId, 23);
        sViewsWithIds.put(R.id.rigthRangeTxtViewId, 24);
        sViewsWithIds.put(R.id.resetBtnId, 25);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView10;
    @NonNull
    private final android.view.View mboundView11;
    @NonNull
    private final android.widget.ImageView mboundView12;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.view.View mboundView3;
    @NonNull
    private final android.widget.ImageView mboundView4;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView5;
    @NonNull
    private final android.widget.TextView mboundView6;
    @NonNull
    private final android.view.View mboundView7;
    @NonNull
    private final android.widget.ImageView mboundView8;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback11;
    @Nullable
    private final android.view.View.OnClickListener mCallback12;
    @Nullable
    private final android.view.View.OnClickListener mCallback13;
    @Nullable
    private final android.view.View.OnClickListener mCallback10;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public BottomSheetLayoutBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 26, sIncludes, sViewsWithIds));
    }
    private BottomSheetLayoutBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.Button) bindings[13]
            , (android.widget.TextView) bindings[23]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[14]
            , (com.rizlee.rangeseekbar.RangeSeekBar) bindings[22]
            , (androidx.recyclerview.widget.RecyclerView) bindings[18]
            , (android.widget.Button) bindings[25]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[19]
            , (android.widget.LinearLayout) bindings[20]
            , (android.widget.TextView) bindings[21]
            , (android.view.View) bindings[16]
            );
        this.bottomSheet.setTag(null);
        this.doneBtnId.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView10 = (android.widget.TextView) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView11 = (android.view.View) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView12 = (android.widget.ImageView) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.view.View) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.ImageView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (android.view.View) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (android.widget.ImageView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[9];
        this.mboundView9.setTag(null);
        this.mkLoaderId.setTag(null);
        setRootTag(root);
        // listeners
        mCallback11 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 2);
        mCallback12 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 3);
        mCallback13 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 4);
        mCallback10 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMTintCost((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeVMTintTopRated((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 3 :
                return onChangeVMTintNearestMe((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMTintCost(androidx.lifecycle.LiveData<java.lang.Boolean> VMTintCost, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMTintTopRated(androidx.lifecycle.LiveData<java.lang.Boolean> VMTintTopRated, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMTintNearestMe(androidx.lifecycle.LiveData<java.lang.Boolean> VMTintNearestMe, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int vMTintTopRatedMboundView3AndroidColorPinkMboundView3AndroidColorGreyDark = 0;
        java.lang.Boolean vMLoaderGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMTintCostGetValue = false;
        com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
        int vMTintNearestMeMboundView7AndroidColorPinkMboundView7AndroidColorGreyDark = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMTintTopRatedGetValue = false;
        androidx.lifecycle.LiveData<java.lang.Boolean> vMTintCost = null;
        int vMTintTopRatedViewVISIBLEViewGONE = 0;
        java.lang.Boolean vMTintNearestMeGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        int vMTintCostMboundView11AndroidColorPinkMboundView11AndroidColorGreyDark = 0;
        androidx.lifecycle.LiveData<java.lang.Boolean> vMTintTopRated = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMTintNearestMeGetValue = false;
        int vMTintCostViewVISIBLEViewGONE = 0;
        java.lang.Boolean vMTintCostGetValue = null;
        int vMTintCostMboundView10AndroidColorPinkMboundView10AndroidColorGreyDark = 0;
        androidx.lifecycle.LiveData<java.lang.Boolean> vMTintNearestMe = null;
        int vMTintTopRatedMboundView2AndroidColorPinkMboundView2AndroidColorGreyDark = 0;
        int vMTintNearestMeViewVISIBLEViewGONE = 0;
        int vMTintNearestMeMboundView6AndroidColorPinkMboundView6AndroidColorGreyDark = 0;
        java.lang.Boolean vMTintTopRatedGetValue = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (vM != null) {
                        // read VM.tintCost
                        vMTintCost = vM.getTintCost();
                    }
                    updateLiveDataRegistration(0, vMTintCost);


                    if (vMTintCost != null) {
                        // read VM.tintCost.getValue()
                        vMTintCostGetValue = vMTintCost.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMTintCostGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMTintCostGetValue);
                if((dirtyFlags & 0x31L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMTintCostGetValue) {
                            dirtyFlags |= 0x8000L;
                            dirtyFlags |= 0x20000L;
                            dirtyFlags |= 0x80000L;
                    }
                    else {
                            dirtyFlags |= 0x4000L;
                            dirtyFlags |= 0x10000L;
                            dirtyFlags |= 0x40000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMTintCostMboundView11AndroidColorPinkMboundView11AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMTintCostGetValue) ? (getColorFromResource(mboundView11, R.color.pink)) : (getColorFromResource(mboundView11, R.color.greyDark)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? View.VISIBLE : View.GONE
                    vMTintCostViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMTintCostGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMTintCostMboundView10AndroidColorPinkMboundView10AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMTintCostGetValue) ? (getColorFromResource(mboundView10, R.color.pink)) : (getColorFromResource(mboundView10, R.color.greyDark)));
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (vM != null) {
                        // read VM.tintTopRated
                        vMTintTopRated = vM.getTintTopRated();
                    }
                    updateLiveDataRegistration(1, vMTintTopRated);


                    if (vMTintTopRated != null) {
                        // read VM.tintTopRated.getValue()
                        vMTintTopRatedGetValue = vMTintTopRated.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMTintTopRatedGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMTintTopRatedGetValue);
                if((dirtyFlags & 0x32L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMTintTopRatedGetValue) {
                            dirtyFlags |= 0x80L;
                            dirtyFlags |= 0x800L;
                            dirtyFlags |= 0x200000L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                            dirtyFlags |= 0x400L;
                            dirtyFlags |= 0x100000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMTintTopRatedMboundView3AndroidColorPinkMboundView3AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMTintTopRatedGetValue) ? (getColorFromResource(mboundView3, R.color.pink)) : (getColorFromResource(mboundView3, R.color.greyDark)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? View.VISIBLE : View.GONE
                    vMTintTopRatedViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMTintTopRatedGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMTintTopRatedMboundView2AndroidColorPinkMboundView2AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMTintTopRatedGetValue) ? (getColorFromResource(mboundView2, R.color.pink)) : (getColorFromResource(mboundView2, R.color.greyDark)));
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(2, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0x34L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (vM != null) {
                        // read VM.tintNearestMe
                        vMTintNearestMe = vM.getTintNearestMe();
                    }
                    updateLiveDataRegistration(3, vMTintNearestMe);


                    if (vMTintNearestMe != null) {
                        // read VM.tintNearestMe.getValue()
                        vMTintNearestMeGetValue = vMTintNearestMe.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMTintNearestMeGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMTintNearestMeGetValue);
                if((dirtyFlags & 0x38L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMTintNearestMeGetValue) {
                            dirtyFlags |= 0x200L;
                            dirtyFlags |= 0x800000L;
                            dirtyFlags |= 0x2000000L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                            dirtyFlags |= 0x400000L;
                            dirtyFlags |= 0x1000000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMTintNearestMeMboundView7AndroidColorPinkMboundView7AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMTintNearestMeGetValue) ? (getColorFromResource(mboundView7, R.color.pink)) : (getColorFromResource(mboundView7, R.color.greyDark)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? View.VISIBLE : View.GONE
                    vMTintNearestMeViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMTintNearestMeGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMTintNearestMeMboundView6AndroidColorPinkMboundView6AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMTintNearestMeGetValue) ? (getColorFromResource(mboundView6, R.color.pink)) : (getColorFromResource(mboundView6, R.color.greyDark)));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.doneBtnId.setOnClickListener(mCallback13);
            this.mboundView1.setOnClickListener(mCallback10);
            this.mboundView5.setOnClickListener(mCallback11);
            this.mboundView9.setOnClickListener(mCallback12);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            this.mboundView10.setTextColor(vMTintCostMboundView10AndroidColorPinkMboundView10AndroidColorGreyDark);
            this.mboundView12.setVisibility(vMTintCostViewVISIBLEViewGONE);
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.mboundView11.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(vMTintCostMboundView11AndroidColorPinkMboundView11AndroidColorGreyDark));
            }
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            this.mboundView2.setTextColor(vMTintTopRatedMboundView2AndroidColorPinkMboundView2AndroidColorGreyDark);
            this.mboundView4.setVisibility(vMTintTopRatedViewVISIBLEViewGONE);
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.mboundView3.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(vMTintTopRatedMboundView3AndroidColorPinkMboundView3AndroidColorGreyDark));
            }
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            this.mboundView6.setTextColor(vMTintNearestMeMboundView6AndroidColorPinkMboundView6AndroidColorGreyDark);
            this.mboundView8.setVisibility(vMTintNearestMeViewVISIBLEViewGONE);
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.mboundView7.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(vMTintNearestMeMboundView7AndroidColorPinkMboundView7AndroidColorGreyDark));
            }
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.allowTintNearestMe(callbackArg_0);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.allowTintCost(callbackArg_0);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.applyFilter(callbackArg_0);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.allowTintTop(callbackArg_0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.tintCost
        flag 1 (0x2L): VM.tintTopRated
        flag 2 (0x3L): VM.loader
        flag 3 (0x4L): VM.tintNearestMe
        flag 4 (0x5L): VM
        flag 5 (0x6L): null
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? View.VISIBLE : View.GONE
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? View.VISIBLE : View.GONE
        flag 12 (0xdL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 13 (0xeL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 14 (0xfL): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 15 (0x10L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 16 (0x11L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? View.VISIBLE : View.GONE
        flag 17 (0x12L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? View.VISIBLE : View.GONE
        flag 18 (0x13L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 19 (0x14L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintCost.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 20 (0x15L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 21 (0x16L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintTopRated.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 22 (0x17L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? View.VISIBLE : View.GONE
        flag 23 (0x18L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? View.VISIBLE : View.GONE
        flag 24 (0x19L): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 25 (0x1aL): androidx.databinding.ViewDataBinding.safeUnbox(VM.tintNearestMe.getValue()) ? @android:color/pink : @android:color/greyDark
    flag mapping end*/
    //end
}