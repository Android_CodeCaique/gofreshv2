package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class CartItemRowBindingImpl extends CartItemRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView8, 5);
        sViewsWithIds.put(R.id.textView93, 6);
        sViewsWithIds.put(R.id.increaseImgId, 7);
        sViewsWithIds.put(R.id.decreaseImgId, 8);
        sViewsWithIds.put(R.id.textView72, 9);
        sViewsWithIds.put(R.id.deleteImgId, 10);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.ImageView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public CartItemRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private CartItemRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[8]
            , (android.widget.ImageView) bindings[10]
            , (androidx.cardview.widget.CardView) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[6]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.ImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.priceTxtViewId.setTag(null);
        this.quantityTxtId.setTag(null);
        this.textView21.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.ModelRoom == variableId) {
            setModelRoom((com.shoohna.gofresh.pojo.model.ProductEntity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModelRoom(@Nullable com.shoohna.gofresh.pojo.model.ProductEntity ModelRoom) {
        this.mModelRoom = ModelRoom;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.ModelRoom);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.shoohna.gofresh.pojo.model.ProductEntity modelRoom = mModelRoom;
        java.lang.String stringValueOfModelRoomQuantity = null;
        java.lang.String modelRoomPrice = null;
        java.lang.String modelRoomImageURL = null;
        java.lang.String stringValueOfModelRoomPrice = null;
        int modelRoomQuantity = 0;
        java.lang.String modelRoomName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (modelRoom != null) {
                    // read ModelRoom.price
                    modelRoomPrice = modelRoom.getPrice();
                    // read ModelRoom.imageURL
                    modelRoomImageURL = modelRoom.getImageURL();
                    // read ModelRoom.quantity
                    modelRoomQuantity = modelRoom.getQuantity();
                    // read ModelRoom.name
                    modelRoomName = modelRoom.getName();
                }


                // read String.valueOf(ModelRoom.price)
                stringValueOfModelRoomPrice = java.lang.String.valueOf(modelRoomPrice);
                // read String.valueOf(ModelRoom.quantity)
                stringValueOfModelRoomQuantity = java.lang.String.valueOf(modelRoomQuantity);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.shoohna.gofresh.util.BindingAdapters.loadImage(this.mboundView1, modelRoomImageURL);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.priceTxtViewId, stringValueOfModelRoomPrice);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.quantityTxtId, stringValueOfModelRoomQuantity);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView21, modelRoomName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): ModelRoom
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}