package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class CheckoutMycartItemRowBindingImpl extends CheckoutMycartItemRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView8, 5);
        sViewsWithIds.put(R.id.imageView9, 6);
        sViewsWithIds.put(R.id.imageView10, 7);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.ImageView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public CheckoutMycartItemRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private CheckoutMycartItemRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[7]
            , (androidx.cardview.widget.CardView) bindings[5]
            , (android.widget.ImageView) bindings[6]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.ImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.textView13.setTag(null);
        this.textView21.setTag(null);
        this.textView22.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.ModelRoom == variableId) {
            setModelRoom((com.shoohna.gofresh.pojo.model.ProductEntity) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModelRoom(@Nullable com.shoohna.gofresh.pojo.model.ProductEntity ModelRoom) {
        this.mModelRoom = ModelRoom;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.ModelRoom);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.shoohna.gofresh.pojo.model.ProductEntity modelRoom = mModelRoom;
        java.lang.String stringValueOfModelRoomQuantity = null;
        boolean modelRoomPriceEmpty = false;
        java.lang.String modelRoomDescEmptyTextView21AndroidStringNoDescriptionModelRoomDesc = null;
        java.lang.String modelRoomPrice = null;
        java.lang.String stringValueOfModelRoomPriceEmptyTextView22AndroidStringNoPriceModelRoomPrice = null;
        java.lang.String modelRoomImageURL = null;
        java.lang.String modelRoomDesc = null;
        boolean modelRoomDescEmpty = false;
        java.lang.String modelRoomPriceEmptyTextView22AndroidStringNoPriceModelRoomPrice = null;
        int modelRoomQuantity = 0;

        if ((dirtyFlags & 0x3L) != 0) {



                if (modelRoom != null) {
                    // read ModelRoom.price
                    modelRoomPrice = modelRoom.getPrice();
                    // read ModelRoom.imageURL
                    modelRoomImageURL = modelRoom.getImageURL();
                    // read ModelRoom.desc
                    modelRoomDesc = modelRoom.getDesc();
                    // read ModelRoom.quantity
                    modelRoomQuantity = modelRoom.getQuantity();
                }


                if (modelRoomPrice != null) {
                    // read ModelRoom.price.empty
                    modelRoomPriceEmpty = modelRoomPrice.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelRoomPriceEmpty) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
                if (modelRoomDesc != null) {
                    // read ModelRoom.desc.empty
                    modelRoomDescEmpty = modelRoomDesc.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelRoomDescEmpty) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
                // read String.valueOf(ModelRoom.quantity)
                stringValueOfModelRoomQuantity = java.lang.String.valueOf(modelRoomQuantity);
        }
        // batch finished

        if ((dirtyFlags & 0x3L) != 0) {

                // read ModelRoom.desc.empty ? @android:string/noDescription : ModelRoom.desc
                modelRoomDescEmptyTextView21AndroidStringNoDescriptionModelRoomDesc = ((modelRoomDescEmpty) ? (textView21.getResources().getString(R.string.noDescription)) : (modelRoomDesc));
                // read ModelRoom.price.empty ? @android:string/noPrice : ModelRoom.price
                modelRoomPriceEmptyTextView22AndroidStringNoPriceModelRoomPrice = ((modelRoomPriceEmpty) ? (textView22.getResources().getString(R.string.noPrice)) : (modelRoomPrice));


                // read String.valueOf(ModelRoom.price.empty ? @android:string/noPrice : ModelRoom.price)
                stringValueOfModelRoomPriceEmptyTextView22AndroidStringNoPriceModelRoomPrice = java.lang.String.valueOf(modelRoomPriceEmptyTextView22AndroidStringNoPriceModelRoomPrice);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.shoohna.gofresh.util.BindingAdapters.loadImage(this.mboundView1, modelRoomImageURL);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView13, stringValueOfModelRoomQuantity);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView21, modelRoomDescEmptyTextView21AndroidStringNoDescriptionModelRoomDesc);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView22, stringValueOfModelRoomPriceEmptyTextView22AndroidStringNoPriceModelRoomPrice);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): ModelRoom
        flag 1 (0x2L): null
        flag 2 (0x3L): ModelRoom.desc.empty ? @android:string/noDescription : ModelRoom.desc
        flag 3 (0x4L): ModelRoom.desc.empty ? @android:string/noDescription : ModelRoom.desc
        flag 4 (0x5L): ModelRoom.price.empty ? @android:string/noPrice : ModelRoom.price
        flag 5 (0x6L): ModelRoom.price.empty ? @android:string/noPrice : ModelRoom.price
    flag mapping end*/
    //end
}