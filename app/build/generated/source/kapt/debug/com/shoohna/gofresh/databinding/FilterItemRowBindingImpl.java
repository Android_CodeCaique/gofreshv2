package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FilterItemRowBindingImpl extends FilterItemRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FilterItemRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 2, sIncludes, sViewsWithIds));
    }
    private FilterItemRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            );
        this.filterItemId.setTag(null);
        this.itemConstraintID.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.FilterModel == variableId) {
            setFilterModel((com.shoohna.gofresh.pojo.responses.FilerMainCategorieData) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setFilterModel(@Nullable com.shoohna.gofresh.pojo.responses.FilerMainCategorieData FilterModel) {
        this.mFilterModel = FilterModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.FilterModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int filterModelCheckedFilterItemIdAndroidColorGreyDarkFilterItemIdAndroidColorGrey = 0;
        boolean filterModelChecked = false;
        java.lang.String filterModelName = null;
        com.shoohna.gofresh.pojo.responses.FilerMainCategorieData filterModel = mFilterModel;

        if ((dirtyFlags & 0x3L) != 0) {



                if (filterModel != null) {
                    // read FilterModel.checked
                    filterModelChecked = filterModel.isChecked();
                    // read FilterModel.name
                    filterModelName = filterModel.getName();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(filterModelChecked) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read FilterModel.checked ? @android:color/greyDark : @android:color/grey
                filterModelCheckedFilterItemIdAndroidColorGreyDarkFilterItemIdAndroidColorGrey = ((filterModelChecked) ? (getColorFromResource(filterItemId, R.color.greyDark)) : (getColorFromResource(filterItemId, R.color.grey)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.filterItemId.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(filterModelCheckedFilterItemIdAndroidColorGreyDarkFilterItemIdAndroidColorGrey));
            }
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.filterItemId, filterModelName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): FilterModel
        flag 1 (0x2L): null
        flag 2 (0x3L): FilterModel.checked ? @android:color/greyDark : @android:color/grey
        flag 3 (0x4L): FilterModel.checked ? @android:color/greyDark : @android:color/grey
    flag mapping end*/
    //end
}