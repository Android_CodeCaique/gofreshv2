package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentAboutUsBindingImpl extends FragmentAboutUsBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView35, 4);
        sViewsWithIds.put(R.id.logo, 5);
        sViewsWithIds.put(R.id.editConstraintId, 6);
        sViewsWithIds.put(R.id.aboutUsViewRecyclerViewId, 7);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback37;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener descriptionandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.description.getValue()
            //         is VM.description.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(description);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.aboutUs.AboutUsViewModel vM = mVM;
            // VM.description != null
            boolean vMDescriptionJavaLangObjectNull = false;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.description.getValue()
            java.lang.String vMDescriptionGetValue = null;
            // VM.description
            androidx.lifecycle.MutableLiveData<java.lang.String> vMDescription = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMDescription = vM.getDescription();

                vMDescriptionJavaLangObjectNull = (vMDescription) != (null);
                if (vMDescriptionJavaLangObjectNull) {




                    vMDescription.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentAboutUsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private FragmentAboutUsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (androidx.recyclerview.widget.RecyclerView) bindings[7]
            , (android.widget.TextView) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (android.widget.ImageView) bindings[5]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[3]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[4]
            );
        this.description.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mkLoaderId.setTag(null);
        this.share.setTag(null);
        setRootTag(root);
        // listeners
        mCallback37 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.aboutUs.AboutUsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.aboutUs.AboutUsViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeVMDescription((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMDescription(androidx.lifecycle.MutableLiveData<java.lang.String> VMDescription, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String vMDescriptionGetValue = null;
        java.lang.Boolean vMLoaderGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        com.shoohna.gofresh.ui.home.ui.aboutUs.AboutUsViewModel vM = mVM;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMDescription = null;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(0, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0xdL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x20L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (vM != null) {
                        // read VM.description
                        vMDescription = vM.getDescription();
                    }
                    updateLiveDataRegistration(1, vMDescription);


                    if (vMDescription != null) {
                        // read VM.description.getValue()
                        vMDescriptionGetValue = vMDescription.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.description, vMDescriptionGetValue);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.description, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, descriptionandroidTextAttrChanged);
            this.share.setOnClickListener(mCallback37);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // VM
        com.shoohna.gofresh.ui.home.ui.aboutUs.AboutUsViewModel vM = mVM;
        // VM != null
        boolean vMJavaLangObjectNull = false;



        vMJavaLangObjectNull = (vM) != (null);
        if (vMJavaLangObjectNull) {



            vM.share(callbackArg_0);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.loader
        flag 1 (0x2L): VM.description
        flag 2 (0x3L): VM
        flag 3 (0x4L): null
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}