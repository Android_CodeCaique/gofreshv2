package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCartBindingImpl extends FragmentCartBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView8, 6);
        sViewsWithIds.put(R.id.back, 7);
        sViewsWithIds.put(R.id.scrollViewID, 8);
        sViewsWithIds.put(R.id.constraintLayout2Id, 9);
        sViewsWithIds.put(R.id.cartRecyclerViewId, 10);
        sViewsWithIds.put(R.id.view4, 11);
        sViewsWithIds.put(R.id.cardView10, 12);
        sViewsWithIds.put(R.id.bar_chart_vertical, 13);
        sViewsWithIds.put(R.id.tableLayout, 14);
        sViewsWithIds.put(R.id.textView73, 15);
        sViewsWithIds.put(R.id.paymentId, 16);
        sViewsWithIds.put(R.id.cardRecyclerViewId, 17);
        sViewsWithIds.put(R.id.addNewCardTxtViewId, 18);
        sViewsWithIds.put(R.id.discountCoponeTxtViewId, 19);
        sViewsWithIds.put(R.id.discountCoponeCardViewId, 20);
        sViewsWithIds.put(R.id.deliveryTimeTxtViewId, 21);
        sViewsWithIds.put(R.id.deliveryTimeCardViewId, 22);
        sViewsWithIds.put(R.id.dateTxtViewId, 23);
        sViewsWithIds.put(R.id.subTotalTxtViewId, 24);
        sViewsWithIds.put(R.id.subTotalPriceTxtViewId, 25);
        sViewsWithIds.put(R.id.deliveryTxtViewId, 26);
        sViewsWithIds.put(R.id.deliveryPriceTxtViewId, 27);
        sViewsWithIds.put(R.id.totalTxtViewId, 28);
        sViewsWithIds.put(R.id.totalPriceTxtViewId, 29);
        sViewsWithIds.put(R.id.useGainedPointsRadioId, 30);
        sViewsWithIds.put(R.id.statusBtnId, 31);
        sViewsWithIds.put(R.id.noDataTxtId, 32);
    }
    // views
    @NonNull
    private final android.widget.ImageView mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback8;
    @Nullable
    private final android.view.View.OnClickListener mCallback7;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentCartBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 33, sIncludes, sViewsWithIds));
    }
    private FragmentCartBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.TextView) bindings[18]
            , (android.widget.ImageView) bindings[7]
            , (me.ithebk.barchart.BarChart) bindings[13]
            , (androidx.recyclerview.widget.RecyclerView) bindings[17]
            , (androidx.cardview.widget.CardView) bindings[12]
            , (androidx.recyclerview.widget.RecyclerView) bindings[10]
            , (androidx.cardview.widget.CardView) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[9]
            , (android.widget.RelativeLayout) bindings[0]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[27]
            , (androidx.cardview.widget.CardView) bindings[22]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[26]
            , (androidx.cardview.widget.CardView) bindings[20]
            , (android.widget.TextView) bindings[19]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[5]
            , (android.widget.TextView) bindings[32]
            , (android.widget.TextView) bindings[16]
            , (androidx.cardview.widget.CardView) bindings[3]
            , (android.widget.ScrollView) bindings[8]
            , (android.widget.Button) bindings[31]
            , (android.widget.TextView) bindings[25]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TableLayout) bindings[14]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[29]
            , (android.widget.TextView) bindings[28]
            , (android.widget.RadioButton) bindings[30]
            , (android.view.View) bindings[11]
            );
        this.cashId.setTag(null);
        this.constraintLayoutID.setTag(null);
        this.creditTxtViewId.setTag(null);
        this.mboundView2 = (android.widget.ImageView) bindings[2];
        this.mboundView2.setTag(null);
        this.mkLoaderId.setTag(null);
        this.paymentId2.setTag(null);
        setRootTag(root);
        // listeners
        mCallback8 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 2);
        mCallback7 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.cart.CartViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.cart.CartViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMSelectedPayment((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMSelectedPayment(androidx.lifecycle.MutableLiveData<java.lang.Integer> VMSelectedPayment, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean vMSelectedPaymentInt1 = false;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vMSelectedPayment = null;
        android.graphics.drawable.Drawable vMSelectedPaymentInt2CreditTxtViewIdAndroidDrawableIcReminderTwoCreditTxtViewIdAndroidDrawableIcReminder = null;
        java.lang.Boolean vMLoaderGetValue = null;
        com.shoohna.gofresh.ui.home.ui.cart.CartViewModel vM = mVM;
        boolean vMSelectedPaymentInt2 = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        android.graphics.drawable.Drawable vMSelectedPaymentInt1MboundView2AndroidDrawableIcReminderTwoMboundView2AndroidDrawableIcReminder = null;
        int androidxDatabindingViewDataBindingSafeUnboxVMSelectedPaymentGetValue = 0;
        java.lang.Integer vMSelectedPaymentGetValue = null;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (vM != null) {
                        // read VM.selectedPayment
                        vMSelectedPayment = vM.getSelectedPayment();
                    }
                    updateLiveDataRegistration(0, vMSelectedPayment);


                    if (vMSelectedPayment != null) {
                        // read VM.selectedPayment.getValue()
                        vMSelectedPaymentGetValue = vMSelectedPayment.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMSelectedPaymentGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMSelectedPaymentGetValue);


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue()) == 1
                    vMSelectedPaymentInt1 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedPaymentGetValue) == (1);
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue()) == 2
                    vMSelectedPaymentInt2 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedPaymentGetValue) == (2);
                if((dirtyFlags & 0xdL) != 0) {
                    if(vMSelectedPaymentInt1) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }
                if((dirtyFlags & 0xdL) != 0) {
                    if(vMSelectedPaymentInt2) {
                            dirtyFlags |= 0x20L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue()) == 1 ? @android:drawable/ic_reminder_two : @android:drawable/ic_reminder
                    vMSelectedPaymentInt1MboundView2AndroidDrawableIcReminderTwoMboundView2AndroidDrawableIcReminder = ((vMSelectedPaymentInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView2.getContext(), R.drawable.ic_reminder_two)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView2.getContext(), R.drawable.ic_reminder)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue()) == 2 ? @android:drawable/ic_reminder_two : @android:drawable/ic_reminder
                    vMSelectedPaymentInt2CreditTxtViewIdAndroidDrawableIcReminderTwoCreditTxtViewIdAndroidDrawableIcReminder = ((vMSelectedPaymentInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(creditTxtViewId.getContext(), R.drawable.ic_reminder_two)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(creditTxtViewId.getContext(), R.drawable.ic_reminder)));
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(1, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0xeL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.cashId.setOnClickListener(mCallback7);
            this.paymentId2.setOnClickListener(mCallback8);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.creditTxtViewId, vMSelectedPaymentInt2CreditTxtViewIdAndroidDrawableIcReminderTwoCreditTxtViewIdAndroidDrawableIcReminder);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView2, vMSelectedPaymentInt1MboundView2AndroidDrawableIcReminderTwoMboundView2AndroidDrawableIcReminder);
        }
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.cart.CartViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setPaymentCredit(callbackArg_0);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.cart.CartViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setPaymentCash(callbackArg_0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.selectedPayment
        flag 1 (0x2L): VM.loader
        flag 2 (0x3L): VM
        flag 3 (0x4L): null
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue()) == 2 ? @android:drawable/ic_reminder_two : @android:drawable/ic_reminder
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue()) == 2 ? @android:drawable/ic_reminder_two : @android:drawable/ic_reminder
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue()) == 1 ? @android:drawable/ic_reminder_two : @android:drawable/ic_reminder
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedPayment.getValue()) == 1 ? @android:drawable/ic_reminder_two : @android:drawable/ic_reminder
    flag mapping end*/
    //end
}