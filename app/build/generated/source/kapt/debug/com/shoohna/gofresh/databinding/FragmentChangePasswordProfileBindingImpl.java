package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentChangePasswordProfileBindingImpl extends FragmentChangePasswordProfileBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView32, 6);
        sViewsWithIds.put(R.id.cardView6, 7);
        sViewsWithIds.put(R.id.textView4, 8);
        sViewsWithIds.put(R.id.oldPasswordLayout, 9);
        sViewsWithIds.put(R.id.newPasswordLayout, 10);
        sViewsWithIds.put(R.id.confirmNewPasswordLayout, 11);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView1;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView2;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView3;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback33;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView1androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.oldPassword.getValue()
            //         is VM.oldPassword.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView1);
            // localize variables for thread safety
            // VM.oldPassword
            androidx.lifecycle.MutableLiveData<java.lang.String> vMOldPassword = null;
            // VM
            com.shoohna.gofresh.ui.home.ui.changePasswordProfile.ChangePasswordViewModel vM = mVM;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.oldPassword != null
            boolean vMOldPasswordJavaLangObjectNull = false;
            // VM.oldPassword.getValue()
            java.lang.String vMOldPasswordGetValue = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMOldPassword = vM.getOldPassword();

                vMOldPasswordJavaLangObjectNull = (vMOldPassword) != (null);
                if (vMOldPasswordJavaLangObjectNull) {




                    vMOldPassword.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView2androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.newPassword.getValue()
            //         is VM.newPassword.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView2);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.changePasswordProfile.ChangePasswordViewModel vM = mVM;
            // VM.newPassword != null
            boolean vMNewPasswordJavaLangObjectNull = false;
            // VM.newPassword.getValue()
            java.lang.String vMNewPasswordGetValue = null;
            // VM.newPassword
            androidx.lifecycle.MutableLiveData<java.lang.String> vMNewPassword = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMNewPassword = vM.getNewPassword();

                vMNewPasswordJavaLangObjectNull = (vMNewPassword) != (null);
                if (vMNewPasswordJavaLangObjectNull) {




                    vMNewPassword.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.confirmNewPassword.getValue()
            //         is VM.confirmNewPassword.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.changePasswordProfile.ChangePasswordViewModel vM = mVM;
            // VM.confirmNewPassword.getValue()
            java.lang.String vMConfirmNewPasswordGetValue = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.confirmNewPassword != null
            boolean vMConfirmNewPasswordJavaLangObjectNull = false;
            // VM.confirmNewPassword
            androidx.lifecycle.MutableLiveData<java.lang.String> vMConfirmNewPassword = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMConfirmNewPassword = vM.getConfirmNewPassword();

                vMConfirmNewPasswordJavaLangObjectNull = (vMConfirmNewPassword) != (null);
                if (vMConfirmNewPasswordJavaLangObjectNull) {




                    vMConfirmNewPassword.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentChangePasswordProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private FragmentChangePasswordProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (android.widget.Button) bindings[4]
            , (androidx.cardview.widget.CardView) bindings[7]
            , (com.google.android.material.textfield.TextInputLayout) bindings[11]
            , (android.widget.ImageView) bindings[6]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[5]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            , (android.widget.TextView) bindings[8]
            );
        this.button2.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (com.google.android.material.textfield.TextInputEditText) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (com.google.android.material.textfield.TextInputEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (com.google.android.material.textfield.TextInputEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mkLoaderId.setTag(null);
        setRootTag(root);
        // listeners
        mCallback33 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.changePasswordProfile.ChangePasswordViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.changePasswordProfile.ChangePasswordViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeVMOldPassword((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeVMNewPassword((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeVMConfirmNewPassword((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMOldPassword(androidx.lifecycle.MutableLiveData<java.lang.String> VMOldPassword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMNewPassword(androidx.lifecycle.MutableLiveData<java.lang.String> VMNewPassword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMConfirmNewPassword(androidx.lifecycle.MutableLiveData<java.lang.String> VMConfirmNewPassword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Boolean vMLoaderGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        int vMLoaderViewVISIBLEViewINVISIBLE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMOldPassword = null;
        com.shoohna.gofresh.ui.home.ui.changePasswordProfile.ChangePasswordViewModel vM = mVM;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMNewPassword = null;
        java.lang.String vMNewPasswordGetValue = null;
        java.lang.String vMConfirmNewPasswordGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMConfirmNewPassword = null;
        java.lang.String vMOldPasswordGetValue = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(0, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0x31L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.INVISIBLE
                    vMLoaderViewVISIBLEViewINVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.INVISIBLE));
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (vM != null) {
                        // read VM.oldPassword
                        vMOldPassword = vM.getOldPassword();
                    }
                    updateLiveDataRegistration(1, vMOldPassword);


                    if (vMOldPassword != null) {
                        // read VM.oldPassword.getValue()
                        vMOldPasswordGetValue = vMOldPassword.getValue();
                    }
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (vM != null) {
                        // read VM.newPassword
                        vMNewPassword = vM.getNewPassword();
                    }
                    updateLiveDataRegistration(2, vMNewPassword);


                    if (vMNewPassword != null) {
                        // read VM.newPassword.getValue()
                        vMNewPasswordGetValue = vMNewPassword.getValue();
                    }
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (vM != null) {
                        // read VM.confirmNewPassword
                        vMConfirmNewPassword = vM.getConfirmNewPassword();
                    }
                    updateLiveDataRegistration(3, vMConfirmNewPassword);


                    if (vMConfirmNewPassword != null) {
                        // read VM.confirmNewPassword.getValue()
                        vMConfirmNewPasswordGetValue = vMConfirmNewPassword.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.button2.setOnClickListener(mCallback33);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView1, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView1androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView2, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView2androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, vMOldPasswordGetValue);
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, vMNewPasswordGetValue);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, vMConfirmNewPasswordGetValue);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewINVISIBLE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // VM
        com.shoohna.gofresh.ui.home.ui.changePasswordProfile.ChangePasswordViewModel vM = mVM;
        // VM != null
        boolean vMJavaLangObjectNull = false;



        vMJavaLangObjectNull = (vM) != (null);
        if (vMJavaLangObjectNull) {






            vM.changePassword(callbackArg_0, oldPasswordLayout, newPasswordLayout, confirmNewPasswordLayout);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.loader
        flag 1 (0x2L): VM.oldPassword
        flag 2 (0x3L): VM.newPassword
        flag 3 (0x4L): VM.confirmNewPassword
        flag 4 (0x5L): VM
        flag 5 (0x6L): null
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.INVISIBLE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.INVISIBLE
    flag mapping end*/
    //end
}