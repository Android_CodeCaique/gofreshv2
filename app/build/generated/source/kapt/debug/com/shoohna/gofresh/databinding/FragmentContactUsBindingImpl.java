package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentContactUsBindingImpl extends FragmentContactUsBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.back, 9);
        sViewsWithIds.put(R.id.textView35, 10);
        sViewsWithIds.put(R.id.constraintLayout5, 11);
        sViewsWithIds.put(R.id.textView36, 12);
        sViewsWithIds.put(R.id.emailLayout, 13);
        sViewsWithIds.put(R.id.phoneLayout, 14);
        sViewsWithIds.put(R.id.messageLayout, 15);
        sViewsWithIds.put(R.id.constraintLayout9, 16);
        sViewsWithIds.put(R.id.textView38, 17);
        sViewsWithIds.put(R.id.imageView20, 18);
        sViewsWithIds.put(R.id.imageView21, 19);
        sViewsWithIds.put(R.id.imageView22, 20);
        sViewsWithIds.put(R.id.facebookIconId, 21);
        sViewsWithIds.put(R.id.snapchatIconId, 22);
        sViewsWithIds.put(R.id.twitterIconId, 23);
        sViewsWithIds.put(R.id.instagramIconId, 24);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback32;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener PhoneEtandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.phone.getValue()
            //         is VM.phone.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(PhoneEt);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel vM = mVM;
            // VM.phone.getValue()
            java.lang.String vMPhoneGetValue = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.phone != null
            boolean vMPhoneJavaLangObjectNull = false;
            // VM.phone
            androidx.lifecycle.MutableLiveData<java.lang.String> vMPhone = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMPhone = vM.getPhone();

                vMPhoneJavaLangObjectNull = (vMPhone) != (null);
                if (vMPhoneJavaLangObjectNull) {




                    vMPhone.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener addressandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.addressCompany.getValue()
            //         is VM.addressCompany.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(address);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel vM = mVM;
            // VM.addressCompany.getValue()
            java.lang.String vMAddressCompanyGetValue = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.addressCompany
            androidx.lifecycle.MutableLiveData<java.lang.String> vMAddressCompany = null;
            // VM.addressCompany != null
            boolean vMAddressCompanyJavaLangObjectNull = false;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMAddressCompany = vM.getAddressCompany();

                vMAddressCompanyJavaLangObjectNull = (vMAddressCompany) != (null);
                if (vMAddressCompanyJavaLangObjectNull) {




                    vMAddressCompany.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener emailandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.emailCompany.getValue()
            //         is VM.emailCompany.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(email);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel vM = mVM;
            // VM.emailCompany
            androidx.lifecycle.MutableLiveData<java.lang.String> vMEmailCompany = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.emailCompany.getValue()
            java.lang.String vMEmailCompanyGetValue = null;
            // VM.emailCompany != null
            boolean vMEmailCompanyJavaLangObjectNull = false;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMEmailCompany = vM.getEmailCompany();

                vMEmailCompanyJavaLangObjectNull = (vMEmailCompany) != (null);
                if (vMEmailCompanyJavaLangObjectNull) {




                    vMEmailCompany.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener emailEtandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.email.getValue()
            //         is VM.email.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(emailEt);
            // localize variables for thread safety
            // VM.email
            androidx.lifecycle.MutableLiveData<java.lang.String> vMEmail = null;
            // VM
            com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel vM = mVM;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.email != null
            boolean vMEmailJavaLangObjectNull = false;
            // VM.email.getValue()
            java.lang.String vMEmailGetValue = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMEmail = vM.getEmail();

                vMEmailJavaLangObjectNull = (vMEmail) != (null);
                if (vMEmailJavaLangObjectNull) {




                    vMEmail.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener messageEtandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.message.getValue()
            //         is VM.message.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(messageEt);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel vM = mVM;
            // VM.message != null
            boolean vMMessageJavaLangObjectNull = false;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.message.getValue()
            java.lang.String vMMessageGetValue = null;
            // VM.message
            androidx.lifecycle.MutableLiveData<java.lang.String> vMMessage = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMMessage = vM.getMessage();

                vMMessageJavaLangObjectNull = (vMMessage) != (null);
                if (vMMessageJavaLangObjectNull) {




                    vMMessage.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener phoneandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.phoneCompany.getValue()
            //         is VM.phoneCompany.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(phone);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel vM = mVM;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.phoneCompany
            androidx.lifecycle.MutableLiveData<java.lang.String> vMPhoneCompany = null;
            // VM.phoneCompany != null
            boolean vMPhoneCompanyJavaLangObjectNull = false;
            // VM.phoneCompany.getValue()
            java.lang.String vMPhoneCompanyGetValue = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMPhoneCompany = vM.getPhoneCompany();

                vMPhoneCompanyJavaLangObjectNull = (vMPhoneCompany) != (null);
                if (vMPhoneCompanyJavaLangObjectNull) {




                    vMPhoneCompany.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentContactUsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 25, sIncludes, sViewsWithIds));
    }
    private FragmentContactUsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (android.widget.TextView) bindings[7]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.Button) bindings[4]
            , (android.widget.LinearLayout) bindings[11]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[16]
            , (android.widget.TextView) bindings[5]
            , (com.google.android.material.textfield.TextInputEditText) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (android.widget.ImageView) bindings[21]
            , (android.widget.ImageView) bindings[18]
            , (android.widget.ImageView) bindings[19]
            , (android.widget.ImageView) bindings[20]
            , (android.widget.ImageView) bindings[24]
            , (com.google.android.material.textfield.TextInputEditText) bindings[3]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[8]
            , (android.widget.TextView) bindings[6]
            , (com.google.android.material.textfield.TextInputLayout) bindings[14]
            , (android.widget.ImageView) bindings[22]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[17]
            , (android.widget.ImageView) bindings[23]
            );
        this.PhoneEt.setTag(null);
        this.address.setTag(null);
        this.button8.setTag(null);
        this.email.setTag(null);
        this.emailEt.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.messageEt.setTag(null);
        this.mkLoaderId.setTag(null);
        this.phone.setTag(null);
        setRootTag(root);
        // listeners
        mCallback32 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMPhoneCompany((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeVMPhone((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeVMAddressCompany((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeVMMessage((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeVMEmailCompany((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 6 :
                return onChangeVMEmail((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMPhoneCompany(androidx.lifecycle.MutableLiveData<java.lang.String> VMPhoneCompany, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMPhone(androidx.lifecycle.MutableLiveData<java.lang.String> VMPhone, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMAddressCompany(androidx.lifecycle.MutableLiveData<java.lang.String> VMAddressCompany, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMMessage(androidx.lifecycle.MutableLiveData<java.lang.String> VMMessage, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMEmailCompany(androidx.lifecycle.MutableLiveData<java.lang.String> VMEmailCompany, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMEmail(androidx.lifecycle.MutableLiveData<java.lang.String> VMEmail, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.String> vMPhoneCompany = null;
        java.lang.Boolean vMLoaderGetValue = null;
        java.lang.String vMMessageGetValue = null;
        com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel vM = mVM;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMPhone = null;
        java.lang.String vMAddressCompanyGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMAddressCompany = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMMessage = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMEmailCompany = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMEmail = null;
        java.lang.String vMPhoneGetValue = null;
        java.lang.String vMEmailGetValue = null;
        java.lang.String vMPhoneCompanyGetValue = null;
        java.lang.String vMEmailCompanyGetValue = null;

        if ((dirtyFlags & 0x1ffL) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (vM != null) {
                        // read VM.phoneCompany
                        vMPhoneCompany = vM.getPhoneCompany();
                    }
                    updateLiveDataRegistration(0, vMPhoneCompany);


                    if (vMPhoneCompany != null) {
                        // read VM.phoneCompany.getValue()
                        vMPhoneCompanyGetValue = vMPhoneCompany.getValue();
                    }
            }
            if ((dirtyFlags & 0x182L) != 0) {

                    if (vM != null) {
                        // read VM.phone
                        vMPhone = vM.getPhone();
                    }
                    updateLiveDataRegistration(1, vMPhone);


                    if (vMPhone != null) {
                        // read VM.phone.getValue()
                        vMPhoneGetValue = vMPhone.getValue();
                    }
            }
            if ((dirtyFlags & 0x184L) != 0) {

                    if (vM != null) {
                        // read VM.addressCompany
                        vMAddressCompany = vM.getAddressCompany();
                    }
                    updateLiveDataRegistration(2, vMAddressCompany);


                    if (vMAddressCompany != null) {
                        // read VM.addressCompany.getValue()
                        vMAddressCompanyGetValue = vMAddressCompany.getValue();
                    }
            }
            if ((dirtyFlags & 0x188L) != 0) {

                    if (vM != null) {
                        // read VM.message
                        vMMessage = vM.getMessage();
                    }
                    updateLiveDataRegistration(3, vMMessage);


                    if (vMMessage != null) {
                        // read VM.message.getValue()
                        vMMessageGetValue = vMMessage.getValue();
                    }
            }
            if ((dirtyFlags & 0x190L) != 0) {

                    if (vM != null) {
                        // read VM.emailCompany
                        vMEmailCompany = vM.getEmailCompany();
                    }
                    updateLiveDataRegistration(4, vMEmailCompany);


                    if (vMEmailCompany != null) {
                        // read VM.emailCompany.getValue()
                        vMEmailCompanyGetValue = vMEmailCompany.getValue();
                    }
            }
            if ((dirtyFlags & 0x1a0L) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(5, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0x1a0L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (vM != null) {
                        // read VM.email
                        vMEmail = vM.getEmail();
                    }
                    updateLiveDataRegistration(6, vMEmail);


                    if (vMEmail != null) {
                        // read VM.email.getValue()
                        vMEmailGetValue = vMEmail.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x182L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.PhoneEt, vMPhoneGetValue);
        }
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.PhoneEt, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, PhoneEtandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.address, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, addressandroidTextAttrChanged);
            this.button8.setOnClickListener(mCallback32);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.email, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, emailandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.emailEt, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, emailEtandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.messageEt, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, messageEtandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.phone, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, phoneandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x184L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.address, vMAddressCompanyGetValue);
        }
        if ((dirtyFlags & 0x190L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.email, vMEmailCompanyGetValue);
        }
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.emailEt, vMEmailGetValue);
        }
        if ((dirtyFlags & 0x188L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.messageEt, vMMessageGetValue);
        }
        if ((dirtyFlags & 0x1a0L) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.phone, vMPhoneCompanyGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // VM
        com.shoohna.gofresh.ui.home.ui.contactUs.ContactUsViewModel vM = mVM;
        // VM != null
        boolean vMJavaLangObjectNull = false;



        vMJavaLangObjectNull = (vM) != (null);
        if (vMJavaLangObjectNull) {






            vM.sendData(callbackArg_0, emailLayout, phoneLayout, messageLayout);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.phoneCompany
        flag 1 (0x2L): VM.phone
        flag 2 (0x3L): VM.addressCompany
        flag 3 (0x4L): VM.message
        flag 4 (0x5L): VM.emailCompany
        flag 5 (0x6L): VM.loader
        flag 6 (0x7L): VM.email
        flag 7 (0x8L): VM
        flag 8 (0x9L): null
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}