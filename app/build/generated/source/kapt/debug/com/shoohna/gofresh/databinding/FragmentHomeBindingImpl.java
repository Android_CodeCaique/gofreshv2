package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentHomeBindingImpl extends FragmentHomeBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(30);
        sIncludes.setIncludes(0, 
            new String[] {"bottom_sheet_layout"},
            new int[] {19},
            new int[] {com.shoohna.gofresh.R.layout.bottom_sheet_layout});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView7, 20);
        sViewsWithIds.put(R.id.circleImageView, 21);
        sViewsWithIds.put(R.id.messangerImgId, 22);
        sViewsWithIds.put(R.id.notificationImgId, 23);
        sViewsWithIds.put(R.id.scrollView2, 24);
        sViewsWithIds.put(R.id.searchEditTextID, 25);
        sViewsWithIds.put(R.id.categorieSpinner, 26);
        sViewsWithIds.put(R.id.filterImgId, 27);
        sViewsWithIds.put(R.id.categorieRecyclerViewId, 28);
        sViewsWithIds.put(R.id.drawerIconId, 29);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    @Nullable
    private final com.shoohna.gofresh.databinding.BottomSheetLayoutBinding mboundView01;
    @NonNull
    private final android.widget.TextView mboundView11;
    @NonNull
    private final android.widget.TextView mboundView14;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    private final android.widget.TextView mboundView5;
    @NonNull
    private final android.widget.TextView mboundView7;
    @NonNull
    private final android.widget.TextView mboundView8;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback27;
    @Nullable
    private final android.view.View.OnClickListener mCallback28;
    @Nullable
    private final android.view.View.OnClickListener mCallback29;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentHomeBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 30, sIncludes, sViewsWithIds));
    }
    private FragmentHomeBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (androidx.recyclerview.widget.RecyclerView) bindings[10]
            , (android.widget.TextView) bindings[9]
            , (androidx.cardview.widget.CardView) bindings[4]
            , (androidx.cardview.widget.CardView) bindings[2]
            , (androidx.cardview.widget.CardView) bindings[6]
            , (androidx.recyclerview.widget.RecyclerView) bindings[28]
            , (android.widget.Spinner) bindings[26]
            , (androidx.recyclerview.widget.RecyclerView) bindings[17]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[21]
            , (android.widget.ImageView) bindings[29]
            , (android.widget.ImageView) bindings[27]
            , (android.widget.TextView) bindings[12]
            , (androidx.recyclerview.widget.RecyclerView) bindings[13]
            , (com.smarteist.autoimageslider.SliderView) bindings[1]
            , (android.widget.ImageView) bindings[22]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[18]
            , (android.widget.ImageView) bindings[23]
            , (androidx.recyclerview.widget.RecyclerView) bindings[16]
            , (android.widget.TextView) bindings[15]
            , (android.widget.ScrollView) bindings[24]
            , (android.widget.EditText) bindings[25]
            , (android.widget.TextView) bindings[20]
            );
        this.bestSaleRecyclerViewId.setTag(null);
        this.bestSaleTxtViewId.setTag(null);
        this.cardView5.setTag(null);
        this.cardView7.setTag(null);
        this.cardView8.setTag(null);
        this.categoryRecyclerViewId.setTag(null);
        this.highRatedTxtViewId.setTag(null);
        this.highRelatedRecyclerViewId.setTag(null);
        this.imageSlider.setTag(null);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView01 = (com.shoohna.gofresh.databinding.BottomSheetLayoutBinding) bindings[19];
        setContainedBinding(this.mboundView01);
        this.mboundView11 = (android.widget.TextView) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView14 = (android.widget.TextView) bindings[14];
        this.mboundView14.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView5 = (android.widget.TextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView7 = (android.widget.TextView) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (android.widget.TextView) bindings[8];
        this.mboundView8.setTag(null);
        this.mkLoaderId.setTag(null);
        this.offersRecyclerViewId.setTag(null);
        this.offersTxtViewId.setTag(null);
        setRootTag(root);
        // listeners
        mCallback27 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        mCallback28 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 2);
        mCallback29 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        mboundView01.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (mboundView01.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        mboundView01.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMSelectedCategory((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeVMCategory((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMSelectedCategory(androidx.lifecycle.MutableLiveData<java.lang.Integer> VMSelectedCategory, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMCategory(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMCategory, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int vMSelectedCategoryInt3MboundView7AndroidColorColorAccentMboundView7AndroidColorGrey4 = 0;
        int vMSelectedCategoryInt1MboundView3AndroidColorColorAccentMboundView3AndroidColorGrey4 = 0;
        java.lang.Boolean vMLoaderGetValue = null;
        boolean vMSelectedCategoryInt2 = false;
        int vMSelectedCategoryInt2MboundView5AndroidColorColorAccentMboundView5AndroidColorGrey4 = 0;
        com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
        boolean vMSelectedCategoryInt3 = false;
        int androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vMSelectedCategory = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        int vMCategoryViewVISIBLEViewGONE = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMCategoryGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMCategory = null;
        java.lang.Integer vMSelectedCategoryGetValue = null;
        boolean vMSelectedCategoryInt1 = false;
        java.lang.Boolean vMCategoryGetValue = null;
        int vMCategoryViewGONEViewVISIBLE = 0;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (vM != null) {
                        // read VM.selectedCategory
                        vMSelectedCategory = vM.getSelectedCategory();
                    }
                    updateLiveDataRegistration(0, vMSelectedCategory);


                    if (vMSelectedCategory != null) {
                        // read VM.selectedCategory.getValue()
                        vMSelectedCategoryGetValue = vMSelectedCategory.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMSelectedCategoryGetValue);


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2
                    vMSelectedCategoryInt2 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue) == (2);
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 3
                    vMSelectedCategoryInt3 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue) == (3);
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1
                    vMSelectedCategoryInt1 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue) == (1);
                if((dirtyFlags & 0x19L) != 0) {
                    if(vMSelectedCategoryInt2) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }
                if((dirtyFlags & 0x19L) != 0) {
                    if(vMSelectedCategoryInt3) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }
                if((dirtyFlags & 0x19L) != 0) {
                    if(vMSelectedCategoryInt1) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/grey4
                    vMSelectedCategoryInt2MboundView5AndroidColorColorAccentMboundView5AndroidColorGrey4 = ((vMSelectedCategoryInt2) ? (getColorFromResource(mboundView5, R.color.colorAccent)) : (getColorFromResource(mboundView5, R.color.grey4)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 3 ? @android:color/colorAccent : @android:color/grey4
                    vMSelectedCategoryInt3MboundView7AndroidColorColorAccentMboundView7AndroidColorGrey4 = ((vMSelectedCategoryInt3) ? (getColorFromResource(mboundView7, R.color.colorAccent)) : (getColorFromResource(mboundView7, R.color.grey4)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/grey4
                    vMSelectedCategoryInt1MboundView3AndroidColorColorAccentMboundView3AndroidColorGrey4 = ((vMSelectedCategoryInt1) ? (getColorFromResource(mboundView3, R.color.colorAccent)) : (getColorFromResource(mboundView3, R.color.grey4)));
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(1, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0x1aL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (vM != null) {
                        // read VM.category
                        vMCategory = vM.getCategory();
                    }
                    updateLiveDataRegistration(2, vMCategory);


                    if (vMCategory != null) {
                        // read VM.category.getValue()
                        vMCategoryGetValue = vMCategory.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.category.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMCategoryGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMCategoryGetValue);
                if((dirtyFlags & 0x1cL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMCategoryGetValue) {
                            dirtyFlags |= 0x4000L;
                            dirtyFlags |= 0x10000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                            dirtyFlags |= 0x8000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.category.getValue()) ? View.VISIBLE : View.GONE
                    vMCategoryViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMCategoryGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.category.getValue()) ? View.GONE : View.VISIBLE
                    vMCategoryViewGONEViewVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxVMCategoryGetValue) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            this.bestSaleRecyclerViewId.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.bestSaleTxtViewId.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.categoryRecyclerViewId.setVisibility(vMCategoryViewVISIBLEViewGONE);
            this.highRatedTxtViewId.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.highRelatedRecyclerViewId.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.imageSlider.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.mboundView11.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.mboundView14.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.mboundView8.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.offersRecyclerViewId.setVisibility(vMCategoryViewGONEViewVISIBLE);
            this.offersTxtViewId.setVisibility(vMCategoryViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.cardView5.setOnClickListener(mCallback28);
            this.cardView7.setOnClickListener(mCallback27);
            this.cardView8.setOnClickListener(mCallback29);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView3, androidx.databinding.adapters.Converters.convertColorToDrawable(vMSelectedCategoryInt1MboundView3AndroidColorColorAccentMboundView3AndroidColorGrey4));
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView5, androidx.databinding.adapters.Converters.convertColorToDrawable(vMSelectedCategoryInt2MboundView5AndroidColorColorAccentMboundView5AndroidColorGrey4));
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView7, androidx.databinding.adapters.Converters.convertColorToDrawable(vMSelectedCategoryInt3MboundView7AndroidColorColorAccentMboundView7AndroidColorGrey4));
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
        executeBindingsOn(mboundView01);
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setSelectedVegtables(callbackArg_0);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setSelectedLeafs(callbackArg_0);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setSelectedFruits(callbackArg_0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.selectedCategory
        flag 1 (0x2L): VM.loader
        flag 2 (0x3L): VM.category
        flag 3 (0x4L): VM
        flag 4 (0x5L): null
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 3 ? @android:color/colorAccent : @android:color/grey4
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 3 ? @android:color/colorAccent : @android:color/grey4
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/grey4
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/grey4
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/grey4
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/grey4
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 12 (0xdL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 13 (0xeL): androidx.databinding.ViewDataBinding.safeUnbox(VM.category.getValue()) ? View.VISIBLE : View.GONE
        flag 14 (0xfL): androidx.databinding.ViewDataBinding.safeUnbox(VM.category.getValue()) ? View.VISIBLE : View.GONE
        flag 15 (0x10L): androidx.databinding.ViewDataBinding.safeUnbox(VM.category.getValue()) ? View.GONE : View.VISIBLE
        flag 16 (0x11L): androidx.databinding.ViewDataBinding.safeUnbox(VM.category.getValue()) ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}