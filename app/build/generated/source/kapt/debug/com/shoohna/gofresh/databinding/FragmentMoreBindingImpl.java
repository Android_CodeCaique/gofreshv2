package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMoreBindingImpl extends FragmentMoreBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView23, 3);
        sViewsWithIds.put(R.id.cardView1Id, 4);
        sViewsWithIds.put(R.id.circleImageView, 5);
        sViewsWithIds.put(R.id.nameTxtId, 6);
        sViewsWithIds.put(R.id.emailTxtId, 7);
        sViewsWithIds.put(R.id.editConstraintId, 8);
        sViewsWithIds.put(R.id.cardView2Id, 9);
        sViewsWithIds.put(R.id.settingConstraintId, 10);
        sViewsWithIds.put(R.id.imageView18, 11);
        sViewsWithIds.put(R.id.textView31, 12);
        sViewsWithIds.put(R.id.walletConstraintId, 13);
        sViewsWithIds.put(R.id.imageView18112, 14);
        sViewsWithIds.put(R.id.textView31121, 15);
        sViewsWithIds.put(R.id.aboutUsConstraintId, 16);
        sViewsWithIds.put(R.id.imageView181, 17);
        sViewsWithIds.put(R.id.textView311, 18);
        sViewsWithIds.put(R.id.familyConstraintId, 19);
        sViewsWithIds.put(R.id.imageVie2, 20);
        sViewsWithIds.put(R.id.textVie, 21);
        sViewsWithIds.put(R.id.contactUsConstraintId, 22);
        sViewsWithIds.put(R.id.imageView1811, 23);
        sViewsWithIds.put(R.id.textView3111, 24);
        sViewsWithIds.put(R.id.helpConstraintId, 25);
        sViewsWithIds.put(R.id.imageView18111, 26);
        sViewsWithIds.put(R.id.textView31111, 27);
        sViewsWithIds.put(R.id.imageLogout, 28);
        sViewsWithIds.put(R.id.logout, 29);
        sViewsWithIds.put(R.id.logoutViewId, 30);
        sViewsWithIds.put(R.id.imageLogIn, 31);
        sViewsWithIds.put(R.id.login, 32);
        sViewsWithIds.put(R.id.loginViewId, 33);
        sViewsWithIds.put(R.id.back, 34);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback23;
    @Nullable
    private final android.view.View.OnClickListener mCallback24;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMoreBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 35, sIncludes, sViewsWithIds));
    }
    private FragmentMoreBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[16]
            , (android.widget.ImageView) bindings[34]
            , (androidx.cardview.widget.CardView) bindings[4]
            , (androidx.cardview.widget.CardView) bindings[9]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[22]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[7]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[19]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[25]
            , (android.widget.ImageView) bindings[31]
            , (android.widget.ImageView) bindings[28]
            , (android.widget.ImageView) bindings[20]
            , (android.widget.ImageView) bindings[11]
            , (android.widget.ImageView) bindings[17]
            , (android.widget.ImageView) bindings[23]
            , (android.widget.ImageView) bindings[26]
            , (android.widget.ImageView) bindings[14]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[1]
            , (android.widget.TextView) bindings[32]
            , (android.view.View) bindings[33]
            , (android.widget.TextView) bindings[29]
            , (android.view.View) bindings[30]
            , (android.widget.TextView) bindings[6]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[10]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[27]
            , (android.widget.TextView) bindings[15]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[13]
            );
        this.logInConstraintId.setTag(null);
        this.logOutConstraintId.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        mCallback23 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        mCallback24 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.more.MoreViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.more.MoreViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.shoohna.gofresh.ui.home.ui.more.MoreViewModel vM = mVM;
        // batch finished
        if ((dirtyFlags & 0x2L) != 0) {
            // api target 1

            this.logInConstraintId.setOnClickListener(mCallback24);
            this.logOutConstraintId.setOnClickListener(mCallback23);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.more.MoreViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.logout(callbackArg_0);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.more.MoreViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.login(callbackArg_0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}