package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMoreSettingBindingImpl extends FragmentMoreSettingBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.back, 2);
        sViewsWithIds.put(R.id.textView23, 3);
        sViewsWithIds.put(R.id.circleImageView, 4);
        sViewsWithIds.put(R.id.nameTxtId, 5);
        sViewsWithIds.put(R.id.emailTxtId, 6);
        sViewsWithIds.put(R.id.imageView19, 7);
        sViewsWithIds.put(R.id.textView34, 8);
        sViewsWithIds.put(R.id.view3, 9);
        sViewsWithIds.put(R.id.textView31, 10);
        sViewsWithIds.put(R.id.notificationSwitchId, 11);
        sViewsWithIds.put(R.id.textView311, 12);
        sViewsWithIds.put(R.id.messageSwitchId, 13);
        sViewsWithIds.put(R.id.textView3111, 14);
        sViewsWithIds.put(R.id.spinner, 15);
        sViewsWithIds.put(R.id.textView31111, 16);
        sViewsWithIds.put(R.id.spinnerCurrency, 17);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMoreSettingBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentMoreSettingBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.ImageView) bindings[2]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[4]
            , (android.widget.TextView) bindings[6]
            , (android.widget.ImageView) bindings[7]
            , (com.suke.widget.SwitchButton) bindings[13]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[1]
            , (android.widget.TextView) bindings[5]
            , (com.suke.widget.SwitchButton) bindings[11]
            , (com.jaredrummler.materialspinner.MaterialSpinner) bindings[15]
            , (com.jaredrummler.materialspinner.MaterialSpinner) bindings[17]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[8]
            , (android.view.View) bindings[9]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mkLoaderId.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.moreSetting.MoreSettingViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.moreSetting.MoreSettingViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Boolean vMLoaderGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        com.shoohna.gofresh.ui.home.ui.moreSetting.MoreSettingViewModel vM = mVM;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;

        if ((dirtyFlags & 0x7L) != 0) {



                if (vM != null) {
                    // read VM.loader
                    vMLoader = vM.getLoader();
                }
                updateLiveDataRegistration(0, vMLoader);


                if (vMLoader != null) {
                    // read VM.loader.getValue()
                    vMLoaderGetValue = vMLoader.getValue();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
            if((dirtyFlags & 0x7L) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.loader
        flag 1 (0x2L): VM
        flag 2 (0x3L): null
        flag 3 (0x4L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}