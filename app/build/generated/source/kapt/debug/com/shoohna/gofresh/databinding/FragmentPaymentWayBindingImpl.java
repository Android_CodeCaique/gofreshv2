package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentPaymentWayBindingImpl extends FragmentPaymentWayBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView8, 11);
        sViewsWithIds.put(R.id.stepViewId, 12);
        sViewsWithIds.put(R.id.imageView27, 13);
        sViewsWithIds.put(R.id.imageView28, 14);
        sViewsWithIds.put(R.id.imageView29, 15);
        sViewsWithIds.put(R.id.imageView30, 16);
        sViewsWithIds.put(R.id.textView20, 17);
        sViewsWithIds.put(R.id.textView37, 18);
        sViewsWithIds.put(R.id.textView57, 19);
        sViewsWithIds.put(R.id.textView60, 20);
        sViewsWithIds.put(R.id.textView61, 21);
        sViewsWithIds.put(R.id.textView62, 22);
        sViewsWithIds.put(R.id.textView63, 23);
        sViewsWithIds.put(R.id.textView42, 24);
        sViewsWithIds.put(R.id.imageView24, 25);
        sViewsWithIds.put(R.id.imageView241, 26);
        sViewsWithIds.put(R.id.discountLayout, 27);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener codeETandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.discountCode.getValue()
            //         is VM.discountCode.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(codeET);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel vM = mVM;
            // VM.discountCode.getValue()
            java.lang.String vMDiscountCodeGetValue = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.discountCode
            androidx.lifecycle.MutableLiveData<java.lang.String> vMDiscountCode = null;
            // VM.discountCode != null
            boolean vMDiscountCodeJavaLangObjectNull = false;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMDiscountCode = vM.getDiscountCode();

                vMDiscountCodeJavaLangObjectNull = (vMDiscountCode) != (null);
                if (vMDiscountCodeJavaLangObjectNull) {




                    vMDiscountCode.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentPaymentWayBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 28, sIncludes, sViewsWithIds));
    }
    private FragmentPaymentWayBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[2]
            , (android.widget.Button) bindings[9]
            , (com.google.android.material.textfield.TextInputEditText) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[27]
            , (android.widget.ImageView) bindings[3]
            , (android.widget.ImageView) bindings[6]
            , (android.widget.ImageView) bindings[25]
            , (android.widget.ImageView) bindings[26]
            , (android.widget.ImageView) bindings[13]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.ImageView) bindings[15]
            , (android.widget.ImageView) bindings[16]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[10]
            , (android.widget.Button) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[12]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[19]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[22]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[11]
            );
        this.allowPaymentOnDelivery.setTag(null);
        this.allowPaymentOnline.setTag(null);
        this.button3.setTag(null);
        this.codeET.setTag(null);
        this.imageView23.setTag(null);
        this.imageView231.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mkLoaderId.setTag(null);
        this.nextBtnId.setTag(null);
        this.textView50.setTag(null);
        this.textView501.setTag(null);
        setRootTag(root);
        // listeners
        mCallback2 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 2);
        mCallback1 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        mCallback4 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 4);
        mCallback3 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeVMPaymentOnDelivery((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeVMPaymentOnline((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 3 :
                return onChangeVMDiscountCode((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMPaymentOnDelivery(androidx.lifecycle.LiveData<java.lang.Boolean> VMPaymentOnDelivery, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMPaymentOnline(androidx.lifecycle.LiveData<java.lang.Boolean> VMPaymentOnline, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMDiscountCode(androidx.lifecycle.MutableLiveData<java.lang.String> VMDiscountCode, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnDeliveryGetValue = false;
        java.lang.Boolean vMLoaderGetValue = null;
        int vMPaymentOnlineImageView23AndroidColorPinkImageView23AndroidColorGreyDark = 0;
        com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel vM = mVM;
        int vMPaymentOnDeliveryImageView231AndroidColorPinkImageView231AndroidColorGreyDark = 0;
        int vMPaymentOnDeliveryTextView501AndroidColorPinkTextView501AndroidColorGreyDark = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        java.lang.Boolean vMPaymentOnlineGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        java.lang.String vMDiscountCodeGetValue = null;
        androidx.lifecycle.LiveData<java.lang.Boolean> vMPaymentOnDelivery = null;
        androidx.lifecycle.LiveData<java.lang.Boolean> vMPaymentOnline = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMDiscountCode = null;
        java.lang.Boolean vMPaymentOnDeliveryGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnlineGetValue = false;
        int vMPaymentOnlineTextView50AndroidColorPinkTextView50AndroidColorGreyDark = 0;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(0, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0x31L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (vM != null) {
                        // read VM.paymentOnDelivery
                        vMPaymentOnDelivery = vM.getPaymentOnDelivery();
                    }
                    updateLiveDataRegistration(1, vMPaymentOnDelivery);


                    if (vMPaymentOnDelivery != null) {
                        // read VM.paymentOnDelivery.getValue()
                        vMPaymentOnDeliveryGetValue = vMPaymentOnDelivery.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnDelivery.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnDeliveryGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMPaymentOnDeliveryGetValue);
                if((dirtyFlags & 0x32L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnDeliveryGetValue) {
                            dirtyFlags |= 0x200L;
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnDelivery.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMPaymentOnDeliveryImageView231AndroidColorPinkImageView231AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnDeliveryGetValue) ? (getColorFromResource(imageView231, R.color.pink)) : (getColorFromResource(imageView231, R.color.greyDark)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnDelivery.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMPaymentOnDeliveryTextView501AndroidColorPinkTextView501AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnDeliveryGetValue) ? (getColorFromResource(textView501, R.color.pink)) : (getColorFromResource(textView501, R.color.greyDark)));
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (vM != null) {
                        // read VM.paymentOnline
                        vMPaymentOnline = vM.getPaymentOnline();
                    }
                    updateLiveDataRegistration(2, vMPaymentOnline);


                    if (vMPaymentOnline != null) {
                        // read VM.paymentOnline.getValue()
                        vMPaymentOnlineGetValue = vMPaymentOnline.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnline.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnlineGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMPaymentOnlineGetValue);
                if((dirtyFlags & 0x34L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnlineGetValue) {
                            dirtyFlags |= 0x80L;
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                            dirtyFlags |= 0x4000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnline.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMPaymentOnlineImageView23AndroidColorPinkImageView23AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnlineGetValue) ? (getColorFromResource(imageView23, R.color.pink)) : (getColorFromResource(imageView23, R.color.greyDark)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnline.getValue()) ? @android:color/pink : @android:color/greyDark
                    vMPaymentOnlineTextView50AndroidColorPinkTextView50AndroidColorGreyDark = ((androidxDatabindingViewDataBindingSafeUnboxVMPaymentOnlineGetValue) ? (getColorFromResource(textView50, R.color.pink)) : (getColorFromResource(textView50, R.color.greyDark)));
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (vM != null) {
                        // read VM.discountCode
                        vMDiscountCode = vM.getDiscountCode();
                    }
                    updateLiveDataRegistration(3, vMDiscountCode);


                    if (vMDiscountCode != null) {
                        // read VM.discountCode.getValue()
                        vMDiscountCodeGetValue = vMDiscountCode.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.allowPaymentOnDelivery.setOnClickListener(mCallback3);
            this.allowPaymentOnline.setOnClickListener(mCallback2);
            this.button3.setOnClickListener(mCallback4);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.codeET, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, codeETandroidTextAttrChanged);
            this.nextBtnId.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.codeET, vMDiscountCodeGetValue);
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.imageView23.setImageTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(vMPaymentOnlineImageView23AndroidColorPinkImageView23AndroidColorGreyDark));
            }
            // api target 1

            this.textView50.setTextColor(vMPaymentOnlineTextView50AndroidColorPinkTextView50AndroidColorGreyDark);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.imageView231.setImageTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(vMPaymentOnDeliveryImageView231AndroidColorPinkImageView231AndroidColorGreyDark));
            }
            // api target 1

            this.textView501.setTextColor(vMPaymentOnDeliveryTextView501AndroidColorPinkTextView501AndroidColorGreyDark);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.allowPaymentOnline(callbackArg_0);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.sendPaymentMethod(callbackArg_0);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.makeDiscount(callbackArg_0);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.allowPaymentOnDelivery(callbackArg_0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.loader
        flag 1 (0x2L): VM.paymentOnDelivery
        flag 2 (0x3L): VM.paymentOnline
        flag 3 (0x4L): VM.discountCode
        flag 4 (0x5L): VM
        flag 5 (0x6L): null
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnline.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnline.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnDelivery.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnDelivery.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnDelivery.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnDelivery.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 12 (0xdL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 13 (0xeL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 14 (0xfL): androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnline.getValue()) ? @android:color/pink : @android:color/greyDark
        flag 15 (0x10L): androidx.databinding.ViewDataBinding.safeUnbox(VM.paymentOnline.getValue()) ? @android:color/pink : @android:color/greyDark
    flag mapping end*/
    //end
}