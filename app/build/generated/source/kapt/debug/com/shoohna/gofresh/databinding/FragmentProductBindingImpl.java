package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProductBindingImpl extends FragmentProductBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.drawerIconId, 4);
        sViewsWithIds.put(R.id.messangerImgId, 5);
        sViewsWithIds.put(R.id.notificationImgId, 6);
        sViewsWithIds.put(R.id.textView7, 7);
        sViewsWithIds.put(R.id.productTypeTxtView, 8);
        sViewsWithIds.put(R.id.cardView9, 9);
        sViewsWithIds.put(R.id.productViewRecyclerViewId, 10);
        sViewsWithIds.put(R.id.linearLayout, 11);
        sViewsWithIds.put(R.id.searchEditTextID, 12);
        sViewsWithIds.put(R.id.categorieSpinner, 13);
        sViewsWithIds.put(R.id.filterImgId, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    private final android.widget.TextView mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback25;
    @Nullable
    private final android.view.View.OnClickListener mCallback26;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentProductBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentProductBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (androidx.cardview.widget.CardView) bindings[9]
            , (android.widget.Spinner) bindings[13]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.ImageView) bindings[5]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[3]
            , (android.widget.ImageView) bindings[6]
            , (android.widget.TextView) bindings[8]
            , (androidx.recyclerview.widget.RecyclerView) bindings[10]
            , (android.widget.EditText) bindings[12]
            , (android.widget.TextView) bindings[7]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mkLoaderId.setTag(null);
        setRootTag(root);
        // listeners
        mCallback25 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        mCallback26 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.product.ProductViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.product.ProductViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeVMSelectedCategory((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMSelectedCategory(androidx.lifecycle.MutableLiveData<java.lang.Integer> VMSelectedCategory, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int vMSelectedCategoryInt2MboundView2AndroidColorColorAccentMboundView2AndroidColorBlack = 0;
        java.lang.Boolean vMLoaderGetValue = null;
        boolean vMSelectedCategoryInt2 = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        com.shoohna.gofresh.ui.home.ui.product.ProductViewModel vM = mVM;
        int vMSelectedCategoryInt1MboundView1AndroidColorColorAccentMboundView1AndroidColorBlack = 0;
        java.lang.Integer vMSelectedCategoryGetValue = null;
        boolean vMSelectedCategoryInt1 = false;
        int androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue = 0;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vMSelectedCategory = null;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xdL) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(0, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0xdL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (vM != null) {
                        // read VM.selectedCategory
                        vMSelectedCategory = vM.getSelectedCategory();
                    }
                    updateLiveDataRegistration(1, vMSelectedCategory);


                    if (vMSelectedCategory != null) {
                        // read VM.selectedCategory.getValue()
                        vMSelectedCategoryGetValue = vMSelectedCategory.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMSelectedCategoryGetValue);


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2
                    vMSelectedCategoryInt2 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue) == (2);
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1
                    vMSelectedCategoryInt1 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue) == (1);
                if((dirtyFlags & 0xeL) != 0) {
                    if(vMSelectedCategoryInt2) {
                            dirtyFlags |= 0x20L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                    }
                }
                if((dirtyFlags & 0xeL) != 0) {
                    if(vMSelectedCategoryInt1) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/black
                    vMSelectedCategoryInt2MboundView2AndroidColorColorAccentMboundView2AndroidColorBlack = ((vMSelectedCategoryInt2) ? (getColorFromResource(mboundView2, R.color.colorAccent)) : (getColorFromResource(mboundView2, R.color.black)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/black
                    vMSelectedCategoryInt1MboundView1AndroidColorColorAccentMboundView1AndroidColorBlack = ((vMSelectedCategoryInt1) ? (getColorFromResource(mboundView1, R.color.colorAccent)) : (getColorFromResource(mboundView1, R.color.black)));
            }
        }
        // batch finished
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            this.mboundView1.setTextColor(vMSelectedCategoryInt1MboundView1AndroidColorColorAccentMboundView1AndroidColorBlack);
            this.mboundView2.setTextColor(vMSelectedCategoryInt2MboundView2AndroidColorColorAccentMboundView2AndroidColorBlack);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback25);
            this.mboundView2.setOnClickListener(mCallback26);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.product.ProductViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setCategoryRaw(callbackArg_0);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.product.ProductViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setCategoryProcess(callbackArg_0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.loader
        flag 1 (0x2L): VM.selectedCategory
        flag 2 (0x3L): VM
        flag 3 (0x4L): null
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/black
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/black
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/black
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/black
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}