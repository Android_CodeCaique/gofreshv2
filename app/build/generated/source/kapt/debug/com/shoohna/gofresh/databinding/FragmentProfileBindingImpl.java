package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProfileBindingImpl extends FragmentProfileBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView8, 10);
        sViewsWithIds.put(R.id.profileImageId, 11);
        sViewsWithIds.put(R.id.addImageBtnId, 12);
        sViewsWithIds.put(R.id.fName1, 13);
        sViewsWithIds.put(R.id.lName1, 14);
        sViewsWithIds.put(R.id.phone1, 15);
        sViewsWithIds.put(R.id.email1, 16);
        sViewsWithIds.put(R.id.view2, 17);
        sViewsWithIds.put(R.id.editConstraintId, 18);
        sViewsWithIds.put(R.id.editTxtViewId, 19);
        sViewsWithIds.put(R.id.imageView5, 20);
        sViewsWithIds.put(R.id.x, 21);
        sViewsWithIds.put(R.id.imageView14, 22);
        sViewsWithIds.put(R.id.textView25, 23);
        sViewsWithIds.put(R.id.imageView15, 24);
        sViewsWithIds.put(R.id.textView26, 25);
        sViewsWithIds.put(R.id.imageView16, 26);
        sViewsWithIds.put(R.id.textView27, 27);
        sViewsWithIds.put(R.id.constraintLayout4, 28);
        sViewsWithIds.put(R.id.imageView17, 29);
        sViewsWithIds.put(R.id.textView28, 30);
        sViewsWithIds.put(R.id.changePasswordTxtId, 31);
        sViewsWithIds.put(R.id.dontSaveImgId, 32);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback15;
    @Nullable
    private final android.view.View.OnClickListener mCallback16;
    @Nullable
    private final android.view.View.OnClickListener mCallback17;
    @Nullable
    private final android.view.View.OnClickListener mCallback14;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener emailandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.email.getValue()
            //         is VM.email.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(email);
            // localize variables for thread safety
            // VM.email
            androidx.lifecycle.MutableLiveData<java.lang.String> vMEmail = null;
            // VM
            com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.email != null
            boolean vMEmailJavaLangObjectNull = false;
            // VM.email.getValue()
            java.lang.String vMEmailGetValue = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMEmail = vM.getEmail();

                vMEmailJavaLangObjectNull = (vMEmail) != (null);
                if (vMEmailJavaLangObjectNull) {




                    vMEmail.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener fNameandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.fName.getValue()
            //         is VM.fName.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(fName);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;
            // VM.fName.getValue()
            java.lang.String vMFNameGetValue = null;
            // VM.fName != null
            boolean vMFNameJavaLangObjectNull = false;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.fName
            androidx.lifecycle.MutableLiveData<java.lang.String> vMFName = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMFName = vM.getFName();

                vMFNameJavaLangObjectNull = (vMFName) != (null);
                if (vMFNameJavaLangObjectNull) {




                    vMFName.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener lNameandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.lName.getValue()
            //         is VM.lName.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(lName);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;
            // VM.lName
            androidx.lifecycle.MutableLiveData<java.lang.String> vMLName = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.lName != null
            boolean vMLNameJavaLangObjectNull = false;
            // VM.lName.getValue()
            java.lang.String vMLNameGetValue = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMLName = vM.getLName();

                vMLNameJavaLangObjectNull = (vMLName) != (null);
                if (vMLNameJavaLangObjectNull) {




                    vMLName.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener phoneandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.phone.getValue()
            //         is VM.phone.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(phone);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;
            // VM.phone.getValue()
            java.lang.String vMPhoneGetValue = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.phone != null
            boolean vMPhoneJavaLangObjectNull = false;
            // VM.phone
            androidx.lifecycle.MutableLiveData<java.lang.String> vMPhone = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMPhone = vM.getPhone();

                vMPhoneJavaLangObjectNull = (vMPhone) != (null);
                if (vMPhoneJavaLangObjectNull) {




                    vMPhone.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 33, sIncludes, sViewsWithIds));
    }
    private FragmentProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 5
            , (android.widget.ImageView) bindings[12]
            , (android.widget.TextView) bindings[31]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[28]
            , (android.widget.ImageView) bindings[8]
            , (android.widget.ImageView) bindings[32]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[18]
            , (android.widget.TextView) bindings[19]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (com.google.android.material.textfield.TextInputLayout) bindings[16]
            , (com.google.android.material.textfield.TextInputEditText) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (android.widget.ImageView) bindings[22]
            , (android.widget.ImageView) bindings[24]
            , (android.widget.ImageView) bindings[26]
            , (android.widget.ImageView) bindings[29]
            , (android.widget.ImageView) bindings[20]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (com.google.android.material.textfield.TextInputLayout) bindings[14]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[9]
            , (com.google.android.material.textfield.TextInputEditText) bindings[3]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[11]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[25]
            , (android.widget.TextView) bindings[27]
            , (android.widget.TextView) bindings[30]
            , (android.widget.TextView) bindings[10]
            , (android.view.View) bindings[17]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[21]
            );
        this.constraintLayout.setTag(null);
        this.constraintLayout2.setTag(null);
        this.constraintLayout3.setTag(null);
        this.correctImgId.setTag(null);
        this.email.setTag(null);
        this.fName.setTag(null);
        this.lName.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mkLoaderId.setTag(null);
        this.phone.setTag(null);
        setRootTag(root);
        // listeners
        mCallback15 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 2);
        mCallback16 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 3);
        mCallback17 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 4);
        mCallback14 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x40L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x20L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMFName((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeVMPhone((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 3 :
                return onChangeVMEmail((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeVMLName((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMFName(androidx.lifecycle.MutableLiveData<java.lang.String> VMFName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMPhone(androidx.lifecycle.MutableLiveData<java.lang.String> VMPhone, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMEmail(androidx.lifecycle.MutableLiveData<java.lang.String> VMEmail, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLName(androidx.lifecycle.MutableLiveData<java.lang.String> VMLName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Boolean vMLoaderGetValue = null;
        java.lang.String vMLNameGetValue = null;
        com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMFName = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMPhone = null;
        java.lang.String vMFNameGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMEmail = null;
        java.lang.String vMPhoneGetValue = null;
        java.lang.String vMEmailGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMLName = null;

        if ((dirtyFlags & 0x7fL) != 0) {


            if ((dirtyFlags & 0x61L) != 0) {

                    if (vM != null) {
                        // read VM.fName
                        vMFName = vM.getFName();
                    }
                    updateLiveDataRegistration(0, vMFName);


                    if (vMFName != null) {
                        // read VM.fName.getValue()
                        vMFNameGetValue = vMFName.getValue();
                    }
            }
            if ((dirtyFlags & 0x62L) != 0) {

                    if (vM != null) {
                        // read VM.phone
                        vMPhone = vM.getPhone();
                    }
                    updateLiveDataRegistration(1, vMPhone);


                    if (vMPhone != null) {
                        // read VM.phone.getValue()
                        vMPhoneGetValue = vMPhone.getValue();
                    }
            }
            if ((dirtyFlags & 0x64L) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(2, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0x64L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x68L) != 0) {

                    if (vM != null) {
                        // read VM.email
                        vMEmail = vM.getEmail();
                    }
                    updateLiveDataRegistration(3, vMEmail);


                    if (vMEmail != null) {
                        // read VM.email.getValue()
                        vMEmailGetValue = vMEmail.getValue();
                    }
            }
            if ((dirtyFlags & 0x70L) != 0) {

                    if (vM != null) {
                        // read VM.lName
                        vMLName = vM.getLName();
                    }
                    updateLiveDataRegistration(4, vMLName);


                    if (vMLName != null) {
                        // read VM.lName.getValue()
                        vMLNameGetValue = vMLName.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x40L) != 0) {
            // api target 1

            this.constraintLayout.setOnClickListener(mCallback15);
            this.constraintLayout2.setOnClickListener(mCallback14);
            this.constraintLayout3.setOnClickListener(mCallback16);
            this.correctImgId.setOnClickListener(mCallback17);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.email, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, emailandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.fName, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, fNameandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.lName, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, lNameandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.phone, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, phoneandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x68L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.email, vMEmailGetValue);
        }
        if ((dirtyFlags & 0x61L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.fName, vMFNameGetValue);
        }
        if ((dirtyFlags & 0x70L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.lName, vMLNameGetValue);
        }
        if ((dirtyFlags & 0x64L) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x62L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.phone, vMPhoneGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.moveToHelp(callbackArg_0);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.moveToOrderStatus(callbackArg_0);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // VM != null
                boolean vMJavaLangObjectNull = false;
                // VM
                com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {














                    vM.updateProfile(callbackArg_0, fName, lName, phone, email, changePasswordTxtId, editConstraintId, correctImgId, dontSaveImgId, addImageBtnId, x, profileImageId);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.profile.ProfileFragmentViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.share(callbackArg_0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.fName
        flag 1 (0x2L): VM.phone
        flag 2 (0x3L): VM.loader
        flag 3 (0x4L): VM.email
        flag 4 (0x5L): VM.lName
        flag 5 (0x6L): VM
        flag 6 (0x7L): null
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}