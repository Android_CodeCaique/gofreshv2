package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentRecietBindingImpl extends FragmentRecietBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView8, 6);
        sViewsWithIds.put(R.id.stepViewId, 7);
        sViewsWithIds.put(R.id.imageView27, 8);
        sViewsWithIds.put(R.id.imageView28, 9);
        sViewsWithIds.put(R.id.imageView29, 10);
        sViewsWithIds.put(R.id.imageView30, 11);
        sViewsWithIds.put(R.id.textView20, 12);
        sViewsWithIds.put(R.id.textView37, 13);
        sViewsWithIds.put(R.id.textView57, 14);
        sViewsWithIds.put(R.id.textView60, 15);
        sViewsWithIds.put(R.id.textView61, 16);
        sViewsWithIds.put(R.id.textView62, 17);
        sViewsWithIds.put(R.id.textView63, 18);
        sViewsWithIds.put(R.id.textView42, 19);
        sViewsWithIds.put(R.id.textView51, 20);
        sViewsWithIds.put(R.id.textView52, 21);
        sViewsWithIds.put(R.id.textView54, 22);
        sViewsWithIds.put(R.id.textView55, 23);
        sViewsWithIds.put(R.id.textView45, 24);
        sViewsWithIds.put(R.id.textView46, 25);
        sViewsWithIds.put(R.id.recietRecyclerViewId, 26);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback9;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentRecietBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 27, sIncludes, sViewsWithIds));
    }
    private FragmentRecietBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (android.widget.Button) bindings[5]
            , (android.widget.ImageView) bindings[8]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.ImageView) bindings[10]
            , (android.widget.ImageView) bindings[11]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[4]
            , (androidx.recyclerview.widget.RecyclerView) bindings[26]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[19]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[25]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[22]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[6]
            );
        this.confirmOrderBtnId.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mkLoaderId.setTag(null);
        this.textView43.setTag(null);
        this.textView44.setTag(null);
        this.textView53.setTag(null);
        setRootTag(root);
        // listeners
        mCallback9 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.checkOutProcess.reciet.RecietViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.checkOutProcess.reciet.RecietViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMTotalPrice((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeVMShopId((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeVMShippingPrice((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMTotalPrice(androidx.lifecycle.MutableLiveData<java.lang.String> VMTotalPrice, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMShopId(androidx.lifecycle.MutableLiveData<java.lang.String> VMShopId, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMShippingPrice(androidx.lifecycle.MutableLiveData<java.lang.String> VMShippingPrice, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.String> vMTotalPrice = null;
        java.lang.Boolean vMLoaderGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        java.lang.String vMShippingPriceGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMShopId = null;
        com.shoohna.gofresh.ui.home.ui.checkOutProcess.reciet.RecietViewModel vM = mVM;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMShippingPrice = null;
        java.lang.String stringValueOfVMShippingPrice = null;
        java.lang.String stringValueOfVMTotalPrice = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        int vMLoaderViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfVMShopId = null;
        java.lang.String vMShopIdGetValue = null;
        java.lang.String vMTotalPriceGetValue = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (vM != null) {
                        // read VM.total_price
                        vMTotalPrice = vM.getTotal_price();
                    }
                    updateLiveDataRegistration(0, vMTotalPrice);


                    if (vMTotalPrice != null) {
                        // read VM.total_price.getValue()
                        vMTotalPriceGetValue = vMTotalPrice.getValue();
                    }


                    // read String.valueOf(VM.total_price.getValue())
                    stringValueOfVMTotalPrice = java.lang.String.valueOf(vMTotalPriceGetValue);
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(1, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0x32L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
                    vMLoaderViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (vM != null) {
                        // read VM.shopId
                        vMShopId = vM.getShopId();
                    }
                    updateLiveDataRegistration(2, vMShopId);


                    if (vMShopId != null) {
                        // read VM.shopId.getValue()
                        vMShopIdGetValue = vMShopId.getValue();
                    }


                    // read String.valueOf(VM.shopId.getValue())
                    stringValueOfVMShopId = java.lang.String.valueOf(vMShopIdGetValue);
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (vM != null) {
                        // read VM.shipping_price
                        vMShippingPrice = vM.getShipping_price();
                    }
                    updateLiveDataRegistration(3, vMShippingPrice);


                    if (vMShippingPrice != null) {
                        // read VM.shipping_price.getValue()
                        vMShippingPriceGetValue = vMShippingPrice.getValue();
                    }


                    // read String.valueOf(VM.shipping_price.getValue())
                    stringValueOfVMShippingPrice = java.lang.String.valueOf(vMShippingPriceGetValue);
            }
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.confirmOrderBtnId.setOnClickListener(mCallback9);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView43, stringValueOfVMShippingPrice);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView44, stringValueOfVMTotalPrice);
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView53, stringValueOfVMShopId);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // VM
        com.shoohna.gofresh.ui.home.ui.checkOutProcess.reciet.RecietViewModel vM = mVM;
        // VM != null
        boolean vMJavaLangObjectNull = false;



        vMJavaLangObjectNull = (vM) != (null);
        if (vMJavaLangObjectNull) {



            vM.confirmOrder(callbackArg_0);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.total_price
        flag 1 (0x2L): VM.loader
        flag 2 (0x3L): VM.shopId
        flag 3 (0x4L): VM.shipping_price
        flag 4 (0x5L): VM
        flag 5 (0x6L): null
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}