package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentRegisterBindingImpl extends FragmentRegisterBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.constraintLayout2, 9);
        sViewsWithIds.put(R.id.textView92, 10);
        sViewsWithIds.put(R.id.cardView4, 11);
        sViewsWithIds.put(R.id.fNameLayout, 12);
        sViewsWithIds.put(R.id.lNameLayout, 13);
        sViewsWithIds.put(R.id.emailLayout, 14);
        sViewsWithIds.put(R.id.phoneLayout, 15);
        sViewsWithIds.put(R.id.passwordLayout, 16);
        sViewsWithIds.put(R.id.confirmPasswordLayout, 17);
        sViewsWithIds.put(R.id.registerGoogle, 18);
        sViewsWithIds.put(R.id.skipTxtViewId, 19);
        sViewsWithIds.put(R.id.textView, 20);
        sViewsWithIds.put(R.id.signInTxtViewId, 21);
        sViewsWithIds.put(R.id.registerFB, 22);
        sViewsWithIds.put(R.id.imageView, 23);
        sViewsWithIds.put(R.id.back, 24);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView1;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView3;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView4;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView5;
    @NonNull
    private final com.google.android.material.textfield.TextInputEditText mboundView6;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback30;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener lNameandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.lName.getValue()
            //         is VM.lName.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(lName);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel vM = mVM;
            // VM.lName
            androidx.lifecycle.MutableLiveData<java.lang.String> vMLName = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.lName != null
            boolean vMLNameJavaLangObjectNull = false;
            // VM.lName.getValue()
            java.lang.String vMLNameGetValue = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMLName = vM.getLName();

                vMLNameJavaLangObjectNull = (vMLName) != (null);
                if (vMLNameJavaLangObjectNull) {




                    vMLName.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView1androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.fName.getValue()
            //         is VM.fName.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView1);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel vM = mVM;
            // VM.fName.getValue()
            java.lang.String vMFNameGetValue = null;
            // VM.fName != null
            boolean vMFNameJavaLangObjectNull = false;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.fName
            androidx.lifecycle.MutableLiveData<java.lang.String> vMFName = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMFName = vM.getFName();

                vMFNameJavaLangObjectNull = (vMFName) != (null);
                if (vMFNameJavaLangObjectNull) {




                    vMFName.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.email.getValue()
            //         is VM.email.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // VM.email
            androidx.lifecycle.MutableLiveData<java.lang.String> vMEmail = null;
            // VM
            com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel vM = mVM;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.email != null
            boolean vMEmailJavaLangObjectNull = false;
            // VM.email.getValue()
            java.lang.String vMEmailGetValue = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMEmail = vM.getEmail();

                vMEmailJavaLangObjectNull = (vMEmail) != (null);
                if (vMEmailJavaLangObjectNull) {




                    vMEmail.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.phone.getValue()
            //         is VM.phone.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // VM
            com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel vM = mVM;
            // VM.phone.getValue()
            java.lang.String vMPhoneGetValue = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.phone != null
            boolean vMPhoneJavaLangObjectNull = false;
            // VM.phone
            androidx.lifecycle.MutableLiveData<java.lang.String> vMPhone = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMPhone = vM.getPhone();

                vMPhoneJavaLangObjectNull = (vMPhone) != (null);
                if (vMPhoneJavaLangObjectNull) {




                    vMPhone.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView5androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.password.getValue()
            //         is VM.password.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView5);
            // localize variables for thread safety
            // VM.password != null
            boolean vMPasswordJavaLangObjectNull = false;
            // VM
            com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel vM = mVM;
            // VM.password.getValue()
            java.lang.String vMPasswordGetValue = null;
            // VM.password
            androidx.lifecycle.MutableLiveData<java.lang.String> vMPassword = null;
            // VM != null
            boolean vMJavaLangObjectNull = false;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMPassword = vM.getPassword();

                vMPasswordJavaLangObjectNull = (vMPassword) != (null);
                if (vMPasswordJavaLangObjectNull) {




                    vMPassword.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView6androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of VM.confirmPasssword.getValue()
            //         is VM.confirmPasssword.setValue((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView6);
            // localize variables for thread safety
            // VM.confirmPasssword != null
            boolean vMConfirmPassswordJavaLangObjectNull = false;
            // VM
            com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel vM = mVM;
            // VM != null
            boolean vMJavaLangObjectNull = false;
            // VM.confirmPasssword.getValue()
            java.lang.String vMConfirmPassswordGetValue = null;
            // VM.confirmPasssword
            androidx.lifecycle.MutableLiveData<java.lang.String> vMConfirmPasssword = null;



            vMJavaLangObjectNull = (vM) != (null);
            if (vMJavaLangObjectNull) {


                vMConfirmPasssword = vM.getConfirmPasssword();

                vMConfirmPassswordJavaLangObjectNull = (vMConfirmPasssword) != (null);
                if (vMConfirmPassswordJavaLangObjectNull) {




                    vMConfirmPasssword.setValue(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentRegisterBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 25, sIncludes, sViewsWithIds));
    }
    private FragmentRegisterBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (android.widget.ImageView) bindings[24]
            , (android.widget.Button) bindings[7]
            , (androidx.cardview.widget.CardView) bindings[11]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (android.widget.ImageView) bindings[9]
            , (com.google.android.material.textfield.TextInputLayout) bindings[14]
            , (com.google.android.material.textfield.TextInputLayout) bindings[12]
            , (android.widget.ImageView) bindings[23]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (com.tuyenmonkey.mkloader.MKLoader) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[16]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (android.widget.ImageView) bindings[22]
            , (android.widget.ImageView) bindings[18]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[19]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[10]
            );
        this.button2.setTag(null);
        this.lName.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (com.google.android.material.textfield.TextInputEditText) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView3 = (com.google.android.material.textfield.TextInputEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (com.google.android.material.textfield.TextInputEditText) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (com.google.android.material.textfield.TextInputEditText) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (com.google.android.material.textfield.TextInputEditText) bindings[6];
        this.mboundView6.setTag(null);
        this.mkLoaderId.setTag(null);
        setRootTag(root);
        // listeners
        mCallback30 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMFName((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeVMPhone((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeVMPassword((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeVMConfirmPasssword((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeVMLoader((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 5 :
                return onChangeVMEmail((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeVMLName((androidx.lifecycle.MutableLiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMFName(androidx.lifecycle.MutableLiveData<java.lang.String> VMFName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMPhone(androidx.lifecycle.MutableLiveData<java.lang.String> VMPhone, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMPassword(androidx.lifecycle.MutableLiveData<java.lang.String> VMPassword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMConfirmPasssword(androidx.lifecycle.MutableLiveData<java.lang.String> VMConfirmPasssword, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLoader(androidx.lifecycle.MutableLiveData<java.lang.Boolean> VMLoader, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMEmail(androidx.lifecycle.MutableLiveData<java.lang.String> VMEmail, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeVMLName(androidx.lifecycle.MutableLiveData<java.lang.String> VMLName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Boolean vMLoaderGetValue = null;
        int vMLoaderViewVISIBLEViewINVISIBLE = 0;
        java.lang.String vMLNameGetValue = null;
        com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel vM = mVM;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMFName = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMPhone = null;
        java.lang.String vMFNameGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMPassword = null;
        java.lang.String vMConfirmPassswordGetValue = null;
        java.lang.String vMPasswordGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMConfirmPasssword = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> vMLoader = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMEmail = null;
        java.lang.String vMPhoneGetValue = null;
        java.lang.String vMEmailGetValue = null;
        androidx.lifecycle.MutableLiveData<java.lang.String> vMLName = null;

        if ((dirtyFlags & 0x1ffL) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (vM != null) {
                        // read VM.fName
                        vMFName = vM.getFName();
                    }
                    updateLiveDataRegistration(0, vMFName);


                    if (vMFName != null) {
                        // read VM.fName.getValue()
                        vMFNameGetValue = vMFName.getValue();
                    }
            }
            if ((dirtyFlags & 0x182L) != 0) {

                    if (vM != null) {
                        // read VM.phone
                        vMPhone = vM.getPhone();
                    }
                    updateLiveDataRegistration(1, vMPhone);


                    if (vMPhone != null) {
                        // read VM.phone.getValue()
                        vMPhoneGetValue = vMPhone.getValue();
                    }
            }
            if ((dirtyFlags & 0x184L) != 0) {

                    if (vM != null) {
                        // read VM.password
                        vMPassword = vM.getPassword();
                    }
                    updateLiveDataRegistration(2, vMPassword);


                    if (vMPassword != null) {
                        // read VM.password.getValue()
                        vMPasswordGetValue = vMPassword.getValue();
                    }
            }
            if ((dirtyFlags & 0x188L) != 0) {

                    if (vM != null) {
                        // read VM.confirmPasssword
                        vMConfirmPasssword = vM.getConfirmPasssword();
                    }
                    updateLiveDataRegistration(3, vMConfirmPasssword);


                    if (vMConfirmPasssword != null) {
                        // read VM.confirmPasssword.getValue()
                        vMConfirmPassswordGetValue = vMConfirmPasssword.getValue();
                    }
            }
            if ((dirtyFlags & 0x190L) != 0) {

                    if (vM != null) {
                        // read VM.loader
                        vMLoader = vM.getLoader();
                    }
                    updateLiveDataRegistration(4, vMLoader);


                    if (vMLoader != null) {
                        // read VM.loader.getValue()
                        vMLoaderGetValue = vMLoader.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMLoaderGetValue);
                if((dirtyFlags & 0x190L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.INVISIBLE
                    vMLoaderViewVISIBLEViewINVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxVMLoaderGetValue) ? (android.view.View.VISIBLE) : (android.view.View.INVISIBLE));
            }
            if ((dirtyFlags & 0x1a0L) != 0) {

                    if (vM != null) {
                        // read VM.email
                        vMEmail = vM.getEmail();
                    }
                    updateLiveDataRegistration(5, vMEmail);


                    if (vMEmail != null) {
                        // read VM.email.getValue()
                        vMEmailGetValue = vMEmail.getValue();
                    }
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (vM != null) {
                        // read VM.lName
                        vMLName = vM.getLName();
                    }
                    updateLiveDataRegistration(6, vMLName);


                    if (vMLName != null) {
                        // read VM.lName.getValue()
                        vMLNameGetValue = vMLName.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            this.button2.setOnClickListener(mCallback30);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.lName, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, lNameandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView1, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView1androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView5, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView5androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView6, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView6androidTextAttrChanged);
        }
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.lName, vMLNameGetValue);
        }
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, vMFNameGetValue);
        }
        if ((dirtyFlags & 0x1a0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, vMEmailGetValue);
        }
        if ((dirtyFlags & 0x182L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, vMPhoneGetValue);
        }
        if ((dirtyFlags & 0x184L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, vMPasswordGetValue);
        }
        if ((dirtyFlags & 0x188L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, vMConfirmPassswordGetValue);
        }
        if ((dirtyFlags & 0x190L) != 0) {
            // api target 1

            this.mkLoaderId.setVisibility(vMLoaderViewVISIBLEViewINVISIBLE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // VM
        com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel vM = mVM;
        // VM != null
        boolean vMJavaLangObjectNull = false;



        vMJavaLangObjectNull = (vM) != (null);
        if (vMJavaLangObjectNull) {



            vM.registerTest(callbackArg_0);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.fName
        flag 1 (0x2L): VM.phone
        flag 2 (0x3L): VM.password
        flag 3 (0x4L): VM.confirmPasssword
        flag 4 (0x5L): VM.loader
        flag 5 (0x6L): VM.email
        flag 6 (0x7L): VM.lName
        flag 7 (0x8L): VM
        flag 8 (0x9L): null
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.INVISIBLE
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(VM.loader.getValue()) ? View.VISIBLE : View.INVISIBLE
    flag mapping end*/
    //end
}