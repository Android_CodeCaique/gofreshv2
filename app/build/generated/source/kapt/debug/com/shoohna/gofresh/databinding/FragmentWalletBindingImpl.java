package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentWalletBindingImpl extends FragmentWalletBinding implements com.shoohna.gofresh.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.constraintLayout7, 5);
        sViewsWithIds.put(R.id.back, 6);
        sViewsWithIds.put(R.id.textView35, 7);
        sViewsWithIds.put(R.id.textView3511, 8);
        sViewsWithIds.put(R.id.textView78, 9);
        sViewsWithIds.put(R.id.textView79, 10);
        sViewsWithIds.put(R.id.textView781, 11);
        sViewsWithIds.put(R.id.textView80, 12);
        sViewsWithIds.put(R.id.textView7811, 13);
        sViewsWithIds.put(R.id.textView82, 14);
        sViewsWithIds.put(R.id.textView81, 15);
        sViewsWithIds.put(R.id.back1, 16);
        sViewsWithIds.put(R.id.back2, 17);
        sViewsWithIds.put(R.id.cubiclinechart, 18);
        sViewsWithIds.put(R.id.back11, 19);
        sViewsWithIds.put(R.id.back22, 20);
        sViewsWithIds.put(R.id.walletAnalysisRecyclerViewId, 21);
        sViewsWithIds.put(R.id.textView86, 22);
        sViewsWithIds.put(R.id.textView87, 23);
        sViewsWithIds.put(R.id.textView861, 24);
        sViewsWithIds.put(R.id.textView88, 25);
        sViewsWithIds.put(R.id.textView8611, 26);
        sViewsWithIds.put(R.id.spinner, 27);
        sViewsWithIds.put(R.id.textView89, 28);
        sViewsWithIds.put(R.id.textView90, 29);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.ScrollView mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback21;
    @Nullable
    private final android.view.View.OnClickListener mCallback20;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentWalletBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 30, sIncludes, sViewsWithIds));
    }
    private FragmentWalletBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.ImageView) bindings[6]
            , (android.widget.ImageView) bindings[16]
            , (android.widget.ImageView) bindings[19]
            , (android.widget.ImageView) bindings[17]
            , (android.widget.ImageView) bindings[20]
            , (android.widget.Button) bindings[2]
            , (android.widget.Button) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (com.github.mikephil.charting.charts.BarChart) bindings[18]
            , (com.jaredrummler.materialspinner.MaterialSpinner) bindings[27]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[22]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[26]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[25]
            , (android.widget.TextView) bindings[28]
            , (android.widget.TextView) bindings[29]
            , (androidx.recyclerview.widget.RecyclerView) bindings[21]
            , (androidx.recyclerview.widget.RecyclerView) bindings[3]
            );
        this.button4.setTag(null);
        this.button5.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (android.widget.ScrollView) bindings[4];
        this.mboundView4.setTag(null);
        this.walletRecyclerViewId.setTag(null);
        setRootTag(root);
        // listeners
        mCallback21 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 2);
        mCallback20 = new com.shoohna.gofresh.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.VM == variableId) {
            setVM((com.shoohna.gofresh.ui.home.ui.wallet.WalletViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVM(@Nullable com.shoohna.gofresh.ui.home.ui.wallet.WalletViewModel VM) {
        this.mVM = VM;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.VM);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeVMSelectedCategory((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeVMSelectedCategory(androidx.lifecycle.MutableLiveData<java.lang.Integer> VMSelectedCategory, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int vMSelectedCategoryInt2Button4AndroidColorWhiteButton4AndroidColorWalletSwitch = 0;
        boolean vMSelectedCategoryInt2 = false;
        com.shoohna.gofresh.ui.home.ui.wallet.WalletViewModel vM = mVM;
        int vMSelectedCategoryInt1ViewVISIBLEViewGONE = 0;
        int androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue = 0;
        androidx.lifecycle.MutableLiveData<java.lang.Integer> vMSelectedCategory = null;
        int vMSelectedCategoryInt2ViewVISIBLEViewGONE = 0;
        int vMSelectedCategoryInt2Button4AndroidColorColorAccentButton4AndroidColorWhite = 0;
        int vMSelectedCategoryInt1Button5AndroidColorWhiteButton5AndroidColorWalletSwitch = 0;
        java.lang.Integer vMSelectedCategoryGetValue = null;
        int vMSelectedCategoryInt1Button5AndroidColorColorAccentButton5AndroidColorWhite = 0;
        boolean vMSelectedCategoryInt1 = false;

        if ((dirtyFlags & 0x7L) != 0) {



                if (vM != null) {
                    // read VM.selectedCategory
                    vMSelectedCategory = vM.getSelectedCategory();
                }
                updateLiveDataRegistration(0, vMSelectedCategory);


                if (vMSelectedCategory != null) {
                    // read VM.selectedCategory.getValue()
                    vMSelectedCategoryGetValue = vMSelectedCategory.getValue();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue())
                androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue = androidx.databinding.ViewDataBinding.safeUnbox(vMSelectedCategoryGetValue);


                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2
                vMSelectedCategoryInt2 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue) == (2);
                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1
                vMSelectedCategoryInt1 = (androidxDatabindingViewDataBindingSafeUnboxVMSelectedCategoryGetValue) == (1);
            if((dirtyFlags & 0x7L) != 0) {
                if(vMSelectedCategoryInt2) {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x100L;
                        dirtyFlags |= 0x400L;
                }
                else {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x80L;
                        dirtyFlags |= 0x200L;
                }
            }
            if((dirtyFlags & 0x7L) != 0) {
                if(vMSelectedCategoryInt1) {
                        dirtyFlags |= 0x40L;
                        dirtyFlags |= 0x1000L;
                        dirtyFlags |= 0x4000L;
                }
                else {
                        dirtyFlags |= 0x20L;
                        dirtyFlags |= 0x800L;
                        dirtyFlags |= 0x2000L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/white : @android:color/walletSwitch
                vMSelectedCategoryInt2Button4AndroidColorWhiteButton4AndroidColorWalletSwitch = ((vMSelectedCategoryInt2) ? (getColorFromResource(button4, R.color.white)) : (getColorFromResource(button4, R.color.walletSwitch)));
                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? View.VISIBLE : View.GONE
                vMSelectedCategoryInt2ViewVISIBLEViewGONE = ((vMSelectedCategoryInt2) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/white
                vMSelectedCategoryInt2Button4AndroidColorColorAccentButton4AndroidColorWhite = ((vMSelectedCategoryInt2) ? (getColorFromResource(button4, R.color.colorAccent)) : (getColorFromResource(button4, R.color.white)));
                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? View.VISIBLE : View.GONE
                vMSelectedCategoryInt1ViewVISIBLEViewGONE = ((vMSelectedCategoryInt1) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/white : @android:color/walletSwitch
                vMSelectedCategoryInt1Button5AndroidColorWhiteButton5AndroidColorWalletSwitch = ((vMSelectedCategoryInt1) ? (getColorFromResource(button5, R.color.white)) : (getColorFromResource(button5, R.color.walletSwitch)));
                // read androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/white
                vMSelectedCategoryInt1Button5AndroidColorColorAccentButton5AndroidColorWhite = ((vMSelectedCategoryInt1) ? (getColorFromResource(button5, R.color.colorAccent)) : (getColorFromResource(button5, R.color.white)));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.button4.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(vMSelectedCategoryInt2Button4AndroidColorWhiteButton4AndroidColorWalletSwitch));
                this.button5.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(vMSelectedCategoryInt1Button5AndroidColorWhiteButton5AndroidColorWalletSwitch));
            }
            // api target 1

            this.button4.setTextColor(vMSelectedCategoryInt2Button4AndroidColorColorAccentButton4AndroidColorWhite);
            this.button5.setTextColor(vMSelectedCategoryInt1Button5AndroidColorColorAccentButton5AndroidColorWhite);
            this.mboundView4.setVisibility(vMSelectedCategoryInt2ViewVISIBLEViewGONE);
            this.walletRecyclerViewId.setVisibility(vMSelectedCategoryInt1ViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.button4.setOnClickListener(mCallback21);
            this.button5.setOnClickListener(mCallback20);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.wallet.WalletViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setAnalysis(callbackArg_0);
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // VM
                com.shoohna.gofresh.ui.home.ui.wallet.WalletViewModel vM = mVM;
                // VM != null
                boolean vMJavaLangObjectNull = false;



                vMJavaLangObjectNull = (vM) != (null);
                if (vMJavaLangObjectNull) {



                    vM.setWithdraw(callbackArg_0);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): VM.selectedCategory
        flag 1 (0x2L): VM
        flag 2 (0x3L): null
        flag 3 (0x4L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/white : @android:color/walletSwitch
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/white : @android:color/walletSwitch
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? View.VISIBLE : View.GONE
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/white
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 2 ? @android:color/colorAccent : @android:color/white
        flag 11 (0xcL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/white : @android:color/walletSwitch
        flag 12 (0xdL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/white : @android:color/walletSwitch
        flag 13 (0xeL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/white
        flag 14 (0xfL): androidx.databinding.ViewDataBinding.safeUnbox(VM.selectedCategory.getValue()) == 1 ? @android:color/colorAccent : @android:color/white
    flag mapping end*/
    //end
}