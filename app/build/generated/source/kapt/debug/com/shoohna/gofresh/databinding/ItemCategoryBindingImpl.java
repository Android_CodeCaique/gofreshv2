package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemCategoryBindingImpl extends ItemCategoryBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemCategoryBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 2, sIncludes, sViewsWithIds));
    }
    private ItemCategoryBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[1]
            , (android.widget.LinearLayout) bindings[0]
            );
        this.content.setTag(null);
        this.linearLayoutId.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.responses.Category) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.responses.Category Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String modelName = null;
        com.shoohna.gofresh.pojo.responses.Category model = mModel;
        boolean modelCheck = false;
        int modelCheckContentAndroidColorPinkContentAndroidColorDarkGrey = 0;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.name
                    modelName = model.getName();
                    // read Model.check
                    modelCheck = model.getCheck();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelCheck) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read Model.check ? @android:color/pink : @android:color/darkGrey
                modelCheckContentAndroidColorPinkContentAndroidColorDarkGrey = ((modelCheck) ? (getColorFromResource(content, R.color.pink)) : (getColorFromResource(content, R.color.darkGrey)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.shoohna.gofresh.util.BindingAdapters.setBold(this.content, modelCheck);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.content, modelName);
            this.content.setTextColor(modelCheckContentAndroidColorPinkContentAndroidColorDarkGrey);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.check ? @android:color/pink : @android:color/darkGrey
        flag 3 (0x4L): Model.check ? @android:color/pink : @android:color/darkGrey
    flag mapping end*/
    //end
}