package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class MyOrderProductItemBindingImpl extends MyOrderProductItemBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView8, 5);
        sViewsWithIds.put(R.id.textView22, 6);
        sViewsWithIds.put(R.id.textView56, 7);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.ImageView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public MyOrderProductItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private MyOrderProductItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[5]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[4]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.ImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.textView21.setTag(null);
        this.textView58.setTag(null);
        this.textView59.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.responses.MyOrderProductsProduct) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.responses.MyOrderProductsProduct Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String modelPrice = null;
        java.lang.String modelName = null;
        com.shoohna.gofresh.pojo.responses.MyOrderProductsProduct model = mModel;
        java.lang.String modelNameEmptyTextView21AndroidStringNoDescriptionModelDesc = null;
        java.lang.String modelPriceEmptyTextView58AndroidStringNoPriceModelPrice = null;
        int modelQuantity = 0;
        java.lang.String stringValueOfModelQuantityInt0TextView59AndroidStringNoDataModelQuantity = null;
        java.lang.Object modelQuantityInt0TextView59AndroidStringNoDataModelQuantity = null;
        boolean modelNameEmpty = false;
        java.lang.String modelImage = null;
        java.lang.String modelDesc = null;
        java.lang.String stringValueOfModelPriceEmptyTextView58AndroidStringNoPriceModelPrice = null;
        boolean modelQuantityInt0 = false;
        boolean modelPriceEmpty = false;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.price
                    modelPrice = model.getPrice();
                    // read Model.name
                    modelName = model.getName();
                    // read Model.quantity
                    modelQuantity = model.getQuantity();
                    // read Model.image
                    modelImage = model.getImage();
                }


                if (modelPrice != null) {
                    // read Model.price.empty
                    modelPriceEmpty = modelPrice.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelPriceEmpty) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
                if (modelName != null) {
                    // read Model.name.empty
                    modelNameEmpty = modelName.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelNameEmpty) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
                // read Model.quantity == 0
                modelQuantityInt0 = (modelQuantity) == (0);
            if((dirtyFlags & 0x3L) != 0) {
                if(modelQuantityInt0) {
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x40L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x3L) != 0) {

                // read Model.price.empty ? @android:string/noPrice : Model.price
                modelPriceEmptyTextView58AndroidStringNoPriceModelPrice = ((modelPriceEmpty) ? (textView58.getResources().getString(R.string.noPrice)) : (modelPrice));
                // read Model.quantity == 0 ? @android:string/noData : Model.quantity
                modelQuantityInt0TextView59AndroidStringNoDataModelQuantity = ((modelQuantityInt0) ? (textView59.getResources().getString(R.string.noData)) : (modelQuantity));


                // read String.valueOf(Model.price.empty ? @android:string/noPrice : Model.price)
                stringValueOfModelPriceEmptyTextView58AndroidStringNoPriceModelPrice = java.lang.String.valueOf(modelPriceEmptyTextView58AndroidStringNoPriceModelPrice);
                // read String.valueOf(Model.quantity == 0 ? @android:string/noData : Model.quantity)
                stringValueOfModelQuantityInt0TextView59AndroidStringNoDataModelQuantity = java.lang.String.valueOf(modelQuantityInt0TextView59AndroidStringNoDataModelQuantity);
        }
        if ((dirtyFlags & 0x4L) != 0) {

                if (model != null) {
                    // read Model.desc
                    modelDesc = model.getDesc();
                }
        }

        if ((dirtyFlags & 0x3L) != 0) {

                // read Model.name.empty ? @android:string/noDescription : Model.desc
                modelNameEmptyTextView21AndroidStringNoDescriptionModelDesc = ((modelNameEmpty) ? (textView21.getResources().getString(R.string.noDescription)) : (modelDesc));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.shoohna.gofresh.util.BindingAdapters.loadImage(this.mboundView1, modelImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView21, modelNameEmptyTextView21AndroidStringNoDescriptionModelDesc);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView58, stringValueOfModelPriceEmptyTextView58AndroidStringNoPriceModelPrice);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView59, stringValueOfModelQuantityInt0TextView59AndroidStringNoDataModelQuantity);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.name.empty ? @android:string/noDescription : Model.desc
        flag 3 (0x4L): Model.name.empty ? @android:string/noDescription : Model.desc
        flag 4 (0x5L): Model.price.empty ? @android:string/noPrice : Model.price
        flag 5 (0x6L): Model.price.empty ? @android:string/noPrice : Model.price
        flag 6 (0x7L): Model.quantity == 0 ? @android:string/noData : Model.quantity
        flag 7 (0x8L): Model.quantity == 0 ? @android:string/noData : Model.quantity
    flag mapping end*/
    //end
}