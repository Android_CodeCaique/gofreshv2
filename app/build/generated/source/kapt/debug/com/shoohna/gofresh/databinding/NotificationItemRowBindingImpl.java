package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class NotificationItemRowBindingImpl extends NotificationItemRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imageView8, 4);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final android.widget.ImageView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public NotificationItemRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private NotificationItemRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[4]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            );
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.ImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.textView21.setTag(null);
        this.textView22.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.responses.NotificationData) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.responses.NotificationData Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String modelName = null;
        com.shoohna.gofresh.pojo.responses.NotificationData model = mModel;
        java.lang.String modelDesc = null;
        boolean modelDescEmpty = false;
        java.lang.String modelNameEmptyTextView21AndroidStringNoTitleModelName = null;
        java.lang.String modelDescEmptyTextView22AndroidStringNoDescriptionModelDesc = null;
        boolean modelNameEmpty = false;
        java.lang.String modelImage = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.name
                    modelName = model.getName();
                    // read Model.desc
                    modelDesc = model.getDesc();
                    // read Model.image
                    modelImage = model.getImage();
                }


                if (modelName != null) {
                    // read Model.name.empty
                    modelNameEmpty = modelName.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelNameEmpty) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
                if (modelDesc != null) {
                    // read Model.desc.empty
                    modelDescEmpty = modelDesc.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelDescEmpty) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x3L) != 0) {

                // read Model.name.empty ? @android:string/noTitle : Model.name
                modelNameEmptyTextView21AndroidStringNoTitleModelName = ((modelNameEmpty) ? (textView21.getResources().getString(R.string.noTitle)) : (modelName));
                // read Model.desc.empty ? @android:string/noDescription : Model.desc
                modelDescEmptyTextView22AndroidStringNoDescriptionModelDesc = ((modelDescEmpty) ? (textView22.getResources().getString(R.string.noDescription)) : (modelDesc));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.shoohna.gofresh.util.BindingAdapters.loadImage(this.mboundView1, modelImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView21, modelNameEmptyTextView21AndroidStringNoTitleModelName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView22, modelDescEmptyTextView22AndroidStringNoDescriptionModelDesc);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.name.empty ? @android:string/noTitle : Model.name
        flag 3 (0x4L): Model.name.empty ? @android:string/noTitle : Model.name
        flag 4 (0x5L): Model.desc.empty ? @android:string/noDescription : Model.desc
        flag 5 (0x6L): Model.desc.empty ? @android:string/noDescription : Model.desc
    flag mapping end*/
    //end
}