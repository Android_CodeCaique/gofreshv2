package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class OrderStatusRowBindingImpl extends OrderStatusRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.mainConstraintId, 5);
        sViewsWithIds.put(R.id.textView19, 6);
        sViewsWithIds.put(R.id.textView30, 7);
        sViewsWithIds.put(R.id.textView64, 8);
        sViewsWithIds.put(R.id.textView66, 9);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public OrderStatusRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private OrderStatusRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[4]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textView29.setTag(null);
        this.textView40.setTag(null);
        this.textView65.setTag(null);
        this.textView67.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.responses.MyOrdersData) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.responses.MyOrdersData Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String modelDate = null;
        int modelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = 0;
        boolean modelStatusInt2 = false;
        java.lang.String modelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = null;
        int modelStatusInt0TextView40AndroidColorYellowModelStatusInt1TextView40AndroidColorYellowModelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = 0;
        java.lang.String modelStatusInt1TextView40AndroidStringNewModelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = null;
        java.lang.String modelReport = null;
        int modelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = 0;
        java.lang.Object modelIdInt0TextView29AndroidStringNoDataModelId = null;
        boolean modelStatusInt1 = false;
        java.lang.String modelReportEmptyTextView65AndroidStringNoNotesModelReport = null;
        int modelId = 0;
        com.shoohna.gofresh.pojo.responses.MyOrdersData model = mModel;
        boolean modelDateEmpty = false;
        boolean modelStatusInt4 = false;
        boolean modelStatusInt0 = false;
        java.lang.String modelStatusInt0TextView40AndroidStringInCompletedModelStatusInt1TextView40AndroidStringNewModelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = null;
        boolean modelIdInt0 = false;
        java.lang.String modelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = null;
        int modelStatusInt1TextView40AndroidColorYellowModelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = 0;
        java.lang.String modelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = null;
        boolean modelReportEmpty = false;
        int modelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = 0;
        boolean modelStatusInt3 = false;
        int modelStatus = 0;
        java.lang.String stringValueOfModelIdInt0TextView29AndroidStringNoDataModelId = null;
        java.lang.String modelDateEmptyTextView67AndroidStringNoDataModelDate = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.date
                    modelDate = model.getDate();
                    // read Model.report
                    modelReport = model.getReport();
                    // read Model.id
                    modelId = model.getId();
                    // read Model.status
                    modelStatus = model.getStatus();
                }


                if (modelDate != null) {
                    // read Model.date.empty
                    modelDateEmpty = modelDate.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelDateEmpty) {
                        dirtyFlags |= 0x8000000L;
                }
                else {
                        dirtyFlags |= 0x4000000L;
                }
            }
                if (modelReport != null) {
                    // read Model.report.empty
                    modelReportEmpty = modelReport.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelReportEmpty) {
                        dirtyFlags |= 0x8000L;
                }
                else {
                        dirtyFlags |= 0x4000L;
                }
            }
                // read Model.id == 0
                modelIdInt0 = (modelId) == (0);
                // read Model.status == 0
                modelStatusInt0 = (modelStatus) == (0);
            if((dirtyFlags & 0x3L) != 0) {
                if(modelIdInt0) {
                        dirtyFlags |= 0x2000L;
                }
                else {
                        dirtyFlags |= 0x1000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelStatusInt0) {
                        dirtyFlags |= 0x80L;
                        dirtyFlags |= 0x20000L;
                }
                else {
                        dirtyFlags |= 0x40L;
                        dirtyFlags |= 0x10000L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x3L) != 0) {

                // read Model.id == 0 ? @android:string/noData : Model.id
                modelIdInt0TextView29AndroidStringNoDataModelId = ((modelIdInt0) ? (textView29.getResources().getString(R.string.noData)) : (modelId));
                // read Model.report.empty ? @android:string/noNotes : Model.report
                modelReportEmptyTextView65AndroidStringNoNotesModelReport = ((modelReportEmpty) ? (textView65.getResources().getString(R.string.noNotes)) : (modelReport));
                // read Model.date.empty ? @android:string/noData : Model.date
                modelDateEmptyTextView67AndroidStringNoDataModelDate = ((modelDateEmpty) ? (textView67.getResources().getString(R.string.noData)) : (modelDate));


                // read String.valueOf(Model.id == 0 ? @android:string/noData : Model.id)
                stringValueOfModelIdInt0TextView29AndroidStringNoDataModelId = java.lang.String.valueOf(modelIdInt0TextView29AndroidStringNoDataModelId);
        }
        if ((dirtyFlags & 0x10040L) != 0) {



                if (model != null) {
                    // read Model.status
                    modelStatus = model.getStatus();
                }


                // read Model.status == 1
                modelStatusInt1 = (modelStatus) == (1);
            if((dirtyFlags & 0x10000L) != 0) {
                if(modelStatusInt1) {
                        dirtyFlags |= 0x200L;
                }
                else {
                        dirtyFlags |= 0x100L;
                }
            }
            if((dirtyFlags & 0x40L) != 0) {
                if(modelStatusInt1) {
                        dirtyFlags |= 0x200000L;
                }
                else {
                        dirtyFlags |= 0x100000L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x100100L) != 0) {



                if (model != null) {
                    // read Model.status
                    modelStatus = model.getStatus();
                }


                // read Model.status == 2
                modelStatusInt2 = (modelStatus) == (2);
            if((dirtyFlags & 0x100000L) != 0) {
                if(modelStatusInt2) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }
            if((dirtyFlags & 0x100L) != 0) {
                if(modelStatusInt2) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x14L) != 0) {



                if (model != null) {
                    // read Model.status
                    modelStatus = model.getStatus();
                }


                // read Model.status == 3
                modelStatusInt3 = (modelStatus) == (3);
            if((dirtyFlags & 0x10L) != 0) {
                if(modelStatusInt3) {
                        dirtyFlags |= 0x800000L;
                }
                else {
                        dirtyFlags |= 0x400000L;
                }
            }
            if((dirtyFlags & 0x4L) != 0) {
                if(modelStatusInt3) {
                        dirtyFlags |= 0x2000000L;
                }
                else {
                        dirtyFlags |= 0x1000000L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x1400000L) != 0) {

                // read Model.status == 4
                modelStatusInt4 = (modelStatus) == (4);
            if((dirtyFlags & 0x1000000L) != 0) {
                if(modelStatusInt4) {
                        dirtyFlags |= 0x800L;
                }
                else {
                        dirtyFlags |= 0x400L;
                }
            }
            if((dirtyFlags & 0x400000L) != 0) {
                if(modelStatusInt4) {
                        dirtyFlags |= 0x80000L;
                }
                else {
                        dirtyFlags |= 0x40000L;
                }
            }

            if ((dirtyFlags & 0x1000000L) != 0) {

                    // read Model.status == 4 ? @android:color/red : @android:color/black
                    modelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = ((modelStatusInt4) ? (getColorFromResource(textView40, R.color.red)) : (getColorFromResource(textView40, R.color.black)));
            }
            if ((dirtyFlags & 0x400000L) != 0) {

                    // read Model.status == 4 ? @android:string/canceled : @android:string/noData
                    modelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = ((modelStatusInt4) ? (textView40.getResources().getString(R.string.canceled)) : (textView40.getResources().getString(R.string.noData)));
            }
        }

        if ((dirtyFlags & 0x10L) != 0) {

                // read Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
                modelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = ((modelStatusInt3) ? (textView40.getResources().getString(R.string.completed)) : (modelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData));
        }
        if ((dirtyFlags & 0x4L) != 0) {

                // read Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
                modelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = ((modelStatusInt3) ? (getColorFromResource(textView40, R.color.green)) : (modelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack));
        }

        if ((dirtyFlags & 0x100000L) != 0) {

                // read Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
                modelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = ((modelStatusInt2) ? (getColorFromResource(textView40, R.color.yellow)) : (modelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack));
        }
        if ((dirtyFlags & 0x100L) != 0) {

                // read Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
                modelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = ((modelStatusInt2) ? (textView40.getResources().getString(R.string.inProgress)) : (modelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData));
        }

        if ((dirtyFlags & 0x10000L) != 0) {

                // read Model.status == 1 ? @android:string/New : Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
                modelStatusInt1TextView40AndroidStringNewModelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = ((modelStatusInt1) ? (textView40.getResources().getString(R.string.New)) : (modelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData));
        }
        if ((dirtyFlags & 0x40L) != 0) {

                // read Model.status == 1 ? @android:color/yellow : Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
                modelStatusInt1TextView40AndroidColorYellowModelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = ((modelStatusInt1) ? (getColorFromResource(textView40, R.color.yellow)) : (modelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack));
        }

        if ((dirtyFlags & 0x3L) != 0) {

                // read Model.status == 0 ? @android:color/yellow : Model.status == 1 ? @android:color/yellow : Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
                modelStatusInt0TextView40AndroidColorYellowModelStatusInt1TextView40AndroidColorYellowModelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack = ((modelStatusInt0) ? (getColorFromResource(textView40, R.color.yellow)) : (modelStatusInt1TextView40AndroidColorYellowModelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack));
                // read Model.status == 0 ? @android:string/inCompleted : Model.status == 1 ? @android:string/New : Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
                modelStatusInt0TextView40AndroidStringInCompletedModelStatusInt1TextView40AndroidStringNewModelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData = ((modelStatusInt0) ? (textView40.getResources().getString(R.string.inCompleted)) : (modelStatusInt1TextView40AndroidStringNewModelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView29, stringValueOfModelIdInt0TextView29AndroidStringNoDataModelId);
            this.textView40.setTextColor(modelStatusInt0TextView40AndroidColorYellowModelStatusInt1TextView40AndroidColorYellowModelStatusInt2TextView40AndroidColorYellowModelStatusInt3TextView40AndroidColorGreenModelStatusInt4TextView40AndroidColorRedTextView40AndroidColorBlack);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView40, modelStatusInt0TextView40AndroidStringInCompletedModelStatusInt1TextView40AndroidStringNewModelStatusInt2TextView40AndroidStringInProgressModelStatusInt3TextView40AndroidStringCompletedModelStatusInt4TextView40AndroidStringCanceledTextView40AndroidStringNoData);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView65, modelReportEmptyTextView65AndroidStringNoNotesModelReport);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView67, modelDateEmptyTextView67AndroidStringNoDataModelDate);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
        flag 3 (0x4L): Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
        flag 4 (0x5L): Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 5 (0x6L): Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 6 (0x7L): Model.status == 0 ? @android:color/yellow : Model.status == 1 ? @android:color/yellow : Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
        flag 7 (0x8L): Model.status == 0 ? @android:color/yellow : Model.status == 1 ? @android:color/yellow : Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
        flag 8 (0x9L): Model.status == 1 ? @android:string/New : Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 9 (0xaL): Model.status == 1 ? @android:string/New : Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 10 (0xbL): Model.status == 4 ? @android:color/red : @android:color/black
        flag 11 (0xcL): Model.status == 4 ? @android:color/red : @android:color/black
        flag 12 (0xdL): Model.id == 0 ? @android:string/noData : Model.id
        flag 13 (0xeL): Model.id == 0 ? @android:string/noData : Model.id
        flag 14 (0xfL): Model.report.empty ? @android:string/noNotes : Model.report
        flag 15 (0x10L): Model.report.empty ? @android:string/noNotes : Model.report
        flag 16 (0x11L): Model.status == 0 ? @android:string/inCompleted : Model.status == 1 ? @android:string/New : Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 17 (0x12L): Model.status == 0 ? @android:string/inCompleted : Model.status == 1 ? @android:string/New : Model.status == 2 ? @android:string/inProgress : Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 18 (0x13L): Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 19 (0x14L): Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 20 (0x15L): Model.status == 1 ? @android:color/yellow : Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
        flag 21 (0x16L): Model.status == 1 ? @android:color/yellow : Model.status == 2 ? @android:color/yellow : Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
        flag 22 (0x17L): Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 23 (0x18L): Model.status == 3 ? @android:string/completed : Model.status == 4 ? @android:string/canceled : @android:string/noData
        flag 24 (0x19L): Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
        flag 25 (0x1aL): Model.status == 3 ? @android:color/green : Model.status == 4 ? @android:color/red : @android:color/black
        flag 26 (0x1bL): Model.date.empty ? @android:string/noData : Model.date
        flag 27 (0x1cL): Model.date.empty ? @android:string/noData : Model.date
    flag mapping end*/
    //end
}