package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ProductDetailsColorItemRowBindingImpl extends ProductDetailsColorItemRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final com.infinityandroid.roundedimageview.RoundedImageView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ProductDetailsColorItemRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 2, sIncludes, sViewsWithIds));
    }
    private ProductDetailsColorItemRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            );
        this.colorConstraintId.setTag(null);
        this.mboundView1 = (com.infinityandroid.roundedimageview.RoundedImageView) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.responses.ProductColor) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.responses.ProductColor Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean modelChecked = false;
        com.shoohna.gofresh.pojo.responses.ProductColor model = mModel;
        int modelCheckedColorConstraintIdAndroidColorColorPrimaryColorConstraintIdAndroidColorWhite = 0;
        java.lang.String modelColorCode = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.checked
                    modelChecked = model.isChecked();
                    // read Model.color_code
                    modelColorCode = model.getColor_code();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelChecked) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read Model.checked ? @android:color/colorPrimary : @android:color/white
                modelCheckedColorConstraintIdAndroidColorColorPrimaryColorConstraintIdAndroidColorWhite = ((modelChecked) ? (getColorFromResource(colorConstraintId, R.color.colorPrimary)) : (getColorFromResource(colorConstraintId, R.color.white)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.colorConstraintId, androidx.databinding.adapters.Converters.convertColorToDrawable(modelCheckedColorConstraintIdAndroidColorColorPrimaryColorConstraintIdAndroidColorWhite));
            com.shoohna.gofresh.util.BindingAdapters.loadHexaColor(this.mboundView1, modelColorCode);
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.colorConstraintId.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(modelCheckedColorConstraintIdAndroidColorColorPrimaryColorConstraintIdAndroidColorWhite));
            }
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.checked ? @android:color/colorPrimary : @android:color/white
        flag 3 (0x4L): Model.checked ? @android:color/colorPrimary : @android:color/white
    flag mapping end*/
    //end
}