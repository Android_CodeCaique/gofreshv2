package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ProductListRowBindingImpl extends ProductListRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.cardView2, 6);
        sViewsWithIds.put(R.id.discountId, 7);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ProductListRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private ProductListRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[6]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[7]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[2]
            , (android.widget.TextView) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.TextView) bindings[4]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[1]
            );
        this.categorieNameId.setTag(null);
        this.discountLayoutShowId.setTag(null);
        this.distanceId.setTag(null);
        this.mainConstraintLayoutId.setTag(null);
        this.priceId.setTag(null);
        this.productImgId.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.model.ProductListModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.model.ProductListModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String modelPrice = null;
        com.shoohna.gofresh.pojo.model.ProductListModel model = mModel;
        java.lang.String modelDistance = null;
        boolean modelDiscountLayoutShow = false;
        int modelDiscountLayoutShowViewVISIBLEViewGONE = 0;
        java.lang.String modelImg = null;
        java.lang.String modelCategoryName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.price
                    modelPrice = model.getPrice();
                    // read Model.distance
                    modelDistance = model.getDistance();
                    // read Model.discountLayoutShow
                    modelDiscountLayoutShow = model.getDiscountLayoutShow();
                    // read Model.img
                    modelImg = model.getImg();
                    // read Model.categoryName
                    modelCategoryName = model.getCategoryName();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelDiscountLayoutShow) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read Model.discountLayoutShow ? View.VISIBLE : View.GONE
                modelDiscountLayoutShowViewVISIBLEViewGONE = ((modelDiscountLayoutShow) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.categorieNameId, modelCategoryName);
            this.discountLayoutShowId.setVisibility(modelDiscountLayoutShowViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.distanceId, modelDistance);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.priceId, modelPrice);
            com.shoohna.gofresh.util.BindingAdapters.loadImage(this.productImgId, modelImg);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.discountLayoutShow ? View.VISIBLE : View.GONE
        flag 3 (0x4L): Model.discountLayoutShow ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}