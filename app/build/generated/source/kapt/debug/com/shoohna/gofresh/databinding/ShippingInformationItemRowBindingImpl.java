package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ShippingInformationItemRowBindingImpl extends ShippingInformationItemRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.mainConstraintLayoutId, 4);
        sViewsWithIds.put(R.id.imageView4, 5);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ShippingInformationItemRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ShippingInformationItemRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[4]
            , (android.widget.ImageView) bindings[3]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.selectAddressImgId.setTag(null);
        this.textView48.setTag(null);
        this.textView49.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.responses.UserAddressData) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.responses.UserAddressData Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean modelChecked = false;
        java.lang.String modelName = null;
        com.shoohna.gofresh.pojo.responses.UserAddressData model = mModel;
        java.lang.String modelAddress = null;
        android.graphics.drawable.Drawable modelCheckedSelectAddressImgIdAndroidDrawableIcStepCurrentSelectAddressImgIdAndroidDrawableIcStepAfter = null;
        java.lang.String modelAddressEmptyTextView49AndroidStringNoDataModelAddress = null;
        boolean modelAddressEmpty = false;
        boolean modelNameEmpty = false;
        java.lang.String modelNameEmptyTextView48AndroidStringNoDataModelName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.checked
                    modelChecked = model.isChecked();
                    // read Model.name
                    modelName = model.getName();
                    // read Model.address
                    modelAddress = model.getAddress();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelChecked) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read Model.checked ? @android:drawable/ic_step_current : @android:drawable/ic_step_after
                modelCheckedSelectAddressImgIdAndroidDrawableIcStepCurrentSelectAddressImgIdAndroidDrawableIcStepAfter = ((modelChecked) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(selectAddressImgId.getContext(), R.drawable.ic_step_current)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(selectAddressImgId.getContext(), R.drawable.ic_step_after)));
                if (modelName != null) {
                    // read Model.name.empty
                    modelNameEmpty = modelName.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelNameEmpty) {
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x40L;
                }
            }
                if (modelAddress != null) {
                    // read Model.address.empty
                    modelAddressEmpty = modelAddress.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelAddressEmpty) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x3L) != 0) {

                // read Model.address.empty ? @android:string/noData : Model.address
                modelAddressEmptyTextView49AndroidStringNoDataModelAddress = ((modelAddressEmpty) ? (textView49.getResources().getString(R.string.noData)) : (modelAddress));
                // read Model.name.empty ? @android:string/noData : Model.name
                modelNameEmptyTextView48AndroidStringNoDataModelName = ((modelNameEmpty) ? (textView48.getResources().getString(R.string.noData)) : (modelName));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.selectAddressImgId, modelCheckedSelectAddressImgIdAndroidDrawableIcStepCurrentSelectAddressImgIdAndroidDrawableIcStepAfter);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView48, modelNameEmptyTextView48AndroidStringNoDataModelName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView49, modelAddressEmptyTextView49AndroidStringNoDataModelAddress);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.checked ? @android:drawable/ic_step_current : @android:drawable/ic_step_after
        flag 3 (0x4L): Model.checked ? @android:drawable/ic_step_current : @android:drawable/ic_step_after
        flag 4 (0x5L): Model.address.empty ? @android:string/noData : Model.address
        flag 5 (0x6L): Model.address.empty ? @android:string/noData : Model.address
        flag 6 (0x7L): Model.name.empty ? @android:string/noData : Model.name
        flag 7 (0x8L): Model.name.empty ? @android:string/noData : Model.name
    flag mapping end*/
    //end
}