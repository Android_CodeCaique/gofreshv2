package com.shoohna.gofresh.databinding;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class SizeItemRowBindingImpl extends SizeItemRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public SizeItemRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 2, sIncludes, sViewsWithIds));
    }
    private SizeItemRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            );
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.sizeConstraintId.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.Model == variableId) {
            setModel((com.shoohna.gofresh.pojo.responses.ProductSize) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.shoohna.gofresh.pojo.responses.ProductSize Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.Model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean modelChecked = false;
        java.lang.String modelName = null;
        int modelCheckedSizeConstraintIdAndroidColorColorPrimarySizeConstraintIdAndroidColorPink = 0;
        com.shoohna.gofresh.pojo.responses.ProductSize model = mModel;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read Model.checked
                    modelChecked = model.isChecked();
                    // read Model.name
                    modelName = model.getName();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelChecked) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read Model.checked ? @android:color/colorPrimary : @android:color/pink
                modelCheckedSizeConstraintIdAndroidColorColorPrimarySizeConstraintIdAndroidColorPink = ((modelChecked) ? (getColorFromResource(sizeConstraintId, R.color.colorPrimary)) : (getColorFromResource(sizeConstraintId, R.color.pink)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, modelName);
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.sizeConstraintId.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(modelCheckedSizeConstraintIdAndroidColorColorPrimarySizeConstraintIdAndroidColorPink));
            }
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): Model
        flag 1 (0x2L): null
        flag 2 (0x3L): Model.checked ? @android:color/colorPrimary : @android:color/pink
        flag 3 (0x4L): Model.checked ? @android:color/colorPrimary : @android:color/pink
    flag mapping end*/
    //end
}