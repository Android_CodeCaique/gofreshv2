package com.shoohna.gofresh.networking.interfaces;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.shoohna.gofresh.pojo.model.ProductEntity;
import com.shoohna.gofresh.pojo.model.ProductSelectionCart;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class ProductDao_Impl implements ProductDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ProductEntity> __insertionAdapterOfProductEntity;

  private final EntityDeletionOrUpdateAdapter<ProductEntity> __deletionAdapterOfProductEntity;

  private final EntityDeletionOrUpdateAdapter<ProductEntity> __updateAdapterOfProductEntity;

  private final SharedSQLiteStatement __preparedStmtOfDeleteProductById;

  private final SharedSQLiteStatement __preparedStmtOfUpdateQuantityOfProduct;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllProductDatabase;

  public ProductDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfProductEntity = new EntityInsertionAdapter<ProductEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `ProductEntity` (`product_id`,`color_id`,`size_id`,`size_name`,`quantity`,`imageURL`,`color_code`,`desc`,`price`,`name`) VALUES (?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductEntity value) {
        stmt.bindLong(1, value.getProduct_id());
        stmt.bindLong(2, value.getColor_id());
        stmt.bindLong(3, value.getSize_id());
        if (value.getSize_name() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getSize_name());
        }
        stmt.bindLong(5, value.getQuantity());
        if (value.getImageURL() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getImageURL());
        }
        if (value.getColor_code() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getColor_code());
        }
        if (value.getDesc() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getDesc());
        }
        if (value.getPrice() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getPrice());
        }
        if (value.getName() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getName());
        }
      }
    };
    this.__deletionAdapterOfProductEntity = new EntityDeletionOrUpdateAdapter<ProductEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `ProductEntity` WHERE `product_id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductEntity value) {
        stmt.bindLong(1, value.getProduct_id());
      }
    };
    this.__updateAdapterOfProductEntity = new EntityDeletionOrUpdateAdapter<ProductEntity>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `ProductEntity` SET `product_id` = ?,`color_id` = ?,`size_id` = ?,`size_name` = ?,`quantity` = ?,`imageURL` = ?,`color_code` = ?,`desc` = ?,`price` = ?,`name` = ? WHERE `product_id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductEntity value) {
        stmt.bindLong(1, value.getProduct_id());
        stmt.bindLong(2, value.getColor_id());
        stmt.bindLong(3, value.getSize_id());
        if (value.getSize_name() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getSize_name());
        }
        stmt.bindLong(5, value.getQuantity());
        if (value.getImageURL() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getImageURL());
        }
        if (value.getColor_code() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getColor_code());
        }
        if (value.getDesc() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getDesc());
        }
        if (value.getPrice() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getPrice());
        }
        if (value.getName() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getName());
        }
        stmt.bindLong(11, value.getProduct_id());
      }
    };
    this.__preparedStmtOfDeleteProductById = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM ProductEntity WHERE product_id = ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateQuantityOfProduct = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE ProductEntity SET quantity = ? WHERE product_id = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllProductDatabase = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM ProductEntity";
        return _query;
      }
    };
  }

  @Override
  public void insertProduct(final ProductEntity productEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfProductEntity.insert(productEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteProduct(final ProductEntity productEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfProductEntity.handle(productEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateProduct(final ProductEntity productEntity) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfProductEntity.handle(productEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteProductById(final int productId) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteProductById.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, productId);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteProductById.release(_stmt);
    }
  }

  @Override
  public void updateQuantityOfProduct(final int quantity, final int productId) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateQuantityOfProduct.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, quantity);
    _argIndex = 2;
    _stmt.bindLong(_argIndex, productId);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateQuantityOfProduct.release(_stmt);
    }
  }

  @Override
  public void deleteAllProductDatabase() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllProductDatabase.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllProductDatabase.release(_stmt);
    }
  }

  @Override
  public List<ProductEntity> getAllProducts() {
    final String _sql = "SELECT * FROM ProductEntity";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfProductId = CursorUtil.getColumnIndexOrThrow(_cursor, "product_id");
      final int _cursorIndexOfColorId = CursorUtil.getColumnIndexOrThrow(_cursor, "color_id");
      final int _cursorIndexOfSizeId = CursorUtil.getColumnIndexOrThrow(_cursor, "size_id");
      final int _cursorIndexOfSizeName = CursorUtil.getColumnIndexOrThrow(_cursor, "size_name");
      final int _cursorIndexOfQuantity = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity");
      final int _cursorIndexOfImageURL = CursorUtil.getColumnIndexOrThrow(_cursor, "imageURL");
      final int _cursorIndexOfColorCode = CursorUtil.getColumnIndexOrThrow(_cursor, "color_code");
      final int _cursorIndexOfDesc = CursorUtil.getColumnIndexOrThrow(_cursor, "desc");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
      final List<ProductEntity> _result = new ArrayList<ProductEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProductEntity _item;
        final int _tmpProduct_id;
        _tmpProduct_id = _cursor.getInt(_cursorIndexOfProductId);
        final int _tmpColor_id;
        _tmpColor_id = _cursor.getInt(_cursorIndexOfColorId);
        final int _tmpSize_id;
        _tmpSize_id = _cursor.getInt(_cursorIndexOfSizeId);
        final String _tmpSize_name;
        _tmpSize_name = _cursor.getString(_cursorIndexOfSizeName);
        final int _tmpQuantity;
        _tmpQuantity = _cursor.getInt(_cursorIndexOfQuantity);
        final String _tmpImageURL;
        _tmpImageURL = _cursor.getString(_cursorIndexOfImageURL);
        final String _tmpColor_code;
        _tmpColor_code = _cursor.getString(_cursorIndexOfColorCode);
        final String _tmpDesc;
        _tmpDesc = _cursor.getString(_cursorIndexOfDesc);
        final String _tmpPrice;
        _tmpPrice = _cursor.getString(_cursorIndexOfPrice);
        final String _tmpName;
        _tmpName = _cursor.getString(_cursorIndexOfName);
        _item = new ProductEntity(_tmpProduct_id,_tmpColor_id,_tmpSize_id,_tmpSize_name,_tmpQuantity,_tmpImageURL,_tmpColor_code,_tmpDesc,_tmpPrice,_tmpName);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<ProductEntity> getAllProductsById(final int productId) {
    final String _sql = "SELECT * FROM ProductEntity WHERE product_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, productId);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfProductId = CursorUtil.getColumnIndexOrThrow(_cursor, "product_id");
      final int _cursorIndexOfColorId = CursorUtil.getColumnIndexOrThrow(_cursor, "color_id");
      final int _cursorIndexOfSizeId = CursorUtil.getColumnIndexOrThrow(_cursor, "size_id");
      final int _cursorIndexOfSizeName = CursorUtil.getColumnIndexOrThrow(_cursor, "size_name");
      final int _cursorIndexOfQuantity = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity");
      final int _cursorIndexOfImageURL = CursorUtil.getColumnIndexOrThrow(_cursor, "imageURL");
      final int _cursorIndexOfColorCode = CursorUtil.getColumnIndexOrThrow(_cursor, "color_code");
      final int _cursorIndexOfDesc = CursorUtil.getColumnIndexOrThrow(_cursor, "desc");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
      final List<ProductEntity> _result = new ArrayList<ProductEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProductEntity _item;
        final int _tmpProduct_id;
        _tmpProduct_id = _cursor.getInt(_cursorIndexOfProductId);
        final int _tmpColor_id;
        _tmpColor_id = _cursor.getInt(_cursorIndexOfColorId);
        final int _tmpSize_id;
        _tmpSize_id = _cursor.getInt(_cursorIndexOfSizeId);
        final String _tmpSize_name;
        _tmpSize_name = _cursor.getString(_cursorIndexOfSizeName);
        final int _tmpQuantity;
        _tmpQuantity = _cursor.getInt(_cursorIndexOfQuantity);
        final String _tmpImageURL;
        _tmpImageURL = _cursor.getString(_cursorIndexOfImageURL);
        final String _tmpColor_code;
        _tmpColor_code = _cursor.getString(_cursorIndexOfColorCode);
        final String _tmpDesc;
        _tmpDesc = _cursor.getString(_cursorIndexOfDesc);
        final String _tmpPrice;
        _tmpPrice = _cursor.getString(_cursorIndexOfPrice);
        final String _tmpName;
        _tmpName = _cursor.getString(_cursorIndexOfName);
        _item = new ProductEntity(_tmpProduct_id,_tmpColor_id,_tmpSize_id,_tmpSize_name,_tmpQuantity,_tmpImageURL,_tmpColor_code,_tmpDesc,_tmpPrice,_tmpName);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<ProductEntity> getCurrentQuantityOfProductId(final int productId) {
    final String _sql = "SELECT * FROM ProductEntity WHERE product_id =?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, productId);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfProductId = CursorUtil.getColumnIndexOrThrow(_cursor, "product_id");
      final int _cursorIndexOfColorId = CursorUtil.getColumnIndexOrThrow(_cursor, "color_id");
      final int _cursorIndexOfSizeId = CursorUtil.getColumnIndexOrThrow(_cursor, "size_id");
      final int _cursorIndexOfSizeName = CursorUtil.getColumnIndexOrThrow(_cursor, "size_name");
      final int _cursorIndexOfQuantity = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity");
      final int _cursorIndexOfImageURL = CursorUtil.getColumnIndexOrThrow(_cursor, "imageURL");
      final int _cursorIndexOfColorCode = CursorUtil.getColumnIndexOrThrow(_cursor, "color_code");
      final int _cursorIndexOfDesc = CursorUtil.getColumnIndexOrThrow(_cursor, "desc");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
      final List<ProductEntity> _result = new ArrayList<ProductEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProductEntity _item;
        final int _tmpProduct_id;
        _tmpProduct_id = _cursor.getInt(_cursorIndexOfProductId);
        final int _tmpColor_id;
        _tmpColor_id = _cursor.getInt(_cursorIndexOfColorId);
        final int _tmpSize_id;
        _tmpSize_id = _cursor.getInt(_cursorIndexOfSizeId);
        final String _tmpSize_name;
        _tmpSize_name = _cursor.getString(_cursorIndexOfSizeName);
        final int _tmpQuantity;
        _tmpQuantity = _cursor.getInt(_cursorIndexOfQuantity);
        final String _tmpImageURL;
        _tmpImageURL = _cursor.getString(_cursorIndexOfImageURL);
        final String _tmpColor_code;
        _tmpColor_code = _cursor.getString(_cursorIndexOfColorCode);
        final String _tmpDesc;
        _tmpDesc = _cursor.getString(_cursorIndexOfDesc);
        final String _tmpPrice;
        _tmpPrice = _cursor.getString(_cursorIndexOfPrice);
        final String _tmpName;
        _tmpName = _cursor.getString(_cursorIndexOfName);
        _item = new ProductEntity(_tmpProduct_id,_tmpColor_id,_tmpSize_id,_tmpSize_name,_tmpQuantity,_tmpImageURL,_tmpColor_code,_tmpDesc,_tmpPrice,_tmpName);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<ProductSelectionCart> getAllSelectedProducts() {
    final String _sql = "SELECT product_id , color_id , size_id , quantity FROM ProductEntity";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfProductId = CursorUtil.getColumnIndexOrThrow(_cursor, "product_id");
      final int _cursorIndexOfColorId = CursorUtil.getColumnIndexOrThrow(_cursor, "color_id");
      final int _cursorIndexOfSizeId = CursorUtil.getColumnIndexOrThrow(_cursor, "size_id");
      final int _cursorIndexOfQuantity = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity");
      final List<ProductSelectionCart> _result = new ArrayList<ProductSelectionCart>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProductSelectionCart _item;
        final int _tmpProduct_id;
        _tmpProduct_id = _cursor.getInt(_cursorIndexOfProductId);
        final int _tmpColor_id;
        _tmpColor_id = _cursor.getInt(_cursorIndexOfColorId);
        final int _tmpSize_id;
        _tmpSize_id = _cursor.getInt(_cursorIndexOfSizeId);
        final int _tmpQuantity;
        _tmpQuantity = _cursor.getInt(_cursorIndexOfQuantity);
        _item = new ProductSelectionCart(_tmpProduct_id,_tmpColor_id,_tmpSize_id,_tmpQuantity);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
