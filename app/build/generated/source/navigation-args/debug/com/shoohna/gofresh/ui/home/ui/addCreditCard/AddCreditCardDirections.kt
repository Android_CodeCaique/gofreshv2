package com.shoohna.gofresh.ui.home.ui.addCreditCard

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class AddCreditCardDirections private constructor() {
  companion object {
    fun actionAddCreditCardToPreAuthorizationPayment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_addCreditCard_to_preAuthorizationPayment)
  }
}
