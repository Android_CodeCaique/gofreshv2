package com.shoohna.gofresh.ui.home.ui.cart

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class CartFragmentDirections private constructor() {
  companion object {
    fun actionCartFragmentToCheckoutProcessFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_cartFragment_to_checkoutProcessFragment)

    fun actionCartFragmentToAddCreditCard(): NavDirections =
        ActionOnlyNavDirections(R.id.action_cartFragment_to_addCreditCard)

    fun actionCartFragmentToMyAddress(): NavDirections =
        ActionOnlyNavDirections(R.id.action_cartFragment_to_myAddress)
  }
}
