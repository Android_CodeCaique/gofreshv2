package com.shoohna.gofresh.ui.home.ui.checkOutProcess.myCart

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class MyCartFragmentDirections private constructor() {
  companion object {
    fun actionCheckoutProcessFragmentToShippingFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_checkoutProcessFragment_to_shippingFragment)
  }
}
