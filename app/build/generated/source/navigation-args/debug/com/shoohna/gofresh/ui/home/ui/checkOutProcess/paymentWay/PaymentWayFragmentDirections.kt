package com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class PaymentWayFragmentDirections private constructor() {
  companion object {
    fun actionPaymentWayFragmentToRecietFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_paymentWayFragment_to_recietFragment)
  }
}
