package com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class ShippingFragmentDirections private constructor() {
  companion object {
    fun actionShippingFragmentToPaymentWayFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_shippingFragment_to_paymentWayFragment)
  }
}
