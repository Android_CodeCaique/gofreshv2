package com.shoohna.gofresh.ui.home.ui.favorite

import android.os.Bundle
import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R
import kotlin.Int
import kotlin.String

class FavoriteFragmentDirections private constructor() {
  private data class ActionFavoriteFragmentToProductDetailsFragment2(
    val productId: Int,
    val imgURL: String,
    val desc: String,
    val price: String
  ) : NavDirections {
    override fun getActionId(): Int = R.id.action_favoriteFragment_to_productDetailsFragment2

    override fun getArguments(): Bundle {
      val result = Bundle()
      result.putInt("productId", this.productId)
      result.putString("imgURL", this.imgURL)
      result.putString("desc", this.desc)
      result.putString("price", this.price)
      return result
    }
  }

  companion object {
    fun actionFavoriteFragmentToProductDetailsFragment2(
      productId: Int,
      imgURL: String,
      desc: String,
      price: String
    ): NavDirections = ActionFavoriteFragmentToProductDetailsFragment2(productId, imgURL, desc,
        price)

    fun actionFavoriteFragmentToChatFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_favoriteFragment_to_chatFragment)

    fun actionFavoriteFragmentToNotificationFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_favoriteFragment_to_notificationFragment)
  }
}
