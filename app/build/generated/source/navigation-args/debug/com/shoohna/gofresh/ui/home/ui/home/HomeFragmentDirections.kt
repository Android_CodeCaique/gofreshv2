package com.shoohna.gofresh.ui.home.ui.home

import android.os.Bundle
import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R
import kotlin.Int
import kotlin.String

class HomeFragmentDirections private constructor() {
  private data class ActionHomeFragmentToProductDetailsFragment2(
    val productId: Int,
    val imgURL: String,
    val desc: String,
    val price: String
  ) : NavDirections {
    override fun getActionId(): Int = R.id.action_homeFragment_to_productDetailsFragment2

    override fun getArguments(): Bundle {
      val result = Bundle()
      result.putInt("productId", this.productId)
      result.putString("imgURL", this.imgURL)
      result.putString("desc", this.desc)
      result.putString("price", this.price)
      return result
    }
  }

  private data class ActionHomeFragmentToProductFragment(
    val productType: String,
    val productSearchName: String?
  ) : NavDirections {
    override fun getActionId(): Int = R.id.action_homeFragment_to_productFragment

    override fun getArguments(): Bundle {
      val result = Bundle()
      result.putString("productType", this.productType)
      result.putString("productSearchName", this.productSearchName)
      return result
    }
  }

  companion object {
    fun actionHomeFragmentToCartFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_cartFragment)

    fun actionHomeFragmentToFavoriteFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_favoriteFragment)

    fun actionHomeFragmentToProfileFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_profileFragment)

    fun actionHomeFragmentToMoreFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_moreFragment)

    fun actionHomeFragmentToNotificationFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_notificationFragment)

    fun actionHomeFragmentToChatFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_chatFragment)

    fun actionHomeFragmentToProductDetailsFragment2(
      productId: Int,
      imgURL: String,
      desc: String,
      price: String
    ): NavDirections = ActionHomeFragmentToProductDetailsFragment2(productId, imgURL, desc, price)

    fun actionHomeFragmentToProductFragment(productType: String, productSearchName: String?):
        NavDirections = ActionHomeFragmentToProductFragment(productType, productSearchName)

    fun actionHomeFragmentSelf(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_self)

    fun actionHomeFragmentToHelpFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_helpFragment)

    fun actionHomeFragmentToContactUsFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_contactUsFragment)

    fun actionHomeFragmentToAboutUsFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_aboutUsFragment)

    fun actionHomeFragmentToMoreSettingFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_homeFragment_to_moreSettingFragment)
  }
}
