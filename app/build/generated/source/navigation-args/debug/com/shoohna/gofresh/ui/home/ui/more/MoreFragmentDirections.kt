package com.shoohna.gofresh.ui.home.ui.more

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class MoreFragmentDirections private constructor() {
  companion object {
    fun actionMoreFragmentToMoreSettingFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_moreFragment_to_moreSettingFragment)

    fun actionMoreFragmentToContactUsFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_moreFragment_to_contactUsFragment)

    fun actionMoreFragmentToAboutUsFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_moreFragment_to_aboutUsFragment)

    fun actionMoreFragmentToHelpFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_moreFragment_to_helpFragment)

    fun actionMoreFragmentToWalletFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_moreFragment_to_walletFragment)

    fun actionMoreFragmentToAddFamilyRegister2(): NavDirections =
        ActionOnlyNavDirections(R.id.action_moreFragment_to_addFamilyRegister2)
  }
}
