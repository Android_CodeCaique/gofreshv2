package com.shoohna.gofresh.ui.home.ui.myAddress

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class MyAddressDirections private constructor() {
  companion object {
    fun actionMyAddressToAddNewAddress(): NavDirections =
        ActionOnlyNavDirections(R.id.action_myAddress_to_addNewAddress)
  }
}
