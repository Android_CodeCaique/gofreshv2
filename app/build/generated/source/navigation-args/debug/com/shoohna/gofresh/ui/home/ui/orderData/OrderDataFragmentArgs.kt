package com.shoohna.gofresh.ui.home.ui.orderData

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.Int
import kotlin.jvm.JvmStatic

data class OrderDataFragmentArgs(
  val shopId: Int
) : NavArgs {
  fun toBundle(): Bundle {
    val result = Bundle()
    result.putInt("shopId", this.shopId)
    return result
  }

  companion object {
    @JvmStatic
    fun fromBundle(bundle: Bundle): OrderDataFragmentArgs {
      bundle.setClassLoader(OrderDataFragmentArgs::class.java.classLoader)
      val __shopId : Int
      if (bundle.containsKey("shopId")) {
        __shopId = bundle.getInt("shopId")
      } else {
        throw IllegalArgumentException("Required argument \"shopId\" is missing and does not have an android:defaultValue")
      }
      return OrderDataFragmentArgs(__shopId)
    }
  }
}
