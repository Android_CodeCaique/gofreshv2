package com.shoohna.gofresh.ui.home.ui.orderStatus

import android.os.Bundle
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R
import kotlin.Int

class OrderStatusFragmentDirections private constructor() {
  private data class ActionOrderStatusFragmentToOrderDataFragment2(
    val shopId: Int
  ) : NavDirections {
    override fun getActionId(): Int = R.id.action_orderStatusFragment_to_orderDataFragment2

    override fun getArguments(): Bundle {
      val result = Bundle()
      result.putInt("shopId", this.shopId)
      return result
    }
  }

  companion object {
    fun actionOrderStatusFragmentToOrderDataFragment2(shopId: Int): NavDirections =
        ActionOrderStatusFragmentToOrderDataFragment2(shopId)
  }
}
