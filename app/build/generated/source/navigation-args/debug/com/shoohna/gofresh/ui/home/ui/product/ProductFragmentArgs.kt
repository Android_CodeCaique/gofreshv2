package com.shoohna.gofresh.ui.home.ui.product

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.String
import kotlin.jvm.JvmStatic

data class ProductFragmentArgs(
  val productType: String,
  val productSearchName: String?
) : NavArgs {
  fun toBundle(): Bundle {
    val result = Bundle()
    result.putString("productType", this.productType)
    result.putString("productSearchName", this.productSearchName)
    return result
  }

  companion object {
    @JvmStatic
    fun fromBundle(bundle: Bundle): ProductFragmentArgs {
      bundle.setClassLoader(ProductFragmentArgs::class.java.classLoader)
      val __productType : String?
      if (bundle.containsKey("productType")) {
        __productType = bundle.getString("productType")
        if (__productType == null) {
          throw IllegalArgumentException("Argument \"productType\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"productType\" is missing and does not have an android:defaultValue")
      }
      val __productSearchName : String?
      if (bundle.containsKey("productSearchName")) {
        __productSearchName = bundle.getString("productSearchName")
      } else {
        throw IllegalArgumentException("Required argument \"productSearchName\" is missing and does not have an android:defaultValue")
      }
      return ProductFragmentArgs(__productType, __productSearchName)
    }
  }
}
