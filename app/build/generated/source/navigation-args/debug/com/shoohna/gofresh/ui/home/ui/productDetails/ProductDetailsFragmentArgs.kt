package com.shoohna.gofresh.ui.home.ui.productDetails

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.Int
import kotlin.String
import kotlin.jvm.JvmStatic

data class ProductDetailsFragmentArgs(
  val productId: Int,
  val imgURL: String,
  val desc: String,
  val price: String
) : NavArgs {
  fun toBundle(): Bundle {
    val result = Bundle()
    result.putInt("productId", this.productId)
    result.putString("imgURL", this.imgURL)
    result.putString("desc", this.desc)
    result.putString("price", this.price)
    return result
  }

  companion object {
    @JvmStatic
    fun fromBundle(bundle: Bundle): ProductDetailsFragmentArgs {
      bundle.setClassLoader(ProductDetailsFragmentArgs::class.java.classLoader)
      val __productId : Int
      if (bundle.containsKey("productId")) {
        __productId = bundle.getInt("productId")
      } else {
        throw IllegalArgumentException("Required argument \"productId\" is missing and does not have an android:defaultValue")
      }
      val __imgURL : String?
      if (bundle.containsKey("imgURL")) {
        __imgURL = bundle.getString("imgURL")
        if (__imgURL == null) {
          throw IllegalArgumentException("Argument \"imgURL\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"imgURL\" is missing and does not have an android:defaultValue")
      }
      val __desc : String?
      if (bundle.containsKey("desc")) {
        __desc = bundle.getString("desc")
        if (__desc == null) {
          throw IllegalArgumentException("Argument \"desc\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"desc\" is missing and does not have an android:defaultValue")
      }
      val __price : String?
      if (bundle.containsKey("price")) {
        __price = bundle.getString("price")
        if (__price == null) {
          throw IllegalArgumentException("Argument \"price\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"price\" is missing and does not have an android:defaultValue")
      }
      return ProductDetailsFragmentArgs(__productId, __imgURL, __desc, __price)
    }
  }
}
