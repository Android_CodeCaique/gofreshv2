package com.shoohna.gofresh.ui.home.ui.productDetails

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class ProductDetailsFragmentDirections private constructor() {
  companion object {
    fun actionProductDetailsFragmentToNotificationFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_productDetailsFragment_to_notificationFragment)

    fun actionProductDetailsFragmentToCartFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_productDetailsFragment_to_cartFragment)
  }
}
