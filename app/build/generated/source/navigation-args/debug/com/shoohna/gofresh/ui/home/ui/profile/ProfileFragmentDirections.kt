package com.shoohna.gofresh.ui.home.ui.profile

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class ProfileFragmentDirections private constructor() {
  companion object {
    fun actionProfileFragmentToChangePasswordProfileFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_profileFragment_to_changePasswordProfileFragment)

    fun actionProfileFragmentToHelpFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_profileFragment_to_helpFragment)

    fun actionProfileFragmentToOrderStatusFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_profileFragment_to_orderStatusFragment)
  }
}
