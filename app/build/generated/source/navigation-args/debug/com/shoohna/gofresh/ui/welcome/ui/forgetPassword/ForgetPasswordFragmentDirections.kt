package com.shoohna.gofresh.ui.welcome.ui.forgetPassword

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class ForgetPasswordFragmentDirections private constructor() {
  companion object {
    fun actionForgetPasswordFragmentToVerifyCodeFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_forgetPasswordFragment_to_verifyCodeFragment)
  }
}
