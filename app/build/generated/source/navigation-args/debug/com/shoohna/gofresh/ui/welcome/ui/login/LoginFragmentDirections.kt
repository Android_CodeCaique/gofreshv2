package com.shoohna.gofresh.ui.welcome.ui.login

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class LoginFragmentDirections private constructor() {
  companion object {
    fun actionLoginFragmentToRegisterFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_loginFragment_to_registerFragment)

    fun actionLoginFragmentToForgetPasswordFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_loginFragment_to_forgetPasswordFragment)
  }
}
