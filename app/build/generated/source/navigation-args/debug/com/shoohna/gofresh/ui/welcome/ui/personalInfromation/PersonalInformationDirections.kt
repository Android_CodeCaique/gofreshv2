package com.shoohna.gofresh.ui.welcome.ui.personalInfromation

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class PersonalInformationDirections private constructor() {
  companion object {
    fun actionPersonalInformationToAddFamilyRegister(): NavDirections =
        ActionOnlyNavDirections(R.id.action_personalInformation_to_addFamilyRegister)
  }
}
