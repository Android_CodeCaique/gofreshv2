package com.shoohna.gofresh.ui.welcome.ui.register

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class RegisterFragmentDirections private constructor() {
  companion object {
    fun actionRegisterFragmentToLoginFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_registerFragment_to_loginFragment)

    fun actionRegisterFragmentToRegisterIntro(): NavDirections =
        ActionOnlyNavDirections(R.id.action_registerFragment_to_registerIntro)
  }
}
