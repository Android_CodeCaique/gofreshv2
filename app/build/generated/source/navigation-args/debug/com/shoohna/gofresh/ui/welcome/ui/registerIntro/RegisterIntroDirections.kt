package com.shoohna.gofresh.ui.welcome.ui.registerIntro

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class RegisterIntroDirections private constructor() {
  companion object {
    fun actionRegisterIntroToPersonalInformation(): NavDirections =
        ActionOnlyNavDirections(R.id.action_registerIntro_to_personalInformation)
  }
}
