package com.shoohna.gofresh.ui.welcome.ui.resetPassword

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class ResetPasswordFragmentDirections private constructor() {
  companion object {
    fun actionResetPasswordFragmentToLoginFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_resetPasswordFragment_to_loginFragment)

    fun actionResetPasswordFragmentToLoginFragment2(): NavDirections =
        ActionOnlyNavDirections(R.id.action_resetPasswordFragment_to_loginFragment2)
  }
}
