package com.shoohna.gofresh.ui.welcome.ui.splash

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class SplashFragmentDirections private constructor() {
  companion object {
    fun actionSplashFragmentToWelcomeFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_splashFragment_to_welcomeFragment)
  }
}
