package com.shoohna.gofresh.ui.welcome.ui.verifyCode

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class VerifyCodeFragmentDirections private constructor() {
  companion object {
    fun actionVerifyCodeFragmentToResetPasswordFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_verifyCodeFragment_to_resetPasswordFragment)
  }
}
