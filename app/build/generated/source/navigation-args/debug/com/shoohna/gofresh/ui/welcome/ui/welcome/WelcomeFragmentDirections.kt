package com.shoohna.gofresh.ui.welcome.ui.welcome

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.shoohna.gofresh.R

class WelcomeFragmentDirections private constructor() {
  companion object {
    fun actionWelcomeFragmentToLoginFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_welcomeFragment_to_loginFragment)

    fun actionWelcomeFragmentToRegisterFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_welcomeFragment_to_registerFragment)
  }
}
