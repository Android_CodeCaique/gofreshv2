
import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b)\n\u0002\u0018\u0002\n\u0002\b\u0017\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\bR\u0011\u0010\r\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\bR\u0011\u0010\u000f\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\bR\u0011\u0010\u0011\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\bR\u0011\u0010\u0013\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\bR\u0011\u0010\u0015\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\bR\u0011\u0010\u0017\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\bR\u0011\u0010\u0019\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\bR\u0011\u0010\u001b\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\bR\u0011\u0010\u001d\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\bR\u0011\u0010\u001f\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\bR\u0011\u0010!\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\bR\u0011\u0010#\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\bR\u0011\u0010%\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\bR\u0011\u0010\'\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\bR\u0011\u0010)\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\bR\u0011\u0010+\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010\bR\u0011\u0010-\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010\bR\u001a\u0010/\u001a\u000200X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u00102\"\u0004\b3\u00104R\u0011\u00105\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b6\u0010\bR\u0011\u00107\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u0010\bR\u0011\u00109\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b:\u0010\bR\u0011\u0010;\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b<\u0010\bR\u0011\u0010=\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b>\u0010\bR\u0011\u0010?\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b@\u0010\bR\u0011\u0010A\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\bB\u0010\bR\u0011\u0010C\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\bD\u0010\bR\u0011\u0010E\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\bF\u0010\b\u00a8\u0006G"}, d2 = {"LApiClient;", "", "()V", "BASE_URL", "", "addCardModule", "Lorg/koin/core/module/Module;", "getAddCardModule", "()Lorg/koin/core/module/Module;", "setAddCardModule", "(Lorg/koin/core/module/Module;)V", "homeGeneralAboutUs", "getHomeGeneralAboutUs", "homeGeneralContactUs", "getHomeGeneralContactUs", "homeGeneralModule", "getHomeGeneralModule", "homeServiceModule", "getHomeServiceModule", "homeServiceModuleCart", "getHomeServiceModuleCart", "homeServiceModuleFavorite", "getHomeServiceModuleFavorite", "homeServiceModuleFilter", "getHomeServiceModuleFilter", "homeServiceModuleHome", "getHomeServiceModuleHome", "homeServiceModuleMainActivity", "getHomeServiceModuleMainActivity", "homeServiceModuleMoreSetting", "getHomeServiceModuleMoreSetting", "homeServiceModuleNotification", "getHomeServiceModuleNotification", "homeServiceModuleOrderData", "getHomeServiceModuleOrderData", "homeServiceModuleOrderStatus", "getHomeServiceModuleOrderStatus", "homeServiceModulePaymentWay", "getHomeServiceModulePaymentWay", "homeServiceModuleProduct", "getHomeServiceModuleProduct", "homeServiceModuleProductDetails", "getHomeServiceModuleProductDetails", "homeServiceModuleShipping", "getHomeServiceModuleShipping", "homeServiceModuleWallet", "getHomeServiceModuleWallet", "sharedHelper", "Lcom/shoohna/gofresh/util/base/SharedHelper;", "getSharedHelper", "()Lcom/shoohna/gofresh/util/base/SharedHelper;", "setSharedHelper", "(Lcom/shoohna/gofresh/util/base/SharedHelper;)V", "welcomeModule", "getWelcomeModule", "welcomeModuleChangePasswordHome", "getWelcomeModuleChangePasswordHome", "welcomeModuleForgetPassword", "getWelcomeModuleForgetPassword", "welcomeModuleLogin", "getWelcomeModuleLogin", "welcomeModuleMore", "getWelcomeModuleMore", "welcomeModuleProfile", "getWelcomeModuleProfile", "welcomeModuleRegister", "getWelcomeModuleRegister", "welcomeModuleResetPassword", "getWelcomeModuleResetPassword", "welcomeModuleVerifyCode", "getWelcomeModuleVerifyCode", "app_debug"})
public final class ApiClient {
    @org.jetbrains.annotations.NotNull()
    private static com.shoohna.gofresh.util.base.SharedHelper sharedHelper;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_URL = "http://phone.shoohna.com/api/";
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModule = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModuleLogin = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModuleRegister = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModuleForgetPassword = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModuleResetPassword = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModuleVerifyCode = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModuleChangePasswordHome = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModuleMore = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module welcomeModuleProfile = null;
    @org.jetbrains.annotations.NotNull()
    private static org.koin.core.module.Module addCardModule;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModule = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleCart = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModulePaymentWay = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleShipping = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleFavorite = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleHome = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleFilter = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleMoreSetting = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleNotification = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleOrderData = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleOrderStatus = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleProduct = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleProductDetails = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleMainActivity = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeServiceModuleWallet = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeGeneralModule = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeGeneralContactUs = null;
    @org.jetbrains.annotations.NotNull()
    private static final org.koin.core.module.Module homeGeneralAboutUs = null;
    public static final ApiClient INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.util.base.SharedHelper getSharedHelper() {
        return null;
    }
    
    public final void setSharedHelper(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.util.base.SharedHelper p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModule() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModuleLogin() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModuleRegister() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModuleForgetPassword() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModuleResetPassword() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModuleVerifyCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModuleChangePasswordHome() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModuleMore() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getWelcomeModuleProfile() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getAddCardModule() {
        return null;
    }
    
    public final void setAddCardModule(@org.jetbrains.annotations.NotNull()
    org.koin.core.module.Module p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModule() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleCart() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModulePaymentWay() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleShipping() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleFavorite() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleHome() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleFilter() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleMoreSetting() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleNotification() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleOrderData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleOrderStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleProduct() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleProductDetails() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleMainActivity() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeServiceModuleWallet() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeGeneralModule() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeGeneralContactUs() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.koin.core.module.Module getHomeGeneralAboutUs() {
        return null;
    }
    
    private ApiClient() {
        super();
    }
}