package com.shoohna.gofresh.networking.interfaces;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J+\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ+\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJI\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\r\u001a\u00020\u00062\b\b\u0001\u0010\u000e\u001a\u00020\u00062\b\b\u0001\u0010\u000f\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/shoohna/gofresh/networking/interfaces/General;", "", "aboutUs", "Lretrofit2/Response;", "Lcom/shoohna/gofresh/pojo/responses/AboutUsResponse2;", "lang", "", "shop_id", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "aboutUsFunction", "Lcom/shoohna/gofresh/pojo/responses/AboutUsResponse;", "contactUsFunction", "Lcom/shoohna/gofresh/util/base/BaseResponse;", "phone", "message", "email", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface General {
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "general/get_info")
    public abstract java.lang.Object aboutUsFunction(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "shop_id")
    java.lang.String shop_id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.AboutUsResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "general/contact_us")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object contactUsFunction(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "shop_id")
    java.lang.String shop_id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "phone")
    java.lang.String phone, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "message")
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "email")
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.util.base.BaseResponse>> p5);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "general/about_us")
    public abstract java.lang.Object aboutUs(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "shop_id")
    java.lang.String shop_id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.AboutUsResponse2>> p2);
}