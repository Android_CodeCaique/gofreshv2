package com.shoohna.gofresh.networking.interfaces;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00e4\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J+\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ5\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJI\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\u0010\u001a\u00020\f2\b\b\u0001\u0010\u0011\u001a\u00020\f2\b\b\u0001\u0010\u0012\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J?\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\u0010\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016JI\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\u0019\u001a\u00020\u001a2\b\b\u0001\u0010\u001b\u001a\u00020\u001a2\b\b\u0001\u0010\u001c\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001dJS\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010 \u001a\u00020\f2\b\b\u0001\u0010!\u001a\u00020\f2\b\b\u0001\u0010\"\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010#J5\u0010$\u001a\b\u0012\u0004\u0012\u00020%0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ]\u0010&\u001a\b\u0012\u0004\u0012\u00020\'0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010(\u001a\u00020\f2\b\b\u0001\u0010)\u001a\u00020\f2\b\b\u0001\u0010*\u001a\u00020\f2\b\b\u0001\u0010\u0019\u001a\u00020\u001a2\b\b\u0001\u0010+\u001a\u00020\u001a2\b\b\u0001\u0010,\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010-J5\u0010.\u001a\b\u0012\u0004\u0012\u00020/0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010*\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ!\u00100\u001a\b\u0012\u0004\u0012\u0002010\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00102J5\u00103\u001a\b\u0012\u0004\u0012\u0002040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ!\u00105\u001a\b\u0012\u0004\u0012\u0002060\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00102J!\u00107\u001a\b\u0012\u0004\u0012\u0002080\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00102J!\u00109\u001a\b\u0012\u0004\u0012\u00020\'0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00102J+\u0010:\u001a\b\u0012\u0004\u0012\u00020;0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ+\u0010<\u001a\b\u0012\u0004\u0012\u00020=0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ+\u0010>\u001a\b\u0012\u0004\u0012\u00020?0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ+\u0010@\u001a\b\u0012\u0004\u0012\u00020A0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ+\u0010B\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ5\u0010C\u001a\b\u0012\u0004\u0012\u00020D0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010E\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010FJ5\u0010G\u001a\b\u0012\u0004\u0012\u00020H0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010I\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010JJ5\u0010K\u001a\b\u0012\u0004\u0012\u00020L0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010JJ?\u0010M\u001a\b\u0012\u0004\u0012\u00020N0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010O\u001a\u00020\f2\b\b\u0001\u0010\u0011\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010PJS\u0010Q\u001a\b\u0012\u0004\u0012\u00020N0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010O\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\u0019\u001a\u00020\u001a2\b\b\u0001\u0010\u001b\u001a\u00020\u001a2\b\b\u0001\u0010\u001c\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010RJ+\u0010S\u001a\b\u0012\u0004\u0012\u00020T0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u000b\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010UJ?\u0010V\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010O\u001a\u00020\f2\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010W\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J+\u0010X\u001a\b\u0012\u0004\u0012\u00020Y0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ5\u0010Z\u001a\b\u0012\u0004\u0012\u00020/0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010[\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010FJ5\u0010\\\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010]\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010FJ5\u0010^\u001a\b\u0012\u0004\u0012\u00020_0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010`\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010JJ+\u0010a\u001a\b\u0012\u0004\u0012\u00020_0\u00032\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ5\u0010b\u001a\b\u0012\u0004\u0012\u00020_0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010]\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010JJ5\u0010c\u001a\b\u0012\u0004\u0012\u00020_0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010d\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010JJ+\u0010e\u001a\b\u0012\u0004\u0012\u00020_0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ+\u0010f\u001a\b\u0012\u0004\u0012\u00020g0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ5\u0010h\u001a\b\u0012\u0004\u0012\u00020i0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010j\u001a\u00020kH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010l\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006m"}, d2 = {"Lcom/shoohna/gofresh/networking/interfaces/Home;", "", "CheckoutCart", "Lretrofit2/Response;", "Lcom/shoohna/gofresh/pojo/responses/CheckoutCartResponse;", "lang", "", "Authorization", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addOrDeleteFromMyWishlist", "Lcom/shoohna/gofresh/util/base/BaseResponse;", "id", "", "(Ljava/lang/String;ILjava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addOrderOnce", "Lcom/shoohna/gofresh/pojo/responses/AddOrderOnceResponse;", "payment_method", "address_id", "code_id", "(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addPaymentMethod", "Lcom/shoohna/gofresh/pojo/responses/PaymentMethodResponse;", "(Ljava/lang/String;ILjava/lang/String;ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addSingleAddress", "Lcom/shoohna/gofresh/pojo/responses/SingleAddressResponse;", "lat", "", "lon", "address", "(Ljava/lang/String;Ljava/lang/String;DDLjava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addToCart", "Lcom/shoohna/gofresh/pojo/responses/AddToCartResponse;", "quantity", "color_id", "size_id", "(Ljava/lang/String;ILjava/lang/String;IIILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "deleteFromCart", "Lcom/shoohna/gofresh/pojo/responses/DeleteFromCartResponse;", "filterData", "Lcom/shoohna/gofresh/pojo/responses/HomeResponse;", "from", "to", "type", "lng", "cat_id", "(Ljava/lang/String;IIIDDILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getAllProductByType", "Lcom/shoohna/gofresh/pojo/responses/ProductByTypeResponse;", "getCategory", "Lcom/shoohna/gofresh/pojo/responses/categoryResponse;", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getCategoryProduct", "Lcom/shoohna/gofresh/pojo/responses/ProductCategoryResponse;", "getCurrencies", "Lcom/shoohna/gofresh/pojo/responses/CurrenciesResponse;", "getFilterMainCategorie", "Lcom/shoohna/gofresh/pojo/responses/FilterMainCategorieResponse;", "getHomeData", "getMyCart", "Lcom/shoohna/gofresh/pojo/responses/MyCartResponse;", "getMyWishlist", "Lcom/shoohna/gofresh/pojo/responses/MyWishlistResponse;", "getNotification", "Lcom/shoohna/gofresh/pojo/responses/NotificationResponse;", "getPerviousMessage", "Lcom/shoohna/gofresh/pojo/responses/PerviousChatResponse;", "logoutFunction", "makeDiscount", "Lcom/shoohna/gofresh/pojo/responses/MakeDiscountResponse;", "code", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "myOrders", "Lcom/shoohna/gofresh/pojo/responses/MyOrdersResponse;", "page", "(Ljava/lang/String;Ljava/lang/String;ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "myOrdersProducts", "Lcom/shoohna/gofresh/pojo/responses/MyOrderProductsResponse;", "orderAddressWithAddressId", "Lcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressResponse;", "pro_id", "(Ljava/lang/String;IILjava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "orderAddressWithoutAddressId", "(Ljava/lang/String;ILjava/lang/String;DDLjava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "productDetails", "Lcom/shoohna/gofresh/pojo/responses/ProductDetailsResponse;", "(Ljava/lang/String;ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "saveRate", "rate", "saveShippingInfo", "Lcom/shoohna/gofresh/pojo/responses/CheckoutShippingResponse;", "searchByName", "name", "sendMessageChat", "message", "settingCurrency", "Lcom/shoohna/gofresh/pojo/responses/SettingResponse;", "currency_id", "settingLang", "settingMessage", "settingNotification", "notification", "settingUser", "showUserAddress", "Lcom/shoohna/gofresh/pojo/responses/ShowUserAddressResponse;", "updateCart", "Lcom/shoohna/gofresh/pojo/responses/UpdateCartResponse;", "pro", "Lokhttp3/RequestBody;", "(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface Home {
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Home/get_main_cat")
    public abstract java.lang.Object getCategory(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.categoryResponse>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Product/products_by_category/{id}")
    public abstract java.lang.Object getCategoryProduct(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "id")
    int id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.ProductCategoryResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Home/home")
    public abstract java.lang.Object getHomeData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.HomeResponse>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Home/filter")
    public abstract java.lang.Object filterData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Query(value = "from")
    int from, @retrofit2.http.Query(value = "to")
    int to, @retrofit2.http.Query(value = "type")
    int type, @retrofit2.http.Query(value = "lat")
    double lat, @retrofit2.http.Query(value = "lng")
    double lng, @retrofit2.http.Query(value = "cat_id")
    int cat_id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.HomeResponse>> p7);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Product/product_details/{id}")
    public abstract java.lang.Object productDetails(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "id")
    int id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.ProductDetailsResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Product/add_to_cart/{id}")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object addToCart(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "id")
    int id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "quantity")
    int quantity, @retrofit2.http.Field(value = "color_id")
    int color_id, @retrofit2.http.Field(value = "size_id")
    int size_id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.AddToCartResponse>> p6);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Home/get_main_cat")
    public abstract java.lang.Object getFilterMainCategorie(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.FilterMainCategorieResponse>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Auth_private/my_cart")
    public abstract java.lang.Object getMyCart(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.MyCartResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Product/delete_from_cart/{id}")
    public abstract java.lang.Object deleteFromCart(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "id")
    int id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.DeleteFromCartResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Order/make_order")
    public abstract java.lang.Object CheckoutCart(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.CheckoutCartResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Order/make_order")
    public abstract java.lang.Object saveShippingInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.CheckoutShippingResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Auth_private/my_wishlist")
    public abstract java.lang.Object getMyWishlist(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.MyWishlistResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Product/get_products")
    public abstract java.lang.Object getAllProductByType(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Query(value = "type")
    int type, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.ProductByTypeResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Product/wishlist/{id}")
    public abstract java.lang.Object addOrDeleteFromMyWishlist(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "id")
    int id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.util.base.BaseResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Product/update_cart")
    public abstract java.lang.Object updateCart(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    okhttp3.RequestBody pro, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.UpdateCartResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Home/search_by_name")
    public abstract java.lang.Object searchByName(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "name")
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.ProductByTypeResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Order/user_address")
    public abstract java.lang.Object showUserAddress(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.ShowUserAddressResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Order/order_address/{pro_id}")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object orderAddressWithAddressId(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "pro_id")
    int pro_id, @retrofit2.http.Field(value = "address_id")
    int address_id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressResponse>> p4);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Order/order_address/{pro_id}")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object orderAddressWithoutAddressId(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "pro_id")
    int pro_id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "lat")
    double lat, @retrofit2.http.Field(value = "lng")
    double lon, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "address")
    java.lang.String address, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressResponse>> p6);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Order/add_payment_method/{pro_id}")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object addPaymentMethod(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "pro_id")
    int id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "payment_method")
    int payment_method, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.PaymentMethodResponse>> p4);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "message/send_message")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object sendMessageChat(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "message")
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.util.base.BaseResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "message/get_message")
    public abstract java.lang.Object getPerviousMessage(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.PerviousChatResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "rate_comment/save_my_rate/{pro_id}")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object saveRate(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @retrofit2.http.Path(value = "pro_id")
    int pro_id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "rate")
    int rate, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.util.base.BaseResponse>> p4);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Auth_private/settings")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object settingLang(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.SettingResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Auth_private/settings")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object settingMessage(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "message")
    int message, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.SettingResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Auth_private/settings")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object settingNotification(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "notification")
    int notification, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.SettingResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Auth_private/settings")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object settingCurrency(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "currency_id")
    int currency_id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.SettingResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Auth_private/settings")
    public abstract java.lang.Object settingUser(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.SettingResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "general/get_currencies")
    public abstract java.lang.Object getCurrencies(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.CurrenciesResponse>> p1);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Order/check_discount_code")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object makeDiscount(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "code")
    java.lang.String code, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.MakeDiscountResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Auth_private/get_notification")
    public abstract java.lang.Object getNotification(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.NotificationResponse>> p2);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Auth_private/add_address")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object addSingleAddress(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "lat")
    double lat, @retrofit2.http.Field(value = "lng")
    double lon, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "address")
    java.lang.String address, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.SingleAddressResponse>> p5);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "Order/add_order_once")
    @retrofit2.http.FormUrlEncoded()
    public abstract java.lang.Object addOrderOnce(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Field(value = "payment_method")
    int payment_method, @retrofit2.http.Field(value = "address_id")
    int address_id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "code_id")
    java.lang.String code_id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.AddOrderOnceResponse>> p5);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Order/my_order")
    public abstract java.lang.Object myOrders(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Query(value = "page")
    int page, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.MyOrdersResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Order/single_order/{id}")
    public abstract java.lang.Object myOrdersProducts(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @retrofit2.http.Path(value = "id")
    int id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.pojo.responses.MyOrderProductsResponse>> p3);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "Auth_private/logout")
    public abstract java.lang.Object logoutFunction(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "lang")
    java.lang.String lang, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String Authorization, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shoohna.gofresh.util.base.BaseResponse>> p2);
}