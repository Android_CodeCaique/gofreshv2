package com.shoohna.gofresh.networking.interfaces;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\'J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u000bH\'J\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u000b2\u0006\u0010\b\u001a\u00020\tH\'J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000bH\'J\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u000b2\u0006\u0010\b\u001a\u00020\tH\'J\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\tH\'\u00a8\u0006\u0014"}, d2 = {"Lcom/shoohna/gofresh/networking/interfaces/ProductDao;", "", "deleteAllProductDatabase", "", "deleteProduct", "productEntity", "Lcom/shoohna/gofresh/pojo/model/ProductEntity;", "deleteProductById", "productId", "", "getAllProducts", "", "getAllProductsById", "getAllSelectedProducts", "Lcom/shoohna/gofresh/pojo/model/ProductSelectionCart;", "getCurrentQuantityOfProductId", "insertProduct", "updateProduct", "updateQuantityOfProduct", "quantity", "app_debug"})
public abstract interface ProductDao {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM ProductEntity")
    public abstract java.util.List<com.shoohna.gofresh.pojo.model.ProductEntity> getAllProducts();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM ProductEntity WHERE product_id = :productId")
    public abstract java.util.List<com.shoohna.gofresh.pojo.model.ProductEntity> getAllProductsById(int productId);
    
    @androidx.room.Query(value = "DELETE FROM ProductEntity WHERE product_id = :productId")
    public abstract void deleteProductById(int productId);
    
    @androidx.room.Query(value = "UPDATE ProductEntity SET quantity = :quantity WHERE product_id = :productId")
    public abstract void updateQuantityOfProduct(int quantity, int productId);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM ProductEntity WHERE product_id =:productId")
    public abstract java.util.List<com.shoohna.gofresh.pojo.model.ProductEntity> getCurrentQuantityOfProductId(int productId);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT product_id , color_id , size_id , quantity FROM ProductEntity")
    public abstract java.util.List<com.shoohna.gofresh.pojo.model.ProductSelectionCart> getAllSelectedProducts();
    
    @androidx.room.Query(value = "DELETE FROM ProductEntity")
    public abstract void deleteAllProductDatabase();
    
    @androidx.room.Update()
    public abstract void updateProduct(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.pojo.model.ProductEntity productEntity);
    
    @androidx.room.Insert()
    public abstract void insertProduct(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.pojo.model.ProductEntity productEntity);
    
    @androidx.room.Delete()
    public abstract void deleteProduct(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.pojo.model.ProductEntity productEntity);
}