package com.shoohna.gofresh.pojo.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u00a8\u0006\r"}, d2 = {"Lcom/shoohna/gofresh/pojo/model/Chat;", "", "Content", "", "Time", "SenderInt", "", "(Ljava/lang/String;Ljava/lang/String;I)V", "getContent", "()Ljava/lang/String;", "getSenderInt", "()I", "getTime", "app_debug"})
public final class Chat {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String Content = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String Time = null;
    private final int SenderInt = 0;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getContent() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTime() {
        return null;
    }
    
    public final int getSenderInt() {
        return 0;
    }
    
    public Chat(@org.jetbrains.annotations.NotNull()
    java.lang.String Content, @org.jetbrains.annotations.NotNull()
    java.lang.String Time, int SenderInt) {
        super();
    }
}