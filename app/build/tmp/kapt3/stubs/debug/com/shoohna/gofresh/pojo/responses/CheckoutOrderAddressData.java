package com.shoohna.gofresh.pojo.responses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b#\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001Bs\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0001\u0012\u0006\u0010\b\u001a\u00020\u0001\u0012\u0006\u0010\t\u001a\u00020\u0001\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0006\u0010\r\u001a\u00020\u0001\u0012\u0006\u0010\u000e\u001a\u00020\u0001\u0012\u0006\u0010\u000f\u001a\u00020\u0006\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0006\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\u0006H\u00c6\u0003J\t\u0010+\u001a\u00020\u0011H\u00c6\u0003J\t\u0010,\u001a\u00020\u0006H\u00c6\u0003J\t\u0010-\u001a\u00020\u0014H\u00c6\u0003J\t\u0010.\u001a\u00020\u0001H\u00c6\u0003J\t\u0010/\u001a\u00020\u0006H\u00c6\u0003J\t\u00100\u001a\u00020\u0001H\u00c6\u0003J\t\u00101\u001a\u00020\u0001H\u00c6\u0003J\t\u00102\u001a\u00020\u0001H\u00c6\u0003J\u000f\u00103\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u00c6\u0003J\t\u00104\u001a\u00020\u0001H\u00c6\u0003J\t\u00105\u001a\u00020\u0001H\u00c6\u0003J\u0091\u0001\u00106\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00012\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00012\b\b\u0002\u0010\b\u001a\u00020\u00012\b\b\u0002\u0010\t\u001a\u00020\u00012\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\b\b\u0002\u0010\r\u001a\u00020\u00012\b\b\u0002\u0010\u000e\u001a\u00020\u00012\b\b\u0002\u0010\u000f\u001a\u00020\u00062\b\b\u0002\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00062\b\b\u0002\u0010\u0013\u001a\u00020\u0014H\u00c6\u0001J\u0013\u00107\u001a\u0002082\b\u00109\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010:\u001a\u00020\u0006H\u00d6\u0001J\t\u0010;\u001a\u00020\u0011H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0007\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0019R\u0011\u0010\b\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0019R\u0011\u0010\t\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0019R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0011\u0010\r\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0019R\u0011\u0010\u000e\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0019R\u0011\u0010\u000f\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001bR\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010\u0012\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001bR\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010(\u00a8\u0006<"}, d2 = {"Lcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressData;", "", "address", "Lcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressAddress;", "email", "id", "", "name", "payment_method", "phone", "products", "", "Lcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressProduct;", "rate", "report", "shipping_price", "status", "", "total_price", "user", "Lcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressUser;", "(Lcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressAddress;Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/String;ILcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressUser;)V", "getAddress", "()Lcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressAddress;", "getEmail", "()Ljava/lang/Object;", "getId", "()I", "getName", "getPayment_method", "getPhone", "getProducts", "()Ljava/util/List;", "getRate", "getReport", "getShipping_price", "getStatus", "()Ljava/lang/String;", "getTotal_price", "getUser", "()Lcom/shoohna/gofresh/pojo/responses/CheckoutOrderAddressUser;", "component1", "component10", "component11", "component12", "component13", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "toString", "app_debug"})
public final class CheckoutOrderAddressData {
    @org.jetbrains.annotations.NotNull()
    private final com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressAddress address = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object email = null;
    private final int id = 0;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object name = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object payment_method = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object phone = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressProduct> products = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object rate = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object report = null;
    private final int shipping_price = 0;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String status = null;
    private final int total_price = 0;
    @org.jetbrains.annotations.NotNull()
    private final com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressUser user = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressAddress getAddress() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getEmail() {
        return null;
    }
    
    public final int getId() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getName() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getPayment_method() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getPhone() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressProduct> getProducts() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getRate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getReport() {
        return null;
    }
    
    public final int getShipping_price() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStatus() {
        return null;
    }
    
    public final int getTotal_price() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressUser getUser() {
        return null;
    }
    
    public CheckoutOrderAddressData(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressAddress address, @org.jetbrains.annotations.NotNull()
    java.lang.Object email, int id, @org.jetbrains.annotations.NotNull()
    java.lang.Object name, @org.jetbrains.annotations.NotNull()
    java.lang.Object payment_method, @org.jetbrains.annotations.NotNull()
    java.lang.Object phone, @org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressProduct> products, @org.jetbrains.annotations.NotNull()
    java.lang.Object rate, @org.jetbrains.annotations.NotNull()
    java.lang.Object report, int shipping_price, @org.jetbrains.annotations.NotNull()
    java.lang.String status, int total_price, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressUser user) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressAddress component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component2() {
        return null;
    }
    
    public final int component3() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressProduct> component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component9() {
        return null;
    }
    
    public final int component10() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component11() {
        return null;
    }
    
    public final int component12() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressUser component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressData copy(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressAddress address, @org.jetbrains.annotations.NotNull()
    java.lang.Object email, int id, @org.jetbrains.annotations.NotNull()
    java.lang.Object name, @org.jetbrains.annotations.NotNull()
    java.lang.Object payment_method, @org.jetbrains.annotations.NotNull()
    java.lang.Object phone, @org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressProduct> products, @org.jetbrains.annotations.NotNull()
    java.lang.Object rate, @org.jetbrains.annotations.NotNull()
    java.lang.Object report, int shipping_price, @org.jetbrains.annotations.NotNull()
    java.lang.String status, int total_price, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.pojo.responses.CheckoutOrderAddressUser user) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}