package com.shoohna.gofresh.pojo.responses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b+\b\u0086\b\u0018\u00002\u00020\u0001B\u0097\u0001\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0006\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0003\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0006\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\u0006\u0010\u0013\u001a\u00020\u0006\u0012\u0006\u0010\u0014\u001a\u00020\u0006\u0012\u0006\u0010\u0015\u001a\u00020\u0006\u0012\u0006\u0010\u0016\u001a\u00020\u0001\u0012\u0006\u0010\u0017\u001a\u00020\u0006\u0012\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u0003\u00a2\u0006\u0002\u0010\u001aJ\u000f\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u00100\u001a\u00020\u0011H\u00c6\u0003J\t\u00101\u001a\u00020\u0006H\u00c6\u0003J\t\u00102\u001a\u00020\u0006H\u00c6\u0003J\t\u00103\u001a\u00020\u0006H\u00c6\u0003J\t\u00104\u001a\u00020\u0001H\u00c6\u0003J\t\u00105\u001a\u00020\u0006H\u00c6\u0003J\u000f\u00106\u001a\b\u0012\u0004\u0012\u00020\u00190\u0003H\u00c6\u0003J\t\u00107\u001a\u00020\u0006H\u00c6\u0003J\t\u00108\u001a\u00020\u0006H\u00c6\u0003J\t\u00109\u001a\u00020\tH\u00c6\u0003J\t\u0010:\u001a\u00020\u0006H\u00c6\u0003J\u000f\u0010;\u001a\b\u0012\u0004\u0012\u00020\f0\u0003H\u00c6\u0003J\t\u0010<\u001a\u00020\u000eH\u00c6\u0003J\t\u0010=\u001a\u00020\u0006H\u00c6\u0003J\t\u0010>\u001a\u00020\u0011H\u00c6\u0003J\u00bb\u0001\u0010?\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u00062\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u00032\b\b\u0002\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u00062\b\b\u0002\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\u0013\u001a\u00020\u00062\b\b\u0002\u0010\u0014\u001a\u00020\u00062\b\b\u0002\u0010\u0015\u001a\u00020\u00062\b\b\u0002\u0010\u0016\u001a\u00020\u00012\b\b\u0002\u0010\u0017\u001a\u00020\u00062\u000e\b\u0002\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u0003H\u00c6\u0001J\u0013\u0010@\u001a\u00020\u000e2\b\u0010A\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010B\u001a\u00020\tH\u00d6\u0001J\t\u0010C\u001a\u00020\u0006H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001eR\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\n\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001eR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001cR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010$R\u0011\u0010\u000f\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u001eR\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0011\u0010\u0012\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010&R\u0011\u0010\u0013\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001eR\u0011\u0010\u0014\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001eR\u0011\u0010\u0015\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001eR\u0011\u0010\u0016\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0011\u0010\u0017\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010\u001eR\u0017\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010\u001c\u00a8\u0006D"}, d2 = {"Lcom/shoohna/gofresh/pojo/responses/MyWishlistData;", "", "colors", "", "Lcom/shoohna/gofresh/pojo/responses/MyWishlistColor;", "desc", "", "distance", "id", "", "image", "images", "Lcom/shoohna/gofresh/pojo/responses/MyWishlistImage;", "is_favorite", "", "is_offer", "lat", "", "lng", "name", "offer_amount", "price_product", "quantity", "rate", "sizes", "Lcom/shoohna/gofresh/pojo/responses/MyWishlistSize;", "(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;ZLjava/lang/String;DDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/util/List;)V", "getColors", "()Ljava/util/List;", "getDesc", "()Ljava/lang/String;", "getDistance", "getId", "()I", "getImage", "getImages", "()Z", "getLat", "()D", "getLng", "getName", "getOffer_amount", "getPrice_product", "getQuantity", "()Ljava/lang/Object;", "getRate", "getSizes", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_debug"})
public final class MyWishlistData {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistColor> colors = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String desc = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String distance = null;
    private final int id = 0;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String image = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistImage> images = null;
    private final boolean is_favorite = false;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String is_offer = null;
    private final double lat = 0.0;
    private final double lng = 0.0;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String name = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String offer_amount = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String price_product = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object quantity = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String rate = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistSize> sizes = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistColor> getColors() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDesc() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDistance() {
        return null;
    }
    
    public final int getId() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getImage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistImage> getImages() {
        return null;
    }
    
    public final boolean is_favorite() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String is_offer() {
        return null;
    }
    
    public final double getLat() {
        return 0.0;
    }
    
    public final double getLng() {
        return 0.0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getName() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOffer_amount() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPrice_product() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getQuantity() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistSize> getSizes() {
        return null;
    }
    
    public MyWishlistData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistColor> colors, @org.jetbrains.annotations.NotNull()
    java.lang.String desc, @org.jetbrains.annotations.NotNull()
    java.lang.String distance, int id, @org.jetbrains.annotations.NotNull()
    java.lang.String image, @org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistImage> images, boolean is_favorite, @org.jetbrains.annotations.NotNull()
    java.lang.String is_offer, double lat, double lng, @org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    java.lang.String offer_amount, @org.jetbrains.annotations.NotNull()
    java.lang.String price_product, @org.jetbrains.annotations.NotNull()
    java.lang.Object quantity, @org.jetbrains.annotations.NotNull()
    java.lang.String rate, @org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistSize> sizes) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistColor> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    public final int component4() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistImage> component6() {
        return null;
    }
    
    public final boolean component7() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component8() {
        return null;
    }
    
    public final double component9() {
        return 0.0;
    }
    
    public final double component10() {
        return 0.0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component14() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistSize> component16() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.pojo.responses.MyWishlistData copy(@org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistColor> colors, @org.jetbrains.annotations.NotNull()
    java.lang.String desc, @org.jetbrains.annotations.NotNull()
    java.lang.String distance, int id, @org.jetbrains.annotations.NotNull()
    java.lang.String image, @org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistImage> images, boolean is_favorite, @org.jetbrains.annotations.NotNull()
    java.lang.String is_offer, double lat, double lng, @org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    java.lang.String offer_amount, @org.jetbrains.annotations.NotNull()
    java.lang.String price_product, @org.jetbrains.annotations.NotNull()
    java.lang.Object quantity, @org.jetbrains.annotations.NotNull()
    java.lang.String rate, @org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistSize> sizes) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}