package com.shoohna.gofresh.ui.home.ui.addCreditCard;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001d"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/addCreditCard/AddCreditCard;", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "()V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentAddCreditCardBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentAddCreditCardBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentAddCreditCardBinding;)V", "nav", "Landroidx/navigation/NavController;", "viewModel", "Lcom/shoohna/gofresh/ui/home/ui/addCreditCard/AddCardViewModel;", "getViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/addCreditCard/AddCardViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "showAlert2", "", "context", "Landroid/content/Context;", "app_debug"})
public final class AddCreditCard extends com.shoohna.shoohna.util.base.BaseFragment {
    private androidx.navigation.NavController nav;
    private final kotlin.Lazy viewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentAddCreditCardBinding binding;
    private java.util.HashMap _$_findViewCache;
    
    private final com.shoohna.gofresh.ui.home.ui.addCreditCard.AddCardViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentAddCreditCardBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentAddCreditCardBinding p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public final void showAlert2(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public AddCreditCard() {
        super();
    }
}