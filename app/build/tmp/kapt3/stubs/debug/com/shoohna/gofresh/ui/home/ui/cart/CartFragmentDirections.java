package com.shoohna.gofresh.ui.home.ui.cart;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/cart/CartFragmentDirections;", "", "()V", "Companion", "app_debug"})
public final class CartFragmentDirections {
    public static final com.shoohna.gofresh.ui.home.ui.cart.CartFragmentDirections.Companion Companion = null;
    
    private CartFragmentDirections() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0005\u001a\u00020\u0004J\u0006\u0010\u0006\u001a\u00020\u0004\u00a8\u0006\u0007"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/cart/CartFragmentDirections$Companion;", "", "()V", "actionCartFragmentToAddCreditCard", "Landroidx/navigation/NavDirections;", "actionCartFragmentToCheckoutProcessFragment", "actionCartFragmentToMyAddress", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionCartFragmentToCheckoutProcessFragment() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionCartFragmentToAddCreditCard() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionCartFragmentToMyAddress() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}