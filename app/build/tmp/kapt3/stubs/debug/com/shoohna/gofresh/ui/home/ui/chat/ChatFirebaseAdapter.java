package com.shoohna.gofresh.ui.home.ui.chat;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00020\u0001:\u0002\u0015\u0016B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\b\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000bH\u0016J\u001c\u0010\u000e\u001a\u00020\u000f2\n\u0010\u0010\u001a\u0006\u0012\u0002\b\u00030\u00022\u0006\u0010\r\u001a\u00020\u000bH\u0016J\u001c\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000bH\u0016R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\u0006\u00a8\u0006\u0017"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/chat/ChatFirebaseAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/shoohna/gofresh/ui/home/ui/chat/MessageViewHolder;", "data", "", "Lcom/shoohna/gofresh/pojo/model/Chat;", "(Ljava/util/List;)V", "getData", "()Ljava/util/List;", "setData", "getItemCount", "", "getItemViewType", "position", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ApplicationMessageViewHolder", "MyMessageViewHolder", "app_debug"})
public final class ChatFirebaseAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.shoohna.gofresh.ui.home.ui.chat.MessageViewHolder<?>> {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.shoohna.gofresh.pojo.model.Chat> data;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.shoohna.gofresh.ui.home.ui.chat.MessageViewHolder<?> onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.chat.MessageViewHolder<?> holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.model.Chat> getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.model.Chat> p0) {
    }
    
    public ChatFirebaseAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.model.Chat> data) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u0016\u0010\u0006\u001a\n \b*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \b*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000f"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/chat/ChatFirebaseAdapter$MyMessageViewHolder;", "Lcom/shoohna/gofresh/ui/home/ui/chat/MessageViewHolder;", "Lcom/shoohna/gofresh/pojo/model/Chat;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "messageContent", "Landroid/widget/TextView;", "kotlin.jvm.PlatformType", "time", "getView", "()Landroid/view/View;", "bind", "", "item", "app_debug"})
    public static final class MyMessageViewHolder extends com.shoohna.gofresh.ui.home.ui.chat.MessageViewHolder<com.shoohna.gofresh.pojo.model.Chat> {
        private final android.widget.TextView messageContent = null;
        private final android.widget.TextView time = null;
        @org.jetbrains.annotations.NotNull()
        private final android.view.View view = null;
        
        @java.lang.Override()
        public void bind(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.pojo.model.Chat item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getView() {
            return null;
        }
        
        public MyMessageViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u0016\u0010\u0006\u001a\n \b*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \b*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000f"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/chat/ChatFirebaseAdapter$ApplicationMessageViewHolder;", "Lcom/shoohna/gofresh/ui/home/ui/chat/MessageViewHolder;", "Lcom/shoohna/gofresh/pojo/model/Chat;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "messageContent", "Landroid/widget/TextView;", "kotlin.jvm.PlatformType", "time", "getView", "()Landroid/view/View;", "bind", "", "item", "app_debug"})
    public static final class ApplicationMessageViewHolder extends com.shoohna.gofresh.ui.home.ui.chat.MessageViewHolder<com.shoohna.gofresh.pojo.model.Chat> {
        private final android.widget.TextView messageContent = null;
        private final android.widget.TextView time = null;
        @org.jetbrains.annotations.NotNull()
        private final android.view.View view = null;
        
        @java.lang.Override()
        public void bind(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.pojo.model.Chat item) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getView() {
            return null;
        }
        
        public ApplicationMessageViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
    }
}