package com.shoohna.gofresh.ui.home.ui.chat;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010B\u001a\u0004\u0018\u00010C2\u0006\u0010D\u001a\u00020E2\b\u0010F\u001a\u0004\u0018\u00010G2\b\u0010H\u001a\u0004\u0018\u00010IH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R \u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001a\u0010\u001c\u001a\u00020\u001dX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u001a\u0010\"\u001a\u00020#X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010\'R\u001a\u0010(\u001a\u00020#X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010%\"\u0004\b*\u0010\'R&\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020.0-0,X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R&\u00103\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170-0,X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u00100\"\u0004\b5\u00102R\u001a\u00106\u001a\u000207X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R\u001a\u0010<\u001a\u00020=X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010?\"\u0004\b@\u0010A\u00a8\u0006J"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/chat/ChatFragment;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentChatBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentChatBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentChatBinding;)V", "channel", "Lcom/pusher/client/channel/Channel;", "getChannel", "()Lcom/pusher/client/channel/Channel;", "setChannel", "(Lcom/pusher/client/channel/Channel;)V", "chatAdapter", "Lcom/shoohna/gofresh/ui/home/ui/chat/ChatAdapter;", "getChatAdapter", "()Lcom/shoohna/gofresh/ui/home/ui/chat/ChatAdapter;", "setChatAdapter", "(Lcom/shoohna/gofresh/ui/home/ui/chat/ChatAdapter;)V", "chatList", "Ljava/util/ArrayList;", "Lcom/shoohna/gofresh/ui/home/ui/chat/MessageItemUi;", "getChatList", "()Ljava/util/ArrayList;", "setChatList", "(Ljava/util/ArrayList;)V", "currentTime", "", "getCurrentTime", "()Ljava/lang/String;", "setCurrentTime", "(Ljava/lang/String;)V", "jsonObject", "Lorg/json/JSONObject;", "getJsonObject", "()Lorg/json/JSONObject;", "setJsonObject", "(Lorg/json/JSONObject;)V", "jsonObject1", "getJsonObject1", "setJsonObject1", "messageData", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/PerviousChatData;", "getMessageData", "()Landroidx/lifecycle/MutableLiveData;", "setMessageData", "(Landroidx/lifecycle/MutableLiveData;)V", "messageDataF", "getMessageDataF", "setMessageDataF", "options", "Lcom/pusher/client/PusherOptions;", "getOptions", "()Lcom/pusher/client/PusherOptions;", "setOptions", "(Lcom/pusher/client/PusherOptions;)V", "pusher", "Lcom/pusher/client/Pusher;", "getPusher", "()Lcom/pusher/client/Pusher;", "setPusher", "(Lcom/pusher/client/Pusher;)V", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class ChatFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentChatBinding binding;
    @org.jetbrains.annotations.NotNull()
    public java.lang.String currentTime;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi> chatList;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.ui.home.ui.chat.ChatAdapter chatAdapter;
    @org.jetbrains.annotations.NotNull()
    public com.pusher.client.PusherOptions options;
    @org.jetbrains.annotations.NotNull()
    public com.pusher.client.Pusher pusher;
    @org.jetbrains.annotations.NotNull()
    public com.pusher.client.channel.Channel channel;
    @org.jetbrains.annotations.NotNull()
    public org.json.JSONObject jsonObject;
    @org.jetbrains.annotations.NotNull()
    public org.json.JSONObject jsonObject1;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.PerviousChatData>> messageData;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi>> messageDataF;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentChatBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentChatBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCurrentTime() {
        return null;
    }
    
    public final void setCurrentTime(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi> getChatList() {
        return null;
    }
    
    public final void setChatList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.chat.ChatAdapter getChatAdapter() {
        return null;
    }
    
    public final void setChatAdapter(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.chat.ChatAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.pusher.client.PusherOptions getOptions() {
        return null;
    }
    
    public final void setOptions(@org.jetbrains.annotations.NotNull()
    com.pusher.client.PusherOptions p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.pusher.client.Pusher getPusher() {
        return null;
    }
    
    public final void setPusher(@org.jetbrains.annotations.NotNull()
    com.pusher.client.Pusher p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.pusher.client.channel.Channel getChannel() {
        return null;
    }
    
    public final void setChannel(@org.jetbrains.annotations.NotNull()
    com.pusher.client.channel.Channel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.json.JSONObject getJsonObject() {
        return null;
    }
    
    public final void setJsonObject(@org.jetbrains.annotations.NotNull()
    org.json.JSONObject p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.json.JSONObject getJsonObject1() {
        return null;
    }
    
    public final void setJsonObject1(@org.jetbrains.annotations.NotNull()
    org.json.JSONObject p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.PerviousChatData>> getMessageData() {
        return null;
    }
    
    public final void setMessageData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.PerviousChatData>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi>> getMessageDataF() {
        return null;
    }
    
    public final void setMessageDataF(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public ChatFragment() {
        super();
    }
}