package com.shoohna.gofresh.ui.home.ui.chat;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&J\u0016\u0010\'\u001a\u00020\"2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR(\u0010\u0010\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u0011\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\r\"\u0004\b\u0014\u0010\u000fR(\u0010\u0015\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u0011\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\r\"\u0004\b\u0018\u0010\u000fR*\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u001b0\u001aj\b\u0012\u0004\u0012\u00020\u001b`\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 \u00a8\u0006,"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/chat/ChatViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "()V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "loader", "Landroidx/lifecycle/MutableLiveData;", "", "getLoader", "()Landroidx/lifecycle/MutableLiveData;", "setLoader", "(Landroidx/lifecycle/MutableLiveData;)V", "messageList", "", "Lcom/shoohna/gofresh/pojo/responses/PerviousChatData;", "getMessageList", "setMessageList", "messageListF", "Lcom/shoohna/gofresh/ui/home/ui/chat/MessageItemUi;", "getMessageListF", "setMessageListF", "myMessages", "Ljava/util/ArrayList;", "Lcom/shoohna/gofresh/pojo/model/Chat;", "Lkotlin/collections/ArrayList;", "getMyMessages", "()Ljava/util/ArrayList;", "setMyMessages", "(Ljava/util/ArrayList;)V", "loadDataFirebase", "", "context", "Landroid/content/Context;", "messages", "Landroidx/recyclerview/widget/RecyclerView;", "sendMessageFirebase", "v", "Landroid/view/View;", "message", "Landroid/widget/EditText;", "app_debug"})
public final class ChatViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    @org.jetbrains.annotations.Nullable()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.PerviousChatData>> messageList;
    @org.jetbrains.annotations.Nullable()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi>> messageListF;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.shoohna.gofresh.pojo.model.Chat> myMessages;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.PerviousChatData>> getMessageList() {
        return null;
    }
    
    public final void setMessageList(@org.jetbrains.annotations.Nullable()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.PerviousChatData>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi>> getMessageListF() {
        return null;
    }
    
    public final void setMessageListF(@org.jetbrains.annotations.Nullable()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.shoohna.gofresh.pojo.model.Chat> getMyMessages() {
        return null;
    }
    
    public final void setMyMessages(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.shoohna.gofresh.pojo.model.Chat> p0) {
    }
    
    public final void loadDataFirebase(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView messages) {
    }
    
    public final void sendMessageFirebase(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    android.widget.EditText message) {
    }
    
    public ChatViewModel() {
        super();
    }
}