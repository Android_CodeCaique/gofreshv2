package com.shoohna.gofresh.ui.home.ui.checkOutProcess.myCart;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u00100\u001a\u0002012\u0006\u00102\u001a\u000203J\u000e\u00104\u001a\u0002012\u0006\u00105\u001a\u000206R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR(\u0010\f\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0014\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R \u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0010\"\u0004\b\u001a\u0010\u0012R\u0014\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00050\u001e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u001a\u0010!\u001a\u00020\"X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R \u0010\'\u001a\b\u0012\u0004\u0012\u00020(0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\u0010\"\u0004\b*\u0010\u0012R \u0010+\u001a\b\u0012\u0004\u0012\u00020(0\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\u0010\"\u0004\b-\u0010\u0012R\u0014\u0010.\u001a\b\u0012\u0004\u0012\u00020/0\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00067"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/myCart/MyCartViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "()V", "_productList", "Landroidx/lifecycle/MutableLiveData;", "Lcom/shoohna/gofresh/pojo/responses/CheckoutCartData;", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "checkoutCartList", "", "Lcom/shoohna/gofresh/pojo/model/ProductEntity;", "getCheckoutCartList", "()Landroidx/lifecycle/MutableLiveData;", "setCheckoutCartList", "(Landroidx/lifecycle/MutableLiveData;)V", "checkoutCartStringList", "Ljava/util/ArrayList;", "getCheckoutCartStringList", "()Ljava/util/ArrayList;", "loader", "", "getLoader", "setLoader", "productId", "", "productList", "Landroidx/lifecycle/LiveData;", "getProductList", "()Landroidx/lifecycle/LiveData;", "sharedHelper", "Lcom/shoohna/gofresh/util/base/SharedHelper;", "getSharedHelper", "()Lcom/shoohna/gofresh/util/base/SharedHelper;", "setSharedHelper", "(Lcom/shoohna/gofresh/util/base/SharedHelper;)V", "shippingPrice", "", "getShippingPrice", "setShippingPrice", "totalPrice", "getTotalPrice", "setTotalPrice", "userData", "Lcom/shoohna/gofresh/pojo/responses/CheckoutCartUser;", "loadDataFromRoom", "", "context", "Landroid/content/Context;", "nextClick", "v", "Landroid/view/View;", "app_debug"})
public final class MyCartViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    @org.jetbrains.annotations.Nullable()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.model.ProductEntity>> checkoutCartList;
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<com.shoohna.gofresh.pojo.model.ProductEntity> checkoutCartStringList = null;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    private androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.responses.CheckoutCartData> _productList;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<com.shoohna.gofresh.pojo.responses.CheckoutCartData> productList = null;
    private androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.responses.CheckoutCartUser> userData;
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> productId;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> totalPrice;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> shippingPrice;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.util.base.SharedHelper sharedHelper;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.model.ProductEntity>> getCheckoutCartList() {
        return null;
    }
    
    public final void setCheckoutCartList(@org.jetbrains.annotations.Nullable()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.model.ProductEntity>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.shoohna.gofresh.pojo.model.ProductEntity> getCheckoutCartStringList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.shoohna.gofresh.pojo.responses.CheckoutCartData> getProductList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getTotalPrice() {
        return null;
    }
    
    public final void setTotalPrice(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getShippingPrice() {
        return null;
    }
    
    public final void setShippingPrice(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.util.base.SharedHelper getSharedHelper() {
        return null;
    }
    
    public final void setSharedHelper(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.util.base.SharedHelper p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void loadDataFromRoom(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void nextClick(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public MyCartViewModel() {
        super();
    }
}