package com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0017"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/paymentWay/PaymentWayFragment;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentPaymentWayBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentPaymentWayBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentPaymentWayBinding;)V", "paymentWayViewModel", "Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/paymentWay/PaymentWayViewModel;", "getPaymentWayViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/paymentWay/PaymentWayViewModel;", "paymentWayViewModel$delegate", "Lkotlin/Lazy;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class PaymentWayFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentPaymentWayBinding binding;
    private final kotlin.Lazy paymentWayViewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentPaymentWayBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentPaymentWayBinding p0) {
    }
    
    private final com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel getPaymentWayViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public PaymentWayFragment() {
        super();
    }
}