package com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)J\u000e\u0010*\u001a\u00020\'2\u0006\u0010(\u001a\u00020)J\b\u0010+\u001a\u00020,H\u0002J\u000e\u0010-\u001a\u00020\'2\u0006\u0010.\u001a\u00020)J\u000e\u0010/\u001a\u00020\'2\u0006\u0010.\u001a\u00020)R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R \u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0012\"\u0004\b\u0017\u0010\u0014R \u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0012\"\u0004\b\u001a\u0010\u0014R\u0017\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00070\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0017\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00070\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u001eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010!\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%\u00a8\u00060"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/paymentWay/PaymentWayViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "serviceGeneral", "Lcom/shoohna/gofresh/networking/interfaces/Home;", "(Lcom/shoohna/gofresh/networking/interfaces/Home;)V", "_paymentOnDelivery", "Landroidx/lifecycle/MutableLiveData;", "", "_paymentOnline", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "discountCode", "", "getDiscountCode", "()Landroidx/lifecycle/MutableLiveData;", "setDiscountCode", "(Landroidx/lifecycle/MutableLiveData;)V", "discountProcessSuccess", "getDiscountProcessSuccess", "setDiscountProcessSuccess", "loader", "getLoader", "setLoader", "paymentOnDelivery", "Landroidx/lifecycle/LiveData;", "getPaymentOnDelivery", "()Landroidx/lifecycle/LiveData;", "paymentOnline", "getPaymentOnline", "textCobon", "getTextCobon", "()Ljava/lang/String;", "setTextCobon", "(Ljava/lang/String;)V", "allowPaymentOnDelivery", "", "view", "Landroid/view/View;", "allowPaymentOnline", "getFilterItem", "", "makeDiscount", "v", "sendPaymentMethod", "app_debug"})
public final class PaymentWayViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _paymentOnline;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Boolean> paymentOnline = null;
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _paymentOnDelivery;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Boolean> paymentOnDelivery = null;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> discountCode;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> discountProcessSuccess;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String textCobon;
    private final com.shoohna.gofresh.networking.interfaces.Home serviceGeneral = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getPaymentOnline() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getPaymentOnDelivery() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    public final void allowPaymentOnline(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void allowPaymentOnDelivery(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getDiscountCode() {
        return null;
    }
    
    public final void setDiscountCode(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getDiscountProcessSuccess() {
        return null;
    }
    
    public final void setDiscountProcessSuccess(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTextCobon() {
        return null;
    }
    
    public final void setTextCobon(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final void sendPaymentMethod(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    private final int getFilterItem() {
        return 0;
    }
    
    public final void makeDiscount(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public PaymentWayViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.networking.interfaces.Home serviceGeneral) {
        super();
    }
}