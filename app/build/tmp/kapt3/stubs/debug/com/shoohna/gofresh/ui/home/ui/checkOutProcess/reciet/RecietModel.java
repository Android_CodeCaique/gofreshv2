package com.shoohna.gofresh.ui.home.ui.checkOutProcess.reciet;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0002\b\t\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0011"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/reciet/RecietModel;", "", "img", "", "productName", "price", "", "quantity", "", "(Ljava/lang/String;Ljava/lang/String;FI)V", "getImg", "()Ljava/lang/String;", "getPrice", "()F", "getProductName", "getQuantity", "()I", "app_debug"})
public final class RecietModel {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String img = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String productName = null;
    private final float price = 0.0F;
    private final int quantity = 0;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getImg() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getProductName() {
        return null;
    }
    
    public final float getPrice() {
        return 0.0F;
    }
    
    public final int getQuantity() {
        return 0;
    }
    
    public RecietModel(@org.jetbrains.annotations.NotNull()
    java.lang.String img, @org.jetbrains.annotations.NotNull()
    java.lang.String productName, float price, int quantity) {
        super();
    }
}