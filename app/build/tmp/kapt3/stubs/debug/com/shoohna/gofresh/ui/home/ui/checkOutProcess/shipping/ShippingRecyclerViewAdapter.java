package com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001.B;\u0012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\b\u0010#\u001a\u00020$H\u0016J\u0014\u0010%\u001a\u00020&2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005J\u0018\u0010\'\u001a\u00020&2\u0006\u0010(\u001a\u00020\u00022\u0006\u0010)\u001a\u00020$H\u0016J\u0018\u0010*\u001a\u00020\u00022\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020$H\u0016R\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R \u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"\u00a8\u0006/"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/shipping/ShippingRecyclerViewAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/shipping/ShippingRecyclerViewAdapter$ViewHolder;", "dataList", "Landroidx/lifecycle/LiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/UserAddressData;", "context", "Landroid/content/Context;", "shippingViewModel", "Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/shipping/ShippingViewModel;", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "MainBinding", "Lcom/shoohna/gofresh/databinding/FragmentShippingBinding;", "(Landroidx/lifecycle/LiveData;Landroid/content/Context;Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/shipping/ShippingViewModel;Landroidx/lifecycle/LifecycleOwner;Lcom/shoohna/gofresh/databinding/FragmentShippingBinding;)V", "getMainBinding", "()Lcom/shoohna/gofresh/databinding/FragmentShippingBinding;", "setMainBinding", "(Lcom/shoohna/gofresh/databinding/FragmentShippingBinding;)V", "getLifecycleOwner", "()Landroidx/lifecycle/LifecycleOwner;", "setLifecycleOwner", "(Landroidx/lifecycle/LifecycleOwner;)V", "makeLoop", "Landroidx/lifecycle/MutableLiveData;", "", "getMakeLoop", "()Landroidx/lifecycle/MutableLiveData;", "setMakeLoop", "(Landroidx/lifecycle/MutableLiveData;)V", "getShippingViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/shipping/ShippingViewModel;", "setShippingViewModel", "(Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/shipping/ShippingViewModel;)V", "getItemCount", "", "loopIsChecked", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ViewHolder", "app_debug"})
public final class ShippingRecyclerViewAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping.ShippingRecyclerViewAdapter.ViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> makeLoop;
    private androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.UserAddressData>> dataList;
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping.ShippingViewModel shippingViewModel;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LifecycleOwner lifecycleOwner;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.databinding.FragmentShippingBinding MainBinding;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getMakeLoop() {
        return null;
    }
    
    public final void setMakeLoop(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping.ShippingRecyclerViewAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping.ShippingRecyclerViewAdapter.ViewHolder holder, int position) {
    }
    
    public final void loopIsChecked(@org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.UserAddressData> dataList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping.ShippingViewModel getShippingViewModel() {
        return null;
    }
    
    public final void setShippingViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping.ShippingViewModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LifecycleOwner getLifecycleOwner() {
        return null;
    }
    
    public final void setLifecycleOwner(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentShippingBinding getMainBinding() {
        return null;
    }
    
    public final void setMainBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentShippingBinding p0) {
    }
    
    public ShippingRecyclerViewAdapter(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.UserAddressData>> dataList, @org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping.ShippingViewModel shippingViewModel, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner lifecycleOwner, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentShippingBinding MainBinding) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J(\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\u0016"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/shipping/ShippingRecyclerViewAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/shoohna/gofresh/databinding/ShippingInformationItemRowBinding;", "makeLoop", "Landroidx/lifecycle/MutableLiveData;", "", "(Lcom/shoohna/gofresh/databinding/ShippingInformationItemRowBinding;Landroidx/lifecycle/MutableLiveData;)V", "getMakeLoop", "()Landroidx/lifecycle/MutableLiveData;", "setMakeLoop", "(Landroidx/lifecycle/MutableLiveData;)V", "bind", "", "item", "Lcom/shoohna/gofresh/pojo/responses/UserAddressData;", "shippingViewModel", "Lcom/shoohna/gofresh/ui/home/ui/checkOutProcess/shipping/ShippingViewModel;", "context", "Landroid/content/Context;", "MainBinding", "Lcom/shoohna/gofresh/databinding/FragmentShippingBinding;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private com.shoohna.gofresh.databinding.ShippingInformationItemRowBinding binding;
        @org.jetbrains.annotations.NotNull()
        private androidx.lifecycle.MutableLiveData<java.lang.Boolean> makeLoop;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.pojo.responses.UserAddressData item, @org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping.ShippingViewModel shippingViewModel, @org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.databinding.FragmentShippingBinding MainBinding) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getMakeLoop() {
            return null;
        }
        
        public final void setMakeLoop(@org.jetbrains.annotations.NotNull()
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.databinding.ShippingInformationItemRowBinding binding, @org.jetbrains.annotations.NotNull()
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> makeLoop) {
            super(null);
        }
    }
}