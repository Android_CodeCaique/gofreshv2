package com.shoohna.gofresh.ui.home.ui.favorite;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002J&\u0010*\u001a\u0004\u0018\u00010+2\u0006\u0010,\u001a\u00020-2\b\u0010.\u001a\u0004\u0018\u00010/2\b\u00100\u001a\u0004\u0018\u000101H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012R&\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001a\u0010\u001d\u001a\u00020\u001eX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R&\u0010#\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u001a\"\u0004\b%\u0010\u001c\u00a8\u00062"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/favorite/FavoriteFragment;", "Landroidx/fragment/app/Fragment;", "()V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentFavoriteBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentFavoriteBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentFavoriteBinding;)V", "favoriteViewModel", "Lcom/shoohna/gofresh/ui/home/ui/favorite/FavoriteViewModel;", "getFavoriteViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/favorite/FavoriteViewModel;", "favoriteViewModel$delegate", "Lkotlin/Lazy;", "filteredList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/MyWishlistData;", "getFilteredList", "()Landroidx/lifecycle/MutableLiveData;", "setFilteredList", "(Landroidx/lifecycle/MutableLiveData;)V", "mAdapter", "Lcom/shoohna/gofresh/ui/home/ui/favorite/FavoriteRecyclerViewAdapter;", "getMAdapter", "()Lcom/shoohna/gofresh/ui/home/ui/favorite/FavoriteRecyclerViewAdapter;", "setMAdapter", "(Lcom/shoohna/gofresh/ui/home/ui/favorite/FavoriteRecyclerViewAdapter;)V", "myWishlistData", "getMyWishlistData", "setMyWishlistData", "filter", "", "text", "", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class FavoriteFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentFavoriteBinding binding;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistData>> myWishlistData;
    private final kotlin.Lazy favoriteViewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.ui.home.ui.favorite.FavoriteRecyclerViewAdapter mAdapter;
    @org.jetbrains.annotations.NotNull()
    public androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistData>> filteredList;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentFavoriteBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentFavoriteBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistData>> getMyWishlistData() {
        return null;
    }
    
    public final void setMyWishlistData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistData>> p0) {
    }
    
    private final com.shoohna.gofresh.ui.home.ui.favorite.FavoriteViewModel getFavoriteViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.favorite.FavoriteRecyclerViewAdapter getMAdapter() {
        return null;
    }
    
    public final void setMAdapter(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.favorite.FavoriteRecyclerViewAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistData>> getFilteredList() {
        return null;
    }
    
    public final void setFilteredList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyWishlistData>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void filter(java.lang.String text) {
    }
    
    public FavoriteFragment() {
        super();
    }
}