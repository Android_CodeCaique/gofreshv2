package com.shoohna.gofresh.ui.home.ui.favorite;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\'\u0010\t\u001a\u0002H\n\"\n\b\u0000\u0010\n*\u0004\u0018\u00010\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016\u00a2\u0006\u0002\u0010\u000eR \u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0005\u00a8\u0006\u000f"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/favorite/FavoriteViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "productListModel", "Landroidx/lifecycle/MutableLiveData;", "Lcom/shoohna/gofresh/pojo/model/ProductListModel;", "(Landroidx/lifecycle/MutableLiveData;)V", "getProductListModel", "()Landroidx/lifecycle/MutableLiveData;", "setProductListModel", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_debug"})
public final class FavoriteViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.model.ProductListModel> productListModel;
    
    @java.lang.Override()
    public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
    java.lang.Class<T> modelClass) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.model.ProductListModel> getProductListModel() {
        return null;
    }
    
    public final void setProductListModel(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.model.ProductListModel> p0) {
    }
    
    public FavoriteViewModelFactory(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.model.ProductListModel> productListModel) {
        super();
    }
}