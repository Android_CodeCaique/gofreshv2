package com.shoohna.gofresh.ui.home.ui.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J&\u0010+\u001a\u0004\u0018\u00010,2\u0006\u0010-\u001a\u00020.2\b\u0010/\u001a\u0004\u0018\u0001002\b\u00101\u001a\u0004\u0018\u000102H\u0016J\u0018\u00103\u001a\u0002042\u0006\u00105\u001a\u0002062\u0006\u00107\u001a\u000206H\u0016J\u0018\u00103\u001a\u0002042\u0006\u00105\u001a\u0002082\u0006\u00107\u001a\u000208H\u0016R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001a\u0010\f\u001a\u00020\rX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0014\u0010\u0015R&\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u001a0\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010\u0005R\u001c\u0010#\u001a\u0004\u0018\u00010$X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010&\"\u0004\b\'\u0010(R\u000e\u0010)\u001a\u00020*X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00069"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/home/FilterBottomSheet;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "Lcom/rizlee/rangeseekbar/RangeSeekBar$OnRangeSeekBarRealTimeListener;", "homeViewModel", "Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;", "(Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;)V", "binding", "Lcom/shoohna/gofresh/databinding/BottomSheetLayoutBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/BottomSheetLayoutBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/BottomSheetLayoutBinding;)V", "filterAdapter", "Lcom/shoohna/gofresh/ui/home/ui/home/adapters/FilterRecyclerViewAdapter;", "getFilterAdapter", "()Lcom/shoohna/gofresh/ui/home/ui/home/adapters/FilterRecyclerViewAdapter;", "setFilterAdapter", "(Lcom/shoohna/gofresh/ui/home/ui/home/adapters/FilterRecyclerViewAdapter;)V", "filterBottomViewModel", "Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/FilterBottomViewModel;", "getFilterBottomViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/FilterBottomViewModel;", "filterBottomViewModel$delegate", "Lkotlin/Lazy;", "filterData", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/FilerMainCategorieData;", "getFilterData", "()Landroidx/lifecycle/MutableLiveData;", "setFilterData", "(Landroidx/lifecycle/MutableLiveData;)V", "getHomeViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;", "setHomeViewModel", "language", "", "getLanguage", "()Ljava/lang/String;", "setLanguage", "(Ljava/lang/String;)V", "sharedPreferences", "Landroid/content/SharedPreferences;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onValuesChanging", "", "minValue", "", "maxValue", "", "app_debug"})
public final class FilterBottomSheet extends com.google.android.material.bottomsheet.BottomSheetDialogFragment implements com.rizlee.rangeseekbar.RangeSeekBar.OnRangeSeekBarRealTimeListener {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.BottomSheetLayoutBinding binding;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.ui.home.ui.home.adapters.FilterRecyclerViewAdapter filterAdapter;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.FilerMainCategorieData>> filterData;
    private android.content.SharedPreferences sharedPreferences;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String language;
    private final kotlin.Lazy filterBottomViewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel homeViewModel;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.BottomSheetLayoutBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.BottomSheetLayoutBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.home.adapters.FilterRecyclerViewAdapter getFilterAdapter() {
        return null;
    }
    
    public final void setFilterAdapter(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.home.adapters.FilterRecyclerViewAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.FilerMainCategorieData>> getFilterData() {
        return null;
    }
    
    public final void setFilterData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.FilerMainCategorieData>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLanguage() {
        return null;
    }
    
    public final void setLanguage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    private final com.shoohna.gofresh.ui.home.ui.home.viewModel.FilterBottomViewModel getFilterBottomViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onValuesChanging(float minValue, float maxValue) {
    }
    
    @java.lang.Override()
    public void onValuesChanging(int minValue, int maxValue) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel getHomeViewModel() {
        return null;
    }
    
    public final void setHomeViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel p0) {
    }
    
    public FilterBottomSheet(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel homeViewModel) {
        super();
    }
}