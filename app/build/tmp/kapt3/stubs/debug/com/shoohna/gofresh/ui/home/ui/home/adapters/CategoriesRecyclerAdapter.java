package com.shoohna.gofresh.ui.home.ui.home.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\'(B=\b\u0016\u0012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fB\u0005\u00a2\u0006\u0002\u0010\u0010J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\u0014\u0010\u001c\u001a\u00020\u001d2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005J\u0018\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020\u00022\u0006\u0010 \u001a\u00020\u001bH\u0016J\u0018\u0010!\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u001bH\u0016J\u000e\u0010%\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\u0012R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/home/adapters/CategoriesRecyclerAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/shoohna/gofresh/ui/home/ui/home/adapters/CategoriesRecyclerAdapter$ViewHolder;", "dataList", "Landroidx/lifecycle/LiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/Category;", "context", "Landroid/content/Context;", "viewModel", "Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "MainBinding", "Lcom/shoohna/gofresh/databinding/FragmentHomeBinding;", "(Landroidx/lifecycle/LiveData;Landroid/content/Context;Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;Landroidx/lifecycle/LifecycleOwner;Lcom/shoohna/gofresh/databinding/FragmentHomeBinding;)V", "()V", "mListener", "Lcom/shoohna/gofresh/ui/home/ui/home/adapters/CategoriesRecyclerAdapter$OnItemClickListener;", "makeLoop", "Landroidx/lifecycle/MutableLiveData;", "", "getMakeLoop", "()Landroidx/lifecycle/MutableLiveData;", "setMakeLoop", "(Landroidx/lifecycle/MutableLiveData;)V", "getItemCount", "", "loopIsChecked", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setOnItemClickListener", "listener", "OnItemClickListener", "ViewHolder", "app_debug"})
public final class CategoriesRecyclerAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.shoohna.gofresh.ui.home.ui.home.adapters.CategoriesRecyclerAdapter.ViewHolder> {
    private androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.Category>> dataList;
    private android.content.Context context;
    private com.shoohna.gofresh.ui.home.ui.home.adapters.CategoriesRecyclerAdapter.OnItemClickListener mListener;
    private com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel viewModel;
    private androidx.lifecycle.LifecycleOwner lifecycleOwner;
    private com.shoohna.gofresh.databinding.FragmentHomeBinding MainBinding;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> makeLoop;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getMakeLoop() {
        return null;
    }
    
    public final void setMakeLoop(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void loopIsChecked(@org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.Category> dataList) {
    }
    
    public final void setOnItemClickListener(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.home.adapters.CategoriesRecyclerAdapter.OnItemClickListener listener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.shoohna.gofresh.ui.home.ui.home.adapters.CategoriesRecyclerAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.home.adapters.CategoriesRecyclerAdapter.ViewHolder holder, int position) {
    }
    
    public CategoriesRecyclerAdapter() {
        super();
    }
    
    public CategoriesRecyclerAdapter(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.Category>> dataList, @org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel viewModel, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner lifecycleOwner, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentHomeBinding MainBinding) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u00a2\u0006\u0002\u0010\tJ(\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u0018"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/home/adapters/CategoriesRecyclerAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/shoohna/gofresh/databinding/ItemCategoryBinding;", "listener", "Lcom/shoohna/gofresh/ui/home/ui/home/adapters/CategoriesRecyclerAdapter$OnItemClickListener;", "makeLoop", "Landroidx/lifecycle/MutableLiveData;", "", "(Lcom/shoohna/gofresh/databinding/ItemCategoryBinding;Lcom/shoohna/gofresh/ui/home/ui/home/adapters/CategoriesRecyclerAdapter$OnItemClickListener;Landroidx/lifecycle/MutableLiveData;)V", "getMakeLoop", "()Landroidx/lifecycle/MutableLiveData;", "setMakeLoop", "(Landroidx/lifecycle/MutableLiveData;)V", "bind", "", "item", "Lcom/shoohna/gofresh/pojo/responses/Category;", "viewModel", "Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;", "context", "Landroid/content/Context;", "MainBinding", "Lcom/shoohna/gofresh/databinding/FragmentHomeBinding;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private com.shoohna.gofresh.databinding.ItemCategoryBinding binding;
        private com.shoohna.gofresh.ui.home.ui.home.adapters.CategoriesRecyclerAdapter.OnItemClickListener listener;
        @org.jetbrains.annotations.NotNull()
        private androidx.lifecycle.MutableLiveData<java.lang.Boolean> makeLoop;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.pojo.responses.Category item, @org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel viewModel, @org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.databinding.FragmentHomeBinding MainBinding) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getMakeLoop() {
            return null;
        }
        
        public final void setMakeLoop(@org.jetbrains.annotations.NotNull()
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.databinding.ItemCategoryBinding binding, @org.jetbrains.annotations.Nullable()
        com.shoohna.gofresh.ui.home.ui.home.adapters.CategoriesRecyclerAdapter.OnItemClickListener listener, @org.jetbrains.annotations.NotNull()
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> makeLoop) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/home/adapters/CategoriesRecyclerAdapter$OnItemClickListener;", "", "onItemClick", "", "i", "", "app_debug"})
    public static abstract interface OnItemClickListener {
        
        public abstract void onItemClick(int i);
    }
}