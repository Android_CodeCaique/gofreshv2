package com.shoohna.gofresh.ui.home.ui.home.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016B+\u0012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\f\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\rH\u0016J\u0018\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\rH\u0016R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/home/adapters/ProductsCategoryAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/shoohna/gofresh/ui/home/ui/home/adapters/ProductsCategoryAdapter$ViewHolder;", "dataList", "Landroidx/lifecycle/LiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/Project1;", "context", "Landroid/content/Context;", "model", "Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;", "(Landroidx/lifecycle/LiveData;Landroid/content/Context;Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ViewHolder", "app_debug"})
public final class ProductsCategoryAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.shoohna.gofresh.ui.home.ui.home.adapters.ProductsCategoryAdapter.ViewHolder> {
    private androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.Project1>> dataList;
    private final android.content.Context context = null;
    private final com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel model = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.shoohna.gofresh.ui.home.ui.home.adapters.ProductsCategoryAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.home.adapters.ProductsCategoryAdapter.ViewHolder holder, int position) {
    }
    
    public ProductsCategoryAdapter(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.Project1>> dataList, @org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel model) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/home/adapters/ProductsCategoryAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/shoohna/gofresh/databinding/ItemProductCategoryBinding;", "model", "Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;", "context", "Landroid/content/Context;", "(Lcom/shoohna/gofresh/databinding/ItemProductCategoryBinding;Lcom/shoohna/gofresh/ui/home/ui/home/viewModel/HomeViewModel;Landroid/content/Context;)V", "bind", "", "item", "Lcom/shoohna/gofresh/pojo/responses/Project1;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private com.shoohna.gofresh.databinding.ItemProductCategoryBinding binding;
        private final com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel model = null;
        private final android.content.Context context = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.pojo.responses.Project1 item) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.databinding.ItemProductCategoryBinding binding, @org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel model, @org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            super(null);
        }
    }
}