package com.shoohna.gofresh.ui.home.ui.more;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\f\"\u0004\b\u0017\u0010\u0018\u00a8\u0006!"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/more/MoreFragment;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentMoreBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentMoreBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentMoreBinding;)V", "moreViewModel", "Lcom/shoohna/gofresh/ui/home/ui/more/MoreViewModel;", "getMoreViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/more/MoreViewModel;", "moreViewModel$delegate", "Lkotlin/Lazy;", "nav", "Landroidx/navigation/NavController;", "getNav", "()Landroidx/navigation/NavController;", "setNav", "(Landroidx/navigation/NavController;)V", "viewModel", "getViewModel", "setViewModel", "(Lcom/shoohna/gofresh/ui/home/ui/more/MoreViewModel;)V", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class MoreFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentMoreBinding binding;
    private final kotlin.Lazy moreViewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.ui.home.ui.more.MoreViewModel viewModel;
    @org.jetbrains.annotations.NotNull()
    public androidx.navigation.NavController nav;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentMoreBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentMoreBinding p0) {
    }
    
    private final com.shoohna.gofresh.ui.home.ui.more.MoreViewModel getMoreViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.more.MoreViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.more.MoreViewModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.navigation.NavController getNav() {
        return null;
    }
    
    public final void setNav(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public MoreFragment() {
        super();
    }
}