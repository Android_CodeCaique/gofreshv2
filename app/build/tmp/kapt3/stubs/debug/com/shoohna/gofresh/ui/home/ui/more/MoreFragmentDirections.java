package com.shoohna.gofresh.ui.home.ui.more;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/more/MoreFragmentDirections;", "", "()V", "Companion", "app_debug"})
public final class MoreFragmentDirections {
    public static final com.shoohna.gofresh.ui.home.ui.more.MoreFragmentDirections.Companion Companion = null;
    
    private MoreFragmentDirections() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0005\u001a\u00020\u0004J\u0006\u0010\u0006\u001a\u00020\u0004J\u0006\u0010\u0007\u001a\u00020\u0004J\u0006\u0010\b\u001a\u00020\u0004J\u0006\u0010\t\u001a\u00020\u0004\u00a8\u0006\n"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/more/MoreFragmentDirections$Companion;", "", "()V", "actionMoreFragmentToAboutUsFragment", "Landroidx/navigation/NavDirections;", "actionMoreFragmentToAddFamilyRegister2", "actionMoreFragmentToContactUsFragment", "actionMoreFragmentToHelpFragment", "actionMoreFragmentToMoreSettingFragment", "actionMoreFragmentToWalletFragment", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionMoreFragmentToMoreSettingFragment() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionMoreFragmentToContactUsFragment() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionMoreFragmentToAboutUsFragment() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionMoreFragmentToHelpFragment() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionMoreFragmentToWalletFragment() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionMoreFragmentToAddFamilyRegister2() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}