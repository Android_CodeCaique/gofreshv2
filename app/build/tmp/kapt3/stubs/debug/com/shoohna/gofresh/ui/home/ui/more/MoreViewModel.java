package com.shoohna.gofresh.ui.home.ui.more;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u0010"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/more/MoreViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "serviceGeneral", "Lcom/shoohna/gofresh/networking/interfaces/auth;", "(Lcom/shoohna/gofresh/networking/interfaces/auth;)V", "sharedHelper", "Lcom/shoohna/gofresh/util/base/SharedHelper;", "getSharedHelper", "()Lcom/shoohna/gofresh/util/base/SharedHelper;", "setSharedHelper", "(Lcom/shoohna/gofresh/util/base/SharedHelper;)V", "login", "", "v", "Landroid/view/View;", "logout", "app_debug"})
public final class MoreViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.util.base.SharedHelper sharedHelper;
    private final com.shoohna.gofresh.networking.interfaces.auth serviceGeneral = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.util.base.SharedHelper getSharedHelper() {
        return null;
    }
    
    public final void setSharedHelper(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.util.base.SharedHelper p0) {
    }
    
    public final void logout(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public final void login(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public MoreViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.networking.interfaces.auth serviceGeneral) {
        super();
    }
}