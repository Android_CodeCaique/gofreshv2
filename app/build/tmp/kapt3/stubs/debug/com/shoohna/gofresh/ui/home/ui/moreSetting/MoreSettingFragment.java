package com.shoohna.gofresh.ui.home.ui.moreSetting;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0006\u0010!\u001a\u00020\"J\b\u0010#\u001a\u00020\"H\u0002J&\u0010$\u001a\u0004\u0018\u00010%2\u0006\u0010&\u001a\u00020\'2\b\u0010(\u001a\u0004\u0018\u00010)2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J0\u0010,\u001a\u00020\"2\f\u0010-\u001a\b\u0012\u0002\b\u0003\u0018\u00010.2\b\u0010/\u001a\u0004\u0018\u00010%2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u000203H\u0016J\u0016\u00104\u001a\u00020\"2\f\u0010-\u001a\b\u0012\u0002\b\u0003\u0018\u00010.H\u0016J\u000e\u00105\u001a\u00020\"2\u0006\u00106\u001a\u000207J\u0010\u00108\u001a\u00020\"2\u0006\u00109\u001a\u000207H\u0002R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\u00020\u000bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR \u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001b\u001a\u00020\u001c8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b\u001d\u0010\u001e\u00a8\u0006:"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/moreSetting/MoreSettingFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/widget/AdapterView$OnItemSelectedListener;", "()V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentMoreSettingBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentMoreSettingBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentMoreSettingBinding;)V", "currencyData", "", "Lcom/shoohna/gofresh/pojo/responses/CurrenciesData;", "getCurrencyData", "()Ljava/util/List;", "setCurrencyData", "(Ljava/util/List;)V", "languageHelper", "Lcom/shoohna/gofresh/util/base/LanguageHelper;", "getLanguageHelper", "()Lcom/shoohna/gofresh/util/base/LanguageHelper;", "moreSettingViewModel", "Lcom/shoohna/gofresh/ui/home/ui/moreSetting/MoreSettingViewModel;", "getMoreSettingViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/moreSetting/MoreSettingViewModel;", "moreSettingViewModel$delegate", "Lkotlin/Lazy;", "handleCurrencySpinner", "", "handleLanguageSpinner", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onItemSelected", "parent", "Landroid/widget/AdapterView;", "view", "position", "", "id", "", "onNothingSelected", "recreate", "lang", "", "setLocate", "Lang", "app_debug"})
public final class MoreSettingFragment extends androidx.fragment.app.Fragment implements android.widget.AdapterView.OnItemSelectedListener {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentMoreSettingBinding binding;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    private final com.shoohna.gofresh.util.base.LanguageHelper languageHelper = null;
    private final kotlin.Lazy moreSettingViewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public java.util.List<com.shoohna.gofresh.pojo.responses.CurrenciesData> currencyData;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentMoreSettingBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentMoreSettingBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.util.base.LanguageHelper getLanguageHelper() {
        return null;
    }
    
    private final com.shoohna.gofresh.ui.home.ui.moreSetting.MoreSettingViewModel getMoreSettingViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.shoohna.gofresh.pojo.responses.CurrenciesData> getCurrencyData() {
        return null;
    }
    
    public final void setCurrencyData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.shoohna.gofresh.pojo.responses.CurrenciesData> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void handleLanguageSpinner() {
    }
    
    private final void setLocate(java.lang.String Lang) {
    }
    
    public final void handleCurrencySpinner() {
    }
    
    @java.lang.Override()
    public void onNothingSelected(@org.jetbrains.annotations.Nullable()
    android.widget.AdapterView<?> parent) {
    }
    
    @java.lang.Override()
    public void onItemSelected(@org.jetbrains.annotations.Nullable()
    android.widget.AdapterView<?> parent, @org.jetbrains.annotations.Nullable()
    android.view.View view, int position, long id) {
    }
    
    public final void recreate(@org.jetbrains.annotations.NotNull()
    java.lang.String lang) {
    }
    
    public MoreSettingFragment() {
        super();
    }
}