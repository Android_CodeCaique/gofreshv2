package com.shoohna.gofresh.ui.home.ui.moreSetting;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200J\u000e\u00101\u001a\u00020.2\u0006\u0010/\u001a\u000200J\u000e\u00102\u001a\u00020.2\u0006\u0010/\u001a\u000200J\u000e\u00103\u001a\u00020.2\u0006\u0010/\u001a\u000200J\u000e\u00104\u001a\u00020.2\u0006\u0010/\u001a\u000200J\u000e\u00105\u001a\u00020.2\u0006\u0010/\u001a\u000200R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR \u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\t\"\u0004\b\u000f\u0010\u000bR \u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R \u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00170\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0013\"\u0004\b\u0019\u0010\u0015R \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\r0\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0013\"\u0004\b\u001c\u0010\u0015R\u001a\u0010\u001d\u001a\u00020\u001eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R \u0010#\u001a\b\u0012\u0004\u0012\u00020$0\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0013\"\u0004\b&\u0010\u0015R \u0010\'\u001a\b\u0012\u0004\u0012\u00020\u00070\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0013\"\u0004\b)\u0010\u0015R \u0010*\u001a\b\u0012\u0004\u0012\u00020\u00070\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0013\"\u0004\b,\u0010\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/moreSetting/MoreSettingViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "serviceGeneral", "Lcom/shoohna/gofresh/networking/interfaces/Home;", "(Lcom/shoohna/gofresh/networking/interfaces/Home;)V", "currencySettingId", "Ljava/util/ArrayList;", "", "getCurrencySettingId", "()Ljava/util/ArrayList;", "setCurrencySettingId", "(Ljava/util/ArrayList;)V", "currencySettingName", "", "getCurrencySettingName", "setCurrencySettingName", "currency_id", "Landroidx/lifecycle/MutableLiveData;", "getCurrency_id", "()Landroidx/lifecycle/MutableLiveData;", "setCurrency_id", "(Landroidx/lifecycle/MutableLiveData;)V", "currentUserSetting", "Lcom/shoohna/gofresh/pojo/responses/SettingData;", "getCurrentUserSetting", "setCurrentUserSetting", "lang", "getLang", "setLang", "languageHelper", "Lcom/shoohna/gofresh/util/base/LanguageHelper;", "getLanguageHelper", "()Lcom/shoohna/gofresh/util/base/LanguageHelper;", "setLanguageHelper", "(Lcom/shoohna/gofresh/util/base/LanguageHelper;)V", "loader", "", "getLoader", "setLoader", "message", "getMessage", "setMessage", "notification", "getNotification", "setNotification", "changeCurrency", "", "context", "Landroid/content/Context;", "changeLang", "changeMessage", "changeNotification", "getCurrency", "getUserSetting", "app_debug"})
public final class MoreSettingViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> lang;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> message;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> notification;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> currency_id;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.responses.SettingData> currentUserSetting;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<java.lang.String> currencySettingName;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<java.lang.Integer> currencySettingId;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.util.base.LanguageHelper languageHelper;
    private final com.shoohna.gofresh.networking.interfaces.Home serviceGeneral = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getLang() {
        return null;
    }
    
    public final void setLang(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getNotification() {
        return null;
    }
    
    public final void setNotification(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getCurrency_id() {
        return null;
    }
    
    public final void setCurrency_id(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.responses.SettingData> getCurrentUserSetting() {
        return null;
    }
    
    public final void setCurrentUserSetting(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.shoohna.gofresh.pojo.responses.SettingData> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> getCurrencySettingName() {
        return null;
    }
    
    public final void setCurrencySettingName(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Integer> getCurrencySettingId() {
        return null;
    }
    
    public final void setCurrencySettingId(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.util.base.LanguageHelper getLanguageHelper() {
        return null;
    }
    
    public final void setLanguageHelper(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.util.base.LanguageHelper p0) {
    }
    
    public final void changeLang(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void changeNotification(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void changeMessage(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void changeCurrency(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void getUserSetting(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void getCurrency(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public MoreSettingViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.networking.interfaces.Home serviceGeneral) {
        super();
    }
}