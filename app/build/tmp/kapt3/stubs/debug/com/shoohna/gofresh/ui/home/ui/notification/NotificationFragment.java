package com.shoohna.gofresh.ui.home.ui.notification;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR&\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u0012X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001b\u0010\u0017\u001a\u00020\u00188BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u0019\u0010\u001a\u00a8\u0006%"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/notification/NotificationFragment;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentNotificationBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentNotificationBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentNotificationBinding;)V", "data", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/NotificationData;", "getData", "()Landroidx/lifecycle/MutableLiveData;", "setData", "(Landroidx/lifecycle/MutableLiveData;)V", "notificationViewAdapter", "Lcom/shoohna/gofresh/ui/home/ui/notification/NotificationRecyclerViewAdapter;", "getNotificationViewAdapter", "()Lcom/shoohna/gofresh/ui/home/ui/notification/NotificationRecyclerViewAdapter;", "setNotificationViewAdapter", "(Lcom/shoohna/gofresh/ui/home/ui/notification/NotificationRecyclerViewAdapter;)V", "notificationViewModel", "Lcom/shoohna/gofresh/ui/home/ui/notification/NotificationViewModel;", "getNotificationViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/notification/NotificationViewModel;", "notificationViewModel$delegate", "Lkotlin/Lazy;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class NotificationFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentNotificationBinding binding;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.ui.home.ui.notification.NotificationRecyclerViewAdapter notificationViewAdapter;
    private final kotlin.Lazy notificationViewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.NotificationData>> data;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentNotificationBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentNotificationBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.notification.NotificationRecyclerViewAdapter getNotificationViewAdapter() {
        return null;
    }
    
    public final void setNotificationViewAdapter(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.notification.NotificationRecyclerViewAdapter p0) {
    }
    
    private final com.shoohna.gofresh.ui.home.ui.notification.NotificationViewModel getNotificationViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.NotificationData>> getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.NotificationData>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public NotificationFragment() {
        super();
    }
}