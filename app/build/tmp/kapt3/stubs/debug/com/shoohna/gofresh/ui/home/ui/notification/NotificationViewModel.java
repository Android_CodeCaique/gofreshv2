package com.shoohna.gofresh.ui.home.ui.notification;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J!\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u00062\u0006\u0010\u0010\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\b\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0010\u001a\u00020\u0011H\u0002R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001c\u0010\f\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/notification/NotificationViewModel;", "Landroidx/lifecycle/ViewModel;", "serviceGeneral", "Lcom/shoohna/gofresh/networking/interfaces/Home;", "(Lcom/shoohna/gofresh/networking/interfaces/Home;)V", "loader", "Landroidx/lifecycle/MutableLiveData;", "", "getLoader", "()Landroidx/lifecycle/MutableLiveData;", "setLoader", "(Landroidx/lifecycle/MutableLiveData;)V", "notificationList", "", "Lcom/shoohna/gofresh/pojo/responses/NotificationData;", "getNotificationList", "context", "Landroid/content/Context;", "getNotificationList$app_debug", "loadData", "", "app_debug"})
public final class NotificationViewModel extends androidx.lifecycle.ViewModel {
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.NotificationData>> notificationList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    private final com.shoohna.gofresh.networking.interfaces.Home serviceGeneral = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    private final void loadData(android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.NotificationData>> getNotificationList$app_debug(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    public NotificationViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.networking.interfaces.Home serviceGeneral) {
        super();
    }
}