package com.shoohna.gofresh.ui.home.ui.orderData;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J!\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u00062\u0006\u0010\u0016\u001a\u00020\u0017H\u0000\u00a2\u0006\u0002\b\u0018J\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0016\u001a\u00020\u0017R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR(\u0010\f\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\t\"\u0004\b\u0010\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\t\"\u0004\b\u0014\u0010\u000b\u00a8\u0006\u001b"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/orderData/OrderDataViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "serviceGeneral", "Lcom/shoohna/gofresh/networking/interfaces/Home;", "(Lcom/shoohna/gofresh/networking/interfaces/Home;)V", "loader", "Landroidx/lifecycle/MutableLiveData;", "", "getLoader", "()Landroidx/lifecycle/MutableLiveData;", "setLoader", "(Landroidx/lifecycle/MutableLiveData;)V", "myOrderProductList", "", "Lcom/shoohna/gofresh/pojo/responses/MyOrderProductsProduct;", "getMyOrderProductList", "setMyOrderProductList", "shopId", "", "getShopId", "setShopId", "geMyOrderProductsList", "context", "Landroid/content/Context;", "geMyOrderProductsList$app_debug", "loadData", "", "app_debug"})
public final class OrderDataViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> shopId;
    @org.jetbrains.annotations.Nullable()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyOrderProductsProduct>> myOrderProductList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    private final com.shoohna.gofresh.networking.interfaces.Home serviceGeneral = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Integer> getShopId() {
        return null;
    }
    
    public final void setShopId(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyOrderProductsProduct>> getMyOrderProductList() {
        return null;
    }
    
    public final void setMyOrderProductList(@org.jetbrains.annotations.Nullable()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyOrderProductsProduct>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void loadData(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.MyOrderProductsProduct>> geMyOrderProductsList$app_debug(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    public OrderDataViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.networking.interfaces.Home serviceGeneral) {
        super();
    }
}