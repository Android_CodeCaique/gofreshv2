package com.shoohna.gofresh.ui.home.ui.product;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\u001eH\u0002J&\u00102\u001a\u0004\u0018\u0001032\u0006\u00104\u001a\u0002052\b\u00106\u001a\u0004\u0018\u0001072\b\u00108\u001a\u0004\u0018\u000109H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR&\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001a\u0010\u001d\u001a\u00020\u001eX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R&\u0010#\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0014\"\u0004\b%\u0010\u0016R\u001b\u0010&\u001a\u00020\'8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b(\u0010)R\u001a\u0010,\u001a\u00020\u001eX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010 \"\u0004\b.\u0010\"\u00a8\u0006:"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/product/ProductFragment;", "Landroidx/fragment/app/Fragment;", "()V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentProductBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentProductBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentProductBinding;)V", "filteredList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/ProductByTypeData;", "getFilteredList", "()Landroidx/lifecycle/MutableLiveData;", "setFilteredList", "(Landroidx/lifecycle/MutableLiveData;)V", "mAdapter", "Lcom/shoohna/gofresh/ui/home/ui/product/ProductRecyclerAdapter;", "getMAdapter", "()Lcom/shoohna/gofresh/ui/home/ui/product/ProductRecyclerAdapter;", "setMAdapter", "(Lcom/shoohna/gofresh/ui/home/ui/product/ProductRecyclerAdapter;)V", "mainTitle", "", "getMainTitle", "()Ljava/lang/String;", "setMainTitle", "(Ljava/lang/String;)V", "productData", "getProductData", "setProductData", "productViewModel", "Lcom/shoohna/gofresh/ui/home/ui/product/ProductViewModel;", "getProductViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/product/ProductViewModel;", "productViewModel$delegate", "Lkotlin/Lazy;", "searchWord", "getSearchWord", "setSearchWord", "filter", "", "text", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class ProductFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentProductBinding binding;
    @org.jetbrains.annotations.NotNull()
    public java.lang.String mainTitle;
    @org.jetbrains.annotations.NotNull()
    public java.lang.String searchWord;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> productData;
    private final kotlin.Lazy productViewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.ui.home.ui.product.ProductRecyclerAdapter mAdapter;
    @org.jetbrains.annotations.NotNull()
    public androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> filteredList;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentProductBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentProductBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMainTitle() {
        return null;
    }
    
    public final void setMainTitle(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSearchWord() {
        return null;
    }
    
    public final void setSearchWord(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> getProductData() {
        return null;
    }
    
    public final void setProductData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> p0) {
    }
    
    private final com.shoohna.gofresh.ui.home.ui.product.ProductViewModel getProductViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.product.ProductRecyclerAdapter getMAdapter() {
        return null;
    }
    
    public final void setMAdapter(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.product.ProductRecyclerAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> getFilteredList() {
        return null;
    }
    
    public final void setFilteredList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void filter(java.lang.String text) {
    }
    
    public ProductFragment() {
        super();
    }
}