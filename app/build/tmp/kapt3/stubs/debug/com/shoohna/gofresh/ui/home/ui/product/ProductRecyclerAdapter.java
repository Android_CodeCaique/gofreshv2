package com.shoohna.gofresh.ui.home.ui.product;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001$B;\u0012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001a\u0010\u0018\u001a\u00020\u00192\u0012\u0010\u001a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\u0018\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020\u001cH\u0016J\u0018\u0010 \u001a\u00020\u00022\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001cH\u0016R\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/product/ProductRecyclerAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/shoohna/gofresh/ui/home/ui/product/ProductRecyclerAdapter$ViewHolder;", "dataList", "Landroidx/lifecycle/LiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/ProductByTypeData;", "context", "Landroid/content/Context;", "model", "Lcom/shoohna/gofresh/ui/home/ui/product/ProductViewModel;", "viewLifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "MainBinding", "Lcom/shoohna/gofresh/databinding/FragmentProductBinding;", "(Landroidx/lifecycle/LiveData;Landroid/content/Context;Lcom/shoohna/gofresh/ui/home/ui/product/ProductViewModel;Landroidx/lifecycle/LifecycleOwner;Lcom/shoohna/gofresh/databinding/FragmentProductBinding;)V", "getMainBinding", "()Lcom/shoohna/gofresh/databinding/FragmentProductBinding;", "setMainBinding", "(Lcom/shoohna/gofresh/databinding/FragmentProductBinding;)V", "getModel", "()Lcom/shoohna/gofresh/ui/home/ui/product/ProductViewModel;", "setModel", "(Lcom/shoohna/gofresh/ui/home/ui/product/ProductViewModel;)V", "filterList", "", "filteredList", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ViewHolder", "app_debug"})
public final class ProductRecyclerAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.shoohna.gofresh.ui.home.ui.product.ProductRecyclerAdapter.ViewHolder> {
    private androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> dataList;
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.ui.home.ui.product.ProductViewModel model;
    private final androidx.lifecycle.LifecycleOwner viewLifecycleOwner = null;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.databinding.FragmentProductBinding MainBinding;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.shoohna.gofresh.ui.home.ui.product.ProductRecyclerAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.product.ProductRecyclerAdapter.ViewHolder holder, int position) {
    }
    
    public final void filterList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> filteredList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.product.ProductViewModel getModel() {
        return null;
    }
    
    public final void setModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.product.ProductViewModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentProductBinding getMainBinding() {
        return null;
    }
    
    public final void setMainBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentProductBinding p0) {
    }
    
    public ProductRecyclerAdapter(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductByTypeData>> dataList, @org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.product.ProductViewModel model, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner viewLifecycleOwner, @org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentProductBinding MainBinding) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J0\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/product/ProductRecyclerAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/shoohna/gofresh/databinding/ProductBestSaleRowBinding;", "(Lcom/shoohna/gofresh/databinding/ProductBestSaleRowBinding;)V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "bind", "", "item", "Lcom/shoohna/gofresh/pojo/responses/ProductByTypeData;", "context", "Landroid/content/Context;", "model", "Lcom/shoohna/gofresh/ui/home/ui/product/ProductViewModel;", "viewLifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "MainBinding", "Lcom/shoohna/gofresh/databinding/FragmentProductBinding;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private com.shoohna.shoohna.util.base.BaseFragment baseFragment;
        private com.shoohna.gofresh.databinding.ProductBestSaleRowBinding binding;
        
        @org.jetbrains.annotations.NotNull()
        public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
            return null;
        }
        
        public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
        com.shoohna.shoohna.util.base.BaseFragment p0) {
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.pojo.responses.ProductByTypeData item, @org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.ui.home.ui.product.ProductViewModel model, @org.jetbrains.annotations.NotNull()
        androidx.lifecycle.LifecycleOwner viewLifecycleOwner, @org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.databinding.FragmentProductBinding MainBinding) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.shoohna.gofresh.databinding.ProductBestSaleRowBinding binding) {
            super(null);
        }
    }
}