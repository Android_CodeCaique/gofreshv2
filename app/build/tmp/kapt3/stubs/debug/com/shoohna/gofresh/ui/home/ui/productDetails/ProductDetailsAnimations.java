package com.shoohna.gofresh.ui.home.ui.productDetails;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\f"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/productDetails/ProductDetailsAnimations;", "", "()V", "animationDown", "", "constraint", "Landroidx/constraintlayout/widget/ConstraintLayout;", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "detailsHeigth", "", "animationUp", "app_debug"})
public final class ProductDetailsAnimations {
    
    public final void animationDown(@org.jetbrains.annotations.NotNull()
    androidx.constraintlayout.widget.ConstraintLayout constraint, @org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView recyclerView, int detailsHeigth) {
    }
    
    public final void animationUp(@org.jetbrains.annotations.NotNull()
    androidx.constraintlayout.widget.ConstraintLayout constraint, @org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView recyclerView, int detailsHeigth) {
    }
    
    public ProductDetailsAnimations() {
        super();
    }
}