package com.shoohna.gofresh.ui.home.ui.productDetails;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010>\u001a\u0004\u0018\u00010?2\u0006\u0010@\u001a\u00020A2\b\u0010B\u001a\u0004\u0018\u00010C2\b\u0010D\u001a\u0004\u0018\u00010EH\u0016J\u000e\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020IR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR&\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R \u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\u00020\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001a\u0010$\u001a\u00020%X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u001b\u0010*\u001a\u00020+8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b,\u0010-R&\u00100\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002010\u00110\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u0014\"\u0004\b3\u0010\u0016R&\u00104\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002050\u00110\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u0010\u0014\"\u0004\b7\u0010\u0016R\u001a\u00108\u001a\u000209X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=\u00a8\u0006J"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/productDetails/ProductDetailsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentProductDetailsBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentProductDetailsBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentProductDetailsBinding;)V", "colorImagesData", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/shoohna/gofresh/pojo/responses/ProductColor;", "getColorImagesData", "()Landroidx/lifecycle/MutableLiveData;", "setColorImagesData", "(Landroidx/lifecycle/MutableLiveData;)V", "dataList", "Ljava/util/ArrayList;", "Lcom/shoohna/gofresh/pojo/model/ProductListModel;", "getDataList", "()Ljava/util/ArrayList;", "setDataList", "(Ljava/util/ArrayList;)V", "detailsHeigth", "", "getDetailsHeigth", "()I", "setDetailsHeigth", "(I)V", "productAnimation", "Lcom/shoohna/gofresh/ui/home/ui/productDetails/ProductDetailsAnimations;", "getProductAnimation", "()Lcom/shoohna/gofresh/ui/home/ui/productDetails/ProductDetailsAnimations;", "setProductAnimation", "(Lcom/shoohna/gofresh/ui/home/ui/productDetails/ProductDetailsAnimations;)V", "productDetailsViewModel", "Lcom/shoohna/gofresh/ui/home/ui/productDetails/ProductDetailsViewModel;", "getProductDetailsViewModel", "()Lcom/shoohna/gofresh/ui/home/ui/productDetails/ProductDetailsViewModel;", "productDetailsViewModel$delegate", "Lkotlin/Lazy;", "productImagesData", "Lcom/shoohna/gofresh/pojo/responses/ProductImage;", "getProductImagesData", "setProductImagesData", "sizeData", "Lcom/shoohna/gofresh/pojo/responses/ProductSize;", "getSizeData", "setSizeData", "vto", "Landroid/view/ViewTreeObserver;", "getVto", "()Landroid/view/ViewTreeObserver;", "setVto", "(Landroid/view/ViewTreeObserver;)V", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "showAlert", "", "context", "Landroid/content/Context;", "app_debug"})
public final class ProductDetailsFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentProductDetailsBinding binding;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.shoohna.gofresh.pojo.model.ProductListModel> dataList;
    @org.jetbrains.annotations.NotNull()
    public android.view.ViewTreeObserver vto;
    private int detailsHeigth;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductImage>> productImagesData;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductColor>> colorImagesData;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductSize>> sizeData;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.ui.home.ui.productDetails.ProductDetailsAnimations productAnimation;
    private final kotlin.Lazy productDetailsViewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentProductDetailsBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentProductDetailsBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.shoohna.gofresh.pojo.model.ProductListModel> getDataList() {
        return null;
    }
    
    public final void setDataList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.shoohna.gofresh.pojo.model.ProductListModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.ViewTreeObserver getVto() {
        return null;
    }
    
    public final void setVto(@org.jetbrains.annotations.NotNull()
    android.view.ViewTreeObserver p0) {
    }
    
    public final int getDetailsHeigth() {
        return 0;
    }
    
    public final void setDetailsHeigth(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductImage>> getProductImagesData() {
        return null;
    }
    
    public final void setProductImagesData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductImage>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductColor>> getColorImagesData() {
        return null;
    }
    
    public final void setColorImagesData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductColor>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductSize>> getSizeData() {
        return null;
    }
    
    public final void setSizeData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.shoohna.gofresh.pojo.responses.ProductSize>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.home.ui.productDetails.ProductDetailsAnimations getProductAnimation() {
        return null;
    }
    
    public final void setProductAnimation(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.home.ui.productDetails.ProductDetailsAnimations p0) {
    }
    
    private final com.shoohna.gofresh.ui.home.ui.productDetails.ProductDetailsViewModel getProductDetailsViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public final void showAlert(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public ProductDetailsFragment() {
        super();
    }
}