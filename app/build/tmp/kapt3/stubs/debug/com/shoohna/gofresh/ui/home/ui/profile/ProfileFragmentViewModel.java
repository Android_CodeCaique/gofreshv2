package com.shoohna.gofresh.ui.home.ui.profile;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u00100\u001a\u0004\u0018\u00010\u00132\u0006\u00101\u001a\u0002022\b\u00103\u001a\u0004\u0018\u000104J\u0016\u00105\u001a\u0002062\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:J\u000e\u0010;\u001a\u0002062\u0006\u00107\u001a\u000208J\u000e\u0010<\u001a\u0002062\u0006\u00107\u001a\u000208J\u000e\u0010=\u001a\u0002062\u0006\u00107\u001a\u000208Jf\u0010>\u001a\u0002062\u0006\u00107\u001a\u0002082\u0006\u0010?\u001a\u00020@2\u0006\u0010A\u001a\u00020@2\u0006\u0010B\u001a\u00020@2\u0006\u0010C\u001a\u00020@2\u0006\u0010D\u001a\u00020E2\u0006\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020I2\u0006\u0010K\u001a\u00020I2\u0006\u0010L\u001a\u00020G2\u0006\u0010M\u001a\u00020:J.\u0010N\u001a\u00020\f2\u0006\u00107\u001a\u0002082\u0006\u0010\u0018\u001a\u00020@2\u0006\u0010\u001b\u001a\u00020@2\u0006\u0010\'\u001a\u00020@2\u0006\u0010\u0011\u001a\u00020@R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R \u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R \u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0015\"\u0004\b\u001a\u0010\u0017R \u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0015\"\u0004\b\u001d\u0010\u0017R \u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\f0\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0015\"\u0004\b \u0010\u0017R\u001a\u0010!\u001a\u00020\"X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R \u0010\'\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0015\"\u0004\b)\u0010\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010*\u001a\u00020+X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/\u00a8\u0006O"}, d2 = {"Lcom/shoohna/gofresh/ui/home/ui/profile/ProfileFragmentViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "serviceGeneral", "Lcom/shoohna/gofresh/networking/interfaces/auth;", "(Lcom/shoohna/gofresh/networking/interfaces/auth;)V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "changeImage", "", "getChangeImage", "()Z", "setChangeImage", "(Z)V", "email", "Landroidx/lifecycle/MutableLiveData;", "", "getEmail", "()Landroidx/lifecycle/MutableLiveData;", "setEmail", "(Landroidx/lifecycle/MutableLiveData;)V", "fName", "getFName", "setFName", "lName", "getLName", "setLName", "loader", "getLoader", "setLoader", "nav", "Landroidx/navigation/NavController;", "getNav", "()Landroidx/navigation/NavController;", "setNav", "(Landroidx/navigation/NavController;)V", "phone", "getPhone", "setPhone", "sharedHelper", "Lcom/shoohna/gofresh/util/base/SharedHelper;", "getSharedHelper", "()Lcom/shoohna/gofresh/util/base/SharedHelper;", "setSharedHelper", "(Lcom/shoohna/gofresh/util/base/SharedHelper;)V", "getRealPathFromUri", "context", "Landroid/content/Context;", "contentUri", "Landroid/net/Uri;", "loadData", "", "v", "Landroid/view/View;", "circleImageView", "Lde/hdodenhof/circleimageview/CircleImageView;", "moveToHelp", "moveToOrderStatus", "share", "updateProfile", "fNameET", "Landroid/widget/EditText;", "lNameET", "phoneET", "emailET", "changePasswordTxtId", "Landroid/widget/TextView;", "editConstraintId", "Landroidx/constraintlayout/widget/ConstraintLayout;", "correctImgId", "Landroid/widget/ImageView;", "dontSaveImgId", "addImageBtnId", "bottom_constraint_id", "profileImg", "validate", "app_debug"})
public final class ProfileFragmentViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    public androidx.navigation.NavController nav;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> email;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> phone;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> fName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> lName;
    private boolean changeImage;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.util.base.SharedHelper sharedHelper;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    private final com.shoohna.gofresh.networking.interfaces.auth serviceGeneral = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.navigation.NavController getNav() {
        return null;
    }
    
    public final void setNav(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getEmail() {
        return null;
    }
    
    public final void setEmail(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPhone() {
        return null;
    }
    
    public final void setPhone(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getFName() {
        return null;
    }
    
    public final void setFName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getLName() {
        return null;
    }
    
    public final void setLName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    public final boolean getChangeImage() {
        return false;
    }
    
    public final void setChangeImage(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.util.base.SharedHelper getSharedHelper() {
        return null;
    }
    
    public final void setSharedHelper(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.util.base.SharedHelper p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void loadData(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    de.hdodenhof.circleimageview.CircleImageView circleImageView) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRealPathFromUri(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.net.Uri contentUri) {
        return null;
    }
    
    public final void updateProfile(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    android.widget.EditText fNameET, @org.jetbrains.annotations.NotNull()
    android.widget.EditText lNameET, @org.jetbrains.annotations.NotNull()
    android.widget.EditText phoneET, @org.jetbrains.annotations.NotNull()
    android.widget.EditText emailET, @org.jetbrains.annotations.NotNull()
    android.widget.TextView changePasswordTxtId, @org.jetbrains.annotations.NotNull()
    androidx.constraintlayout.widget.ConstraintLayout editConstraintId, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView correctImgId, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView dontSaveImgId, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView addImageBtnId, @org.jetbrains.annotations.NotNull()
    androidx.constraintlayout.widget.ConstraintLayout bottom_constraint_id, @org.jetbrains.annotations.NotNull()
    de.hdodenhof.circleimageview.CircleImageView profileImg) {
    }
    
    public final boolean validate(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    android.widget.EditText fName, @org.jetbrains.annotations.NotNull()
    android.widget.EditText lName, @org.jetbrains.annotations.NotNull()
    android.widget.EditText phone, @org.jetbrains.annotations.NotNull()
    android.widget.EditText email) {
        return false;
    }
    
    public final void share(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public final void moveToHelp(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public final void moveToOrderStatus(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public ProfileFragmentViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.networking.interfaces.auth serviceGeneral) {
        super();
    }
}