package com.shoohna.gofresh.ui.welcome.ui.forgetPassword;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'J\u0016\u0010(\u001a\u00020\u00132\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u000f\"\u0004\b\u0015\u0010\u0011R\u001a\u0010\u0016\u001a\u00020\u0017X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001c\u001a\u00020\u001dX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!\u00a8\u0006)"}, d2 = {"Lcom/shoohna/gofresh/ui/welcome/ui/forgetPassword/ForgetPasswordViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "serviceGeneral", "Lcom/shoohna/gofresh/networking/interfaces/auth;", "(Lcom/shoohna/gofresh/networking/interfaces/auth;)V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "email", "Landroidx/lifecycle/MutableLiveData;", "", "getEmail", "()Landroidx/lifecycle/MutableLiveData;", "setEmail", "(Landroidx/lifecycle/MutableLiveData;)V", "loader", "", "getLoader", "setLoader", "nav", "Landroidx/navigation/NavController;", "getNav", "()Landroidx/navigation/NavController;", "setNav", "(Landroidx/navigation/NavController;)V", "sharedHelper", "Lcom/shoohna/gofresh/util/base/SharedHelper;", "getSharedHelper", "()Lcom/shoohna/gofresh/util/base/SharedHelper;", "setSharedHelper", "(Lcom/shoohna/gofresh/util/base/SharedHelper;)V", "forgetPassword", "", "v", "Landroid/view/View;", "emailLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "validate", "app_debug"})
public final class ForgetPasswordViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> email;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.util.base.SharedHelper sharedHelper;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    @org.jetbrains.annotations.NotNull()
    public androidx.navigation.NavController nav;
    private final com.shoohna.gofresh.networking.interfaces.auth serviceGeneral = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getEmail() {
        return null;
    }
    
    public final void setEmail(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.util.base.SharedHelper getSharedHelper() {
        return null;
    }
    
    public final void setSharedHelper(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.util.base.SharedHelper p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.navigation.NavController getNav() {
        return null;
    }
    
    public final void setNav(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController p0) {
    }
    
    public final void forgetPassword(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout emailLayout) {
    }
    
    public final boolean validate(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout emailLayout) {
        return false;
    }
    
    public ForgetPasswordViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.networking.interfaces.auth serviceGeneral) {
        super();
    }
}