package com.shoohna.gofresh.ui.welcome.ui.register;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H\u0002J\u0016\u0010)\u001a\u00020&2\u0006\u0010\'\u001a\u00020(2\u0006\u0010\"\u001a\u00020\rJ.\u0010*\u001a\u00020&2\u0006\u0010+\u001a\u00020,2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\r2\u0006\u0010-\u001a\u00020\rJ>\u0010*\u001a\u00020&2\u0006\u0010\'\u001a\u00020(2\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020/2\u0006\u00101\u001a\u00020/2\u0006\u00102\u001a\u00020/2\u0006\u00103\u001a\u00020/2\u0006\u00104\u001a\u00020/J\u000e\u00105\u001a\u00020&2\u0006\u0010\'\u001a\u00020(J>\u00106\u001a\u00020\u001c2\u0006\u0010\'\u001a\u00020(2\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020/2\u0006\u00101\u001a\u00020/2\u0006\u00102\u001a\u00020/2\u0006\u00103\u001a\u00020/2\u0006\u00104\u001a\u00020/R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u000f\"\u0004\b\u0014\u0010\u0011R \u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u000f\"\u0004\b\u0017\u0010\u0011R \u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u000f\"\u0004\b\u001a\u0010\u0011R \u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u000f\"\u0004\b\u001e\u0010\u0011R \u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u000f\"\u0004\b!\u0010\u0011R \u0010\"\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u000f\"\u0004\b$\u0010\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"}, d2 = {"Lcom/shoohna/gofresh/ui/welcome/ui/register/RegisterViewModel;", "Lcom/shoohna/shoohna/util/base/BaseViewModel;", "serviceGeneral", "Lcom/shoohna/gofresh/networking/interfaces/auth;", "(Lcom/shoohna/gofresh/networking/interfaces/auth;)V", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "confirmPasssword", "Landroidx/lifecycle/MutableLiveData;", "", "getConfirmPasssword", "()Landroidx/lifecycle/MutableLiveData;", "setConfirmPasssword", "(Landroidx/lifecycle/MutableLiveData;)V", "email", "getEmail", "setEmail", "fName", "getFName", "setFName", "lName", "getLName", "setLName", "loader", "", "getLoader", "setLoader", "password", "getPassword", "setPassword", "phone", "getPhone", "setPhone", "activeAccountApi", "", "v", "Landroid/view/View;", "getActivationCode", "register", "context", "Landroid/content/Context;", "image", "fNameLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "lNameLayout", "emailLayout", "phoneLayout", "passwordLayout", "confirmPasswordLayout", "registerTest", "validate", "app_debug"})
public final class RegisterViewModel extends com.shoohna.shoohna.util.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> fName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> lName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> phone;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> password;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> confirmPasssword;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> email;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> loader;
    private final com.shoohna.gofresh.networking.interfaces.auth serviceGeneral = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getFName() {
        return null;
    }
    
    public final void setFName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getLName() {
        return null;
    }
    
    public final void setLName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPhone() {
        return null;
    }
    
    public final void setPhone(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPassword() {
        return null;
    }
    
    public final void setPassword(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getConfirmPasssword() {
        return null;
    }
    
    public final void setConfirmPasssword(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getEmail() {
        return null;
    }
    
    public final void setEmail(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoader() {
        return null;
    }
    
    public final void setLoader(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void registerTest(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public final void register(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout fNameLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout lNameLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout emailLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout phoneLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout passwordLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout confirmPasswordLayout) {
    }
    
    public final boolean validate(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout fNameLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout lNameLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout emailLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout phoneLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout passwordLayout, @org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout confirmPasswordLayout) {
        return false;
    }
    
    public final void getActivationCode(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    java.lang.String phone) {
    }
    
    public final void register(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    java.lang.String fName, @org.jetbrains.annotations.NotNull()
    java.lang.String lName, @org.jetbrains.annotations.NotNull()
    java.lang.String image) {
    }
    
    private final void activeAccountApi(android.view.View v) {
    }
    
    public RegisterViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.networking.interfaces.auth serviceGeneral) {
        super();
    }
}