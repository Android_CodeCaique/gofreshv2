package com.shoohna.gofresh.ui.welcome.ui.splash;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0006X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006\'"}, d2 = {"Lcom/shoohna/gofresh/ui/welcome/ui/splash/SplashFragment;", "Landroidx/fragment/app/Fragment;", "()V", "PERMISSIONS_REQUEST", "", "SPLASH_TIME", "", "getSPLASH_TIME", "()J", "baseFragment", "Lcom/shoohna/shoohna/util/base/BaseFragment;", "getBaseFragment", "()Lcom/shoohna/shoohna/util/base/BaseFragment;", "setBaseFragment", "(Lcom/shoohna/shoohna/util/base/BaseFragment;)V", "binding", "Lcom/shoohna/gofresh/databinding/FragmentSplashBinding;", "getBinding", "()Lcom/shoohna/gofresh/databinding/FragmentSplashBinding;", "setBinding", "(Lcom/shoohna/gofresh/databinding/FragmentSplashBinding;)V", "viewModel", "Lcom/shoohna/gofresh/ui/welcome/ui/splash/SplashViewModel;", "getViewModel", "()Lcom/shoohna/gofresh/ui/welcome/ui/splash/SplashViewModel;", "setViewModel", "(Lcom/shoohna/gofresh/ui/welcome/ui/splash/SplashViewModel;)V", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "setLocate", "", "Lang", "", "app_debug"})
public final class SplashFragment extends androidx.fragment.app.Fragment {
    private final long SPLASH_TIME = 2000L;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.databinding.FragmentSplashBinding binding;
    private final int PERMISSIONS_REQUEST = 1;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.shoohna.util.base.BaseFragment baseFragment;
    @org.jetbrains.annotations.NotNull()
    public com.shoohna.gofresh.ui.welcome.ui.splash.SplashViewModel viewModel;
    private java.util.HashMap _$_findViewCache;
    
    public final long getSPLASH_TIME() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.databinding.FragmentSplashBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.databinding.FragmentSplashBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.shoohna.util.base.BaseFragment getBaseFragment() {
        return null;
    }
    
    public final void setBaseFragment(@org.jetbrains.annotations.NotNull()
    com.shoohna.shoohna.util.base.BaseFragment p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.ui.welcome.ui.splash.SplashViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.ui.welcome.ui.splash.SplashViewModel p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void setLocate(java.lang.String Lang) {
    }
    
    public SplashFragment() {
        super();
    }
}