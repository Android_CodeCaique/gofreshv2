package com.shoohna.gofresh.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\bH\u0007J\u0018\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007J\u0018\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0007\u00a8\u0006\u0013"}, d2 = {"Lcom/shoohna/gofresh/util/BindingAdapters;", "", "()V", "loadHexaColor", "", "view", "Landroid/widget/ImageView;", "color", "", "loadImage", "url", "setBold", "Landroid/widget/TextView;", "isBoolean", "", "statusOfOrder", "txtView", "orderNumberState", "", "app_debug"})
public final class BindingAdapters {
    public static final com.shoohna.gofresh.util.BindingAdapters INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"imageUrl"})
    public static final void loadImage(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView view, @org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"loadHexaColor"})
    public static final void loadHexaColor(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView view, @org.jetbrains.annotations.NotNull()
    java.lang.String color) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"isBold"})
    public static final void setBold(@org.jetbrains.annotations.NotNull()
    android.widget.TextView view, boolean isBoolean) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"statusOfOrder"})
    public static final void statusOfOrder(@org.jetbrains.annotations.NotNull()
    android.widget.TextView txtView, int orderNumberState) {
    }
    
    private BindingAdapters() {
        super();
    }
}