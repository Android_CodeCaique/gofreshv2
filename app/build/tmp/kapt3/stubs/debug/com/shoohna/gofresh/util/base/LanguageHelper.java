package com.shoohna.gofresh.util.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a8\u0006\t"}, d2 = {"Lcom/shoohna/gofresh/util/base/LanguageHelper;", "", "()V", "ChangeLang", "", "resources", "Landroid/content/res/Resources;", "locale1", "", "app_debug"})
public final class LanguageHelper {
    
    public final void ChangeLang(@org.jetbrains.annotations.NotNull()
    android.content.res.Resources resources, @org.jetbrains.annotations.Nullable()
    java.lang.String locale1) {
    }
    
    public LanguageHelper() {
        super();
    }
}