package com.shoohna.shoohna.util.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u001a\u001a\u00020\u001bH\u0002J\u0006\u0010\u001c\u001a\u00020\u001bJ\u0006\u0010\u001d\u001a\u00020\u001bJ\u000e\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020 J\"\u0010!\u001a\u00020\u001b2\u0006\u0010\"\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u00062\b\u0010$\u001a\u0004\u0018\u00010%H\u0014J-\u0010&\u001a\u00020\u001b2\u0006\u0010\"\u001a\u00020\u00062\u000e\u0010\'\u001a\n\u0012\u0006\b\u0001\u0012\u00020)0(2\u0006\u0010*\u001a\u00020+H\u0016\u00a2\u0006\u0002\u0010,J\u000e\u0010-\u001a\u00020\u001b2\u0006\u0010.\u001a\u00020\rJ\u000e\u0010/\u001a\u00020\u001b2\u0006\u0010.\u001a\u00020\rJ\u000e\u00100\u001a\u00020\u001b2\u0006\u0010\u0002\u001a\u00020\u0003J\u000e\u00101\u001a\u00020\u001b2\u0006\u00102\u001a\u00020)J&\u00103\u001a\u00020\u001b2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u00104\u001a\u00020)2\u0006\u00102\u001a\u00020)2\u0006\u00105\u001a\u000206J\u000e\u00107\u001a\u00020\u001b2\u0006\u00102\u001a\u00020)R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\u0004R\u001a\u0010\f\u001a\u00020\rX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00068"}, d2 = {"Lcom/shoohna/shoohna/util/base/BaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "CAMERA_REQUEST", "", "GALLERY_REQUEST", "PERMISSIONS_REQUEST", "getContext", "()Landroid/content/Context;", "setContext", "imageView", "Landroid/widget/ImageView;", "getImageView", "()Landroid/widget/ImageView;", "setImageView", "(Landroid/widget/ImageView;)V", "imgUri", "Landroid/net/Uri;", "getImgUri", "()Landroid/net/Uri;", "setImgUri", "(Landroid/net/Uri;)V", "progress", "Landroid/app/ProgressDialog;", "askForPermissions", "", "checkPermissions", "dismissProgressDialog", "gotoAnotherActivity", "activity", "Landroid/app/Activity;", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "openCameraIntent", "view", "openGalleryIntent", "showDialog", "showLongToast", "message", "showProgressDialog", "title", "cancellable", "", "showShortToast", "app_debug"})
public final class BaseActivity extends androidx.appcompat.app.AppCompatActivity {
    private android.app.ProgressDialog progress;
    private final int PERMISSIONS_REQUEST = 1;
    private final int GALLERY_REQUEST = 2;
    private final int CAMERA_REQUEST = 3;
    @org.jetbrains.annotations.NotNull()
    public android.widget.ImageView imageView;
    @org.jetbrains.annotations.NotNull()
    public android.net.Uri imgUri;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context context;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.ImageView getImageView() {
        return null;
    }
    
    public final void setImageView(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.net.Uri getImgUri() {
        return null;
    }
    
    public final void setImgUri(@org.jetbrains.annotations.NotNull()
    android.net.Uri p0) {
    }
    
    public final void showShortToast(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void showLongToast(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void showProgressDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean cancellable) {
    }
    
    public final void dismissProgressDialog() {
    }
    
    public final void checkPermissions() {
    }
    
    private final void askForPermissions() {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public final void gotoAnotherActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void openGalleryIntent(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView view) {
    }
    
    public final void openCameraIntent(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView view) {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public final void showDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    public BaseActivity(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}