package com.shoohna.shoohna.util.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u001b\u001a\u00020\u001cJ\u0006\u0010\u001d\u001a\u00020\u001cJ\u0006\u0010\u001e\u001a\u00020\u001fJ\u0006\u0010 \u001a\u00020\u001fJ\u0006\u0010!\u001a\u00020\u001fJ\u0006\u0010\"\u001a\u00020\u001cJ\u000e\u0010#\u001a\u00020\u000e2\u0006\u0010$\u001a\u00020%J\u000e\u0010&\u001a\u00020\u001c2\u0006\u0010\'\u001a\u00020(J\"\u0010)\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u00042\u0006\u0010+\u001a\u00020\u00042\b\u0010,\u001a\u0004\u0018\u00010-H\u0016J-\u0010.\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\u00042\u000e\u0010/\u001a\n\u0012\u0006\b\u0001\u0012\u000201002\u0006\u00102\u001a\u000203H\u0016\u00a2\u0006\u0002\u00104J\u000e\u00105\u001a\u00020\u001c2\u0006\u00106\u001a\u000207J\u000e\u00108\u001a\u00020\u001c2\u0006\u00106\u001a\u000207J\u000e\u00109\u001a\u00020\u001c2\u0006\u0010:\u001a\u00020;J\u000e\u0010<\u001a\u00020\u001c2\u0006\u0010=\u001a\u00020>J\u0016\u0010?\u001a\u00020\u001c2\u0006\u0010=\u001a\u00020>2\u0006\u0010@\u001a\u000201J\u000e\u0010A\u001a\u00020\u001c2\u0006\u0010@\u001a\u000201J\u000e\u0010B\u001a\u00020\u001c2\u0006\u0010:\u001a\u000207J&\u0010C\u001a\u00020\u001c2\u0006\u0010=\u001a\u00020>2\u0006\u0010D\u001a\u0002012\u0006\u0010@\u001a\u0002012\u0006\u0010E\u001a\u00020\u001fJ\u000e\u0010F\u001a\u00020\u001c2\u0006\u0010@\u001a\u000201J\u000e\u0010G\u001a\u00020\u001f2\u0006\u0010=\u001a\u00020>R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u00020\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006H"}, d2 = {"Lcom/shoohna/shoohna/util/base/BaseFragment;", "Landroidx/fragment/app/Fragment;", "()V", "CAMERA_REQUEST", "", "GALLERY_REQUEST", "PERMISSIONS_REQUEST", "imageView", "Landroid/widget/ImageView;", "getImageView", "()Landroid/widget/ImageView;", "setImageView", "(Landroid/widget/ImageView;)V", "imgUri", "Landroid/net/Uri;", "getImgUri", "()Landroid/net/Uri;", "setImgUri", "(Landroid/net/Uri;)V", "progress", "Landroid/app/ProgressDialog;", "sharedHelper", "Lcom/shoohna/gofresh/util/base/SharedHelper;", "getSharedHelper", "()Lcom/shoohna/gofresh/util/base/SharedHelper;", "setSharedHelper", "(Lcom/shoohna/gofresh/util/base/SharedHelper;)V", "askForPermissions", "", "askForPermissionsImage", "checkPermissionGallery", "", "checkPermissionImage", "checkPermissions", "dismissProgressDialog", "getImageUriFromBitmap", "bitmap", "Landroid/graphics/Bitmap;", "gotoAnotherActivity", "activityToGo", "Landroid/app/Activity;", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "openCameraIntent", "view", "Lde/hdodenhof/circleimageview/CircleImageView;", "openGalleryIntent", "share", "v", "Landroid/view/View;", "showAlert", "context", "Landroid/content/Context;", "showDialog", "message", "showLongToast", "showPictureDialog", "showProgressDialog", "title", "cancellable", "showShortToast", "verifyAvailableNetwork", "app_debug"})
public class BaseFragment extends androidx.fragment.app.Fragment {
    private android.app.ProgressDialog progress;
    private final int PERMISSIONS_REQUEST = 1;
    private final int GALLERY_REQUEST = 2;
    private final int CAMERA_REQUEST = 3;
    @org.jetbrains.annotations.NotNull()
    public android.widget.ImageView imageView;
    @org.jetbrains.annotations.NotNull()
    public android.net.Uri imgUri;
    @org.jetbrains.annotations.NotNull()
    private com.shoohna.gofresh.util.base.SharedHelper sharedHelper;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.ImageView getImageView() {
        return null;
    }
    
    public final void setImageView(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.net.Uri getImgUri() {
        return null;
    }
    
    public final void setImgUri(@org.jetbrains.annotations.NotNull()
    android.net.Uri p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shoohna.gofresh.util.base.SharedHelper getSharedHelper() {
        return null;
    }
    
    public final void setSharedHelper(@org.jetbrains.annotations.NotNull()
    com.shoohna.gofresh.util.base.SharedHelper p0) {
    }
    
    public final void showShortToast(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void showLongToast(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void showProgressDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean cancellable) {
    }
    
    public final void dismissProgressDialog() {
    }
    
    public final boolean checkPermissions() {
        return false;
    }
    
    public final void askForPermissions() {
    }
    
    public final void askForPermissionsImage() {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public final void gotoAnotherActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activityToGo) {
    }
    
    public final void openGalleryIntent(@org.jetbrains.annotations.NotNull()
    de.hdodenhof.circleimageview.CircleImageView view) {
    }
    
    public final void openCameraIntent(@org.jetbrains.annotations.NotNull()
    de.hdodenhof.circleimageview.CircleImageView view) {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.net.Uri getImageUriFromBitmap(@org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap bitmap) {
        return null;
    }
    
    public final boolean verifyAvailableNetwork(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public final void showPictureDialog(@org.jetbrains.annotations.NotNull()
    de.hdodenhof.circleimageview.CircleImageView v) {
    }
    
    public final boolean checkPermissionImage() {
        return false;
    }
    
    public final boolean checkPermissionGallery() {
        return false;
    }
    
    public final void showDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void showAlert(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void share(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public BaseFragment() {
        super();
    }
}