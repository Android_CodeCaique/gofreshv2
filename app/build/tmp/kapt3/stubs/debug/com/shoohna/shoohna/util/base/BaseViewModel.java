package com.shoohna.shoohna.util.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0006J&\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0006J\u001e\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0006J\u001e\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0006J\u0016\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u001e\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001dJ\u0016\u0010!\u001a\u00020\u00192\u0006\u0010 \u001a\u00020\u00062\u0006\u0010\u001a\u001a\u00020\u001bR&\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\n\u00a8\u0006\""}, d2 = {"Lcom/shoohna/shoohna/util/base/BaseViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "liveList", "Landroidx/lifecycle/MutableLiveData;", "Ljava/util/ArrayList;", "", "getLiveList", "()Landroidx/lifecycle/MutableLiveData;", "setLiveList", "(Landroidx/lifecycle/MutableLiveData;)V", "modelData", "getModelData", "setModelData", "checkEmpty", "", "textInputLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "data", "message", "checkEquals", "data2", "checkValidEmail", "checkValidPassword", "confirmOrderDialog", "", "v", "Landroid/view/View;", "context", "Landroid/content/Context;", "showAlertProduct", "view", "title", "showDialog", "app_debug"})
public class BaseViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> modelData;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.ArrayList<java.lang.String>> liveList;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getModelData() {
        return null;
    }
    
    public final void setModelData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.ArrayList<java.lang.String>> getLiveList() {
        return null;
    }
    
    public final void setLiveList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    public final boolean checkEmpty(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout textInputLayout, @org.jetbrains.annotations.NotNull()
    java.lang.String data, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
        return false;
    }
    
    public final boolean checkValidEmail(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout textInputLayout, @org.jetbrains.annotations.NotNull()
    java.lang.String data, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
        return false;
    }
    
    public final boolean checkValidPassword(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout textInputLayout, @org.jetbrains.annotations.NotNull()
    java.lang.String data, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
        return false;
    }
    
    public final boolean checkEquals(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout textInputLayout, @org.jetbrains.annotations.NotNull()
    java.lang.String data, @org.jetbrains.annotations.NotNull()
    java.lang.String data2, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
        return false;
    }
    
    public final void showDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    public final void showAlertProduct(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void confirmOrderDialog(@org.jetbrains.annotations.NotNull()
    android.view.View v, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public BaseViewModel() {
        super();
    }
}