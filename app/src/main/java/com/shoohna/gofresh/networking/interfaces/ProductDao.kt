package com.shoohna.gofresh.networking.interfaces

import androidx.room.*
import com.shoohna.gofresh.pojo.model.ProductEntity
import com.shoohna.gofresh.pojo.model.ProductSelectionCart

@Dao
interface ProductDao {

    @Query("SELECT * FROM ProductEntity")
    fun getAllProducts(): List<ProductEntity>

    @Query("SELECT * FROM ProductEntity WHERE product_id = :productId")
    fun getAllProductsById(productId:Int) : List<ProductEntity>

    @Query("DELETE FROM ProductEntity WHERE product_id = :productId")
    fun deleteProductById(productId: Int)

    @Query("UPDATE ProductEntity SET quantity = :quantity WHERE product_id = :productId")
    fun updateQuantityOfProduct(quantity:Int , productId: Int)

    @Query("SELECT * FROM ProductEntity WHERE product_id =:productId")
    fun getCurrentQuantityOfProductId(productId: Int): List<ProductEntity>

    @Query("SELECT product_id , color_id , size_id , quantity FROM ProductEntity")
    fun getAllSelectedProducts() : List<ProductSelectionCart>

    @Query("DELETE FROM ProductEntity")
    fun deleteAllProductDatabase()

    @Update
    fun updateProduct(productEntity: ProductEntity)

    @Insert
    fun insertProduct(productEntity: ProductEntity)

    @Delete
    fun deleteProduct(productEntity: ProductEntity)
}