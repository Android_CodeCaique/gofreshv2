package com.shoohna.gofresh.pojo.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class ProductEntity (
    @PrimaryKey val product_id: Int,
    @ColumnInfo(name = "color_id") val color_id: Int,
    @ColumnInfo(name = "size_id") val size_id: Int,
    @ColumnInfo(name = "size_name")val size_name: String,
    @ColumnInfo(name = "quantity") val quantity: Int,
    @ColumnInfo(name = "imageURL") val imageURL :  String,
    @ColumnInfo(name = "color_code") val color_code:String,
    @ColumnInfo(name = "desc") val desc:String,
    @ColumnInfo(name = "price") val price:String,
    @ColumnInfo(name = "name") val name:String
)
//, typeAffinity = ColumnInfo.BLOB
//@NonNull