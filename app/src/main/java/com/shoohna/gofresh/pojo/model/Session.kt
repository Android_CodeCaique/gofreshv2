package com.shoohna.gofresh.pojo.model

class Session(val email: String,
              val frist_name: Any,
              val id: Int,
              val image: String,
              val lang: Any,
              val last_name: Any,
              val lat: String,
              val lng: String,
              val message: Int,
              val notification: Int,
              val phone: Any,
              val social: String,
              val status: String,
              val token: String) {

}