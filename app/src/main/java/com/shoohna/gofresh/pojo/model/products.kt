package com.shoohna.gofresh.pojo.model

import com.google.gson.annotations.SerializedName

data class products (

    @SerializedName("products")
    var x : List<ProductSelectionCart>

)
