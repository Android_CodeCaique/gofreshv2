package com.shoohna.gofresh.pojo.request

import com.shoohna.gofresh.pojo.responses.RegisterResponse

data class LoginRequest(val message: String, val status: Boolean, val token: String, val user: RegisterResponse)

