package com.shoohna.gofresh.pojo.request

import android.text.TextUtils
import android.util.Patterns
import androidx.databinding.BaseObservable

class RegisterRequest(private var phone :String ,
                      private var email : String ,
                      private var frist_name : String ,
                      private var last_name : String  ,
                      private var lat : String ,
                      private var lng : String ,
                      private var password : String ,
                      private var password_confirmation : String ,
                      private var verify_type : String ) : BaseObservable()
{

    // This for all data validation for final all
    val isDataValid : Boolean
        get() = (!TextUtils.isEmpty(email))
            && Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches()
            && email.contains("@")
            && email.contains(".")
            && (!TextUtils.isEmpty(password))
            && password.length > 5
            && (!TextUtils.isEmpty(phone))
            && phone.length > 10
            && (!TextUtils.isEmpty(frist_name))
            && (!TextUtils.isEmpty(last_name))
            && (!TextUtils.isEmpty(verify_type))
            && (!TextUtils.isEmpty(password_confirmation))
            && password_confirmation.length > 5
            && password.equals(password_confirmation)

    fun getEmail() : String {
        return  email 
    }
    fun getPhone() : String {
        return  phone 
    }
    fun getFrist_name() : String {
        return  frist_name 
    }
    fun getLast_name() : String {
        return  last_name 
    }
    fun getLat() : String {
        return  lat 
    }
    fun getLng() : String {
        return  lng 
    }
    fun getPassword() : String {
        return  password 
    }
    fun getPassword_confirmation() : String {
        return  password_confirmation 
    }
    fun getVerify_type() : String {
        return  verify_type 
    }

    fun setEmail( email: String){
        this.email = email
    }

    fun setPhone( email: String){
        this.email = email
    }

}