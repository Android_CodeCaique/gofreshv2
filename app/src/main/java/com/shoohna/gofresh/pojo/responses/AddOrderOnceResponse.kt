package com.shoohna.gofresh.pojo.responses

data class AddOrderOnceResponse(
    val `data`: AddOrderOnceData,
    val message: String,
    val status: Int
)

data class AddOrderOnceData(
    val address: AddOrderOnceAddress,
    val date: String,
    val email: Any,
    val id: Int,
    val name: Any,
    val payment_method: String,
    val phone: Any,
    val products: List<AddOrderOnceProduct>,
    val rate: Any,
    val report: Any,
    val shipping_price: Double,
    val status: Int,
    val total_price: Double
)

data class AddOrderOnceAddress(
    val address: String,
    val id: Int,
    val lat: Int,
    val lng: Int,
    val name: String
)

data class AddOrderOnceProduct(
    val color: AddOrderOnceColor,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price: String,
    val quantity: Int,
    val rate: String,
    val size: AddOrderOnceSize
)

data class AddOrderOnceColor(
    val color_code: String,
    val id: Int
)

data class AddOrderOnceSize(
    val id: Int,
    val name: String
)