package com.shoohna.gofresh.pojo.responses

data class AddToCartResponse(
    val data: String,
    val message: String,
    val status: Boolean
)

