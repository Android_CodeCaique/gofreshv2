package com.shoohna.gofresh.pojo.responses

data class CheckoutOrderAddressResponse(
    val data: CheckoutOrderAddressData,
    val message: String,
    val status: Int
)

data class CheckoutOrderAddressData(
    val address: CheckoutOrderAddressAddress,
    val email: Any,
    val id: Int,
    val name: Any,
    val payment_method: Any,
    val phone: Any,
    val products: List<CheckoutOrderAddressProduct>,
    val rate: Any,
    val report: Any,
    val shipping_price: Int,
    val status: String,
    val total_price: Int,
    val user: CheckoutOrderAddressUser
)

data class CheckoutOrderAddressAddress(
    val address: Any,
    val id: Int,
    val lat: Double,
    val lng: Double,
    val name: String
)

data class CheckoutOrderAddressProduct(
    val color: CheckoutOrderAddressColor,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price: String,
    val quantity: Int,
    val rate: String,
    val size: CheckoutOrderAddressSize
)

data class CheckoutOrderAddressUser(
    val code: Any,
    val created_at: String,
    val currency_id: Any,
    val email: String,
    val fire_base_token: Any,
    val frist_name: String,
    val id: Int,
    val image: Any,
    val lang: Any,
    val last_name: String,
    val lat: String,
    val lng: String,
    val message: Any,
    val notification: Any,
    val phone: String,
    val shop_id: String,
    val social: String,
    val status: String,
    val updated_at: String
)

data class CheckoutOrderAddressColor(
    val color_code: String,
    val id: Int
)

data class CheckoutOrderAddressSize(
    val id: Int,
    val name: String
)