package com.shoohna.gofresh.pojo.responses

data class DeleteFromCartResponse(
    val data: Any,
    val message: String,
    val status: Boolean
)