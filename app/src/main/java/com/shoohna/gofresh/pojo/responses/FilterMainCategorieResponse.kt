package com.shoohna.gofresh.pojo.responses

data class FilterMainCategorieResponse(
    val data: List<FilerMainCategorieData>,
    val message: String,
    val status: Int
)

data class FilerMainCategorieData(
    val desc: String,
    val id: Int,
    val image: String,
    val name: String,
    var isChecked:Boolean = false

)