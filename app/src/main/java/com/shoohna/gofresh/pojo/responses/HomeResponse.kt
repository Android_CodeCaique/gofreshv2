package com.shoohna.gofresh.pojo.responses

data class HomeResponse(val data: Data, val message: String, val status: Int)

data class Data(val Offers: List<Offer>, val best_seller: List<BestSeller>, val high_rated: List<HighRated>,val sliders: List<sliders>)

data class Offer(
    val colors: List<Color>,
    val desc: String,
    val distance: String,
    val id: Int,
    val image: String,
    val images: List<Image>,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price_product: String,
    val price_after_offer: String,
    val quantity: String,
    val rate: Any,
    val sizes: List<Size>
)

data class BestSeller(
    val colors: List<Color>,
    val desc: String,
    val distance: String,
    val id: Int,
    val image: String,
    val images: List<Image>,
    val is_favorite: Boolean,
    val is_offer: Int,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price_product: String,
    val quantity: Any,
    val rate: String,
    val sizes: List<Size>
)

data class sliders(
        val id : Int ,
        val image : String,
        val title_ar : String ,
        val title_en : String ,
        val sub_title_ar : String ,
        val sub_title_en : String ,
        val btn_text_ar : String ,
        val btn_text_en : String ,
        val btn_link : String
)

data class HighRated(
    val colors: List<Color>,
    val desc: String,
    val distance: String,
    val id: Int,
    val image: String,
    val images: List<Image>,
    val is_favorite: Boolean,
    val is_offer: Int,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: Any,
    val price_product: String,
    val quantity: Any,
    val rate: String,
    val sizes: List<Size>
)

data class Color(val color_code: String, val id: Int)

data class Image(val id: Int, val image: String)

data class Size(val id: Int, val name: String)