package com.shoohna.gofresh.pojo.responses

data class MyCartResponse(
    val `data`: List<MyCartData>,
    val message: String,
    val status: Int,
    val total_price: Int
)

data class MyCartData(
    val color: MyCartColor,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price: Int,
    val quantity: Int,
    val rate: String,
    val size: MyCartSize
)

data class MyCartColor(
    val color_code: String,
    val id: Int
)

data class MyCartSize(
    val id: Int,
    val name: String
)