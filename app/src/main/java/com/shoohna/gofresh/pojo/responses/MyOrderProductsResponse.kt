package com.shoohna.gofresh.pojo.responses

data class MyOrderProductsResponse(
    val `data`: MyOrderProductsData,
    val message: String,
    val status: Int
)

data class MyOrderProductsData(
    val address: MyOrderProductsAddress,
    val date: String,
    val id: Int,
    val payment_method: Int,
    val products: List<MyOrderProductsProduct>,
    val rate: Int,
    val report: Any,
    val shipping_price: Int,
    val status: Int,
    val total_price: Double
)

data class MyOrderProductsAddress(
    val address: String,
    val id: Int,
    val lat: Int,
    val lng: Int,
    val name: String
)

data class MyOrderProductsProduct(
    val color: Any,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: Int,
    val lat: Int,
    val lng: Int,
    val name: String,
    val offer_amount: String,
    val price: String,
    val quantity: Int,
    val rate: String,
    val size: Any
)