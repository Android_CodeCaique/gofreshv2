package com.shoohna.gofresh.pojo.responses

data class MyOrdersResponse(
    val `data`: List<MyOrdersData>,
    val message: String,
    val status: Int
)

data class MyOrdersData(
    val address: MyOrdersAddress,
    val date: String,
    val id: Int,
    val payment_method: Int,
    val products: List<MyOrdersProduct>,
    val rate: Int,
    val report: String,
    val shipping_price: Int,
    val status: Int,
    val total_price: Double
)

data class MyOrdersAddress(
    val address: String,
    val id: Int,
    val lat: Int,
    val lng: Int,
    val name: String
)

data class MyOrdersProduct(
    val color: Any,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: Int,
    val lat: Int,
    val lng: Int,
    val name: String,
    val offer_amount: String,
    val price: String,
    val quantity: Int,
    val rate: String,
    val size: Any
)