package com.shoohna.gofresh.pojo.responses

data class PerviousChatResponse(
    val data: List<PerviousChatData>,
    val message: String,
    val status: Int
)

data class PerviousChatData(
    val id: Int,
    val message: String,
    val sender: Int
)