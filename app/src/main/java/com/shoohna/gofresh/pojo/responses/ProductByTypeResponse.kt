package com.shoohna.gofresh.pojo.responses

data class ProductByTypeResponse(
    val data: List<ProductByTypeData>,
    val message: String,
    val status: Int
)

data class ProductByTypeData(
    val colors: List<ProductByTypeColor>,
    val desc: String,
    val distance: String,
    val id: Int,
    val image: String,
    val images: List<ProductByTypeImage>,
    val is_favorite: Boolean,
    val is_offer: Any,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: Any,
    val price_product: String,
    val quantity: Any,
    val rate: String,
    val sizes: List<ProductByTypeSize>
)

data class ProductByTypeColor(
    val color_code: String,
    val id: Int
)

data class ProductByTypeImage(
    val id: Int,
    val image: String
)


data class ProductByTypeSize(
    val id: Int,
    val name: String
)