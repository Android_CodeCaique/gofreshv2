package com.shoohna.gofresh.pojo.responses

data class ProductDetailsResponse(
    val data: ProductData,
    val message: String,
    val status: Int
)

data class ProductData(
    val colors: List<ProductColor>,
    val desc: String,
    val distance: String,
    val id: Int,
    val image: String,
    val images: List<ProductImage>,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price_product: String,
    val price_after_offer: String,
    val quantity: Any,
    val rate: String,
    val sizes: List<ProductSize>
)

data class ProductColor(
    val color_code: String,
    val id: Int,
    var isChecked:Boolean = false
)

data class ProductImage(
    val id: Int,
    val image: String
)

data class ProductSize(
    val id: Int,
    val name: String,
    var isChecked:Boolean = false

)