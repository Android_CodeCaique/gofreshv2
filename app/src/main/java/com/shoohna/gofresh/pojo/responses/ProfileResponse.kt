package com.shoohna.gofresh.pojo.responses

data class ProfileResponse(
    val `data`: ProfileData,
    val message: String,
    val status: Boolean
)

data class ProfileData(
    val email: String,
    val frist_name: String,
    val id: Int,
    val image: String,
    val lang: String,
    val last_name: String,
    val lat: String,
    val lng: String,
    val message: Int,
    val notification: Int,
    val phone: String,
    val social: String,
    val status: String
)