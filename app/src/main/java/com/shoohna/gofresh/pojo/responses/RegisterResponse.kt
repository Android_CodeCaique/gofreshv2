package com.shoohna.gofresh.pojo.responses

data class RegisterResponse(
    val `data`: UserData,
    val message: String,
    val status: Int
)

data class UserData(
    val code: String,
    val created_at: String,
    val currency_id: String,
    val email: String,
    val frist_name: String,
    val id: Int,
    val image: Any,
    val lang: String,
    val last_name: String,
    val lat: String,
    val lng: String,
    val message: String,
    val notification: String,
    val phone: String,
    val shop_id: String,
    val social: String,
    val status: String,
    val token: String,
    val updated_at: String
)