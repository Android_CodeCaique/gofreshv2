package com.shoohna.gofresh.pojo.responses

data class UpdateCartResponse(
    val data: UpdateCartData,
    val message: String,
    val status: Int
)

data class UpdateCartData(
    val my_cart: List<UpdateCartMyCart>,
    val products_price: Double,
    val shipping_price: Double
)

data class UpdateCartMyCart(
    val color: UpdateCartColor,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: Int,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price: String,
    val quantity: Int,
    val rate: String,
    val size: UpdateCartSize
)

data class UpdateCartColor(
    val color_code: String,
    val id: Int
)

data class UpdateCartSize(
    val id: Int,
    val name: String
)