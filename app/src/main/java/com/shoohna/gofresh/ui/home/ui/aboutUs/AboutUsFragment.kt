package com.shoohna.gofresh.ui.home.ui.aboutUs

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shoohna.gofresh.databinding.FragmentAboutUsBinding
import com.shoohna.gofresh.databinding.FragmentContactUsBinding
import com.shoohna.gofresh.pojo.responses.AboutUsData2
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class AboutUsFragment : Fragment() {

    lateinit var binding: FragmentAboutUsBinding
    var aboutUsData= MutableLiveData<List<AboutUsData2>>()
    private val aboutUsViewModel: AboutUsViewModel by inject()   // 1viewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentAboutUsBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val viewModel = ViewModelProvider(this).get(AboutUsViewModel::class.java)

        binding.vm = aboutUsViewModel

        aboutUsViewModel.loadData(binding.root , binding.logo)

        aboutUsViewModel.getAboutUsList(requireActivity()).observe(viewLifecycleOwner, Observer {

            aboutUsData.value = it

            binding.aboutUsViewRecyclerViewId.adapter =
                AboutUsRecyclerAdapter(aboutUsData, requireActivity())

        })

        return binding.root
    }

}
