package com.shoohna.gofresh.ui.home.ui.aboutUs

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.google.android.material.snackbar.Snackbar
import com.shoohna.gofresh.R
import com.shoohna.gofresh.networking.interfaces.General
import com.shoohna.gofresh.pojo.responses.AboutUsData2
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import retrofit2.HttpException


class AboutUsViewModel(private val serviceGeneral: General) : BaseViewModel() {

    var description = MutableLiveData<String>("")
    var baseFragment: BaseFragment = BaseFragment()
    private var aboutUsList: MutableLiveData<List<AboutUsData2>>? = null
    var loader = MutableLiveData<Boolean>(false)

    fun loadData(v: View , imageView: ImageView) {
        try {

            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )


            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.aboutUsFunction(lang.toString() , "1")
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful) {
                                if (it.body()!!.status == 1) {
                                    //Do something with response e.g show to the UI.
                                    description.value = it.body()!!.data.about

                                    Picasso.get()
                                        .load(it.body()!!.data.logo)
                                        .error(R.drawable.ic_profile_upload)
                                        .into(imageView)

//                            Snackbar.make(v, response.message(), Snackbar.LENGTH_SHORT).show();
                                } else if (it.body()!!.status == 2) {
//                            Snackbar.make(v, response.body()?.message!!, Snackbar.LENGTH_SHORT)
//                                .show()
                                    baseFragment.dismissProgressDialog()
                                } else if (it.body()!!.status == 0) {
                                    Snackbar.make(
                                        v,
                                        "${it.body()!!.message}",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                    baseFragment.dismissProgressDialog()
                                    //baseFragment.dismissProgressDialog()
                                } else {

                                }
                            }

                        } catch (e: HttpException) {
                            Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                                .show();
                            //baseFragment.dismissProgressDialog()

                        } catch (e: Throwable) {
                            Snackbar.make(
                                v,
                                "Ooops: Something else went wrong",
                                Snackbar.LENGTH_SHORT
                            )
                                .show();
                            //baseFragment.dismissProgressDialog()
                        }
                    }
                }.onFailure {
                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT)
                        .show();
                }
            }

//            val service = ApiClient.makeRetrofitServiceGeneral()
//            CoroutineScope(Dispatchers.IO).async {
//                val response = service.aboutUsFunction(lang.toString() , "1")
//                withContext(Dispatchers.Main) {
//                    try {
//                        if (response.isSuccessful) {
//                            if (response.body()!!.status == 1) {
//                                //Do something with response e.g show to the UI.
//                                description.value = response.body()!!.data.about
//
//                                Picasso.get()
//                                    .load(response.body()!!.data.logo)
//                                    .error(R.drawable.ic_logo_two)
//                                    .into(imageView)
//
////                            Snackbar.make(v, response.message(), Snackbar.LENGTH_SHORT).show();
//                            } else if (response.body()!!.status == 2) {
////                            Snackbar.make(v, response.body()?.message!!, Snackbar.LENGTH_SHORT)
////                                .show()
//                                baseFragment.dismissProgressDialog()
//                            } else if (response.body()!!.status == 0) {
//                                Snackbar.make(
//                                    v,
//                                    "${response.body()!!.message}",
//                                    Snackbar.LENGTH_SHORT
//                                ).show()
//                                baseFragment.dismissProgressDialog()
//                                //baseFragment.dismissProgressDialog()
//                            } else {
//
//                            }
//                        }
//
//                    } catch (e: HttpException) {
//                        Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT).show();
//                        //baseFragment.dismissProgressDialog()
//
//                    } catch (e: Throwable) {
//                        Snackbar.make(v, "Ooops: Something else went wrong", Snackbar.LENGTH_SHORT)
//                            .show();
//                        //baseFragment.dismissProgressDialog()
//                    }
//                }
//            }
        }catch (e:Exception)
        {
            Snackbar.make(v, e.message.toString(), Snackbar.LENGTH_SHORT).show();
        }
    }
    fun share (v : View )
    {
        baseFragment.share(v)
    }

    fun loadShopData(context: Context)
    {
        loader.value = true

        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(context , "MyLang" )

        CoroutineScope(Dispatchers.IO).async {
            runCatching {
                serviceGeneral.aboutUs(lang.toString() , "1")
            }.onSuccess {
                withContext(Dispatchers.Main) {
                    try {
                        if (it.isSuccessful && it.body()?.status == 1) {
                            Log.i("response", it.body()!!.message)
                            aboutUsList!!.postValue(it.body()!!.data)
                            loader.value = false

                        } else {
                            Log.i("loadData1", it.message().toString())
                            loader.value = false
                        }
                    } catch (e: Exception) {
                        Log.i("loadData2", e.message.toString())
                        loader.value = false

                    }
                }
            }.onFailure {
                Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
            }
        }


//        val service = ApiClient.makeRetrofitServiceGeneral()
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.aboutUs(lang.toString() , "1")
//            withContext(Dispatchers.Main) {
//                try{
//                    if(response.isSuccessful && response.body()?.status == 1){
//                        Log.i("response",response.body()!!.message)
//                        aboutUsList!!.postValue(response.body()!!.data)
//                        loader.value = false
//
//                    }
//                    else {
//                        Log.i("loadData1",response.message().toString())
//                        loader.value = false
//                    }
//                }catch (e:Exception){
//                    Log.i("loadData2",e.message.toString())
//                    loader.value = false
//
//                }
//
//            }
//        }
    }

    internal fun getAboutUsList(context: Context): MutableLiveData<List<AboutUsData2>> {
        if (aboutUsList == null) {
            aboutUsList = MutableLiveData()
            loadShopData(context)

        }
        return aboutUsList as MutableLiveData<List<AboutUsData2>>
    }
}

