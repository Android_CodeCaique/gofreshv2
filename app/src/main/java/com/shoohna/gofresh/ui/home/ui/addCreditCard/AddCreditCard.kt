package com.shoohna.gofresh.ui.home.ui.addCreditCard

import ApiClient.sharedHelper
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentAddCreditCardBinding
import com.shoohna.gofresh.ui.welcome.WelcomeActivity
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.shoohna.util.base.BaseFragment
import org.koin.android.ext.android.inject

class AddCreditCard : BaseFragment() {
    private lateinit var nav: NavController

    private val viewModel:AddCardViewModel by inject()

    lateinit var binding : FragmentAddCreditCardBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentAddCreditCardBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel=viewModel

        binding.btnSaveId.setOnClickListener {
            if (sharedHelper.getKey(requireContext(), Constants.getToken())?.isEmpty()!!) {
                showAlert(requireContext())
            } else {
                showAlert2(requireContext())
            }
        }
        nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)

        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }



        return binding.root
    }

    fun showAlert2(context: Context)
    {
        val alert: Dialog? = Dialog(context)
        alert?.setContentView(R.layout.ask_authorization_payment)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert?.findViewById<Button>(R.id.confirmBtnId)?.setOnClickListener {
            val selectedId = alert?.findViewById<RadioGroup>(R.id.radioGroupId).checkedRadioButtonId
            val radioButton = alert.findViewById<RadioButton>(selectedId)
            if(radioButton.text == resources.getString(R.string.switchToPreAuthorizationPayment)){
                nav.navigate(R.id.action_addCreditCard_to_preAuthorizationPayment)
                alert.dismiss()
            }
            if(radioButton.text == resources.getString(R.string.noPreAuthorization)) {
                requireActivity().onBackPressed()
                alert.dismiss()

            }
        }


        alert?.show()
    }

}