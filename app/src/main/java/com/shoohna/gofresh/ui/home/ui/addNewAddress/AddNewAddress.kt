package com.shoohna.gofresh.ui.home.ui.addNewAddress

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.navigation.Navigation
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentAddNewAddressBinding


class AddNewAddress : Fragment() {

    lateinit var binding : FragmentAddNewAddressBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentAddNewAddressBinding.inflate(inflater)

        binding.btnCheckoutId.setOnClickListener {
            showAlert(requireContext())
        }
        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        return binding.root
    }

    fun showAlert(context: Context)
    {
        val alert: Dialog? = Dialog(context)
        alert?.setContentView(R.layout.request_requested_dialog)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        alert?.findViewById<Button>(R.id.confirmBtnId)?.setOnClickListener {
//
//        }


        alert?.show()
    }

}