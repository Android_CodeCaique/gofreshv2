package com.shoohna.gofresh.ui.home.ui.cart

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TimePicker
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.PagerSnapHelper
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentCartBinding
import com.shoohna.gofresh.pojo.model.ProductEntity
import me.ithebk.barchart.BarChartModel
import org.koin.android.ext.android.inject
import java.util.*


/**
 * A simple [Fragment] subclass.
 */

class CartFragment : Fragment() {
    lateinit var binding: FragmentCartBinding
    var myCartData= MutableLiveData<List<ProductEntity>>()
    private val cartViewModel: CartViewModel by inject()   // 1
    val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)
    val hour = c.get(Calendar.HOUR)
    val min = c.get(Calendar.MINUTE)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCartBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val model = ViewModelProvider(this).get(CartViewModel::class.java)
        binding.vm = cartViewModel

        val helper = PagerSnapHelper()
        helper.attachToRecyclerView(binding.cardRecyclerViewId)
        binding.cardRecyclerViewId.setHasFixedSize(true)

        cartViewModel.loadDataFromRoom(requireActivity())
        cartViewModel.myCartList.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) {
                binding.noDataTxtId.visibility = View.VISIBLE
                binding.cartRecyclerViewId.visibility = View.GONE
//                binding.checkOutBtnId.visibility = View.GONE
            } else {
                binding.noDataTxtId.visibility = View.GONE
                binding.cartRecyclerViewId.visibility = View.VISIBLE
//                binding.checkOutBtnId.visibility = View.VISIBLE

                myCartData.value = it
                binding.cartRecyclerViewId.adapter =
                    CartRecyclerViewAdapter(myCartData, requireActivity(), cartViewModel)
            }
        })

        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }

//        model.checkEmptyRoomData(activity!!)
//        model.noDataTxt.observe(viewLifecycleOwner, Observer {
//            if(it) {
//                binding.noDataTxtId.visibility = View.VISIBLE
//                binding.cartRecyclerViewId.visibility = View.GONE
//                binding.checkOutBtnId.visibility = View.GONE
//            }
//            else {
//                binding.noDataTxtId.visibility = View.GONE
//                binding.cartRecyclerViewId.visibility = View.VISIBLE
//                binding.checkOutBtnId.visibility = View.VISIBLE
//            }
//
//        })

//        binding.checkOutBtnId.setOnClickListener {
//            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_cartFragment_to_checkoutProcessFragment) }
//            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//        }
//        binding.statusBtnId.setOnClickListener {
//            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_cartFragment_to_orderStatusFragment) }
//            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//        }

        binding.barChartVertical.barMaxValue = 100

        val barChartModel = BarChartModel()
        barChartModel.barValue = 50
        barChartModel.barColor = Color.parseColor("#9C27B0")
        barChartModel.barText = "Calorie"

        val barChartModel2 = BarChartModel()
        barChartModel2.barValue = 80
        barChartModel2.barColor = Color.parseColor("#EE1C25")
        barChartModel2.barText = "Oxygen"
        val barChartModel3 = BarChartModel()
        barChartModel3.barValue = 30
        barChartModel3.barColor = Color.parseColor("#B8CF55")
        barChartModel3.barText = "H20"
        binding.barChartVertical.addBar(barChartModel)
        binding.barChartVertical.addBar(barChartModel2)
        binding.barChartVertical.addBar(barChartModel3)
//        val barChartModelList: List<BarChartModel> = ArrayList()
//        binding.barChartVertical.addBar(barChartModelList)


        binding.cardRecyclerViewId.adapter = CreditCardRecyclerViewAdapter()


//        val time = TimePickerDialog(requireActivity() , TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
//            binding.startHourTxtId.setText("$hourOfDay : $minute ${onTimeSet(view,hourOfDay,minute)}")
//            model.fromTime.value = "$hourOfDay:$minute"
//        },hour,min,false)
//
////            time.updateTime(hour,min)
//        time.show()

        binding.dateTxtViewId.setOnClickListener {
            val dpd = DatePickerDialog(requireActivity(), DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//                binding.dateTxtViewId.text = "$year/${monthOfYear+1}/$dayOfMonth"
                val time = TimePickerDialog(requireActivity() , TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
//                    binding.startHourTxtId.setText("$hourOfDay : $minute ${onTimeSet(view,hourOfDay,minute)}")
//                    model.fromTime.value = "$hourOfDay:$minute"
                    binding.dateTxtViewId.text = "$year/${monthOfYear+1}/$dayOfMonth   $hourOfDay:$minute"

                },hour,min,false)
                time.show()

            }, year, month, day)

            dpd.datePicker.minDate = System.currentTimeMillis() - 1000

            dpd.show()
        }

        binding.addNewCardTxtViewId.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_cartFragment_to_addCreditCard)
        }

        binding.statusBtnId.setOnClickListener {
            binding.useGainedPointsRadioId.isChecked = false
            Navigation.findNavController(it).navigate(R.id.action_cartFragment_to_myAddress)
        }

        binding.useGainedPointsRadioId.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked) showAlert(requireContext())
        }

        return binding.root
    }

    fun showAlert(context: Context)
    {
        val alert: Dialog? = Dialog(context)
        alert?.setContentView(R.layout.gaint_point_dialog)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        alert?.findViewById<Button>(R.id.confirmBtnId)?.setOnClickListener {
//
//        }


        alert?.show()
    }

    fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) : String {
        var hourOfDay = hourOfDay
        var AM_PM = " AM"
        var mm_precede = ""
        if (hourOfDay >= 12) {
            AM_PM = " PM"
            if (hourOfDay in 13..23) {
                hourOfDay -= 12
            } else {
                hourOfDay = 12
            }
        } else if (hourOfDay == 0) {
            hourOfDay = 12
        }
        if (minute < 10) {
            mm_precede = "0"
        }
        return AM_PM
    }

}
