package com.shoohna.gofresh.ui.home.ui.cart

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.CartItemRowBinding
import com.shoohna.gofresh.pojo.model.ProductEntity
import com.shoohna.gofresh.util.base.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CartRecyclerViewAdapter (private var dataList: LiveData<List<ProductEntity>>, private val context: Context, val viewModel: CartViewModel) : RecyclerView.Adapter<CartRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CartItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false),context
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position], this.viewModel , this.context)

    }


    class ViewHolder(private var binding: CartItemRowBinding, context: Context) : RecyclerView.ViewHolder(binding.root) {
        val db = Room.databaseBuilder(context, AppDatabase::class.java, "product_db").build()

        fun bind(item: ProductEntity ,viewModel: CartViewModel , context: Context) {
            binding.modelRoom = item
            binding.executePendingBindings()
            binding.deleteImgId.setOnClickListener {
                viewModel.deleteFromCartRoom(context,item.product_id)
            }
            binding.increaseImgId.setOnClickListener {
                var currentQuantity :Int = binding.quantityTxtId.text.toString().toInt() + 1
                binding.quantityTxtId.text = currentQuantity.toString()
//                var currentPrice :Int = binding.priceTxtViewId.text.toString().toInt()
                binding.priceTxtViewId.text = (item.price.toInt() * currentQuantity).toString()
//                viewModel.currentQuantity.value = currentQuantity
                CoroutineScope(Dispatchers.IO).launch { db.productDao().updateQuantityOfProduct(currentQuantity,item.product_id) }

            }
            binding.decreaseImgId.setOnClickListener {
                var currentQuantity :Int = binding.quantityTxtId.text.toString().toInt() - 1
                if (currentQuantity == 0) Toast.makeText(context,context.getString(R.string.cannotMakeZeroQuantity),
                    Toast.LENGTH_SHORT).show()
                else {
                    binding.quantityTxtId.text = currentQuantity.toString()
//                    var currentPrice :Int = binding.priceTxtViewId.text.toString().toInt()
                    binding.priceTxtViewId.text = (item.price.toInt() * currentQuantity).toString()
//                    viewModel.currentQuantity.value = currentQuantity
                    CoroutineScope(Dispatchers.IO).launch { db.productDao().updateQuantityOfProduct(currentQuantity,item.product_id) }

                }
            }
        }

    }
}
