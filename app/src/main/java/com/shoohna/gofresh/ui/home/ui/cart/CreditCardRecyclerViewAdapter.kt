package com.shoohna.gofresh.ui.home.ui.cart

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.CartItemRowBinding
import com.shoohna.gofresh.databinding.CreditCardViewBinding
import com.shoohna.gofresh.pojo.model.ProductEntity
import com.shoohna.gofresh.util.base.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CreditCardRecyclerViewAdapter () : RecyclerView.Adapter<CreditCardRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CreditCardViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bind(dataList.value!![position])

    }


    class ViewHolder(private var binding: CreditCardViewBinding) : RecyclerView.ViewHolder(binding.root) {

//        fun bind(item: ProductEntity ) {
//            binding.modelRoom = item
//            binding.executePendingBindings()
//        }

    }
}
