package com.shoohna.gofresh.ui.home.ui.changePasswordProfile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shoohna.gofresh.databinding.FragmentChangePasswordProfileBinding
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class ChangePasswordProfileFragment : Fragment() {

    lateinit var binding: FragmentChangePasswordProfileBinding
    private val changePasswordViewModel: ChangePasswordViewModel by inject()   // 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentChangePasswordProfileBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val viewModel = ViewModelProvider(this).get(ChangePasswordViewModel::class.java)

        binding.vm = changePasswordViewModel

        return binding.root
    }

}
