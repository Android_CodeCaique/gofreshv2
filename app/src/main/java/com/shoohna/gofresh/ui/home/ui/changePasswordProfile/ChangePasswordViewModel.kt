package com.shoohna.gofresh.ui.home.ui.changePasswordProfile

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.shoohna.gofresh.R
import com.shoohna.gofresh.networking.interfaces.auth
import com.shoohna.gofresh.ui.welcome.WelcomeActivity
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import retrofit2.HttpException

class ChangePasswordViewModel(private val serviceGeneral: auth) : BaseViewModel() {

    var newPassword = MutableLiveData<String>("")
    var confirmNewPassword = MutableLiveData<String>("")
    var oldPassword = MutableLiveData<String>("")
    var sharedHelper: SharedHelper = SharedHelper()
    var baseFragment: BaseFragment = BaseFragment()
    var loader = MutableLiveData<Boolean>(false)



    fun changePassword(v: View, oldPasswordLayout: TextInputLayout, newPasswordLayout: TextInputLayout, confirmNewPasswordLayout: TextInputLayout)
    {
        if(validate(v ,oldPasswordLayout , newPasswordLayout , confirmNewPasswordLayout))
            return

        if(oldPassword.value?.isNotEmpty()!! && newPassword.value?.isNotEmpty()!! ) {
            loader.value = true

            try {
//                val service = ApiClient.makeRetrofitService()
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )

                viewModelScope.async {
                    runCatching {
                        serviceGeneral.changePasswordFunction(lang.toString() ,
                            oldPassword.value!!,
                            newPassword.value!!,
                            "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}"
                        )
                    }.onSuccess {
                        try {
                            if (it.isSuccessful) {
                                if (it.body()!!.status == 1) {
                                    //Do something with response e.g show to the UI.

                                    Snackbar.make(
                                        v,
                                        it.body()?.message!!,
                                        Snackbar.LENGTH_SHORT
                                    )
                                        .show();
                                    loader.value = false
                                    Snackbar.make(v, it.message(), Snackbar.LENGTH_SHORT)
                                        .show();
                                    oldPassword.value = ""
                                    newPassword.value = ""
                                    confirmNewPassword.value = ""

                                    sharedHelper.putKey(v.context, Constants.getToken(), "")
                                    sharedHelper.putKey(v.context, "USER_ID", "")
                                    sharedHelper.putKey(v.context, "USER_PHOTO", "")
                                    sharedHelper.putKey(v.context, "USER_NAME", "")
                                    sharedHelper.putKey(v.context, "USER_EMAIL", "")
                                    sharedHelper.putKey(v.context, "OPEN", "OPEN")

                                    val intent: Intent =
                                        Intent(v!!.context, WelcomeActivity::class.java)
                                    v!!.context.startActivity(intent)
                                    (v.context as Activity).finish()
                                } else {
                                    Snackbar.make(
                                        v,
                                        "${it.body()?.message!!}",
                                        Snackbar.LENGTH_SHORT
                                    ).show();
                                    loader.value = false
                                }
                            } else {

                            }
                        } catch (e: HttpException) {
                            Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                                .show();
                            loader.value = false

                        } catch (e: Throwable) {
                            Snackbar.make(
                                v,
                                "Ooops: Something else went wrong",
                                Snackbar.LENGTH_SHORT
                            ).show();
                            loader.value = false
                        }
                    }.onFailure {
                        Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                    }
                }



//                CoroutineScope(Dispatchers.IO).async {
//
//
//                    val response = service.changePasswordFunction(lang.toString() ,
//                        oldPassword.value!!,
//                        newPassword.value!!,
//                        "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}"
//                    )
//                    withContext(Dispatchers.Main) {
//
//
//                    }
//                }
            }catch (e:Exception){
                Snackbar.make(v, e.message.toString(), Snackbar.LENGTH_SHORT).show();

            }
        }
        else
            Snackbar.make(v, v.resources.getString(R.string.pleaseInsertAllData), Snackbar.LENGTH_SHORT).show();

    }

    fun validate(v: View, oldPasswordLayout: TextInputLayout, newPasswordLayout: TextInputLayout, confirmNewPasswordLayout: TextInputLayout): Boolean
    {

        var cancel : Boolean = false

        if (checkEmpty(confirmNewPasswordLayout , confirmNewPassword.value.toString(),v.resources.getString(R.string.required) ))
        {
            cancel = true
        }
        else if (checkValidPassword(confirmNewPasswordLayout , confirmNewPassword.value.toString(),v.resources.getString(
                R.string.notValidPassword) ))
        {
            cancel = true
        }
        else if (checkEquals(confirmNewPasswordLayout , newPassword.value.toString(), confirmNewPassword.value.toString() , v.resources.getString(
                R.string.notMatching) ))
        {
            cancel = true
        }

        if (checkEmpty(newPasswordLayout , newPassword.value.toString(),v.resources.getString(R.string.required) ))
        {
            cancel = true
        }
        else if (checkValidPassword(newPasswordLayout , newPassword.value.toString(),v.resources.getString(
                R.string.notValidPassword) ))
        {
            cancel = true
        }

        if (checkEmpty(oldPasswordLayout , oldPassword.value.toString(),v.resources.getString(R.string.required) ))
        {
            cancel = true
        }



        if (cancel)
            return true
        else
            return false
    }
}