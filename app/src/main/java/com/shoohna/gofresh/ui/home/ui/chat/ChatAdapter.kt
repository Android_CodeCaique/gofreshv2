package com.shoohna.gofresh.ui.home.ui.chat

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.R
import com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi.Companion.TYPE_APPLICATION_MESSAGE
import com.shoohna.gofresh.ui.home.ui.chat.MessageItemUi.Companion.TYPE_MY_MESSAGE

class ChatAdapter (var data: MutableList<MessageItemUi>) : RecyclerView.Adapter<MessageViewHolder<*>>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder<*> {
        val context = parent.context

        return when (viewType) {
            TYPE_MY_MESSAGE -> {
                val view =
                    LayoutInflater.from(context).inflate(R.layout.my_chat_item, parent, false)
                MyMessageViewHolder(view)

            }
            TYPE_APPLICATION_MESSAGE -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.application_chat_item, parent, false)
                ApplicationMessageViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder<*>, position: Int) {
        val item = data[position]
        Log.d("adapter View", position.toString() + item.content)
        when (holder) {
            is MyMessageViewHolder -> holder.bind(item)
            is ApplicationMessageViewHolder -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int = data[position].messageType

    class MyMessageViewHolder(val view: View) : MessageViewHolder<MessageItemUi>(view) {
        private val messageContent = view.findViewById<TextView>(R.id.message)
        private val time = view.findViewById<TextView>(R.id.timeTxtViewId)

        override fun bind(item: MessageItemUi) {
            messageContent.text = item.content
            time.text = item.time
        }
    }

    class ApplicationMessageViewHolder(val view: View) : MessageViewHolder<MessageItemUi>(view) {
        private val messageContent = view.findViewById<TextView>(R.id.message)
        private val time = view.findViewById<TextView>(R.id.timeTxtViewId)

        override fun bind(item: MessageItemUi) {
            messageContent.text = item.content
            time.text = item.time

        }


    }
}