package com.shoohna.gofresh.ui.home.ui.chat

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.shoohna.gofresh.R
import com.shoohna.gofresh.pojo.model.Chat
import com.shoohna.gofresh.pojo.responses.PerviousChatData
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatViewModel : BaseViewModel() {

    var baseFragment: BaseFragment = BaseFragment()
    var loader = MutableLiveData<Boolean>(false)
    var messageList: MutableLiveData<List<PerviousChatData>>? = MutableLiveData()
    var messageListF: MutableLiveData<List<MessageItemUi>>? = MutableLiveData()
    var myMessages : ArrayList<Chat> = ArrayList()


//    fun sendMessage(v:View , message : EditText) {
//        if (message.text.trim().isNotEmpty()) {
//            loader.value = true
//
//            try {
//                val service = ApiClient.makeRetrofitServiceHome()
//                CoroutineScope(Dispatchers.IO).async {
//                    val sharedHelper : SharedHelper = SharedHelper()
//                    val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
//
//                    val response = service.sendMessageChat(
//                        lang.toString() ,
//                        "Bearer ${sharedHelper.getKey(v.rootView.context, Constants.getToken())}",
//                        message.text.toString()
//                    )
//                    withContext(Dispatchers.Main) {
//                        try {
//                            if (response.isSuccessful && response.body()?.status == 1) {
//                                Log.i("response", response.body()!!.message)
//                                Toast.makeText(
//                                    v.rootView.context,
//                                    response.body()!!.message,
//                                    Toast.LENGTH_SHORT
//                                ).show()
////                            baseFragment.dismissProgressDialog()
//                                message.text.clear()
//                                loader.value = false
//
//
//                            } else {
//                                Log.i("loadData1", response.message().toString())
////                            baseFragment.dismissProgressDialog()
//                                loader.value = false
//
//                            }
//                        } catch (e: Exception) {
//                            Log.i("loadData2", e.message.toString())
////                        baseFragment.dismissProgressDialog()
//                            loader.value = false
//
//                        }
//
//                    }
//                }
//            }catch (e:java.lang.Exception)
//            {
//                Snackbar.make(v, e.message.toString(), Snackbar.LENGTH_SHORT).show();
//
//            }
//        }
//        else
//            Toast.makeText(v.rootView.context, v.rootView.context.resources.getString(R.string.emptyMessage), Toast.LENGTH_SHORT).show()
//
//    }
//    fun loadData(context: Context) {
//        loader.value = true
//
//        val sharedHelper : SharedHelper = SharedHelper()
//        val lang : String? = sharedHelper.getKey(context , "MyLang" )
//        val service = ApiClient.makeRetrofitServiceHome()
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.getPerviousMessage(lang.toString() , "Bearer ${sharedHelper.getKey(context, Constants.getToken())}")
//            withContext(Dispatchers.Main) {
//                try {
//                    if (response.isSuccessful && response.body()?.status == 1) {
//                        Log.i("response", response.body()!!.message)
//                        messageList!!.postValue(response.body()!!.data)
//                        loader.value = false
//
//                    } else {
//                        Log.i("loadData1", response.message().toString())
//                        loader.value = false
//                    }
//                } catch (e: Exception) {
//                    Log.i("loadData2", e.message.toString())
//                    loader.value = false
//
//                }
//
//            }
//        }
//    }

    fun loadDataFirebase(context: Context , messages : RecyclerView){
        val database  = FirebaseDatabase.getInstance()
        val sharedHelper = SharedHelper()
        val UserID : String = sharedHelper.getKey(context , "USER_ID").toString()
        val myRef = database.getReference().child("User"+UserID)
        loader.value = true

        myMessages = ArrayList()
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists())
                {
                   Log.d("D" , " Loading Data ")
                    myMessages = ArrayList()
                    myMessages.clear()
                    for (snapshot in dataSnapshot.children) {
                        val Sender : String = snapshot.child("userId").getValue().toString()
                        var SenderInt = 0
                        val Content : String = snapshot.child("userMessage").getValue().toString()
                        val Time : String = snapshot.child("userMessageTime").getValue().toString()

                        Log.d("Sender" , Sender)
                        if (Sender == "1")
                        {
                            SenderInt = 1
                        }
                        else  if (Sender == "0")
                        {
                            SenderInt = 0
                        }
                        val chat = Chat(Content , Time , SenderInt)
                        myMessages.add(chat)
                    }
                    val chatAdapter= ChatFirebaseAdapter(myMessages)
                    messages.adapter = chatAdapter

                    loader.value = false

                    Log.d("Data Length" , myMessages.size.toString())
                }
                else
                {
                    loader.value = false
                    Log.d("D" , " No Data ")
                }
            }

            override fun onCancelled(error: DatabaseError) {
                loader.value = false
                Log.w("D", "Failed to read value.", error.toException())
            }
        })
    }
    fun sendMessageFirebase (v:View , message : EditText)
    {
        if (message.text.trim().isNotEmpty()) {
            loader.value = true

            val sharedHelper = SharedHelper()
            val currentTime : String = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())
            val UserID : String = sharedHelper.getKey(v!!.context , "USER_ID").toString()
            val UserIDInt : Int = UserID.toInt()
            val Content : String = message.text.trim().toString()

            val database  = FirebaseDatabase.getInstance()
            val myRef = database.getReference().child("User"+UserID)
            val MessageID : String = myRef.push().key.toString()

            myRef.child(MessageID).child("userId").setValue(UserIDInt)
            myRef.child(MessageID).child("userMessage").setValue(Content)
            myRef.child(MessageID).child("userMessageTime").setValue(currentTime)

            message.text.clear()
            loader.value = false
        }
        else
        {
            Toast.makeText(v.rootView.context, v.rootView.context.resources.getString(R.string.emptyMessage), Toast.LENGTH_SHORT).show()
            loader.value = false
        }

    }

}