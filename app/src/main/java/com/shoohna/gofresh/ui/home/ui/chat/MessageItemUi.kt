package com.shoohna.gofresh.ui.home.ui.chat

class MessageItemUi (val content:String,val time:String,val messageType:Int ){
    companion object {
        const val TYPE_MY_MESSAGE = 0 // Sender is me
        const val TYPE_APPLICATION_MESSAGE = 1 // Sender is Admin
    }
}

//https://medium.com/swlh/kotlin-chat-tutorial-the-clean-way-fca8f754aeb3