package com.shoohna.gofresh.ui.home.ui.checkOutProcess.myCart


import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import androidx.room.Room
import com.shoohna.gofresh.R
import com.shoohna.gofresh.pojo.model.ProductEntity
import com.shoohna.gofresh.pojo.responses.CheckoutCartData
import com.shoohna.gofresh.pojo.responses.CheckoutCartUser
import com.shoohna.gofresh.util.base.AppDatabase
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MyCartViewModel :BaseViewModel() {
    var checkoutCartList: MutableLiveData<List<ProductEntity>>? = MutableLiveData<List<ProductEntity>>()
    val checkoutCartStringList = ArrayList<ProductEntity>()
    var baseFragment: BaseFragment = BaseFragment()

    private var _productList = MutableLiveData<CheckoutCartData>()
    val productList: LiveData<CheckoutCartData> = _productList

    private var userData = MutableLiveData<CheckoutCartUser>()
    private var productId = MutableLiveData<Int>(0)
    var totalPrice = MutableLiveData<String>("")
    var shippingPrice = MutableLiveData<String>("")

    var sharedHelper: SharedHelper = SharedHelper()

    var loader = MutableLiveData<Boolean>(false)

    fun loadDataFromRoom(context: Context)
    {
        try {
            val db = Room.databaseBuilder(context, AppDatabase::class.java, "product_db").build()
            CoroutineScope(Dispatchers.IO).launch {
                checkoutCartStringList.addAll(db.productDao().getAllProducts())
                checkoutCartList?.postValue(checkoutCartStringList)
//                CoroutineScope(Dispatchers.Main).launch { Toast.makeText(context,context.getString(R.string.dataLoaded),Toast.LENGTH_SHORT).show() }
            }
        }catch (e:Exception)
        {
            Toast.makeText(context,"Exception ${e.message.toString()}",Toast.LENGTH_SHORT).show()
        }
        totalPrice.value = sharedHelper.getKey(context , "TOTAL")
        shippingPrice.value = sharedHelper.getKey(context , "SHIPPING")

        sharedHelper.putKey(context , "TOTAL" , "")
        sharedHelper.putKey(context , "SHIPPING" , "")

    }

//    fun loadData(context: Context)
//    {
//        baseFragment.showProgressDialog(context,context.getString(R.string.loading),context.resources.getString(R.string.pleaseWaitUntilLoading),false)
//
//        val service = ApiClient.makeRetrofitServiceHome()
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.CheckoutCart("Bearer ${sharedHelper.getKey(context, Constants.getToken())}")
//            withContext(Dispatchers.Main) {
//                try{
//                    if(response.isSuccessful && response.body()?.status == true){
//                        Log.i("response",response.body()!!.message)
//                        checkoutCartStringList.addAll(response.body()!!.data.products)
//                        checkoutCartList!!.postValue(checkoutCartStringList)
//                        _productList.value = response.body()!!.data
//                        userData.value = response.body()!!.data.user
//                        baseFragment.dismissProgressDialog()
//                        productId.value = response.body()!!.data.id
//
//                    }
//                    else {
//                        Log.i("loadData1",response.message().toString())
//                        baseFragment.dismissProgressDialog()
//                    }
//                }catch (e:Exception){
//                    Log.i("loadData2",e.message.toString())
//                    baseFragment.dismissProgressDialog()
//
//                }
//
//            }
//        }
//    }
//
//    internal fun getCheckoutCartList(context: Context): MutableLiveData<List<CheckoutCartProduct>> {
//        if (checkoutCartList == null) {
//            checkoutCartList = MutableLiveData()
//            loadData(context)
//
//        }
//        return checkoutCartList as MutableLiveData<List<CheckoutCartProduct>>
//    }

    fun nextClick(v:View)
    {

        Navigation.findNavController(v).navigate(R.id.shippingFragment)
//
//        loader.value = true
//
//        val service = ApiClient.makeRetrofitServiceHome()
//        val db = Room.databaseBuilder(v.rootView.context, AppDatabase::class.java, "product_db").build()
//
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.saveShippingInfo("Bearer ${sharedHelper.getKey(v.rootView.context, Constants.getToken())}")
//            withContext(Dispatchers.Main) {
//                try{
//                    if(response.isSuccessful && response.body()?.status == 1){
//                        Log.i("response",response.body()!!.message)
//                        Navigation.findNavController(v).navigate(R.id.action_checkoutProcessFragment_to_shippingFragment)
//
//                        Toast.makeText(v.context,response.body()!!.message,Toast.LENGTH_SHORT).show()
//                        sharedHelper.putKey(v.rootView.context , "PRODUCT_ID" , response.body()!!.data.id.toString())
//                        Log.i("ProductId",sharedHelper.getKey(v.rootView.context, "PRODUCT_ID"))
//                        loader.value = false
//
//                        CoroutineScope(Dispatchers.IO).launch { db.productDao().deleteAllProductDatabase() }
//                    }
//                    else {
//                        Log.i("loadData1",response.message().toString())
//                        Log.i("loadData1S",response.code().toString())
//                        Toast.makeText(v.context,response.message(),Toast.LENGTH_SHORT).show()
//
//                        loader.value = false
//                    }
//                }catch (e:Exception){
//                    Log.i("loadData2",e.message.toString())
//                    loader.value = false
//                    Toast.makeText(v.context,"Exception ${e.message.toString()}",Toast.LENGTH_SHORT).show()
//
//                }
//
//            }
//        }
    }
}