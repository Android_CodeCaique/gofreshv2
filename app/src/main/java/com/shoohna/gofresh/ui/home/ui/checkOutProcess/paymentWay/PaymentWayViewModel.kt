package com.shoohna.gofresh.ui.home.ui.checkOutProcess.paymentWay

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import com.shoohna.gofresh.R
import com.shoohna.gofresh.networking.interfaces.Home
import com.shoohna.gofresh.util.base.AppDatabase
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class PaymentWayViewModel(private val serviceGeneral: Home) :BaseViewModel() {

    private var _paymentOnline = MutableLiveData<Boolean>(false)
    val paymentOnline: LiveData<Boolean> = _paymentOnline

    private var _paymentOnDelivery = MutableLiveData<Boolean>(false)
    val paymentOnDelivery: LiveData<Boolean> = _paymentOnDelivery

    var baseFragment: BaseFragment = BaseFragment()

    fun allowPaymentOnline(view: View)
    {
        _paymentOnline.value = true
        _paymentOnDelivery.value = false
    }

    fun allowPaymentOnDelivery(view: View)
    {
        _paymentOnline.value = false
        _paymentOnDelivery.value = true
    }
    var loader = MutableLiveData<Boolean>(false)

    var discountCode = MutableLiveData<String>("")
    var discountProcessSuccess = MutableLiveData<Boolean>(false)

    var textCobon = ""

    fun sendPaymentMethod(v:View) {
        if(_paymentOnDelivery.value == false && _paymentOnline.value == false)
        {
            Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.pleaseSelectPaymentMethod),Toast.LENGTH_SHORT).show()
        }
        else {
            loader.value = true

            try {
//            sharedHelper.putKey(v.context,"PAYMENT_ID_SHIPPING" ,getFilterItem().toString())
                val db = Room.databaseBuilder(v.rootView.context, AppDatabase::class.java, "product_db").build()
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )



//                val service = ApiClient.makeRetrofitServiceHome()



                CoroutineScope(Dispatchers.IO).async {

                    if (discountProcessSuccess.value == true) {
                        runCatching {
                            serviceGeneral.addOrderOnce(
                                lang.toString(),
                                "Bearer ${sharedHelper.getKey(
                                    v.rootView.context,
                                    Constants.getToken()
                                )}",
                                getFilterItem(),
                                sharedHelper.getKey(v.rootView.context, "ADDRESS_ID_SHIPPING")
                                    ?.toInt()!!,
                                textCobon
                            )
                        }.onSuccess {
                            withContext(Dispatchers.Main) {
                                try {
                                    if (it.isSuccessful && it.body()?.status == 1) {
                                        Log.i("response", it.body()!!.message)
                                        Toast.makeText(
                                            v.rootView.context,
                                            it.body()!!.message,
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        loader.value = false
                                        Navigation.findNavController(v)
                                            .navigate(R.id.action_paymentWayFragment_to_recietFragment)
//                                CoroutineScope(Dispatchers.IO).launch { db.productDao().deleteAllProductDatabase() }
//                                sharedHelper.putKey(v.rootView.context, "PAYMENT_WAY", getFilterItem().toString())
//                                sharedHelper.putKey(v.rootView.context, "DISCOUNT_CODE_ID", discountCode.value?.toString()!!)

                                        sharedHelper.putKey(
                                            v.rootView.context,
                                            "RECIET_SHOP_ID",
                                            it.body()!!.data.id.toString()
                                        )
                                        sharedHelper.putKey(
                                            v.rootView.context,
                                            "RECIET_TOTAL_PRICE",
                                            it.body()!!.data.total_price.toString()
                                        )
                                        sharedHelper.putKey(
                                            v.rootView.context,
                                            "RECIET_SHIPPING_PRICE",
                                            it.body()!!.data.shipping_price.toString()
                                        )


                                    } else {
                                        Log.i("loadData1", it.message().toString())
                                        Log.i("loadData1", it.code().toString())
                                        loader.value = false
                                    }
                                } catch (e: Exception) {
                                    Toast.makeText(
                                        v.rootView.context,
                                        "Exception ${e.message.toString()}",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    loader.value = false
                                }
                            }
                        }.onFailure {
                            Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                        }
                    } else {
                        val sharedHelper : SharedHelper = SharedHelper()
                        val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
                        runCatching {
                            serviceGeneral.addOrderOnce(
                                lang.toString(),
                                "Bearer ${sharedHelper.getKey(
                                    v.rootView.context,
                                    Constants.getToken()
                                )}",
                                getFilterItem(),
                                sharedHelper.getKey(v.rootView.context, "ADDRESS_ID_SHIPPING")
                                    ?.toInt()!!, "0"
                            )
                        }.onSuccess {
                            withContext(Dispatchers.Main) {
                                try {
                                    if (it.isSuccessful && it.body()?.status == 1) {
                                        Log.i("response", it.body()!!.message)
                                        Log.i("responseData", it.body()!!.data.toString())
                                        Toast.makeText(
                                            v.rootView.context,
                                            it.body()!!.message,
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        loader.value = false
                                        Navigation.findNavController(v)
                                            .navigate(R.id.action_paymentWayFragment_to_recietFragment)
//                                CoroutineScope(Dispatchers.IO).launch { db.productDao().deleteAllProductDatabase() }
//                                sharedHelper.putKey(v.rootView.context, "PAYMENT_WAY", getFilterItem().toString())
//                                sharedHelper.putKey(v.rootView.context, "DISCOUNT_CODE_ID", "0")
//
                                        sharedHelper.putKey(
                                            v.rootView.context,
                                            "RECIET_SHOP_ID",
                                            it.body()!!.data.id.toString()
                                        )
                                        sharedHelper.putKey(
                                            v.rootView.context,
                                            "RECIET_TOTAL_PRICE",
                                            it.body()!!.data.total_price.toString()
                                        )
                                        sharedHelper.putKey(
                                            v.rootView.context,
                                            "RECIET_SHIPPING_PRICE",
                                            it.body()!!.data.shipping_price.toString()
                                        )

                                    } else {
                                        Log.i("loadData1", it.message().toString())
                                        Log.i("loadData1", it.code().toString())
                                        loader.value = false
                                    }

                                } catch (e: Exception) {
                                    Toast.makeText(
                                        v.rootView.context,
                                        "Exception ${e.message.toString()}",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    loader.value = false
                                }
                            }
                        }.onFailure {
                            Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                        }
                    }

                }
            }catch (e:java.lang.Exception)
            {
                Snackbar.make(v, e.message.toString(), Snackbar.LENGTH_SHORT).show();

            }
        }
    }

    private fun getFilterItem() : Int
    {
        return when {
            _paymentOnDelivery.value == true -> 1
            else -> 2
        }
    }

    fun makeDiscount(v:View)
    {
        if(discountCode.value!!.isEmpty()){
            Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.pleaseWriteCode),Toast.LENGTH_SHORT).show()
        }
        else
        {
            loader.value = true

//            val service = ApiClient.makeRetrofitServiceHome()



            CoroutineScope(Dispatchers.IO).launch {
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
                runCatching {
                    serviceGeneral.makeDiscount(
                        lang.toString(),
                        "Bearer ${sharedHelper.getKey(v.rootView.context, Constants.getToken())}",
                        discountCode.value!!.toString()
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                textCobon = it.body()!!.data.code_id.toString()
                                Log.i("response", it.body()!!.message)
                                Toast.makeText(
                                    v.rootView.context,
                                    it.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                                loader.value = false
                                discountProcessSuccess.value = it.body()!!.data.code_id != 0

                            } else if (it.isSuccessful && it.body()?.status == 0) {
                                Toast.makeText(
                                    v.rootView.context,
                                    it.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                                loader.value = false
                            } else {
                                Log.i("loadData1", it.message().toString())
                                Log.i("loadData1", it.code().toString())
                                loader.value = false
                            }

                        } catch (e: Exception) {
                            Toast.makeText(
                                v.rootView.context,
                                "Exception ${e.message.toString()}",
                                Toast.LENGTH_SHORT
                            ).show()
                            loader.value = false


                        }
                    }
                }.onFailure {
                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                }
            }
        }
    }
}