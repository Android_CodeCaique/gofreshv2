package com.shoohna.gofresh.ui.home.ui.checkOutProcess.reciet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.shoohna.gofresh.databinding.FragmentRecietBinding
import com.shoohna.gofresh.databinding.FragmentRegisterBinding
import com.shoohna.gofresh.pojo.model.ProductEntity

/**
 * A simple [Fragment] subclass.
 */
class RecietFragment : Fragment() {
    lateinit var binding: FragmentRecietBinding

    var finalData= MutableLiveData<List<ProductEntity>>()



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRecietBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        val model = ViewModelProvider(this).get(RecietViewModel::class.java)
        binding.vm = model

        model.loadDataFromRoom(activity!!)
        model.finalDataList.observe(viewLifecycleOwner, Observer {
            finalData.value = it
            binding.recietRecyclerViewId.adapter =RecietRecyclerViewAdapter(finalData, context?.applicationContext)
        })




        return binding.root
    }

//    private fun confirmOrder(v: View) {
//        val alert: Dialog? = Dialog(v.rootView.context)
//        alert?.setContentView(R.layout.confirm_order_layout)
//        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        alert?.findViewById<Button>(R.id.homeBtnId)?.setOnClickListener {
//            (context as MainActivity?)?.iconHomeClickedFromHome()
//            (context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
//
//        }
//        alert?.findViewById<Button>(R.id.dismissBtnId)?.setOnClickListener {
//            alert.dismiss()
//        }
//        alert?.show()
//
//    }


}
