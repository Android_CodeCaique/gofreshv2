package com.shoohna.gofresh.ui.home.ui.checkOutProcess.reciet

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.databinding.RecietItemRowBinding
import com.shoohna.gofresh.pojo.model.ProductEntity

class RecietRecyclerViewAdapter (private var dataList: LiveData<List<ProductEntity>>, private
                                 val context: Context?) : RecyclerView.Adapter<RecietRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RecietItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position])
    }


    class ViewHolder(private var binding: RecietItemRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductEntity) {
            binding.model = item
            binding.executePendingBindings()

        }

    }
}
