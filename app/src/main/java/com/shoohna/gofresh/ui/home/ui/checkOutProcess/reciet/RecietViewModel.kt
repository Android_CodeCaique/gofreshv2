package com.shoohna.gofresh.ui.home.ui.checkOutProcess.reciet

import ApiClient.sharedHelper
import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.shoohna.gofresh.pojo.model.ProductEntity
import com.shoohna.gofresh.pojo.responses.AddOrderOnceData
import com.shoohna.gofresh.util.base.AppDatabase
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RecietViewModel :BaseViewModel() {
    var baseFragment: BaseFragment = BaseFragment()
    var finalDataList: MutableLiveData<List<ProductEntity>> = MutableLiveData()

    private var _dataList = MutableLiveData<AddOrderOnceData>()
    val dataList: LiveData<AddOrderOnceData> = _dataList
    var loader = MutableLiveData<Boolean>(false)

    var shopId = MutableLiveData<String>("")
    var shipping_price = MutableLiveData<String>("")
    var total_price = MutableLiveData<String>("")


//    fun loadData(context: Context) {
//        loader.value = true
//
//        val service = ApiClient.makeRetrofitServiceHome()
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.addPaymentMethod(sharedHelper.getKey(context, "PRODUCT_ID")?.toInt()!!,
//                "Bearer ${sharedHelper.getKey(context, Constants.getToken())}",
//            sharedHelper.getKey(context,"PAYMENT_WAY")?.toInt()!!)
//            withContext(Dispatchers.Main) {
//                try{
//                    if(response.isSuccessful && response.body()?.status == 1){
//                        Log.i("response",response.body()!!.message)
//
//                        finalDataList.value = response.body()!!.data.products
//                        _dataList.value = response.body()!!.data
//                        shopId.value = response.body()!!.data.user.shop_id
//                        loader.value = false
//
//                    }
//                    else {
//                        Log.i("loadData1",response.message().toString())
//                        loader.value = false
//                    }
//                }catch (e:Exception){
//                    Log.i("loadData2",e.message.toString())
//                    loader.value = false
//
//                }
//
//            }
//        }
//
//    }

//    fun loadData(context: Context) {
//        loader.value = true
//        Log.i("responseReciet","1")
//        Log.i("responseReciet",  sharedHelper.getKey(context,"PAYMENT_WAY")?.toString()!!)
//        Log.i("responseReciet",sharedHelper.getKey(context,"ADDRESS_ID_SHIPPING")?.toString()!!)
//        Log.i("responseReciet",sharedHelper.getKey(context,"DISCOUNT_CODE_ID")?.toString()!!)
//        try{
//
//        val service = ApiClient.makeRetrofitServiceHome()
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.addOrderOnce(
//                "Bearer ${sharedHelper.getKey(context, Constants.getToken())}",
//                sharedHelper.getKey(context,"PAYMENT_WAY")?.toInt()!!
//                ,sharedHelper.getKey(context,"ADDRESS_ID_SHIPPING")?.toInt()!!
//                ,sharedHelper.getKey(context,"DISCOUNT_CODE_ID")?.toInt()!!)
//
//            Log.i("responseReciet","2")
//
//            withContext(Dispatchers.Main) {
//                    Log.i("responseReciet","3")
//
//                    if(response.isSuccessful && response.body()?.status == 1){
//                        Log.i("response",response.body()!!.message)
//                        Log.i("responseReciet","4")
//
//                        finalDataList.value = response.body()!!.data.products
//                        _dataList.value = response.body()!!.data
//                        shopId.value = response.body()!!.data.id.toString()
//                        loader.value = false
//                        Log.i("responseReciet","5")
//
//                    }
//                    else {
//                        Log.i("loadData1",response.message().toString())
//                        loader.value = false
//                    }
//
//
//            }
//        }
//        }catch (e:Exception){
//            Log.i("loadData2",e.message.toString())
//            loader.value = false
//
//        }
//
//    }

    fun loadDataFromRoom(context: Context)
    {
        try {
            loader.value = true
             val db = Room.databaseBuilder(context, AppDatabase::class.java, "product_db").build()
            CoroutineScope(Dispatchers.IO).launch {
                finalDataList.postValue(db.productDao().getAllProducts())
                db.productDao().deleteAllProductDatabase()
//            CoroutineScope(Dispatchers.Main).launch {  }
            }
            shopId.value =  sharedHelper.getKey(context,"RECIET_SHOP_ID").toString()
            total_price.value =  sharedHelper.getKey(context,"RECIET_TOTAL_PRICE").toString()
            shipping_price.value =  sharedHelper.getKey(context,"RECIET_SHIPPING_PRICE").toString()

            loader.value = false

        }catch (e:Exception)
        {
            CoroutineScope(Dispatchers.Main).launch { Toast.makeText(context,"Exception Load ${e.message.toString()}",
                Toast.LENGTH_SHORT).show() }
        }


    }

    fun confirmOrder(v: View) {

        confirmOrderDialog(v,v.context)

    }
}