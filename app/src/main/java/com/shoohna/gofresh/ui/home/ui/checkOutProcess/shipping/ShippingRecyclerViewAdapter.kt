package com.shoohna.gofresh.ui.home.ui.checkOutProcess.shipping

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.databinding.FragmentProductDetailsBinding
import com.shoohna.gofresh.databinding.FragmentShippingBinding
import com.shoohna.gofresh.databinding.ShippingInformationItemRowBinding
import com.shoohna.gofresh.pojo.responses.UserAddressData

class ShippingRecyclerViewAdapter  (private var dataList: LiveData<List<UserAddressData>>,
                                    private val context: Context?,
                                    var shippingViewModel: ShippingViewModel,
                                    var lifecycleOwner: LifecycleOwner,
                                    var MainBinding : FragmentShippingBinding)
    : RecyclerView.Adapter<ShippingRecyclerViewAdapter.ViewHolder>() {

    var makeLoop = MutableLiveData<Boolean>(false)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ShippingInformationItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false),makeLoop
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position],  shippingViewModel , context,MainBinding)

        makeLoop.observe(lifecycleOwner, Observer {
            if(it)
            {
                loopIsChecked(dataList.value!!)
            }
        })
    }

    fun loopIsChecked(dataList: List<UserAddressData>)
    {
        for (i in dataList) {
            i.isChecked = false
        }
        makeLoop.value = false
    }


    class ViewHolder(private var binding: ShippingInformationItemRowBinding , var makeLoop:MutableLiveData<Boolean>) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UserAddressData , shippingViewModel: ShippingViewModel , context: Context? , MainBinding : FragmentShippingBinding) {
            binding.model = item
            binding.executePendingBindings()
            binding.mainConstraintLayoutId.setOnClickListener {
                shippingViewModel.userAddressChecked.value = item.id
//                Toast.makeText(context,context?.getString(R.string.selectedAddress)+"\n${item.address}",Toast.LENGTH_SHORT).show()
                makeLoop.value = true
                item.isChecked = true
                MainBinding.shippingRecycvlerViewId.adapter?.notifyDataSetChanged()
            }
        }

    }
}