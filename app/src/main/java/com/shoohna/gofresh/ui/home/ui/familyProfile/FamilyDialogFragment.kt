package com.shoohna.gofresh.ui.home.ui.familyProfile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.PagerSnapHelper
import com.shoohna.gofresh.databinding.FragmentFamilyDialogBinding

class FamilyDialogFragment : DialogFragment() {

    lateinit var binding : FragmentFamilyDialogBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFamilyDialogBinding.inflate(inflater)
        binding.familyRecyclerViewId.adapter = FamilyDialogRecyclerAdapter()

        val helper = PagerSnapHelper()
        helper.attachToRecyclerView(binding.familyRecyclerViewId)
        binding.familyRecyclerViewId.setHasFixedSize(true)

        return binding.root
    }
}