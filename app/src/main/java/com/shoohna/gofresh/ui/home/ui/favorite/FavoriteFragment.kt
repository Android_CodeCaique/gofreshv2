package com.shoohna.gofresh.ui.home.ui.favorite

import ApiClient.sharedHelper
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentFavoriteBinding
import com.shoohna.gofresh.pojo.responses.MyWishlistData
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.shoohna.util.base.BaseFragment
import org.koin.android.ext.android.inject


class FavoriteFragment : Fragment() {

    lateinit var binding: FragmentFavoriteBinding
//    lateinit var favoriteViewModel: FavoriteViewModel
//    lateinit var favoriteViewModelFactory: FavoriteViewModelFactory

    var myWishlistData= MutableLiveData<List<MyWishlistData>>()
    private val favoriteViewModel: FavoriteViewModel by inject()  // 1
    lateinit var baseFragment: BaseFragment
    lateinit var mAdapter: FavoriteRecyclerViewAdapter
    lateinit var filteredList: MutableLiveData<List<MyWishlistData>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavoriteBinding.inflate(layoutInflater, container, false)
//        favoriteViewModelFactory = FavoriteViewModelFactory(getData())
//        favoriteViewModel = ViewModelProvider(this,favoriteViewModelFactory).get(FavoriteViewModel::class.java)
//        binding.vm = favoriteViewModel

//        val model = ViewModelProvider(this).get(FavoriteViewModel::class.java)
        binding.vm = favoriteViewModel
        binding.lifecycleOwner = this
        baseFragment = BaseFragment()
        filteredList =  MutableLiveData()

//        try {
////            Picasso.get().load(sharedHelper.getKey(activity!!, "USER_PHOTO")).into(binding.circleImageView)
//            if(sharedHelper.getKey(requireActivity(), "USER_PHOTO").toString() == "http://api.shoohna.com/images/1.png")
//                Glide.with(requireActivity()).load(R.drawable.ic_profile_upload).error(R.drawable.ic_profile_upload).into(binding.circleImageView)
//            else
//                Glide.with(requireActivity()).load(sharedHelper.getKey(requireActivity(), "USER_PHOTO")).error(R.drawable.ic_profile_upload).into(binding.circleImageView)
//
//        }catch (e:Exception)
//        {}

        mAdapter = FavoriteRecyclerViewAdapter(myWishlistData, requireActivity(), favoriteViewModel)


        favoriteViewModel.getMyWishlistList(requireActivity()).observe(viewLifecycleOwner, Observer {

                myWishlistData.value = it

                binding.favoriteViewRecyclerViewId.adapter = mAdapter
                filteredList.value = it

        })

        favoriteViewModel.myWishlistList?.observe(viewLifecycleOwner, Observer {
            if(it.isEmpty())
                binding.showNoDataId.visibility = View.VISIBLE
            else
                binding.showNoDataId.visibility = View.GONE
        })
        binding.notificationImgId.setOnClickListener {
            if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                baseFragment.showAlert(it.rootView.context)
            }else {
                view?.let { it1 ->
                    Navigation.findNavController(it1)
                        .navigate(R.id.action_favoriteFragment_to_notificationFragment)
                }
                activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                    View.GONE
            }
        }

        binding.messangerImgId.setOnClickListener {
            if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                baseFragment.showAlert(it.rootView.context)
            }else {
                view?.let { it1 ->
                    Navigation.findNavController(it1)
                        .navigate(R.id.action_favoriteFragment_to_chatFragment)
                }
                activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                    View.GONE
            }
        }

//        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//
//            }
//
//            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
////                filter(s.toString())
//
//            }
//
//            override fun afterTextChanged(s: Editable) {
//                filter(s.toString())
//
//            }
//        })

//        getData()

        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        return binding.root
    }

    private fun filter(text: String) {
        val listed = ArrayList<MyWishlistData>()
        val listed1 : MutableLiveData<List<MyWishlistData>> = MutableLiveData<List<MyWishlistData>>()

        for (item in filteredList.value!!) {
            if (item.name.toLowerCase().contains(text.toLowerCase())) {
                listed.add(item)
            }
        }
        listed1.value = listed
//        filteredList.value = listed
        mAdapter.filterList(listed1)
        mAdapter.notifyDataSetChanged()
    }


//    private fun getData(): MutableLiveData<ProductListModel> {
//        val model = MutableLiveData<ProductListModel>()
//        val product1 = ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false)
//        val product2 = ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false)
//        val product3 = ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false)
//        val product4 = ProductListModel("https://images-na.ssl-images-amazon.com/images/I/51E-7Y2kEdL._AC_UL1000_.jpg","Populist","356.22","1 Km","",false)
//
//        val list: MutableList<ProductListModel> = ArrayList()
//        list.add(product1)
//        list.add(product2)
//        list.add(product3)
//        list.add(product4)
//        val productListModel = ProductListModel("","","","","",false,list)
//        model.value = productListModel
//        return model
//    }

}
