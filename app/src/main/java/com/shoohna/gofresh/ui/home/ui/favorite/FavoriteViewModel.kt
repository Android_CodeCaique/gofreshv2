package com.shoohna.gofresh.ui.home.ui.favorite

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.shoohna.gofresh.networking.interfaces.Home
import com.shoohna.gofresh.pojo.responses.MyWishlistData
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*


class FavoriteViewModel(private val serviceGeneral: Home) : BaseViewModel() {

    var myWishlistList: MutableLiveData<List<MyWishlistData>>? = null
    val myWishlistStringList = ArrayList<MyWishlistData>()

    var baseFragment: BaseFragment = BaseFragment()
    var productId = MutableLiveData<Int>(0)
    var loader = MutableLiveData<Boolean>(false)
    var searchEditText = MutableLiveData<String>("")

    fun loadData(context: Context)
    {
        loader.value = true
        try{
            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )

//            val service = ApiClient.makeRetrofitServiceHome()

        CoroutineScope(Dispatchers.IO).async {
            runCatching { serviceGeneral.getMyWishlist(lang.toString() , "Bearer ${sharedHelper.getKey(context, Constants.getToken())}") }
                .onSuccess {
                    withContext(Dispatchers.Main) {

                        if (it.isSuccessful && it.body()?.status == 1) {
                            Log.i("response", it.body()!!.message)
                            myWishlistStringList.addAll(it.body()!!.data)
                            myWishlistList!!.postValue(myWishlistStringList)
                            loader.value = false

                        } else {
                            Log.i("loadData1", it.message().toString())
                            loader.value = false
                        }


                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
        }
        }catch (e:Exception){
            Log.i("loadData2",e.message.toString())
            loader.value = false

        }
    }

    fun addToWishlist(context: Context)
    {
        loader.value = true

        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(context , "MyLang" )

//        val service = ApiClient.makeRetrofitServiceHome()


        CoroutineScope(Dispatchers.IO).launch {
            runCatching { serviceGeneral.addOrDeleteFromMyWishlist( lang.toString() , productId.value?.toInt()!!,"Bearer ${sharedHelper.getKey(context, Constants.getToken())}")}
                .onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
                                Toast.makeText(
                                    context,
                                    it.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
//                        baseFragment.showDialog(context,response.body()!!.message)
                                loader.value = false
                                myWishlistStringList.clear()

                                loadData(context)
                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                                Toast.makeText(context, it.message(), Toast.LENGTH_SHORT)
                                    .show()
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false
                            Toast.makeText(
                                context,
                                "Exception ${e.message.toString()}",
                                Toast.LENGTH_SHORT
                            ).show()

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                }
        }
    }

    internal fun getMyWishlistList(context: Context): MutableLiveData<List<MyWishlistData>> {
        if (myWishlistList == null) {
            myWishlistList = MutableLiveData()
            loadData(context)

        }
        return myWishlistList as MutableLiveData<List<MyWishlistData>>
    }
}