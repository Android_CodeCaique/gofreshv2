package com.shoohna.gofresh.ui.home.ui.home

import ApiClient.sharedHelper
import android.app.Activity
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rizlee.rangeseekbar.RangeSeekBar
import com.shoohna.gofresh.ui.home.ui.home.viewModel.HomeViewModel
import com.shoohna.gofresh.databinding.BottomSheetLayoutBinding
import com.shoohna.gofresh.pojo.responses.FilerMainCategorieData
import com.shoohna.gofresh.ui.home.ui.home.adapters.FilterRecyclerViewAdapter
import com.shoohna.gofresh.ui.home.ui.home.viewModel.FilterBottomViewModel
import org.koin.android.ext.android.inject

//var homeViewModel: HomeViewModel
class FilterBottomSheet(var homeViewModel: HomeViewModel) : BottomSheetDialogFragment() ,
    RangeSeekBar.OnRangeSeekBarRealTimeListener {
    lateinit var binding:BottomSheetLayoutBinding
    //val model: HomeViewModel by viewModels()
    lateinit var filterAdapter: FilterRecyclerViewAdapter
    var filterData= MutableLiveData<List<FilerMainCategorieData>>()


    private lateinit var sharedPreferences: SharedPreferences
    var language :String? = ""

    private val filterBottomViewModel: FilterBottomViewModel by inject() // 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = BottomSheetLayoutBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        sharedPreferences = context?.getSharedPreferences("Settings", Activity.MODE_PRIVATE)!!
        language = sharedHelper.getKey(requireActivity(), "MyLang" )
        if(language.equals("ar"))
        {
            binding.leftRangeTxtViewId.text = "1000"
            binding.rigthRangeTxtViewId.text = "0"
        }
//        val filterViewModel = ViewModelProvider(this).get(FilterBottomViewModel::class.java)
//        val homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

//        https://www.youtube.com/watch?v=5qlIPTDE274
        binding.vm = homeViewModel
        binding.recyclerView.layoutManager = GridLayoutManager(this.context, 2, RecyclerView.HORIZONTAL, false)

        filterBottomViewModel.getFilterList(requireActivity()).observe(viewLifecycleOwner, Observer {
            filterData.value = it
            binding.recyclerView.adapter = FilterRecyclerViewAdapter(filterData, context?.applicationContext ,homeViewModel, viewLifecycleOwner,binding)
        })

        binding.rangeSeekBar.listenerRealTime = this
        binding.rangeSeekBar.setRange(0,1000,1)

//        binding.doneBtnId.setOnClickListener { this.dismiss() }

        binding.resetBtnId.setOnClickListener {
            homeViewModel.loadData(requireActivity())
            this.dismiss()
        }

        homeViewModel.dismissSheet.observe(viewLifecycleOwner, Observer {
            if(it){
                this.dismiss()
                homeViewModel.dismissSheet.value = false
            }
        })

        return binding.root
    }

    override fun onValuesChanging(minValue: Float, maxValue: Float) {
        TODO("Not yet implemented")
    }

    override fun onValuesChanging(minValue: Int, maxValue: Int) {
        when {
            language.equals("en") -> {
                binding.leftRangeTxtViewId.text = minValue.toString()
                binding.rigthRangeTxtViewId.text = maxValue.toString()
            }
            language.equals("ar") -> {
                binding.leftRangeTxtViewId.text = maxValue.toString()
                binding.rigthRangeTxtViewId.text = minValue.toString()
            }
            else -> {
                binding.leftRangeTxtViewId.text = minValue.toString()
                binding.rigthRangeTxtViewId.text = maxValue.toString()
            }
        }
        homeViewModel.range_from.value = minValue.toInt()
        homeViewModel.range_to.value = maxValue.toInt()
    }


}

