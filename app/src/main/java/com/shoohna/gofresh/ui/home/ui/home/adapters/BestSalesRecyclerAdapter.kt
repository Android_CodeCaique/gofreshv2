package com.shoohna.gofresh.ui.home.ui.home.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.ui.home.ui.home.HomeFragmentDirections
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.BestSalesListRowBinding
import com.shoohna.gofresh.pojo.responses.BestSeller
import com.shoohna.gofresh.ui.home.MainActivity

class BestSalesRecyclerAdapter (private var dataList: LiveData<List<BestSeller>>, private val context: Context?) : RecyclerView.Adapter<BestSalesRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            BestSalesListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position])


    }


    class ViewHolder(private var binding: BestSalesListRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: BestSeller) {
            binding.model = item

            binding.executePendingBindings()
            binding.mainConstraintLayoutId.setOnClickListener {
//                Navigation.findNavController(itemView).navigate(R.id.action_homeFragment_to_productDetailsFragment)

//                val action = HomeFragmentDirections.actionHomeFragmentToProductFragment3(resources.getString(R.string.bestSale))
//                view?.let { it1 -> Navigation.findNavController(it1).navigate(action) }
                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE

                val action = HomeFragmentDirections.actionHomeFragmentToProductDetailsFragment2(item.id,item.image,item.desc,item.price_product)
                Navigation.findNavController(it).navigate(action)

                Log.i("Item Id",item.id.toString())
            }
        }

    }

}