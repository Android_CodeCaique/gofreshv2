package com.shoohna.gofresh.ui.home.ui.home.adapters

import android.content.Context
import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.ui.home.ui.home.HomeFragmentDirections
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.OffersListRowBinding
import com.shoohna.gofresh.pojo.responses.Offer
import com.shoohna.gofresh.ui.home.MainActivity

class OffersRecyclerAdapter (private var dataList: LiveData<List<Offer>>, private val context: Context?) : RecyclerView.Adapter<OffersRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            OffersListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position])
    }


    class ViewHolder(private var binding: OffersListRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Offer) {
            binding.model = item
            binding.executePendingBindings()
            binding.distanceId.setPaintFlags(binding.distanceId.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
            binding.mainConstraintLayoutId.setOnClickListener {
//                Navigation.findNavController(itemView).navigate(R.id.action_homeFragment_to_productDetailsFragment)
                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
                val action = HomeFragmentDirections.actionHomeFragmentToProductDetailsFragment2(item.id,item.image,item.desc,item.price_product)
                Navigation.findNavController(it).navigate(action)

                Log.i("Item Id",item.id.toString())
            }
        }

    }

}