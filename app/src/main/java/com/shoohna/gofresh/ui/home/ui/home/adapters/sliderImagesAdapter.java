package com.shoohna.gofresh.ui.home.ui.home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.shoohna.gofresh.R;
import com.shoohna.gofresh.pojo.responses.sliders;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class sliderImagesAdapter extends SliderViewAdapter<sliderImagesAdapter.SliderAdapterVH> {

    private Context context;
    private List<sliders> mSliderItems = new ArrayList<>();

    public sliderImagesAdapter(Context context) {
        this.context = context;
    }

    public void renewItems(List<sliders> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mSliderItems.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(sliders sliderItem) {
        this.mSliderItems.add(sliderItem);
        notifyDataSetChanged();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

        sliders sliderItem = mSliderItems.get(position);
        Glide.with(viewHolder.itemView)
                .load(sliderItem.getImage())
                .fitCenter()
                .into(viewHolder.fl_shadow_container);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getCount() {
        return mSliderItems.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView fl_shadow_container;
        public SliderAdapterVH(View itemView) {
            super(itemView);
            fl_shadow_container = itemView.findViewById(R.id.fl_shadow_container);
            this.itemView = itemView;
        }
    }

}