package com.shoohna.gofresh.ui.home.ui.home.viewModel

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.shoohna.gofresh.networking.interfaces.Home
import com.shoohna.gofresh.pojo.responses.FilerMainCategorieData
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class FilterBottomViewModel(private val serviceGeneral: Home) : BaseViewModel() {
    private var filterList: MutableLiveData<List<FilerMainCategorieData>>? = null
    val filterStringList = ArrayList<FilerMainCategorieData>()
    var baseFragment: BaseFragment = BaseFragment()

    var loader = MutableLiveData<Boolean>(false)

    fun loadData(context: Context)
    {
//        baseFragment.showProgressDialog(context,context.getString(R.string.loading),context.resources.getString(R.string.pleaseWaitUntilLoading),false)
        loader.value = true

        try {
            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )

//            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                runCatching { serviceGeneral.getFilterMainCategorie(lang.toString()) }
                    .onSuccess {
                        withContext(Dispatchers.Main) {
                            try {
                                if (it.isSuccessful && it.body()?.status == 1) {
                                    Log.i("response", it.body()!!.message)
                                    filterStringList.addAll(it.body()!!.data)
                                    filterList!!.postValue(filterStringList)
//                        baseFragment.dismissProgressDialog()
                                    loader.value = false

                                } else {
                                    Log.i("loadData1", it.message().toString())
//                        baseFragment.dismissProgressDialog()
                                    loader.value = false

                                }
                            } catch (e: Exception) {
                                Log.i("loadData2", e.message.toString())
//                    baseFragment.dismissProgressDialog()
                                loader.value = false


                            }

                        }
                    }.onFailure {
                        Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                    }
            }
        }catch (e:Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()
        }
    }





    internal fun getFilterList(context: Context): MutableLiveData<List<FilerMainCategorieData>> {
        if (filterList == null) {
            filterList = MutableLiveData()
            loadData(context)

        }
        return filterList as MutableLiveData<List<FilerMainCategorieData>>
    }

}