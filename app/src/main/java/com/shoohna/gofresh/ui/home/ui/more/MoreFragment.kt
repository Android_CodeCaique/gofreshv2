package com.shoohna.gofresh.ui.home.ui.more

import ApiClient.sharedHelper
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentMoreBinding
import com.shoohna.gofresh.ui.home.ui.familyProfile.FamilyDialogFragment
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.gofresh.util.base.SharedHelper
import org.koin.android.ext.android.inject
import java.lang.Exception


class MoreFragment : Fragment() {
    lateinit var binding: FragmentMoreBinding
//    lateinit var viewModel: MoreViewModel
    private val moreViewModel: MoreViewModel by inject()  // 1

    lateinit var viewModel: MoreViewModel
    lateinit var nav: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentMoreBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        viewModel = ViewModelProvider(this).get(MoreViewModel::class.java)

        binding.vm = moreViewModel


        try {
            binding.nameTxtId.text = sharedHelper.getKey(requireActivity(),"USER_NAME")
            binding.emailTxtId.text = sharedHelper.getKey(requireActivity(),"USER_EMAIL")
//            Picasso.get().load(sharedHelper.getKey(activity!!, "USER_PHOTO")).into(binding.circleImageView)
            if(sharedHelper.getKey(requireActivity(), "USER_PHOTO").toString() == "http://api.shoohna.com/images/1.png")
                Glide.with(requireActivity()).load(R.drawable.ic_profile_upload).error(R.drawable.ic_profile_upload).into(binding.circleImageView)
            else
                Glide.with(requireActivity()).load(sharedHelper.getKey(requireActivity(), "USER_PHOTO")).error(R.drawable.ic_profile_upload).into(binding.circleImageView)

        }catch (e: Exception)
        {}//        binding.logOutConstraintId.setOnClickListener {
//            showExitAlert()
//        }

        binding.settingConstraintId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.moreSettingFragment)
        }
        binding.editConstraintId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.profileFragment)
        }

        binding.contactUsConstraintId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.contactUsFragment)
        }

        binding.aboutUsConstraintId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.aboutUsFragment)
        }

        binding.helpConstraintId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.helpFragment)
        }

        binding.walletConstraintId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.walletFragment)
        }
        binding.familyConstraintId.setOnClickListener {
            val familyDialog = FamilyDialogFragment()
            familyDialog.show(requireActivity().supportFragmentManager,"")
        }

        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }

        val x : SharedHelper = SharedHelper()
        var token : String? =""
        token = context?.let { x.getKey(it, Constants.getToken()) }

        if (token.isNullOrEmpty() || token == "")
        {
            binding.logout.visibility = View.GONE
            binding.imageLogout.visibility = View.GONE
            binding.logoutViewId.visibility = View.GONE
            binding.logOutConstraintId.visibility = View.GONE


            binding.login.visibility = View.VISIBLE
            binding.imageLogIn.visibility = View.VISIBLE
            binding.loginViewId.visibility = View.VISIBLE
            binding.logInConstraintId.visibility = View.VISIBLE
        }
        else
        {
            binding.logout.visibility = View.VISIBLE
            binding.imageLogout.visibility = View.VISIBLE
            binding.logoutViewId.visibility = View.VISIBLE
            binding.logOutConstraintId.visibility = View.VISIBLE


            binding.login.visibility = View.GONE
            binding.imageLogIn.visibility = View.GONE
            binding.loginViewId.visibility = View.GONE
            binding.logInConstraintId.visibility = View.GONE
        }

        return binding.root
    }

//    private fun showExitAlert()
//    {
//        val alert: Dialog? = activity?.let { Dialog(it) }
//        alert?.setContentView(R.layout.exit_alert_layout)
//        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        alert?.findViewById<Button>(R.id.logOutBtnId)?.setOnClickListener {
//        }
//        alert?.show()
//    }


}
