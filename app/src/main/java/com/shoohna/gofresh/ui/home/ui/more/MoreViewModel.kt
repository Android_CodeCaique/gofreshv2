package com.shoohna.gofresh.ui.home.ui.more

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.Button
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import com.shoohna.gofresh.R
import com.shoohna.gofresh.networking.interfaces.auth
import com.shoohna.gofresh.ui.welcome.WelcomeActivity
import com.shoohna.gofresh.util.base.AppDatabase
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.lang.Exception

class MoreViewModel(private val serviceGeneral: auth) : BaseViewModel() {
    var sharedHelper: SharedHelper = SharedHelper()
    fun logout (v : View)
    {
        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
        val db = Room.databaseBuilder(v.rootView.context, AppDatabase::class.java, "product_db").build()

        val alert: Dialog? = Dialog(v.rootView.context)
        alert?.setContentView(R.layout.exit_alert_layout)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert?.findViewById<Button>(R.id.logOutBtnId)?.setOnClickListener {
            try {
            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.logoutFunction(lang.toString() , "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}")
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful) {
                                if (it.body()!!.status == 1) {
                                    //Do something with response e.g show to the UI.
                                    Snackbar.make(
                                        v,
                                        it.body()?.message!!,
                                        Snackbar.LENGTH_SHORT
                                    )
                                        .show();
                                    Snackbar.make(v, it.message(), Snackbar.LENGTH_SHORT)
                                        .show();

                                    sharedHelper.putKey(v.context, Constants.getToken(), "")
                                    sharedHelper.putKey(v.context, "USER_ID", "")
                                    sharedHelper.putKey(v.context, "USER_PHOTO", "")
                                    sharedHelper.putKey(v.context, "USER_NAME", "")
                                    sharedHelper.putKey(v.context, "USER_EMAIL", "")
                                    sharedHelper.putKey(v.context, "OPEN", "OPEN")

                                    val intent: Intent =
                                        Intent(v!!.context, WelcomeActivity::class.java)
                                    v!!.context.startActivity(intent)
                                    (v.context as Activity).finish()
                                    CoroutineScope(Dispatchers.IO).launch {
                                        db.productDao().deleteAllProductDatabase()
                                    }
                                } else if (it.body()!!.status == 2) {
                                    Snackbar.make(
                                        v,
                                        it.body()?.message!!,
                                        Snackbar.LENGTH_SHORT
                                    )
                                        .show()
                                } else if (it.body()!!.status == 0) {
                                    Snackbar.make(
                                        v,
                                        "${it.body()!!.message}",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                    //baseFragment.dismissProgressDialog()
                                } else {
                                }
                            }

                        } catch (e: HttpException) {
                            Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                                .show();

                        } catch (e: Throwable) {
                            //   Snackbar.make(v, "Ooops: Something else went wrong ${e.message}", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }.onFailure {
                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                }
            }




//            val service = ApiClient.makeRetrofitService()





//                CoroutineScope(Dispatchers.IO).async {
//                    val response = service.logoutFunction(lang.toString() ,
//                        "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}"
//                    )
//                    withContext(Dispatchers.Main) {
//
//                    }
//                }
            }catch (e:Exception)
            {
                Snackbar.make(v, e.message.toString(), Snackbar.LENGTH_SHORT).show();

            }
        }
        alert?.findViewById<Button>(R.id.BackBtnId)?.setOnClickListener {
            alert.dismiss()
        }
        alert?.show()


    }


    fun login(v : View)
    {
        var intent : Intent = Intent(v!!.context , WelcomeActivity::class.java)
        v!!.context.startActivity(intent)
        var activity : Activity = v!!.context as Activity
        activity.finish()
    }
}
