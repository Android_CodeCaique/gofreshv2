package com.shoohna.gofresh.ui.home.ui.moreSetting

import ApiClient.sharedHelper
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.shoohna.gofresh.R
import com.shoohna.gofresh.networking.interfaces.Home
import com.shoohna.gofresh.pojo.responses.SettingData
import com.shoohna.gofresh.ui.welcome.WelcomeActivity
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.gofresh.util.base.LanguageHelper
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class MoreSettingViewModel(private val serviceGeneral: Home) : BaseViewModel() {

    var lang = MutableLiveData<String>("")
    var message = MutableLiveData<Int>(0)
    var notification = MutableLiveData<Int>(0)
    var currency_id = MutableLiveData<Int>(0)
    var loader = MutableLiveData<Boolean>(false)

    var currentUserSetting = MutableLiveData<SettingData>()

    var currencySettingName = ArrayList<String>()
    var currencySettingId = ArrayList<Int>()
    var languageHelper = LanguageHelper()
//    var currencySetting2 = MutableLiveData<CurrenciesData>()
//    var baseFragment = BaseFragment()
    fun changeLang(context: Context)
    {
        loader.value = true

        try {
//            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                serviceGeneral.settingLang(
                    "Bearer ${sharedHelper.getKey(
                        context,
                        Constants.getToken()
                    )}", lang.value.toString()
                ) }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
                                Toast.makeText(
                                    context,
                                    it.body()!!.message,
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                                loader.value = false
                                val sharedHelper = SharedHelper()
                                sharedHelper.putKey(context, "OPEN", "OPEN")
                                sharedHelper.putKey(context, "MyLang", lang.value.toString())

                                val intent = Intent(context, WelcomeActivity::class.java)
                                (context as Activity).finish()
                                context.startActivity(intent)
                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
            }
        }catch (e:Exception)
        {
//            Snackbar.make(v, e.message.toString(), Snackbar.LENGTH_SHORT).show();
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()
        }
    }

    fun changeNotification(context: Context)
    {
        loader.value = true

        try {
//            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(context , "MyLang" )
                runCatching {
                    serviceGeneral.settingNotification(
                    lang.toString() ,
                    "Bearer ${sharedHelper.getKey(
                        context,
                        Constants.getToken()
                    )}", notification.value?.toInt()!!
                )}.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
//                        Toast.makeText(context,response.body()!!.message,Toast.LENGTH_SHORT).show()
                                loader.value = false

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                }
            }
        }catch (e:Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()
        }
    }

    fun changeMessage(context: Context)
    {
        loader.value = true

//        val service = ApiClient.makeRetrofitServiceHome()
        try{
        CoroutineScope(Dispatchers.IO).async {
            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )
            runCatching { serviceGeneral.settingMessage(lang.toString() , "Bearer ${sharedHelper.getKey(context, Constants.getToken())}",message.value?.toInt()!!) }
                .onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
//                        Toast.makeText(context,response.body()!!.message,Toast.LENGTH_SHORT).show()
                                loader.value = false

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                }
        }
        }catch (e:Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

        }
    }

    fun changeCurrency(context: Context)
    {
        loader.value = true

        try {
//            val service = ApiClient.makeRetrofitServiceHome()

            CoroutineScope(Dispatchers.IO).async {
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(context , "MyLang" )
                runCatching {
                    serviceGeneral.settingCurrency(
                        lang.toString(),
                        "Bearer ${sharedHelper.getKey(
                            context,
                            Constants.getToken()
                        )}", currency_id.value?.toInt()!!
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
//                        Toast.makeText(context,response.body()!!.message,Toast.LENGTH_SHORT).show()
                                loader.value = false

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                }
            }
        }catch (e:Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

        }
    }

    fun getUserSetting(context: Context)
    {
        loader.value = true

        try {
            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )

//            val service = ApiClient.makeRetrofitServiceHome()

            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.settingUser(
                        lang.toString(),
                        "Bearer ${sharedHelper.getKey(
                            context,
                            Constants.getToken()
                        )}"
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
                                loader.value = false
                                currentUserSetting.postValue(it.body()!!.data)

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                }
            }
        }catch (e:java.lang.Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

        }
    }

    fun getCurrency(context: Context)
    {
        try {

//            val service = ApiClient.makeRetrofitServiceHome()

            CoroutineScope(Dispatchers.IO).async {
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(context , "MyLang" )
                runCatching { serviceGeneral.getCurrencies(lang.toString()) }
                    .onSuccess {
                        withContext(Dispatchers.Main) {
                            try {
                                if (it.isSuccessful && it.body()?.status == 1) {
                                    Log.i("response", it.body()!!.message)
                                    Log.i("responseee", it.body()!!.data.toString())
                                    currencySettingName.add(context.getString(R.string.currency))
                                    currencySettingId.add(0)

                                    for (i in it.body()!!.data) {
                                        currencySettingName.add(i.name)
                                        currencySettingId.add(i.id)
                                    }


                                } else {
                                    Log.i("loadData1", it.message().toString())
                                }
                            } catch (e: Exception) {
                                Log.i("loadData2", e.message.toString())

                            }

                        }
                    }.onFailure {
                        Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                    }
            }
        }catch (e:Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

        }
    }
}