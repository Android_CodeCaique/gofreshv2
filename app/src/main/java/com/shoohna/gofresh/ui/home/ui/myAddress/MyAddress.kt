package com.shoohna.gofresh.ui.home.ui.myAddress

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentMyAddressBinding

class MyAddress : Fragment() {

    lateinit var binding : FragmentMyAddressBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentMyAddressBinding.inflate(inflater)

        binding.myAddressRecyclerViewId.adapter = MyAddressRecyclerViewAdapter()
        binding.addAddressTxtViewId.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_myAddress_to_addNewAddress)
        }
        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        return binding.root
    }

}