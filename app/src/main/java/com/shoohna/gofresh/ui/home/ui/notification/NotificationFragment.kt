package com.shoohna.gofresh.ui.home.ui.notification

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shoohna.gofresh.databinding.FragmentNotificationBinding
import com.shoohna.gofresh.pojo.responses.NotificationData
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class NotificationFragment : Fragment() {

    lateinit var binding: FragmentNotificationBinding
    lateinit var notificationViewAdapter: NotificationRecyclerViewAdapter
    private val notificationViewModel: NotificationViewModel by inject()   // 1

    var data= MutableLiveData<List<NotificationData>>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentNotificationBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
//        val model = ViewModelProvider(this).get(NotificationViewModel::class.java)
        binding.vm = notificationViewModel
        notificationViewModel.getNotificationList(requireActivity()).observe(viewLifecycleOwner, Observer {notificationList ->
            data.value=notificationList

            binding.notificationRecyclerViewId.adapter = NotificationRecyclerViewAdapter(data, context?.applicationContext)

        })


        return binding.root
    }


}
