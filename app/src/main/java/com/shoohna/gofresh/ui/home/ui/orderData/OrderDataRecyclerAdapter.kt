package com.shoohna.gofresh.ui.home.ui.orderData

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.databinding.MyOrderProductItemBinding
import com.shoohna.gofresh.pojo.responses.MyOrderProductsProduct

// TODO -> make copy of reciet recycler adapter and change layout
class OrderDataRecyclerAdapter (private var dataList: LiveData<List<MyOrderProductsProduct>>, private val context: Context?) : RecyclerView.Adapter<OrderDataRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            MyOrderProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position])
    }


    class ViewHolder(private var binding: MyOrderProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MyOrderProductsProduct) {
            binding.model = item
            binding.executePendingBindings()

        }

    }
}