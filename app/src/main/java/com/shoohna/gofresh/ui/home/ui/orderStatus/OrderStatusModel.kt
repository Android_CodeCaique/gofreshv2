package com.shoohna.gofresh.ui.home.ui.orderStatus

data class OrderStatusModel(val orderId:Int , val orderStatus :String , val notes:String)