package com.shoohna.gofresh.ui.home.ui.orderStatus

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.databinding.OrderStatusRowBinding
import com.shoohna.gofresh.pojo.responses.MyOrdersData

class OrderStatusRecyclerAdapter (val data: LiveData<List<MyOrdersData>>, private val context: Context?) : RecyclerView.Adapter<OrderStatusRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            OrderStatusRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data.value!![position] )

    }

    class ViewHolder(private var binding: OrderStatusRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MyOrdersData) {
            binding.model = item
            binding.executePendingBindings()
            binding.mainConstraintId.setOnClickListener {
//                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//
                val action = OrderStatusFragmentDirections.actionOrderStatusFragmentToOrderDataFragment2(item.id)
                Navigation.findNavController(it).navigate(action)
            }

        }

    }

}