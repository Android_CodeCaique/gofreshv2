package com.shoohna.gofresh.ui.home.ui.orderStatus

import ApiClient.sharedHelper
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.shoohna.gofresh.networking.interfaces.Home
import com.shoohna.gofresh.pojo.responses.MyOrdersData
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class OrderStatusViewModel(private val serviceGeneral: Home) : BaseViewModel() {

    var myOrdersList: MutableLiveData<List<MyOrdersData>>? = null
    var loader = MutableLiveData<Boolean>(false)

    fun loadData(context: Context)
    {
        loader.value = true
        try{
//            val service = ApiClient.makeRetrofitServiceHome()
            Log.d("ORDERS" ,"HERE ORDERS")
            Log.i("TokenOrder",sharedHelper.getKey(context, Constants.getToken()).toString())
            CoroutineScope(Dispatchers.IO).async {
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(context , "MyLang" )
                runCatching { serviceGeneral.myOrders(lang.toString() , "Bearer ${sharedHelper.getKey(context, Constants.getToken())}",0) }
                    .onSuccess {
                        withContext(Dispatchers.Main) {

                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.data.size.toString())
                                Log.i("responseData", it.body()!!.data.toString())
                                myOrdersList!!.postValue(it.body()!!.data)
                                loader.value = false

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                            }


                        }
                    }.onFailure {
                        Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                    }
            }
        }catch (e:Exception){
            Log.i("loadData2",e.message.toString())
            loader.value = false

        }
    }

    internal fun geMyOrdersList(context: Context): MutableLiveData<List<MyOrdersData>> {
        if (myOrdersList == null) {
            myOrdersList = MutableLiveData()
            loadData(context)

        }
        return myOrdersList as MutableLiveData<List<MyOrdersData>>
    }


}