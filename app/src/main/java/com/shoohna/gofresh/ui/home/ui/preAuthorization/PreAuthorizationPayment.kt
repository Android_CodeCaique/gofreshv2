package com.shoohna.gofresh.ui.home.ui.preAuthorization

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentPreAuthorizationPaymentBinding


class PreAuthorizationPayment : Fragment() {

    lateinit var binding : FragmentPreAuthorizationPaymentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentPreAuthorizationPaymentBinding.inflate(inflater)
        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        return binding.root
    }
    
}