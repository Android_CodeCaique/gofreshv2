package com.shoohna.gofresh.ui.home.ui.productDetails

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.databinding.FragmentProductDetailsBinding
import com.shoohna.gofresh.databinding.ProductDetailsColorItemRowBinding
import com.shoohna.gofresh.pojo.responses.ProductColor

class ColorProductDetailsRecyclerViewAdapter (private var dataList: LiveData<List<ProductColor>>,
                                              private val context: Context?,
                                              val viewModel: ProductDetailsViewModel,
                                              var lifecycleOwner: LifecycleOwner, var MainBinding : FragmentProductDetailsBinding)
    : RecyclerView.Adapter<ColorProductDetailsRecyclerViewAdapter.ViewHolder>() {

    var makeLoop = MutableLiveData<Boolean>(false)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ProductDetailsColorItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false) , makeLoop)

    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position] , this.viewModel,context , lifecycleOwner , MainBinding)
        makeLoop.observe(lifecycleOwner, Observer {
            if(it)
            {
                loopIsChecked(dataList.value!!)
            }
        })
    }


    fun loopIsChecked(dataList: List<ProductColor>)
    {
        for (i in dataList) {
            i.isChecked = false
        }
        makeLoop.value = false
    }



    class ViewHolder(private var binding: ProductDetailsColorItemRowBinding , var makeLoop:MutableLiveData<Boolean>) : RecyclerView.ViewHolder(binding.root) {
//        private var restoreInside = MutableLiveData<Boolean>(false)
        fun bind(item: ProductColor , viewModel: ProductDetailsViewModel , context: Context? , lifecycleOwner: LifecycleOwner , MainBinding: FragmentProductDetailsBinding ) {
            binding.model = item
            binding.executePendingBindings()
            binding.colorConstraintId.setOnClickListener {
                Log.i("Constraint Click Color","True")
                viewModel.colorId.value = item.id
                viewModel.colorCode.value = item.color_code
                makeLoop.value = true
                item.isChecked = true
//                MainBinding.include2.colorRecyclerViewId.adapter?.notifyDataSetChanged()
            }




    }
    }
}