package com.shoohna.gofresh.ui.home.ui.productDetails

import android.animation.ObjectAnimator
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class ProductDetailsAnimations {

    fun animationDown(constraint:ConstraintLayout , recyclerView : RecyclerView , detailsHeigth:Int)
    {
        val scaleYAnimator = ObjectAnimator.ofFloat(constraint, "translationY", detailsHeigth.toFloat())
        scaleYAnimator.duration = 500
        val scaleYAnimator2 = ObjectAnimator.ofFloat(recyclerView, "translationY", detailsHeigth.toFloat())
        scaleYAnimator2.duration = 500
        scaleYAnimator.start()
        scaleYAnimator2.start()
    }
    fun animationUp(constraint: ConstraintLayout, recyclerView : RecyclerView, detailsHeigth:Int)
    {
        val scaleYAnimator = ObjectAnimator.ofFloat(constraint, "translationY",  detailsHeigth.toFloat(),0f)
        scaleYAnimator.duration = 500
        val scaleYAnimator2 = ObjectAnimator.ofFloat(recyclerView, "translationY",  detailsHeigth.toFloat(),0f)
        scaleYAnimator2.duration = 500
        scaleYAnimator.start()
        scaleYAnimator2.start()
    }

}