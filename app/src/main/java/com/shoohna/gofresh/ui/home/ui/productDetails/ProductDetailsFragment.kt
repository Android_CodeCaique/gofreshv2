package com.shoohna.gofresh.ui.home.ui.productDetails

import ApiClient.sharedHelper
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.shoohna.e_commercehappytimes.ui.home.ui.productDetails.ProductDetailsRecyclerViewAdapter
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentProductDetailsBinding
import com.shoohna.gofresh.pojo.model.ProductListModel
import com.shoohna.gofresh.pojo.responses.ProductColor
import com.shoohna.gofresh.pojo.responses.ProductImage
import com.shoohna.gofresh.pojo.responses.ProductSize
import com.shoohna.gofresh.util.OnSwipeTouchListener
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.shoohna.util.base.BaseFragment
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject


/**
 * A simple [Fragment] subclass.
 */
class ProductDetailsFragment : Fragment() {
    lateinit var binding:FragmentProductDetailsBinding
    var dataList = ArrayList<ProductListModel>()

    lateinit var vto : ViewTreeObserver
    var detailsHeigth : Int = 0

    var productImagesData= MutableLiveData<List<ProductImage>>()
    var colorImagesData= MutableLiveData<List<ProductColor>>()
    var sizeData= MutableLiveData<List<ProductSize>>()

    lateinit var baseFragment: BaseFragment
    lateinit var productAnimation: ProductDetailsAnimations

    private val productDetailsViewModel: ProductDetailsViewModel by inject()   // 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentProductDetailsBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        vto = binding.include2.detailsConstraintId.viewTreeObserver
//        val model = ViewModelProvider(this).get(ProductDetailsViewModel::class.java)
        baseFragment = BaseFragment()
        binding.include2.vm = productDetailsViewModel

//        binding.include2.before.paintFlags = binding.include2.before.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        productAnimation = ProductDetailsAnimations()
//        Picasso.get().load("http://api.shoohna.com/images/2.png").into(binding.productBackgroundImageId)

        arguments?.let { val safeArgs = ProductDetailsFragmentArgs.fromBundle(it)
            Log.i("Item IDDD",safeArgs.productId.toString())
            productDetailsViewModel.productId.value = safeArgs.productId
            productDetailsViewModel.imgURL.value = safeArgs.imgURL
            productDetailsViewModel.desc.value = safeArgs.desc
            productDetailsViewModel.price.value = safeArgs.price
            try{ Picasso.get().load(safeArgs.imgURL).into(binding.productBackgroundImageId)}catch (e:Exception){}

            productDetailsViewModel.getProductImagesList(requireActivity()).observe(viewLifecycleOwner, Observer { list ->
                productImagesData.value = list
                binding.detailsImageRecyclerViewId.adapter = ProductDetailsRecyclerViewAdapter(productImagesData, context?.applicationContext,binding.productBackgroundImageId)
            })
            productDetailsViewModel.getColorImagesList(requireActivity()).observe(viewLifecycleOwner, Observer { list ->
                colorImagesData.value = list
//                binding.include2.colorRecyclerViewId.adapter =
//                    ColorProductDetailsRecyclerViewAdapter(colorImagesData, context?.applicationContext,productDetailsViewModel , viewLifecycleOwner , binding)
            })
            productDetailsViewModel.getSizeList(requireActivity()).observe(viewLifecycleOwner, Observer { list ->
                sizeData.value = list
//                binding.include2.sizeRecyclerViewId.adapter =
//                    SizeRecyclerViewAdapter(sizeData, context?.applicationContext,productDetailsViewModel, viewLifecycleOwner , binding)
            })
        }

        binding.btnBackId.setOnClickListener {
            activity?.onBackPressed()
            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE

        }
        activity?.let {
            binding.include2.detailsConstraintId.setOnTouchListener(object : OnSwipeTouchListener(it) {

                override fun onSwipeTop() {
                    super.onSwipeTop()
                    Log.i("onSwipeTop","true")
                    productAnimation.animationUp(binding.include2.detailsConstraintId,binding.detailsImageRecyclerViewId,detailsHeigth)
                }

                override fun onSwipeBottom() {
                    super.onSwipeBottom()
                    Log.i("onSwipeBottom","true")
//                    animationDown()
                    productAnimation.animationDown(binding.include2.detailsConstraintId,binding.detailsImageRecyclerViewId,detailsHeigth)

                }

                override fun onSwipeLeft() {
                    super.onSwipeLeft()
                    Log.i("onSwipeLeft","true")

                }

                override fun onSwipeRight() {
                    super.onSwipeRight()
                    Log.i("onSwipeRight","true")

                }
            })
        }
        vto.addOnGlobalLayoutListener { detailsHeigth = binding.include2.linearLayout3.height }

        binding.include2.increaseImgId.setOnClickListener {
            val currentQuantity :Int = binding.include2.quantityTxtId.text.toString().toInt() + 1
            binding.include2.quantityTxtId.text = currentQuantity.toString()
            productDetailsViewModel.currentQuantity.value = currentQuantity
        }
        binding.include2.decreaseImgId.setOnClickListener {
            val currentQuantity :Int = binding.include2.quantityTxtId.text.toString().toInt() - 1
            if (currentQuantity == 0) Toast.makeText(requireActivity(),getString(R.string.cannotMakeZeroQuantity),Toast.LENGTH_SHORT).show()
            else {
                binding.include2.quantityTxtId.text = currentQuantity.toString()
                productDetailsViewModel.currentQuantity.value = currentQuantity
            }
        }

        binding.include2.disountTxtViewId.paintFlags = binding.include2.disountTxtViewId.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


        binding.include2.addFavoriteImgViewId.setOnClickListener {
            productDetailsViewModel.addToWishlist(requireContext())
        }

        binding.notificationBtnId.setOnClickListener {
            if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                baseFragment.showAlert(it.rootView.context)
            }else {
                view?.let { it1 ->
                    Navigation.findNavController(it1)
                        .navigate(R.id.action_productDetailsFragment_to_notificationFragment)
                }
            }
        }



        return binding.root
    }


    fun showAlert(context: Context)
    {
        val alert: Dialog? = Dialog(context)
        alert?.setContentView(R.layout.rate_product)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert?.findViewById<Button>(R.id.submitBtnId)?.setOnClickListener {

        }

        alert?.show()
    }



}
