package com.shoohna.gofresh.ui.home.ui.productDetails

import ApiClient.sharedHelper
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RatingBar
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import com.shoohna.gofresh.R
import com.shoohna.gofresh.networking.interfaces.Home
import com.shoohna.gofresh.pojo.model.ProductEntity
import com.shoohna.gofresh.pojo.responses.ProductColor
import com.shoohna.gofresh.pojo.responses.ProductData
import com.shoohna.gofresh.pojo.responses.ProductImage
import com.shoohna.gofresh.pojo.responses.ProductSize
import com.shoohna.gofresh.util.base.AppDatabase
import com.shoohna.gofresh.util.base.Constants
import com.shoohna.gofresh.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class ProductDetailsViewModel(private val serviceGeneral: Home) : BaseViewModel() {
    public var productId = MutableLiveData<Int>(0)

    private var imagesProductList: MutableLiveData<List<ProductImage>>? = null

    private var colorProductList: MutableLiveData<List<ProductColor>>? = null

    private var sizeList: MutableLiveData<List<ProductSize>>? = null

    var baseFragment: BaseFragment = BaseFragment()


    private var _productList = MutableLiveData<ProductData>()
    val productList: LiveData<ProductData> = _productList

    var currentQuantity = MutableLiveData<Int>(1)
    var sizeId = MutableLiveData<Int>(0)
    var sizeName = MutableLiveData<String>("")
    var colorId = MutableLiveData<Int>(0)
    var imgURL = MutableLiveData<String>("")
    var colorCode = MutableLiveData<String>("")
    var desc = MutableLiveData<String>("")
    var price = MutableLiveData<String>("")
    var pro_name = MutableLiveData<String>("")

    var loader = MutableLiveData<Boolean>(false)
//    var rate = MutableLiveData<Int>(0)

//    var selected = MutableLiveData<Boolean>(false)

//    var allowCheckColor = MutableLiveData<Boolean>(true)
//    var allowCheckSize = MutableLiveData<Boolean>(true)


//    val selectedID: LiveData<Int> = colorId

    fun loadData(context: Context) {

        try {
            loader.value = true
            Log.i("productId", productId.value.toString())


            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )


//            val service = ApiClient.makeRetrofitServiceHome()
//            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                runCatching { serviceGeneral.productDetails(lang.toString() , productId.value?.toInt()!!)  }//productId.value?.toInt()!!
                    .onSuccess {
                        withContext(Dispatchers.Main) {
                            if (it.isSuccessful && it.body()?.status == 1) {

                                imagesProductList!!.postValue(it.body()!!.data.images)
//                                if(it.body()!!.data.colors.size > 0)
                                colorProductList!!.postValue(it.body()!!.data.colors)
                                sizeList!!.postValue(it.body()!!.data.sizes)
                                _productList.value = it.body()!!.data
                                loader.value = false
                                pro_name.value = it.body()!!.data.name
                                Log.i("Size Data", it.body()!!.data.sizes.toString())
                            } else {
                                Log.i("filterDataElse", it.message())
                                Log.i("filterDataElse", it.code().toString())
                                loader.value = false

                            }
                        }
                    }.onFailure {
                        Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                    }

            }
        } catch (e: Exception) {
            Log.i("Exception", "${e.message}")
            loader.value = false

        }
    }

    internal fun getProductImagesList(context: Context): MutableLiveData<List<ProductImage>> {
        if (imagesProductList == null) {
            imagesProductList = MutableLiveData()
            checkAllList(context)
        }
        return imagesProductList as MutableLiveData<List<ProductImage>>
    }

    internal fun getColorImagesList(context: Context): MutableLiveData<List<ProductColor>> {
        if (colorProductList == null) {
            colorProductList = MutableLiveData()
            checkAllList(context)
        }
        return colorProductList as MutableLiveData<List<ProductColor>>
    }

    internal fun getSizeList(context: Context): MutableLiveData<List<ProductSize>> {
        if (sizeList == null) {
            sizeList = MutableLiveData()
            checkAllList(context)
        }
        return sizeList as MutableLiveData<List<ProductSize>>
    }

    private fun checkAllList(context: Context) {
        if (colorProductList != null && imagesProductList != null && sizeList != null)
            loadData(context)
    }

    fun addToCartUsingRoom(v: View) {

            try {
//                if (checkDataBeforeAddToCart(v)) {
                    val db =
                        Room.databaseBuilder(
                            v.rootView.context,
                            AppDatabase::class.java,
                            "product_db"
                        )
                            .build()
                    var productEntity = ProductEntity(
                        productId.value!!,
                        colorId.value!!,
                        sizeId.value!!,
                        sizeName.value!!,
                        currentQuantity.value!!,
                        imgURL.value!!,
                        colorCode.value!!,
                        desc.value!!,
                        price.value!!,
                        pro_name.value!!
                    )
                    CoroutineScope(Dispatchers.IO).launch {
                        if (db.productDao().getAllProductsById(productId.value!!).size == 1) {
                            db.productDao().updateProduct(productEntity)
                            CoroutineScope(Dispatchers.Main).launch {
                                showAlertProduct(v,v.rootView.context.getString(R.string.productUpdated) , v!!.context)
//                                Toast.makeText(
//                                    v.rootView.context,
//                                    v.rootView.context.getString(R.string.productUpdated),
//                                    Toast.LENGTH_SHORT
//                                ).show()
                            }
                        } else {
                            db.productDao().insertProduct(productEntity)
                            CoroutineScope(Dispatchers.Main).launch {
                                showAlertProduct(v, v.rootView.context.getString(R.string.productAdded) ,  v!!.context)

//                                Toast.makeText(
//                                    v.rootView.context,
//                                    v.rootView.context.getString(R.string.productAdded),
//                                    Toast.LENGTH_SHORT
//                                ).show()
                            }
                        }
                    }
//                }
            } catch (e: Exception) {
                Toast.makeText(v.rootView.context, e.message, Toast.LENGTH_SHORT).show()
            }

    }

//    fun addToCart(v:View)
//    {
//        try {
//            if(checkDataBeforeAddToCart(v)) {
//                baseFragment.showProgressDialog(
//                    v.rootView.context,
//                    v.rootView.context.getString(R.string.addToCart),
//                    v.rootView.context.getString(R.string.addToCartIsInProgress),
//                    false
//                )
//                Log.i("Shared", sharedHelper.getKey(v.context, Constants.getToken()))
//                Log.i("Color", this.colorId.value.toString())
//
//                val service = ApiClient.makeRetrofitServiceHome()
//                CoroutineScope(Dispatchers.IO).launch {
//                    val response = service.addToCart(
//                        productId.value?.toInt()!!,
//                        "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}",
//                        currentQuantity.value!!,
//                        colorId.value!!,
//                        sizeId.value!!
//                    ) //productId.value?.toInt()!!
//                    withContext(Dispatchers.Main) {
//                        if (response.isSuccessful && response.body()?.status == true) {
//                            Log.i("Order", "Success")
//                            baseFragment.dismissProgressDialog()
//                            Snackbar.make(v, response.body()!!.message, Snackbar.LENGTH_SHORT)
//                                .show();
//
//                        } else {
//                            Log.i("Order", response.code().toString())
//                            Log.i("Order2", response.message())
//                            baseFragment.dismissProgressDialog()
//                            Snackbar.make(v, response.message(), Snackbar.LENGTH_SHORT).show();
//
//                        }
//                    }
//
//                }
//            }
//        }catch (e:java.lang.Exception)
//        {
//            Log.i("Exception", "${e.message}")
//            baseFragment.dismissProgressDialog()
//            Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT).show();
//        }
//    }

    private fun checkDataBeforeAddToCart(v: View): Boolean {
        if (colorProductList?.value?.isEmpty()!! && sizeList?.value?.isEmpty()!!) {
            Log.i("No Data List","true")
            return true
        }else if (colorProductList?.value?.isEmpty()!! && sizeId.value == 0) {
            Log.i("ProductDetails","1")
            Snackbar.make(v, v.resources.getString(R.string.pleaseSelectSize), Snackbar.LENGTH_SHORT).show()
            return false
        }
        else if (sizeList?.value?.isEmpty()!! && colorId.value == 0) {
            Log.i("ProductDetails","2")
            Snackbar.make(v, v.resources.getString(R.string.pleaseSelectColor), Snackbar.LENGTH_SHORT).show()
            return false
        }
        else if (colorId.value == 0 && colorProductList?.value?.isNotEmpty()!!) {
            Log.i("ProductDetails","3")
            Snackbar.make(v, v.resources.getString(R.string.pleaseSelectColor), Snackbar.LENGTH_SHORT).show()
            return false
        }
        else if (sizeId.value == 0 && sizeList?.value?.isNotEmpty()!!) {
            Log.i("ProductDetails","4")
            Snackbar.make(v, v.resources.getString(R.string.pleaseSelectSize), Snackbar.LENGTH_SHORT).show()
            return false
        }
        else
            return true
    }

    fun addToWishlist(context: Context) {
        if (sharedHelper.getKey(context, Constants.getToken())?.isEmpty()!!) {
            baseFragment.showAlert(context)
        }
            else
            {
                loader.value = true
                try {
                    val sharedHelper : SharedHelper = SharedHelper()
                    val lang : String? = sharedHelper.getKey(context , "MyLang" )

//                    val service = ApiClient.makeRetrofitServiceHome()
                    CoroutineScope(Dispatchers.IO).async {
                        runCatching {
                            serviceGeneral.addOrDeleteFromMyWishlist(
                                lang.toString(),
                                productId.value?.toInt()!!,
                                "Bearer ${sharedHelper.getKey(context, Constants.getToken())}"
                            )
                        }.onSuccess {
                            withContext(Dispatchers.Main) {
                                try {
                                    if (it.isSuccessful && it.body()?.status == 1) {
                                        Log.i("response", it.body()!!.message)
                                        Toast.makeText(
                                            context,
                                            it.body()!!.message,
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        loader.value = false

                                    } else {
                                        Log.i("loadData1", it.message().toString())
                                        loader.value = false
                                        Toast.makeText(
                                            context,
                                            it.message(),
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                    }
                                } catch (e: Exception) {
                                    Log.i("loadData2", e.message.toString())
                                    loader.value = false
                                    Toast.makeText(
                                        context,
                                        "Exception ${e.message.toString()}",
                                        Toast.LENGTH_SHORT
                                    ).show()

                                }

                            }
                        }.onFailure {
                            Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                        }
                    }
                }catch (e:java.lang.Exception)
                {
                    Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

                }
            }

    }

    fun sendProductRate(v:View) {
        if (sharedHelper.getKey(v.rootView.context, Constants.getToken())?.isEmpty()!!) {
            baseFragment.showAlert(v.rootView.context)
        } else {
            val alert: Dialog? = Dialog(v.rootView.context)
            alert?.setContentView(R.layout.rate_product)
            alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alert?.findViewById<Button>(R.id.submitBtnId)?.setOnClickListener {
                var rate = alert.findViewById<RatingBar>(R.id.ratingBar).rating.toInt()
                try {
                    loader.value = true
                    Log.i("productId", productId.value.toString())


//                    val service = ApiClient.makeRetrofitServiceHome()


                    CoroutineScope(Dispatchers.IO).async {
                        val sharedHelper : SharedHelper = SharedHelper()
                        val lang : String? = sharedHelper.getKey(v!!.context  , "MyLang" )

                        runCatching {
                            serviceGeneral.saveRate(
                                lang.toString(),
                                productId.value?.toInt()!!,
                                "Bearer ${sharedHelper.getKey(
                                    v.rootView.context,
                                    Constants.getToken()
                                )}",
                                rate
                            )
                        }.onSuccess {
                            withContext(Dispatchers.Main) {
                                if (it.isSuccessful && it.body()?.status == 1) {
                                    Toast.makeText(
                                        v.rootView.context,
                                        it.body()!!.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    alert.dismiss()
                                    loader.value = false
                                } else {

                                    loader.value = false

                                }
                            }
                        }.onFailure {
                            Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                        }

                    }
                } catch (e: Exception) {
                    Log.i("Exception", "${e.message}")
                    loader.value = false

                }
            }

            alert?.show()
        }
    }


}