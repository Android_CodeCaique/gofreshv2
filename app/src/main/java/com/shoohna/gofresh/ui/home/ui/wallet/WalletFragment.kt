package com.shoohna.gofresh.ui.home.ui.wallet

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.ColorTemplate
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentWalletBinding
import com.shoohna.gofresh.ui.home.ui.wallet.adapters.WalletAnaylsisRecyclerViewAdapter
import com.shoohna.gofresh.ui.home.ui.wallet.adapters.WalletRecyclerViewAdapter
import com.shoohna.gofresh.ui.home.ui.wallet.barChart.AxisValueFormatter
import com.shoohna.gofresh.ui.home.ui.wallet.barChart.MyMarkerView
import org.koin.android.ext.android.inject


open class WalletFragment : Fragment(), OnChartValueSelectedListener, OnChartGestureListener {

    lateinit var binding: FragmentWalletBinding
    private val viewModel: WalletViewModel by inject()   // 1
    private val TAG = "WalletFragment"
    private lateinit var myMarkerView: MyMarkerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentWalletBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.vm = viewModel

        myMarkerView = MyMarkerView(activity, R.layout.custom_marker_view)
        binding.walletRecyclerViewId.adapter = WalletRecyclerViewAdapter()
        binding.walletAnalysisRecyclerViewId.adapter = WalletAnaylsisRecyclerViewAdapter()


        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }


        setupChart()

        return binding.root
    }


    private fun setupChart() {

        val chart = binding.cubiclinechart

        chart.description.isEnabled = false
        chart.onChartGestureListener = this
        chart.isDoubleTapToZoomEnabled = false

        myMarkerView.chartView = chart // For bounds control

        chart.marker = myMarkerView

        chart.setDrawGridBackground(false)
        chart.setDrawBarShadow(false)

        chart.data = generateBarData(1, 20000F, 12)

        val xAxis = chart.xAxis

        val xAxisFormatter: ValueFormatter = AxisValueFormatter(mLabels)
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f // only intervals of 1 day
        xAxis.labelCount = mLabels.size
        xAxis.valueFormatter = xAxisFormatter

    }

    private val mLabels =
        arrayOf(" A", " B", " C", " D", " E", " F"," A", " B", " C", " D", " E", " F")

    private fun getLabel(i: Int): String? = mLabels[i]

    private fun generateBarData(dataSets: Int, range: Float, count: Int): BarData? {

        val sets: ArrayList<IBarDataSet> = ArrayList()
        for (i in 0 until dataSets) {
            val entries: ArrayList<BarEntry> = ArrayList()
            for (j in 0 until count) {
                val r = (Math.random() * range).toFloat() + range / 4
                Log.e(TAG, "generateBarData: $r")
                entries.add(BarEntry(j.toFloat(), r))
            }
            val ds = BarDataSet(entries, getLabel(i))
            ds.setColors(*ColorTemplate.VORDIPLOM_COLORS)
            sets.add(ds)
        }

        return BarData(sets)
    }


    override fun onValueSelected(e: Entry?, h: Highlight?) {


    }

    override fun onNothingSelected() {


    }

    override fun onChartGestureStart(
        me: MotionEvent?,
        lastPerformedGesture: ChartTouchListener.ChartGesture?
    ) {
    }

    override fun onChartGestureEnd(
        me: MotionEvent?,
        lastPerformedGesture: ChartTouchListener.ChartGesture?
    ) {
    }

    override fun onChartLongPressed(me: MotionEvent?) {
    }

    override fun onChartDoubleTapped(me: MotionEvent?) {
    }

    override fun onChartSingleTapped(me: MotionEvent?) {

        val entry = binding.cubiclinechart.getEntryByTouchPoint(me!!.x, me.y)

        Log.e(TAG, "onChartSingleTapped: x ${entry.x}")
        Log.e(TAG, "onChartSingleTapped: y ${entry.y}")

    }

    override fun onChartFling(
        me1: MotionEvent?,
        me2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ) {
    }

    override fun onChartScale(me: MotionEvent?, scaleX: Float, scaleY: Float) {
    }

    override fun onChartTranslate(me: MotionEvent?, dX: Float, dY: Float) {
    }

}