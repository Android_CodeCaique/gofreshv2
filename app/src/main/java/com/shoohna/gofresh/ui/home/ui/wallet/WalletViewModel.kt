package com.shoohna.gofresh.ui.home.ui.wallet

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shoohna.gofresh.networking.interfaces.Home

class WalletViewModel(private val serviceGeneral: Home)  : ViewModel()  {
    var selectedCategory = MutableLiveData<Int>(1)

    fun setWithdraw(v: View){
        selectedCategory.value = 1
    }
    fun setAnalysis(v: View){
        selectedCategory.value = 2
    }
}