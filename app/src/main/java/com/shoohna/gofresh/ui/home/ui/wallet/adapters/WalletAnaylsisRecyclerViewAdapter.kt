package com.shoohna.gofresh.ui.home.ui.wallet.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.gofresh.databinding.WalletAnalysisRowBinding

class WalletAnaylsisRecyclerViewAdapter () : RecyclerView.Adapter<WalletAnaylsisRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            WalletAnalysisRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bind(dataList.value!![position])

    }


    class ViewHolder(private var binding: WalletAnalysisRowBinding) : RecyclerView.ViewHolder(binding.root) {

//        fun bind(item: ProductEntity ) {
//            binding.modelRoom = item
//            binding.executePendingBindings()
//        }

    }
}
