package com.shoohna.gofresh.ui.home.ui.wallet.barChart

import com.github.mikephil.charting.formatter.ValueFormatter

class AxisValueFormatter( private val labels: Array<String>) :
    ValueFormatter() {
    override fun getFormattedValue(value: Float): String = labels[value.toInt()]
}