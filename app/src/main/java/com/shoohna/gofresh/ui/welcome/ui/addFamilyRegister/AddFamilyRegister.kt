package com.shoohna.gofresh.ui.welcome.ui.addFamilyRegister

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentAddFamilyRegisterBinding

class AddFamilyRegister : Fragment() {

    lateinit var binding : FragmentAddFamilyRegisterBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentAddFamilyRegisterBinding.inflate(inflater)

        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        return binding.root
    }


}