package com.shoohna.gofresh.ui.welcome.ui.forgetPassword

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shoohna.gofresh.databinding.FragmentForgetPasswordBinding
import com.shoohna.gofresh.databinding.FragmentLoginBinding
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class ForgetPasswordFragment : Fragment() {

    lateinit var binding:FragmentForgetPasswordBinding

    private val forgetPasswordViewModel: ForgetPasswordViewModel by inject()   // 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentForgetPasswordBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val viewModel = ViewModelProvider(this).get(ForgetPasswordViewModel::class.java)

        binding.back.setOnClickListener {
            activity?.onBackPressed()
        }

        
        binding.vm = forgetPasswordViewModel

        return binding.root
    }

}
