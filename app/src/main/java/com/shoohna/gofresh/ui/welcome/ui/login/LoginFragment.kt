package com.shoohna.gofresh.ui.welcome.ui.login

import ApiClient.sharedHelper
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentLoginBinding
import com.shoohna.gofresh.ui.home.MainActivity
import com.shoohna.gofresh.util.base.SharedHelper
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import java.net.MalformedURLException
import java.net.URL

/**
 * A simple [Fragment] subclass.
 */
const val RC_SIGN_IN = 123
class LoginFragment : Fragment() {

    lateinit var binding: FragmentLoginBinding
    public lateinit var nav: NavController
    private val loginViewModel: LoginViewModel by inject()   // 1
    var callbackManager : CallbackManager?= null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        sharedHelper.putKey(requireActivity(), "GoToLogin", "false")

        binding.vm = loginViewModel

        binding.signUpTxtViewId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.registerFragment)
        }

        binding.forgetPasswordTxtId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.forgetPasswordFragment)
        }

        binding.skipTxtViewId.setOnClickListener {
            startActivity(Intent(activity, MainActivity::class.java))
            activity?.finish()
            val sharedHelper = SharedHelper()
            sharedHelper.putKey(requireActivity(), "OPEN", "OPEN")
        }

        binding.back.setOnClickListener {
            activity?.onBackPressed()
        }

        binding.loginFB.setOnClickListener { clickOnFaceBook() }
        binding.loginGoogle.setOnClickListener {
            clickOnGoogle()

        }
        return  binding.root
    }

    private fun clickOnFaceBook ()
    {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .logInWithReadPermissions(this, listOf("email", "public_profile"))
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult?) {
                    //To change body of created functions use File | Settings | File Templates.
                    // Here we will add to get photo , email
                    // App code
                    val UserId: String = loginResult!!.accessToken.userId

                    val graphRequest = GraphRequest.newMeRequest(
                        loginResult!!.accessToken
                    ) { `object`, _ -> getData(`object`) }
                    val parameters = Bundle()
                    parameters.putString("fields", "first_name , last_name , email , id")
                    graphRequest.parameters = parameters
                    graphRequest.executeAsync()
                }

                override fun onError(error: FacebookException?) {
                    //To change body of created functions use File | Settings | File Templates.
                    Log.d("Here", "Here 1")
                }

                override fun onCancel() {
                    //To change body of created functions use File | Settings | File Templates.
                    Log.d("Here", "Here 2")
                }
            })
    }

    private fun getData(`object`: JSONObject) {
        var first_name: String?
        var last_name: String?
        var email: String?
        val id: String
        first_name = ""
        last_name = ""
        email = ""
        val url: URL
        try {
            first_name = `object`.getString("first_name")
            last_name = `object`.getString("last_name")
            email = `object`.getString("email")
            id = `object`.getString("id")
            url =
                URL("https://graph.facebook.com/" + `object`.getString("id") + "/picture?width=250&height=250")
            // LoginFBFunction(first_name+" "+last_name , email , id , url.toString());
//            loginViewModel.login(requireView(),email)
            loginViewModel.login(requireContext(),email)

            Log.d("IMAGE", url.toString())
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        callbackManager?.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {


            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                Log.d("Google sign in", "Google sign in successed")
                handleSignInResult(task)

            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.d("Google sign in", "Google sign in failed"+e)
                // ...
            }
        }
        else
        {
            callbackManager?.onActivityResult(requestCode , resultCode , data)

        }
    }
    private fun  handleSignInResult( completedTask : Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            val email  : String? = account!!.email
            loginViewModel.login(requireContext(),email)
        } catch ( e :Exception) {

        }
    }

    private fun clickOnGoogle()
    {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        val  mGoogleSignInClient = GoogleSignIn.getClient(requireView().context, gso)

        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)

        val account = GoogleSignIn.getLastSignedInAccount(requireView().context)
    }
}
// login facebook by kotlin https://www.youtube.com/watch?v=eyDV9GVoK0M
