package com.shoohna.gofresh.ui.welcome.ui.personalInfromation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentPersonalInformationBinding

class PersonalInformation : Fragment() {

    lateinit var binding : FragmentPersonalInformationBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentPersonalInformationBinding.inflate(inflater)

        binding.addFamilyTxtViewId.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_personalInformation_to_addFamilyRegister)
        }

        return binding.root
    }


}