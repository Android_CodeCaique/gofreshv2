package com.shoohna.gofresh.ui.welcome.ui.registerIntro

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.gofresh.R
import com.shoohna.gofresh.databinding.FragmentRegisterIntroBinding

class RegisterIntro : Fragment() {

    lateinit var binding : FragmentRegisterIntroBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterIntroBinding.inflate(inflater)
        binding.lifecycleOwner = this

        binding.btnContiune.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_registerIntro_to_personalInformation)
        }

        return binding.root
    }


}