package com.shoohna.gofresh.ui.welcome.ui.splash

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import androidx.lifecycle.MutableLiveData
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import java.util.*

class SplashViewModel :BaseViewModel() {

    var baseFragment = BaseFragment()

    var loader = MutableLiveData<Boolean>(false)

    private lateinit var sharedPreferences: SharedPreferences
    var language :String? = ""



//    fun getUserLanguage(context: Context)
//    {
//        if(baseFragment.verifyAvailableNetwork(context)){
////            loader.value = true
//            sharedPreferences = context.getSharedPreferences("Settings", Activity.MODE_PRIVATE)!!
//            language = sharedPreferences.getString("My_Lang", "")
//
//            val service = ApiClient.makeRetrofitServiceHome()
//            CoroutineScope(Dispatchers.IO).launch {
//                val sharedHelper : SharedHelper = SharedHelper()
//                val lang : String? = sharedHelper.getKey(context , "MyLang" )
//
//                val response = service.settingUser(lang.toString() ,"Bearer ${sharedHelper.getKey(context, Constants.getToken())}")
//                withContext(Dispatchers.Main) {
//                    try{
//                        if(response.isSuccessful && response.body()?.status == 1){
//                            Log.i("response",response.body()!!.message)
////                            loader.value = false
//                            if(language != response.body()!!.data.lang) {
//                                setLocate(context,response.body()!!.data.lang)
//                                (context as Activity).recreate()
//                            }else{}
//
//                        }
//                        else {
//                            Log.i("loadData1",response.message().toString())
////                            loader.value = false
//                        }
//                    }catch (e:Exception){
//                        Log.i("loadData2",e.message.toString())
////                        loader.value = false
//
//                    }
//
//                }
//            }
//        }
//        else
//            Toast.makeText(context,context.resources.getString(R.string.noIntenetConnection),Toast.LENGTH_SHORT).show()
//    }

    private fun setLocate(context: Context,Lang: String) {

        val locale = Locale(Lang)

        Locale.setDefault(locale)

        val config = Configuration()

        config.locale = locale
        context.resources?.updateConfiguration(config, context.resources.displayMetrics)

        val editor = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)?.edit()
        editor?.putString("My_Lang", Lang)
        editor?.apply()
    }
}