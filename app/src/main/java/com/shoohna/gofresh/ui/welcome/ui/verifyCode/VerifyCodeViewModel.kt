package com.shoohna.gofresh.ui.welcome.ui.verifyCode

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import com.shoohna.gofresh.R
import com.shoohna.gofresh.networking.interfaces.auth
import com.shoohna.gofresh.util.base.SharedHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import retrofit2.HttpException


class VerifyCodeViewModel(private val serviceGeneral: auth) : ViewModel() {

    public var _otp1 = MutableLiveData<String>("")

    public var _otp2 = MutableLiveData<String>("")

    public var _otp3 = MutableLiveData<String>("")

    public var _otp4 = MutableLiveData<String>("")

    public var _otp5 = MutableLiveData<String>("")

    public var _otp6 = MutableLiveData<String>("")

    var loader = MutableLiveData<Boolean>(false)


    fun showAllCode(v: View) {
        val inputCode : String ="${_otp1.value}${_otp2.value}${_otp3.value}${_otp4.value}${_otp5.value}${_otp6.value}"
        val sharedHelper = SharedHelper()
        sharedHelper.putKey(v!!.context , "OPEN" , "OPEN")
        Log.d("CODE", inputCode)
        loader.value = true
        verifyVerificationCode(v, inputCode)
    }

    private fun verifyVerificationCode(v : View , code: String) {
        //creating the credential
        val sharedHelper = SharedHelper()
        val verificationId : String? =  sharedHelper.getKey(v!!.context , "ActiveCode"  )
        val auth = FirebaseAuth.getInstance();
        val credential = PhoneAuthProvider.getCredential(verificationId.toString(), code)
        auth.signInWithCredential(credential)
            .addOnCompleteListener {task->
                if (task.isSuccessful()) {
                    activeAccountApi(v)
                    Log.d("D", "1")
                } else {
                    Snackbar.make(v, v.resources.getString(R.string.errorCode), Snackbar.LENGTH_SHORT).show()
                    loader.value = false
                    Log.d("D", "2")
                }
            }
            .addOnFailureListener {
                loader.value = false
                Log.d("D", "3")
                Snackbar.make(v, v.resources.getString(R.string.errorCode), Snackbar.LENGTH_SHORT).show()
            }
    }

    private fun activeAccountApi(v : View){
        val sharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
//        val service = ApiClient.makeRetrofitService()


        CoroutineScope(Dispatchers.IO).async {
            runCatching {
                serviceGeneral.ActiveFunction(lang.toString(), "Bearer ${sharedHelper.getKey(v.context, "PHONETOKEN")}")
            }.onSuccess {
                withContext(Dispatchers.Main) {
                    try {

                        if (it.isSuccessful) {
                            Log.d("RESPONSE", it.toString())
                            if (it.body()!!.status == 1) {
                                loader.value = false
                                Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
                                    .show()
                            } else if (it.body()!!.status == 2) {
                                Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
                                    .show()
                                loader.value = false

                            } else if (it.body()!!.status == 0) {
                                Snackbar.make(
                                    v,
                                    "${it.body()!!.message}",
                                    Snackbar.LENGTH_SHORT
                                ).show()
                                loader.value = false

                                //baseFragment.dismissProgressDialog()
                            } else {

                            }
                        } else {
                        }

                    } catch (e: HttpException) {
                        Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT).show();
                        //baseFragment.dismissProgressDialog()
                        loader.value = false

                    } catch (e: Throwable) {
                        Log.d("ERROR", "ERRORE" + e.message)
                        //  Snackbar.make(v, "Ooops: Something else went wrong", Snackbar.LENGTH_SHORT).show();
                        //baseFragment.dismissProgressDialog()
                        loader.value = false
                    }
                }
            }.onFailure {
                Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }
        }


//            CoroutineScope(Dispatchers.IO).async {
//                val response = service.ActiveFunction(lang.toString(), "Bearer ${sharedHelper.getKey(v.context, "PHONETOKEN")}")
//                withContext(Dispatchers.Main) {
//
//                }
//            }
        }
    }