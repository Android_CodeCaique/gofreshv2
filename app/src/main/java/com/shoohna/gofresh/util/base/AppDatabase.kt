package com.shoohna.gofresh.util.base

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shoohna.gofresh.networking.interfaces.ProductDao
import com.shoohna.gofresh.pojo.model.ProductEntity

@Database(entities = [ProductEntity::class], version = 2)
abstract class AppDatabase : RoomDatabase(){
    abstract fun productDao(): ProductDao

}