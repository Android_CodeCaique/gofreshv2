package com.shoohna.gofresh.util.base

//import ApiClient.netModule
import ApiClient.addCardModule
import ApiClient.homeGeneralAboutUs
import ApiClient.homeGeneralContactUs
import ApiClient.homeGeneralModule
import ApiClient.homeServiceModule
import ApiClient.homeServiceModuleCart
import ApiClient.homeServiceModuleFavorite
import ApiClient.homeServiceModuleFilter
import ApiClient.homeServiceModuleHome
import ApiClient.homeServiceModuleMainActivity
import ApiClient.homeServiceModuleMoreSetting
import ApiClient.homeServiceModuleNotification
import ApiClient.homeServiceModuleOrderData
import ApiClient.homeServiceModuleOrderStatus
import ApiClient.homeServiceModulePaymentWay
import ApiClient.homeServiceModuleProduct
import ApiClient.homeServiceModuleProductDetails
import ApiClient.homeServiceModuleShipping
import ApiClient.homeServiceModuleWallet
import ApiClient.welcomeModule
import ApiClient.welcomeModuleChangePasswordHome
import ApiClient.welcomeModuleForgetPassword
import ApiClient.welcomeModuleLogin
import ApiClient.welcomeModuleMore
import ApiClient.welcomeModuleProfile
import ApiClient.welcomeModuleRegister
import ApiClient.welcomeModuleResetPassword
import ApiClient.welcomeModuleVerifyCode
import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

class BaseApplication: Application() {

//    var listofModules = module {
//        single { ApiClientGeneral() }
//    }

    override fun onCreate() {
        super.onCreate()

        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        startKoin {

            androidLogger()
            androidContext(this@BaseApplication)
            modules(listOf(homeGeneralModule,
                homeGeneralContactUs,
                homeGeneralAboutUs,
                welcomeModule,
                welcomeModuleLogin,
                welcomeModuleRegister,
                welcomeModuleForgetPassword,
                welcomeModuleResetPassword,
                welcomeModuleVerifyCode,
                welcomeModuleChangePasswordHome,
                welcomeModuleMore,
                welcomeModuleProfile,
                homeServiceModule,
                homeServiceModuleCart,
                homeServiceModulePaymentWay,
                homeServiceModuleShipping,
                homeServiceModuleFavorite,
                homeServiceModuleHome,
                homeServiceModuleFilter,
                homeServiceModuleMoreSetting,
                homeServiceModuleNotification,
                homeServiceModuleOrderData,
                homeServiceModuleOrderStatus,
                homeServiceModuleProduct,
                homeServiceModuleProductDetails,
                homeServiceModuleMainActivity,homeServiceModuleWallet,
                addCardModule
            ))
        }
    }
}