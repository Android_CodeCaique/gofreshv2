package com.shoohna.gofresh.util.base

data class BaseResponse(
    val data: String,
    val message: String,
    val status: Int
)