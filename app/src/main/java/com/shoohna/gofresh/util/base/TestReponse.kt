package com.shoohna.gofresh.util.base

 data class TestReponse(
    val `data`: Any,
    val message: String,
    val status: Boolean
)