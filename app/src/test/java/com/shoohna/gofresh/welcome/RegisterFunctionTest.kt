package com.shoohna.gofresh.welcome

import androidx.lifecycle.MutableLiveData
import com.google.android.material.textfield.TextInputLayout
import com.shoohna.gofresh.R
import com.shoohna.gofresh.ui.welcome.WelcomeActivity
import com.shoohna.gofresh.ui.welcome.ui.register.RegisterViewModel
import kotlinx.android.synthetic.main.bottom_sheet_layout.*
import org.junit.Test

class RegisterFunctionTest {
    val vm = RegisterViewModel()
    var activity : WelcomeActivity = WelcomeActivity()



    @Test
    fun registerFunctionSuccess() {
        vm.fName = MutableLiveData<String>("bob")
        vm.lName = MutableLiveData<String>("heba")
        vm.phone = MutableLiveData<String>("01060945044")
        vm.password = MutableLiveData<String>("55555555")
        vm.confirmPasssword = MutableLiveData<String>("55555555")
        vm.email = MutableLiveData<String>("hebaMuhammd@gmail.com")
        vm.loader = MutableLiveData<Boolean>(false)

        val fNameLayout: TextInputLayout = activity.findViewById(R.id.fNameLayout)
        val lNameLayout: TextInputLayout = activity.findViewById(R.id.lNameLayout)
        val emailLayout: TextInputLayout = activity.findViewById(R.id.emailLayout)
        val phoneLayout: TextInputLayout = activity.findViewById(R.id.phoneLayout)
        val passwordLayout: TextInputLayout = activity.findViewById(R.id.passwordLayout)
        val confirmPasswordLayout: TextInputLayout = activity.findViewById(R.id.confirmPasswordLayout)

        vm.register(activity!!.view, fNameLayout , lNameLayout, emailLayout, phoneLayout, passwordLayout, confirmPasswordLayout )


    }
}